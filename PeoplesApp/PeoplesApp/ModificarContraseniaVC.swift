//
//  ModificarContraseniaVC.swift
//  PeoplesAppPro
//
//  Created by Jesús García on 29/11/17.
//  Copyright © 2017 Firewall Soluciones. All rights reserved.
//

import UIKit
import CocoaLumberjack

class ModificarContraseniaVC: UIViewController, UITextFieldDelegate {
    
    //Outlets.
    var txtContraseniaActual: UITextField!
    var txtNuevaContrasenia: UITextField!
    var txtRepiteContrasenia: UITextField!
    var btnChangePassword: UIButton!
    var scrollView : UIScrollView!
    
    //Vars.
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("ChangePassword", comment: "")
        self.automaticallyAdjustsScrollViewInsets = false
        self.view.backgroundColor = UIColor.white
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow),
                                               name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide),
                                               name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        self.buildInterface()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - TextField
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return self.view.endEditing(true)
    }
    
    //MARK: - Events
    @objc func seleccionaBotonModifContrasenia(_ sender: UIButton) {
        if !validaCampos() {
            return
        }
        
        let contrasenia = txtContraseniaActual.text?.trimmed()
        let contraseniaNueva = txtNuevaContrasenia.text?.trimmed()
        
        Functions().cambiarContrasenia(contrasenia: contrasenia!,
                                       contraseniaNueva: contraseniaNueva!) { (success, mensaje, error) in
                                        
                                        if error != nil {
                                            let message = NSLocalizedString("ErrorChangePassword", comment: "")
                                            self.mensajeSimple(mensaje: message)
                                            return
                                        }
                                        
                                        if success {
                                            let alert = UIAlertController(title: NSLocalizedString("AppName", comment: ""),
                                                                          message: mensaje,
                                                                          preferredStyle: UIAlertControllerStyle.alert)
                                            
                                            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""),
                                                                          style: UIAlertActionStyle.cancel,
                                                                          handler: {(
                                                                            alert: UIAlertAction!) in
                                                                            self.navigationController?.popViewController(animated: true)
                                            }))
                                            self.present(alert, animated: true, completion: nil)
                                        }
                                        else {
                                            self.mensajeSimple(mensaje: mensaje)
                                        }
        }
    }
    
    //MARK: - Custom
    func buildInterface() {
        var xframe = CGRect.zero
        let cornerCampos : CGFloat = 3
        
        self.scrollView = UIScrollView(frame: self.view.frame)
        
        xframe.origin = CGPoint(x: 10, y: 20)
        xframe.size =  CGSize(width: Constantes.tamaños.anchoTextfield, height: Constantes.tamaños.altoTextfield)
        txtContraseniaActual = UITextField(frame: xframe)
        txtContraseniaActual.placeholder = NSLocalizedString("CurrentPassword", comment: "")
        txtContraseniaActual.layer.borderWidth = Constantes.tamaños.sizeBordeTextfield
        txtContraseniaActual.layer.borderColor = Constantes.paletaColores.bordeTextfield.cgColor
        txtContraseniaActual.layer.cornerRadius = cornerCampos
        txtContraseniaActual.center.x = Constantes.tamaños.centroHorizontal
        txtContraseniaActual.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0)
        txtContraseniaActual.font = Constantes.currentFonts.NunitoRegular.base
        txtContraseniaActual.isSecureTextEntry = true
        addToolBar(textField: txtContraseniaActual)
        scrollView.addSubview(txtContraseniaActual)
        
        xframe.origin = CGPoint(x: 10, y: txtContraseniaActual.frame.maxY + 20)
        xframe.size = CGSize(width: Constantes.tamaños.anchoTextfield, height: Constantes.tamaños.altoTextfield)
        txtNuevaContrasenia = UITextField(frame: xframe)
        txtNuevaContrasenia.placeholder = NSLocalizedString("NewPassword", comment: "")
        txtNuevaContrasenia.layer.borderWidth = Constantes.tamaños.sizeBordeTextfield
        txtNuevaContrasenia.layer.borderColor = Constantes.paletaColores.bordeTextfield.cgColor
        txtNuevaContrasenia.layer.cornerRadius = cornerCampos
        txtNuevaContrasenia.center.x = Constantes.tamaños.centroHorizontal
        txtNuevaContrasenia.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0)
        txtNuevaContrasenia.font = Constantes.currentFonts.NunitoRegular.base
        txtNuevaContrasenia.isSecureTextEntry = true
        addToolBar(textField: txtNuevaContrasenia)
        scrollView.addSubview(txtNuevaContrasenia)
        
        xframe.origin = CGPoint(x: 10, y: txtNuevaContrasenia.frame.maxY + 20)
        xframe.size = CGSize(width: Constantes.tamaños.anchoTextfield, height: Constantes.tamaños.altoTextfield)
        txtRepiteContrasenia = UITextField(frame: xframe)
        txtRepiteContrasenia.placeholder = NSLocalizedString("RepeatPassword", comment: "")
        txtRepiteContrasenia.layer.borderWidth = Constantes.tamaños.sizeBordeTextfield
        txtRepiteContrasenia.layer.borderColor = Constantes.paletaColores.bordeTextfield.cgColor
        txtRepiteContrasenia.layer.cornerRadius = cornerCampos
        txtRepiteContrasenia.center.x = Constantes.tamaños.centroHorizontal
        txtRepiteContrasenia.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0)
        txtRepiteContrasenia.font = Constantes.currentFonts.NunitoRegular.base
        txtRepiteContrasenia.isSecureTextEntry = true
        addToolBar(textField: txtRepiteContrasenia)
        scrollView.addSubview(txtRepiteContrasenia)
        
        self.view.addSubview(self.scrollView)
        
        //Button change password
        let xpadding =  (Constantes.tamaños.altoBotonTop) + (self.navigationController?.navigationBar.frame.size.height)! + Constantes.tamaños.altoStatusBar
        xframe.origin = CGPoint(x: 0, y: Constantes.tamaños.altoPantalla - xpadding)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: Constantes.tamaños.altoBotonTop)
        btnChangePassword = UIButton(frame: xframe)
        btnChangePassword.backgroundColor = Constantes.paletaColores.verdePeoples
        btnChangePassword.setTitle(NSLocalizedString("ChangePassword", comment: "").uppercased(), for: .normal)
        btnChangePassword.titleLabel?.font = Constantes.currentFonts.NunitoRegular.base
        btnChangePassword.addTarget(self, action: #selector(seleccionaBotonModifContrasenia), for: .touchUpInside)
        
        self.view.addSubview(btnChangePassword)
        
    }
    
    func validaCampos() -> Bool {
        var success = true
        
        if txtContraseniaActual.text?.trimmed() == "" {
            let message = NSLocalizedString("AlertCurrentPassword", comment: "")
            self.mensajeSimple(mensaje: message)
            success = false
        }
        
        if txtNuevaContrasenia.text?.trimmed() == "" {
            let message = NSLocalizedString("AlertNewPassword", comment: "")
            self.mensajeSimple(mensaje: message)
            success = false
        }
        
        if(txtNuevaContrasenia.text?.trimmed() != txtRepiteContrasenia.text?.trimmed()) {
            let message = NSLocalizedString("AlertPasswordNotMatch", comment: "")
            self.mensajeSimple(mensaje: message)
            success = false
        }
        return success
    }
    
    //MARK: - Keyboard
    @objc func keyboardWillShow(sender: NSNotification) {
        var userInfo = sender.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height
        self.scrollView.contentInset = contentInset
        
        var visibleRect = self.view.frame
        visibleRect.size.height -= keyboardFrame.size.height
        
        var elOrigin = CGPoint.zero
        var scroll = false
        
        if self.txtNuevaContrasenia.isFirstResponder {
            visibleRect.size.height -= ((self.txtNuevaContrasenia.inputAccessoryView?.frame.size.height)! * 2)
            elOrigin = self.txtNuevaContrasenia.frame.origin
            scroll = true
        }
        
        if self.txtRepiteContrasenia.isFirstResponder {
            visibleRect.size.height -= ((self.txtRepiteContrasenia.inputAccessoryView?.frame.size.height)! * 2)
            elOrigin = self.txtRepiteContrasenia.frame.origin
            scroll = true
        }
        
        if ((scroll) && !(visibleRect.contains(elOrigin))){
            let scrollPoint = CGPoint(x: 0, y: elOrigin.y - visibleRect.size.height)
            self.scrollView.setContentOffset(scrollPoint, animated: true)
        }
    }
    
    @objc func keyboardWillHide(sender: NSNotification) {
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        self.scrollView.contentInset = contentInset
    }
    
    
    
    
}
