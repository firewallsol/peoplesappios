//
//  ConsumosCashbackVC.swift
//  PeoplesApp
//
//  Created by Jesús García on 15/02/18.
//  Copyright © 2018 firewallsoluciones. All rights reserved.
//

import UIKit

class ConsumosCashbackVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    //MARK: - Components
    var tableView: UITableView!
    var arrayCashbacks = [Cashback]()
    
    //MARK: - Vars
    var consumoCellIdentifier = "consumoCellIdentifier"
    
    //MARK: - Init
    init() {
        super.init(nibName: nil, bundle: nil)
        tableView = UITableView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - View
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        
        configureTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - TableView
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableView.validaExistenciaDatos(datos: arrayCashbacks as AnyObject)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayCashbacks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: consumoCellIdentifier, for: indexPath) as! CeldaTVConsumoCashback
        let cashback = arrayCashbacks[indexPath.row]

        cell.lblDeudorNombre.text = cashback.deudor.nombre
        cell.lblDeudorNombre.sizeToFit()

        let consumo = NSLocalizedString(cashback.tipo.rawValue, comment: "").capitalized
        let monto = cashback.monto.formatoPrecio()
        let divisa = cashback.divisa.abreviacion!

        cell.lblMontoConsumo.text = "\(consumo):\n\(monto) \(divisa)"
        cell.lblMontoConsumo.sizeToFit()

        cell.lblStatus.text = cashback.estatus.descripcion
        cell.lblStatus.sizeToFit()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cashbackSelected = arrayCashbacks[indexPath.row]
        let detalleCashbackVC = DetalleConsumoCashbackVC()
        detalleCashbackVC.cashback = cashbackSelected

        self.navigationController?.pushViewController(detalleCashbackVC, animated: true)
    }
    
    //MARK: - Custom
    func configureTableView() {
        tableView.frame = self.view.frame
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableViewAutomaticDimension
        
        tableView.register(CeldaTVConsumoCashback.self, forCellReuseIdentifier: consumoCellIdentifier)
        tableView.dataSource = self
        tableView.delegate = self

        //Table constraints.
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
    }
}
