//
//  Constantes.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 29/06/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit


enum senderSolicitaServicio : String {
    case Main = "main"
    case listaPro = "listaProfesionales"
    case detallePro = "detalleProfesional"
}

//enum estatusAfiliacion : String {
//    case noAfiliado = "1"
//    case solicitada = "2"
//    case preaprobada = "3"
//    case rechazada = "4"
//    case completada = "5"
//    case denegada = "6"
//}

struct Constantes {
    
    struct URLS {
//        static let baseServicios = "http://firewallsoluciones.com.mx/peoplesapp/src/services/"
        static let baseServicios = "https://www.pplesapp.com/admin/src/services/"
        ///TEST
        static let afiliarNegocio = "https://www.pplesapp.com/"
        ///
        static let registroUsuario = "registro.php"
        static let loginUsuario = "login.php"
        static let mainData = "principal.php"
        static let getProfesionalesCategoria = "getprofesionales.php"
        static let solicitarServicio = "solicitud.php"
        static let enviaMsgChat = "chat.php"
        static let getChatMsgs = "historico.php"
        static let sender_chat_message = "cliente"
        static let listadoSolicitudes = "listado.php"
        static let responderPropuesta = "responderpropuesta.php"
        static let calificarServicio = "calificarservicio.php"
        static let actualizaServicio = "estatusservicio.php"
        static let afiliarServicio = "afiliar.php"
        static let infoCashBack = "infocashback.php"
        static let updateFormaPago = "updformapago.php"
        static let updateUsuario = "updusuarioperfil.php"
        static let registrarConsumo = "registrarconsumo.php"
        static let valoracionDeProfesional = "valoracionespro.php"
        static let recuperarContrasenia = "solicitudrecovery.php"
        static let cambiarContrasenia = "updpwdpro.php"
        static let validaCodigoAfiliador = "validacodigo.php"
        static let listadoCashback = "listadocashbackcliente.php"

    }
    
    struct strings {
        static let GoogleMapsApiKey = "AIzaSyDc6ILzU54PsXLjq5bldRcd-pJqa1EpXtk"
        static let GooglePlacesApiKey = "AIzaSyAJ5CCsWBE3TDl7h18cv1BQTM-niYjcJ0w"
        
//        struct afiliateText {
//            static let parrafo1 = "Afiliate y por cada negocio que afilies y que visite a uno de nuestros afiliados recibe reembolsos en tu cuenta."
//            static let esFacil = "Es muy fácil sólo sigue los siguientes pasos:"
//            static let paso1 = "Completa tu registro básico"
//            static let paso2 = "Utiliza el escaneo de archivos para adjuntar tus documentos"
//            static let paso3 = "Realiza el pago de inscripción y comienza a ganar con People's App"
//            static let tituloCargaDoctos = "Utiliza el escaneo de archivos para añadir los siguientes documentos:"
//            static let doctoID = "Identificación oficial"
//            static let doctoComprobante = "Comprobante de domicilio"
//            static let doctoRFC = "Registro Tributario"
//            static let doctoPayPal = "Añadir cuenta Paypal"
//        }
    }
    
    struct OneSignalAppId {
        static let pplesCliente = "243500b5-9a7d-44ac-81c4-3edcba12135a"
        static let pplesProfesional = "8cd3fb7e-217f-49c0-90f0-ddf3a5f4ce51"
    }
    
    struct notificationNames {
        static let backToListadoPedidos = Notification.Name("backToListadoPedidos")
        static let backToListadoPropuesta = Notification.Name("backToListadoPropuesta")
    }
    
    
    static let ArrayLugarTrabajo = ["Administración Pública/Gobierno",
                                    "Alimentos",
                                    "Automotriz",
                                    "Comercio",
                                    "Construcción",
                                    "Editorial",
                                    "Educación",
                                    "Energético",
                                    "Inmobiliario",
                                    "Instituciones Financieras",
                                    "Legal",
                                    "Logística",
                                    "Manufactura",
                                    "Mercadotecnia",
                                    "Medios y Entretenimiento",
                                    "Petrolera",
                                    "Retail",
                                    "Salud",
                                    "Servicios Profesionales",
                                    "Tecnología y Sistemas",
                                    "Telecomunicaciones",
                                    "Transporte",
                                    "Turismo / Hotelera",
                                    "Otros"]
    
    
    struct tamaños {
        static let altoStatusBar = UIApplication.shared.statusBarFrame.height
        static let screenBounds = UIScreen.main.bounds
        static let tamanioPantalla   = screenBounds.size
        static let anchoPantalla  = tamanioPantalla.width
        static let altoPantalla = tamanioPantalla.height
        static let altoCeldaNegociosMain = Constantes.tamaños.anchoPantalla * 0.3
        static let altoCeldaOpinion = Constantes.tamaños.anchoPantalla * 0.2
        static let altoCeldaPedidos = Constantes.tamaños.anchoPantalla * 0.3
        static let altoBotonTop = Constantes.tamaños.anchoPantalla * 0.13
        static let centroHorizontal = Constantes.tamaños.anchoPantalla / 2
        static let centroVertical = Constantes.tamaños.altoPantalla / 2
        static let altoTextfield = Constantes.tamaños.anchoPantalla * 0.13
        static let anchoTextfield = Constantes.tamaños.anchoPantalla * 0.9
        static let sizeBordeTextfield : CGFloat = 0.5
        static let bordeBotonesCalificar : CGFloat = 1.5
        static let altoCeldaPerfilDefault = Constantes.tamaños.anchoPantalla * 0.13
        static let loaderSize = CGSize(width: Constantes.tamaños.anchoPantalla * 0.2, height: Constantes.tamaños.anchoPantalla * 0.2)
        
        struct fuentes {
            static let LabelFontSize = UIFont.labelFontSize
            
            struct minus {
                static let LabelFontSize_1 = UIFont.labelFontSize - 1
                static let LabelFontSize_2 = UIFont.labelFontSize - 2
                static let LabelFontSize_3 = UIFont.labelFontSize - 3
                static let LabelFontSize_4 = UIFont.labelFontSize - 4
            }
            
            struct plus {
                static let LabelFontSize_1 = UIFont.labelFontSize + 1
                static let LabelFontSize_2 = UIFont.labelFontSize + 2
                static let LabelFontSize_3 = UIFont.labelFontSize + 3
                static let LabelFontSize_4 = UIFont.labelFontSize + 4
            }
            
        }
    }
    
    struct paletaColores {
        static let verdePeoples = UIColor(red: 32/255, green: 151/255, blue: 185/255, alpha: 1)
        static let azulFacebook = UIColor(red: 66/255, green: 97/255, blue: 155/255, alpha: 1)
        static let rojoGoogle = UIColor(red: 243/255, green: 62/255, blue: 59/255, alpha: 1)
        static let bordeTextfield = UIColor.lightGray
        static let verdeCompletado = UIColor(red: 52/255, green: 157/255, blue: 67/255, alpha: 1)
    }
    
    struct fontNames {
        static let NunitoSemiBold = "Nunito-SemiBold"
        static let NunitoRegular = "Nunito-Regular"
        static let NunitoMedium = "Nunito-Medium"
        static let NunitoBold = "Nunito-Bold"
        static let NunitoBlack = "Nunito-Black"
        static let NunitoExtraLight = "Nunito-ExtraLight"
        static let NunitoExtraBold = "Nunito-ExtraBold"
        static let NunitoLight = "Nunito-Light"
    }
    
    struct currentFonts {
        struct NunitoRegular {
            static let base = UIFont(name: Constantes.fontNames.NunitoRegular, size: Constantes.tamaños.fuentes.LabelFontSize)
            struct minus {
                static let LabelFontSize_1 = UIFont(name: Constantes.fontNames.NunitoRegular, size: Constantes.tamaños.fuentes.minus.LabelFontSize_1)
                static let LabelFontSize_2 = UIFont(name: Constantes.fontNames.NunitoRegular, size: Constantes.tamaños.fuentes.minus.LabelFontSize_2)
                static let LabelFontSize_3 = UIFont(name: Constantes.fontNames.NunitoRegular, size: Constantes.tamaños.fuentes.minus.LabelFontSize_3)
                static let LabelFontSize_4 = UIFont(name: Constantes.fontNames.NunitoRegular, size: Constantes.tamaños.fuentes.minus.LabelFontSize_4)
            }
            
            struct plus {
                static let LabelFontSize_1 = UIFont(name: Constantes.fontNames.NunitoRegular, size: Constantes.tamaños.fuentes.plus.LabelFontSize_1)
                static let LabelFontSize_2 = UIFont(name: Constantes.fontNames.NunitoRegular, size: Constantes.tamaños.fuentes.plus.LabelFontSize_2)
                static let LabelFontSize_3 = UIFont(name: Constantes.fontNames.NunitoRegular, size: Constantes.tamaños.fuentes.plus.LabelFontSize_3)
                static let LabelFontSize_4 = UIFont(name: Constantes.fontNames.NunitoRegular, size: Constantes.tamaños.fuentes.plus.LabelFontSize_4)
            }
        }
        
        struct NunitoBold {
            static let base = UIFont(name: Constantes.fontNames.NunitoBold, size: Constantes.tamaños.fuentes.LabelFontSize)
            struct minus {
                static let LabelFontSize_1 = UIFont(name: Constantes.fontNames.NunitoBold, size: Constantes.tamaños.fuentes.minus.LabelFontSize_1)
                static let LabelFontSize_2 = UIFont(name: Constantes.fontNames.NunitoBold, size: Constantes.tamaños.fuentes.minus.LabelFontSize_2)
                static let LabelFontSize_3 = UIFont(name: Constantes.fontNames.NunitoBold, size: Constantes.tamaños.fuentes.minus.LabelFontSize_3)
                static let LabelFontSize_4 = UIFont(name: Constantes.fontNames.NunitoBold, size: Constantes.tamaños.fuentes.minus.LabelFontSize_4)
            }
            
            struct plus {
                static let LabelFontSize_1 = UIFont(name: Constantes.fontNames.NunitoBold, size: Constantes.tamaños.fuentes.plus.LabelFontSize_1)
                static let LabelFontSize_2 = UIFont(name: Constantes.fontNames.NunitoBold, size: Constantes.tamaños.fuentes.plus.LabelFontSize_2)
                static let LabelFontSize_3 = UIFont(name: Constantes.fontNames.NunitoBold, size: Constantes.tamaños.fuentes.plus.LabelFontSize_3)
                static let LabelFontSize_4 = UIFont(name: Constantes.fontNames.NunitoBold, size: Constantes.tamaños.fuentes.plus.LabelFontSize_4)
            }
        }
        
    }
    
    //Catalogo de paises.
    static let arrayPaises = [Pais(id: "1", descripcion: NSLocalizedString("Mexico", comment:"")),
                            Pais(id: "2", descripcion: NSLocalizedString("United States of America", comment:""))]
    
    static let dummyOpiniones = [
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
        "Nam ornare tellus justo, ac tristique neque interdum sed. Suspendisse sit amet tortor eget lacus pretium vulputate at aliquet mauris. In blandit posuere est, ac gravida risus interdum vel.",
        "Nullam congue dolor eget massa elementum commodo. Nullam congue dolor eget massa elementum commodo.",
        "Proin imperdiet sapien et odio pellentesque, maximus mattis nisi bibendum. Mauris sit amet dui cursus, mattis sapien non, rhoncus nunc.",
        "Est architecto blanditiis fugit ut quae sunt. Incidunt pariatur sit reprehenderit reprehenderit id provident. Voluptate animi provident incidunt quaerat rerum possimus voluptatem. Aut et aut aliquid consectetur eum adipisci. Veniam mollitia deserunt quo."]
    
    static let dummyNombreServicios = ["Reparacion de TV", "Instalacion de TV", "Reparacion de calentador de agua", "Reparacion de lavadora", "Limpieza de sala", "Pintura en habitacion"]
}
