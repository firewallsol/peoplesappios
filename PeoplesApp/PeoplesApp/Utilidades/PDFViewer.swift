//
//  PDFViewer.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 03/10/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import WebKit

class PDFViewer: UIViewController {
    
    var webView : UIWebView!

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadurl(laUrl: URL){
        
        self.webView = UIWebView(frame: self.view.frame)
        self.view.addSubview(self.webView)
         let req = NSMutableURLRequest(url: laUrl)
        self.webView.loadRequest(req as URLRequest)
    }

}
