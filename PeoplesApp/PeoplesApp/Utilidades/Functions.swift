//
//  Functions.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 30/06/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import JSQMessagesViewController
import CocoaLumberjack

enum tipoRegistroLogin : Int {
    case email = 1
    case facebook = 2
    case google = 3
}

enum tipoDiccFavorito : Int {
    case empresa = 1
    case profesional = 2
    case usuario = 99
}

//enum tipoDivisa : String {
//    case MXN = "1"
//    case USD = "2"
//    case EUR = "3"
//}

protocol Empresa_Profesional {
}

class Functions  {
    
    let manager = {() -> Alamofire.SessionManager in
        struct Static {
            static var dispatchOnceToken: dispatch_time_t = 0
            static var instance: Alamofire.SessionManager!
        }
        
        let config = URLSessionConfiguration.default
        config.requestCachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        config.urlCache = nil
        
        let cookies = HTTPCookieStorage.shared
        config.httpCookieStorage = cookies
        Static.instance = Alamofire.SessionManager(configuration: config)
        
        return Static.instance
    }()
    
    let userDefaults = UserDefaults.standard
    
    //MARK: - SERVICIOS WEB
    func getMainData(latitud: String, longitud: String, completionHandler: @escaping (Bool, [Categoria], [Empresa], [Profesional], Error?) ->()){
        var arrayCategorias = [Categoria]()
        var arrayEmpresas = [Empresa]()
        var arrayProfesionales = [Profesional]()
        
        let queryString = Constantes.URLS.baseServicios + Constantes.URLS.mainData
        
        let params = [
            "lat" : latitud,
            "lng" : longitud,
            "index" : "0"
        ]
        
        DDLogInfo("PARAMS: \(params)")
        
        self.manager.request(queryString, method: .post, parameters: params).responseJSON{ response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                
                DDLogVerbose("JSON: \(json)")
                
                if json["result"].string == "SUCCESS" {
                    arrayCategorias = self.jsonToCategoriaArray(elJson: json)
                    arrayEmpresas = self.jsonToEmpresasArray(elJson: json)
                    arrayProfesionales = self.jsonToProfesionalArray(elJson: json, paraQuien: "main")
                    completionHandler(true, arrayCategorias, arrayEmpresas, arrayProfesionales, nil)
                } else {
                    completionHandler(false, arrayCategorias, arrayEmpresas, arrayProfesionales, nil)
                }
                
            case .failure(let error):
                DDLogError(error.localizedDescription)
                completionHandler(false, arrayCategorias, arrayEmpresas, arrayProfesionales, error)
            }
        }
    }
    
    func registerUser(rnombre: String,
                      rapellidos: String,
                      rcorreo: String,
                      rpasswd: String,
                      rtipo: tipoRegistroLogin,
                      telefono: String = "",
                      rcashback: String = "",
                      avatar: String = "",
                      paisId: String = "",
                      completionHandler: @escaping (Bool, String, Error?) -> ()){
        DDLogInfo("Ingresando...")
        
        let queryString = Constantes.URLS.baseServicios + Constantes.URLS.registroUsuario
        
        let params = [
            "nombre" : rnombre,
            "apellidos" : rapellidos,
            "email" : rcorreo,                  //obligatorio
            "password" : rpasswd,               //obligatorio - ID Google/Facebook
            "telefono" : telefono,
            "login" : "\(rtipo.rawValue)",      //obligatorio
            "cashback" : rcashback,
            "avatar" : avatar,
            "pais": paisId                      //1-MEX, 2-EUA
        ]

        DDLogInfo("PARAMS: \(params)")
        
        self.manager.request(queryString, method: .post, parameters: params).responseJSON{ response in
            //print(response)
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                print(json)
                let desc = json["desc"].string!
                if json["result"].string == "SUCCESS" {
                    DDLogInfo("Se registró usuario con éxito!")
                    
                    let usuario = self.jsonToUsuario(elJson: json)
                    self.saveUserData(elUsuario: usuario)
                    completionHandler(true, desc, nil)
                } else {
                    completionHandler(false, desc, nil)
                }
                break
            case .failure(let error):
                DDLogError(error.localizedDescription)
                completionHandler(false,"Ha ocurrido un error al intentar registrar su usuario, revise su conexion", error)
            }
        }
    }
    
    func loginUserEmail(username: String, password: String, idDispositivo: String, completionHandler: @escaping (Bool, String, Error?) -> ()){
        DDLogInfo("Ingresando...")
        
        let queryString = Constantes.URLS.baseServicios + Constantes.URLS.loginUsuario
        
        let params = [
            "email" : username,
            "password" : password,
            "deviceid" : idDispositivo
        ]
        
        DDLogDebug("PARAMS: \(params)")
        
        self.manager.request(queryString, method: .post, parameters: params).responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                DDLogVerbose("JSON: \(json)")
                
                let desc = json["desc"].string!
                if json["result"].string == "SUCCESS" {
                    DDLogInfo("Realizó login con éxito!")
                    
                    let usuario = self.jsonToUsuario(elJson: json)
                    self.saveUserData(elUsuario: usuario)
                    completionHandler(true, desc, nil)
                } else {
                    completionHandler(false, desc, nil)
                }
                break
            case .failure(let error):
                print(error)
                completionHandler(false,"Ha ocurrido un error al intentar hacer login, revise su conexion", error)
            }
        }
    }
    
    func getProfesionalesCategoria(latitud: String, longitud: String, categoria: String,
                                   completionHandler: @escaping (Bool, String, [Profesional], Error?) ->()){
        DDLogInfo("Ingresando...")
        
        var arrayProfesionales = [Profesional]()
        let queryString = Constantes.URLS.baseServicios + Constantes.URLS.getProfesionalesCategoria
        var mensaje = ""
        
        let params = [
            "lat" : latitud,
            "lng" : longitud,
            //WARNING: Index Hardcoded
            "index" : "0",
            "categoria" : categoria
        ]
        
        DDLogDebug("PARAMS: \(params)")
        
        self.manager.request(queryString, method: .get, parameters: params).responseJSON{ response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                
                if let value = json["desc"].string {
                    mensaje = value
                }
                DDLogVerbose("JSON: \(json)")
                
                if json["result"].string == "SUCCESS" {
                    arrayProfesionales = self.jsonToProfesionalArray(elJson: json, paraQuien:"categoria")
                    completionHandler(true, mensaje, arrayProfesionales, nil)
                } else {
                    completionHandler(false, mensaje, arrayProfesionales, nil)
                }
                
            case .failure(let error):
                print(error)
                completionHandler(false, mensaje, arrayProfesionales, error)
            }
        }
        
    }
    
    func solicitarServicio(titulo: String,
                           descripcion: String,
                           categoria: String,
                           presupuesto: String,
                           idDivisa: String,
                           fecha: String,
                           direccion: String,
                           latitude: String,
                           longitude: String,
                           idProfesional: String,
                           completionHandler: @escaping (Bool, String, Error?) ->()){
        
        let queryString = Constantes.URLS.baseServicios + Constantes.URLS.solicitarServicio
        
        let params = [
            "usuario" : self.getUserId(),
            "titulo" : titulo,
            "descr" : descripcion,
            "categoria" : categoria,
            "presupuesto" : presupuesto,
            "divisa" : idDivisa,
            "fecha" : fecha,
            "direccion" : direccion,
            "lat" : latitude,
            "lng" : longitude,
            "profesional" : idProfesional
        ]
        
        print("PARAMETROS: \(params)")
        
        self.manager.request(queryString, method: .post, parameters: params).responseJSON{ response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                
                DDLogVerbose("JSON: \(json)")
                
                var mensaje = "Solicitud creada con exito"
                if let value = json["desc"].string {
                    mensaje = value
                }
                if json["result"].string == "SUCCESS" {
                    completionHandler(true, mensaje, nil)
                } else {
                    completionHandler(false, mensaje, nil)
                }
                
            case .failure(let error):
                print(error)
                completionHandler(false, "", error)
            }
        }
        
    }
    
    func getMensajesChat(idServicio : String,
                         fecha: String,
                         idProfesional: String ,
                         completionHandler: @escaping ([JSQMessage], Bool, String, Error?) ->()){
        
        let queryString = Constantes.URLS.baseServicios + Constantes.URLS.getChatMsgs
        
        let xparametros = [
            "idservicio" : idServicio,
            "fecha" : fecha,
            "profesional" : idProfesional,
            "cliente" : self.getUserId()
        ]
        
        var arrayMensajes = [JSQMessage]()
        
        self.manager.request(queryString, method: .post, parameters: xparametros).responseJSON{ response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                
                DDLogVerbose("JSON: \(json)")
                
                if json["result"].string == "SUCCESS" {
                    arrayMensajes = self.jsonToArrayMensajes(json: json["chat"])
                    completionHandler(arrayMensajes, true, "", nil)
                } else {
                    var msg = "Problemas con el chat"
                    if let value = json["desc"].string {
                        msg = value
                    }
                    completionHandler(arrayMensajes, false, msg, nil)
                }
                
            case .failure(let error):
                print(error)
                completionHandler(arrayMensajes, false, error.localizedDescription, error)
            }
        }
    }
    
    func sendMensajeChat(idProfesional: String, idServicio : String, mensaje: String ,completionHandler: @escaping (Bool, String, Error?) ->()){
        
        let queryString = Constantes.URLS.baseServicios + Constantes.URLS.enviaMsgChat
        
        let xparametros = [
            "idcliente" : self.getUserId(),
            "idprofesional" : idProfesional,
            "from" : Constantes.URLS.sender_chat_message,
            "msj" : mensaje,
            "idservicio" : idServicio
        ]
        
        
        Alamofire.request(queryString, method: .post, parameters: xparametros).responseJSON{ response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                DDLogVerbose("JSON: \(json)")
                
                if json["result"].string == "SUCCESS" {
                    completionHandler(true, "", nil)
                } else {
                    var msg = "Error envio de mensaje chat"
                    if let value = json["desc"].string {
                        msg = value
                    }
                    completionHandler(false, msg, nil)
                }
                
            case .failure(let error):
                print("print del failure->\(error)")
                completionHandler(false, error.localizedDescription, error)
            }
        }
        
    }
    
    func getListadosolicitudes(index: Int, completionHandler: @escaping (Bool, [Servicio], [Servicio], [Servicio], Error?) ->()){
        let queryString = Constantes.URLS.baseServicios + Constantes.URLS.listadoSolicitudes
        
        let params = [
            "usuario" : self.getUserId(),
            "index" : "\(index)"
        ]
        
        var serviciosSolicitados = [Servicio]()
        var serviciosActivos = [Servicio]()
        var serviciosHistoricos = [Servicio]()
        
        self.manager.request(queryString, method: .post, parameters: params).responseJSON{ response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                DDLogVerbose("JSON: \(json)")

                if json["result"].string == "SUCCESS" {
                    serviciosSolicitados = self.jsonToArrayServicio(elJson: json["data"]["solicitadas"])
                    serviciosActivos = self.jsonToArrayServicio(elJson: json["data"]["activas"])
                    serviciosHistoricos = self.jsonToArrayServicio(elJson: json["data"]["historico"])
                    completionHandler(true, serviciosSolicitados, serviciosActivos, serviciosHistoricos, nil)
                } else {
                    completionHandler(false, serviciosSolicitados, serviciosActivos, serviciosHistoricos, nil)
                }
                
            case .failure(let error):
                print(error)
                completionHandler(false, serviciosSolicitados, serviciosActivos, serviciosHistoricos, error)
            }
        }
        
    }
    
    
    func responderPropuesta(idServicio: String, idPropuesta: String, accion: String, completionHandler: @escaping (Bool, String, Error?) ->()){
        
        let queryString = Constantes.URLS.baseServicios + Constantes.URLS.responderPropuesta
        
        let params = [
            "idservicio" : idServicio,
            "idpropuesta" : idPropuesta,
            "accion" : accion
        ]
        
        DDLogVerbose("Params: \(params)")
        var mensaje = ""
        
        self.manager.request(queryString, method: .post, parameters: params).responseJSON{ response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                DDLogVerbose("JSON: \(json)")
                
                if let value = json["desc"].string {
                    mensaje = value
                }
                if json["result"].string == "SUCCESS" {
                    DDLogInfo("Success")
                    completionHandler(true, mensaje, nil)
                } else {
                    completionHandler(false, mensaje, nil)
                }
                
            case .failure(let error):
                DDLogError(error.localizedDescription)
                completionHandler(false, mensaje, error)
            }
        }
    }
    
    func calificarServicio(idProfesional: String, idServicio: String, satisfaccion: String, calificacion: String, comentario: String, completionHandler: @escaping (Bool, String, Error?) ->()){
        
        let queryString = Constantes.URLS.baseServicios + Constantes.URLS.calificarServicio
        
        var params = [
            "califica" : self.getUserId(),
            "calificado" : idProfesional,
            "servicio" : idServicio,
            "calificacion" : calificacion,
            "profesional" : "1" //Bandera para identificar quien califica, Usuario-0, Profesional-1
        ]
        
        if satisfaccion != "0" {
            params["satisfaccion"] = satisfaccion
        }
    
        if comentario != "" {
            params["comentario"] = comentario
        }
        
        
        var mensaje = ""
        
        self.manager.request(queryString, method: .post, parameters: params).responseJSON{ response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                DDLogVerbose("JSON: \(json)")
                
                if let value = json["desc"].string {
                    mensaje = value
                }
                if json["result"].string == "SUCCESS" {
                    completionHandler(true, mensaje, nil)
                } else {
                    completionHandler(false, mensaje, nil)
                }
                
            case .failure(let error):
                print(error)
                completionHandler(false, mensaje, error)
            }
        }
        
    }
    
    func actualizaServicio(idServicio: String, estatus: String, completionHandler: @escaping (Bool, String, Error?) ->()){
        
        let queryString = Constantes.URLS.baseServicios + Constantes.URLS.actualizaServicio
        
        let params = [
            "servicio" : idServicio,
            "estatus" : estatus
        ]
        
        DDLogDebug("PARAMS: \(params)")
        
        var mensaje = ""
        
        self.manager.request(queryString, method: .post, parameters: params).responseJSON{ response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                
                DDLogVerbose("JSON: \(json)")
                
                if let value = json["desc"].string {
                    mensaje = value
                }
                if json["result"].string == "SUCCESS" {
                    completionHandler(true, mensaje, nil)
                } else {
                    completionHandler(false, mensaje, nil)
                }
                
            case .failure(let error):
                print(error)
                completionHandler(false, mensaje, error)
            }
        }
        
    }
    
    func sendDataAfiliate(unUsuario: Usuario, codigoAfiliador: String, urlPdf: URL, completionHandler: @escaping (Bool, String, Error?) -> ()){
        let queryString = Constantes.URLS.baseServicios + Constantes.URLS.afiliarServicio
        
        let params = [
            "usuario" : self.getUserId(),
            "nombre" : unUsuario.nombre,
            "apellidos" : unUsuario.apellidos,
            "rfc" : unUsuario.tinRfc,
            "telefono" : unUsuario.telefono,
            "direccion" : unUsuario.direcciones.first?.descripcion!,
            "paypal" : unUsuario.cuentaPaypal,
            "codigo" : codigoAfiliador
        ]
        //print("voy a enviar los datos...")
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                
                for (key, value) in params {
                    multipartFormData.append(((value)?.data(using: .utf8))!, withName: key)
                }
                
                multipartFormData.append(urlPdf, withName: "documentacion")
        },
            to: queryString,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseString { response in
                        
                        debugPrint(response)
                        
                        if let responseString = response.value {
                            let json = JSON(parseJSON: responseString)
                            
                            let desc = json["desc"].string!
                            
                            DDLogVerbose("JSON: \(json)")
                            
                            if json["result"].string == "SUCCESS" {
                                let usuario = self.jsonToUsuario(elJson: json)
                                self.saveUserData(elUsuario: usuario)
                                completionHandler(true, desc, nil)
                            } else {
                                completionHandler(false, desc, nil)
                            }
                        } else {
                            completionHandler(false, "Ocurrio un error al obtener respuesta del servidor, revise su conexion", nil)
                        }
                        
                    }
                case .failure(let encodingError):
                    print(encodingError)
                    completionHandler(false, "Ocurrio un problema al enviar la informacion, revise su conexion", encodingError)
                }
        }
        )
        
        
    }
    
    func validaCodigoAfiliador(codigoAfiliador: String, completion:@escaping(Bool, String, Error?)->()) {
        let queryString = Constantes.URLS.baseServicios + Constantes.URLS.validaCodigoAfiliador
        
        let params = [
            "codigo" : codigoAfiliador
        ]
        
        DDLogDebug("PARAMS: \(params)")
        
        var mensaje = ""
        self.manager.request(queryString, method: .post, parameters: params).responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                print("actualizaServicio json-> \(json)")
                if let value = json["desc"].string {
                    mensaje = value
                }
                if json["result"].string == "SUCCESS" {
                    completion(true, mensaje, nil)
                } else {
                    completion(false, mensaje, nil)
                }
                
            case .failure(let error):
                print(error)
                completion(false, mensaje, error)
            }
        }
    }
    
    func getInfoCashBack(completionHandler: @escaping (Bool, String, Usuario, DatosCashback, Error?) -> ()){
        DDLogInfo("Ingresando...")
        
        let queryString = Constantes.URLS.baseServicios + Constantes.URLS.infoCashBack
        let params = [
            "usuario" : self.getUserId()
        ]
        
        var mensaje = "Ha ocurrido un error al consultar la información por favor revise su conexión"
        var usuario = Usuario()
        var datoscb = DatosCashback()
        
        self.manager.request(queryString, method: .post, parameters: params).responseJSON{ response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                DDLogVerbose("JSON: \(json)")
                
                if let value = json["desc"].string {
                    mensaje = value
                }
                if json["result"].string == "SUCCESS" {
                    usuario = self.jsonToUsuario(elJson: json, paraPerfil: true)
                    self.saveUserData(elUsuario: usuario)
                    datoscb = self.jsonToDatosCashBack(elJson: json["data"]["cashback"])
                    completionHandler(true, mensaje, usuario, datoscb, nil)
                } else {
                    completionHandler(false, mensaje, usuario, datoscb, nil)
                }
                
            case .failure(let error):
                DDLogError(error.localizedDescription)
                completionHandler(false, mensaje, usuario, datoscb, error)
            }
        }
    }
    
    ///updformapago.php?usuario=1&paypal=zero@mail.com
    func updateFormaPago(paypal: String, completionHandler: @escaping (Bool, String, Error?) -> ()){
        let queryString = Constantes.URLS.baseServicios + Constantes.URLS.updateFormaPago
        
        let params = [
            "usuario" : self.getUserId(),
            "paypal" : paypal
        ]
        
        var mensaje = "Ha ocurrido un error al enviar la información por favor revise su conexion"
        
        self.manager.request(queryString, method: .post, parameters: params).responseJSON{ response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                print("updateFormaPago json-> \(json)")
                if let value = json["desc"].string {
                    mensaje = value
                }
                if json["result"].string == "SUCCESS" {
                    let usuario = self.jsonToUsuario(elJson: json)
                    self.saveUserData(elUsuario: usuario)
                    completionHandler(true, mensaje, nil)
                } else {
                    completionHandler(false, mensaje, nil)
                }
                
            case .failure(let error):
                print(error)
                completionHandler(false, mensaje, error)
            }
        }
    }
    
    func updateUsuarioPerfil(imagePath:String,
                             nombre: String,
                             apellidos: String,
                             telefono: String,
                             pais:String,
                             idDireccion: String,
                             direccion:String,
                             latitud: String,
                             longitud: String,
                             completionHandler: @escaping (Bool, String, Error?) -> ()){
        
        let queryString = Constantes.URLS.baseServicios + Constantes.URLS.updateUsuario
        
        let params = [
            "usuario" : self.getUserId(),
            "dir_id" : idDireccion,
            "dir_direccion" :direccion,
            "dir_lat" : latitud,
            "dir_lng" : longitud,
            "nombre" : nombre,
            "apellidos" : apellidos,
            "telefono" : telefono,
            "pais": pais
        ]
                
        DDLogInfo("PARAMS: \(params)")
        var mensaje = "Ha ocurrido un error al enviar la información por favor revise su conexion"
        
        Alamofire.upload( multipartFormData: { multipartFormData in
            ///Agregar Parametros al formulario.
            for (key, value) in params {
                //Convierte los strings a Data
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
            
            ///Agrega imagen de perfil, solo si contiene una.
            if imagePath != "" {
                let filePath = URL(fileURLWithPath: imagePath)
                DDLogDebug("FilePath: \(filePath)")
                
                // add file as Request Body with fieldname "uploadImage"
                multipartFormData.append(filePath, withName: "uploadImage")
            }
        },
                          to: queryString,
                          encodingCompletion: { encodingResult in
                            
                            switch encodingResult {
                            case .success(let upload, _, _):
                                DDLogInfo("SUCCESS!")
                                
                                //Obtiene response.
                                upload.responseJSON { response in
                                    
                                    let json = JSON(response.result.value!)
                                    DDLogVerbose("JSON: \(json)")
                                    
                                    if let value = json["desc"].string {
                                        mensaje = value
                                    }
                                    
                                    if json["result"].string == "SUCCESS" {
                                        let usuario = Functions().jsonToUsuario(elJson: json)
                                        self.saveUserData(elUsuario: usuario)
                                        completionHandler(true, mensaje, nil)
                                    }
                                    else {
                                        completionHandler(false, mensaje, nil)
                                    }
                                }
                            case .failure(let error):
                                DDLogError(error.localizedDescription)
                                completionHandler(false, mensaje, error)
                            }
        })
    }
    
    ////registrarconsumo.php?cliente=1&empresa=1&monto=100&ticket=123ERV
    func registrarConsumo(idEmpresa: String, monto: String, ticket: String, completionHandler: @escaping (Bool, String, QR ,Error?) -> ()){
        
        let queryString = Constantes.URLS.baseServicios + Constantes.URLS.registrarConsumo
         var qrCode = QR()
        let params = [
            "cliente" : self.getUserId(),
            "empresa" : idEmpresa,
            "monto" : monto,
            "ticket" : ticket
        ]
       
        
        var mensaje = "Ha ocurrido un error al enviar la información por favor revise su conexion"
        
        self.manager.request(queryString, method: .post, parameters: params).responseJSON{ response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                
                DDLogVerbose("JSON: \(json)")

                if let value = json["desc"].string {
                    mensaje = value
                }
                if json["result"].string == "SUCCESS" {
                   qrCode = self.jsonToQR(elJson: json)
                    completionHandler(true, mensaje, qrCode, nil)
                } else {
                    completionHandler(false, mensaje, qrCode, nil)
                }
                
            case .failure(let error):
                print(error)
                completionHandler(false, mensaje, qrCode, error)
            }
        }
    }
    
    func obtenerValoracionDeProfesional(idProfesional: String, completionHandler: @escaping(Bool, [Opinion]?, String, Error?)->()) {
        DDLogInfo("Ingresando...")
        
        let queryString = Constantes.URLS.baseServicios + Constantes.URLS.valoracionDeProfesional
        var mensaje = ""
        
        let params = [
            "profesional" : idProfesional,
            //FIXME: El indice debe ser dinamico.
            "index" : "0"
        ]
        
        Alamofire.request(queryString, method: .post, parameters: params).responseJSON{ response in
            switch response.result {
            case .success(let value):
                DDLogDebug("SUCCESS!")
                
                let json = JSON(value)
                DDLogVerbose("JSON: \(json)")
                
                if let value = json["desc"].string {
                    mensaje = value
                }
                if json["result"].string == "SUCCESS" {
                    
                    //Nodo de datos.
                    let data = json["data"]
                    
                    let arrayOpiniones = self.jsonToArrayOpiniones(json: data)
                    completionHandler(true, arrayOpiniones, mensaje, nil)
                } else {
                    completionHandler(false, nil, mensaje, nil)
                }
                
            case .failure(let error):
                DDLogError(error.localizedDescription)
                completionHandler(false, nil, mensaje, error)
            }
        }
    }
    
    func recuperarContrasenia(email:String, completionHandler: @escaping(Bool, String, Error?)->()) {
        DDLogInfo("Ingresando...")
        
        let queryString = Constantes.URLS.baseServicios + Constantes.URLS.recuperarContrasenia
        var mensaje = ""
        
        let params = ["email": email]
        DDLogInfo("PARAMS: \(params)")
        
        self.manager.request(queryString, method: .get, parameters: params).responseJSON { response in
            switch response.result {
            case .success(let value):
                DDLogDebug("SUCCESS!")
                
                let json = JSON(value)
                DDLogVerbose("JSON: \(json)")
                
                if let value = json["desc"].string {
                    mensaje = value
                }
                if json["result"].string == "SUCCESS" {                    
                    completionHandler(true, mensaje, nil)
                } else {
                    completionHandler(false, mensaje, nil)
                }
                
            case .failure(let error):
                DDLogError(error.localizedDescription)
                completionHandler(false, mensaje, error)
            }
        }
    }
    
    func cambiarContrasenia(contrasenia: String, contraseniaNueva: String, completionHandler: @escaping(Bool, String, Error?)->()) {
        DDLogInfo("Ingresando...")
        
        let queryString = Constantes.URLS.baseServicios + Constantes.URLS.cambiarContrasenia
        var mensaje = ""
        
        let params = [
            "usuario" : Functions().getUserId(),
            "password" : contrasenia,
            "nuevo" : contraseniaNueva
        ]
        
        Alamofire.request (queryString, method: .post, parameters: params).responseJSON { response in
            switch response.result {
            case .success(let value):
                DDLogDebug("SUCCESS!")
                
                let json = JSON(value)
                DDLogVerbose("JSON: \(json)")
                
                if let value = json["desc"].string {
                    mensaje = value
                }
                if json["result"].string == "SUCCESS" {
                    completionHandler(true, mensaje, nil)
                } else {
                    completionHandler(false, mensaje, nil)
                }
                
            case .failure(let error):
                DDLogError(error.localizedDescription)
                completionHandler(false, mensaje, error)
            }
        }
    }
    
    func listadoCashbacks(completionHandler: @escaping(Bool, String, [Cashback]?, Error?)->()) {
        DDLogInfo("Ingresando...")
        
        let queryString = Constantes.URLS.baseServicios + Constantes.URLS.listadoCashback
        var mensaje = ""
        
        let params = ["cliente": Functions().getUserId()]
        DDLogInfo("PARAMS: \(params)")
        
        Alamofire.request (queryString, method: .get, parameters: params).responseJSON { response in
            switch response.result {
            case .success(let value):
                DDLogDebug("SUCCESS!")
                
                let json = JSON(value)
                DDLogVerbose("JSON: \(json)")
                
                if let value = json["desc"].string {
                    mensaje = value
                }
                if json["result"].string == "SUCCESS" {
                    //Nodo de datos.
                    let data = json["data"]
                    //Extrae jsonToCashback
                    let arrayCashback = self.jsonToArrayCashbacks(json: data)
                    
                    DDLogVerbose("ArrayCashback: \(String(describing: arrayCashback?.count))")
                    
                    completionHandler(true, mensaje, arrayCashback, nil)
                } else {
                    completionHandler(false, mensaje, nil, nil)
                }
                
            case .failure(let error):
                DDLogError(error.localizedDescription)
                completionHandler(false, mensaje, nil, error)
            }
        }
    }
    
    
    //MARK: - JSON TO OBJETOS
    func jsonToQR(elJson:JSON)->QR{
    
        
        
        var nodoRaiz : JSON!
        
        nodoRaiz = elJson;
         let QRCode = QR()
        QRCode.id = ""
        QRCode.idTicket = ""
        QRCode.idRegistro = ""
        QRCode.nombreEmpresa = ""
        QRCode.nombreCliente = ""
        QRCode.Monto = ""
        QRCode.fechaHora = ""
        
        
        if nodoRaiz.exists() {
            
             if let value = elJson["data"]["monto"].string {
                    QRCode.Monto = value
                }
                if let value = elJson["data"]["id"].string {
                    QRCode.idRegistro = value
                }
                if let value = elJson["data"]["ticket"].string {
                    QRCode.idTicket = value
                }
                if let value = elJson["data"]["fecha"].string {
                    QRCode.fechaHora = value
                }
                if let value = elJson["data"]["empresa"]["razonSocial"].string {
                    QRCode.nombreEmpresa = value
                }
                if let value = elJson["data"]["cliente"]["email"].string {
                QRCode.correoCliente = value
                }
                if let value = elJson["data"]["cliente"]["nombre"].string {
                QRCode.nombreCliente = value
                }
         }
     
    return QRCode
    }
    
    
    func jsonToEmpresasArray(elJson: JSON, paraQuien: String = "main")->[Empresa]{
        var tempArray = [Empresa]()
        
        var nodoRaiz : JSON!
        
        if paraQuien == "main" {
            nodoRaiz = elJson["data"]["empresas"]
        } else { //perfil
            nodoRaiz = elJson
        }
        
        if nodoRaiz.exists() {
            for item in nodoRaiz.array! {
                let unNegocio = Empresa()
                
                unNegocio.id = ""
                unNegocio.imagen = URL(string:"")
                unNegocio.cash_back = ""
                unNegocio.distancia = ""
                unNegocio.razonSocial = ""
                unNegocio.tin_rfc = ""
                unNegocio.descripcion = ""
                unNegocio.calificacion = 0
                unNegocio.categoria = Categoria()
                unNegocio.esPrincipal = false
                unNegocio.direcciones = [Direccion]()
                unNegocio.jsonData = ""
                
                if let value = item["id"].string {
                    unNegocio.id = value
                }
                
                if let value = item["imagen"].string {
                    unNegocio.imagen = URL(string:value)
                }
                
                if item["direcciones"].exists() {
                    unNegocio.direcciones = self.jsonToArrayDireccion(elJson: item["direcciones"])
                }
                
                if let value = item["cashback"].string {
                    unNegocio.cash_back = value
                }
                
                if let value = item["distancia"].string {
                    unNegocio.distancia = value
                }
                
                if let value = item["razonSocial"].string {
                    unNegocio.razonSocial = value
                }
                
                if let value = item["tin_rfc"].string {
                    unNegocio.tin_rfc = value
                }
                
                if let value = item["descripcion"].string {
                    unNegocio.descripcion = value
                }
                
                if let value = item["calificacion"].string {
                    unNegocio.calificacion = Float(value)
                }
                
                unNegocio.categoria.id = ""
                unNegocio.categoria.nombre = ""
                unNegocio.categoria.imagen = URL(string:"")
                if item["categoria"].exists() {
                    if let value = item["categoria"]["id"].string {
                        unNegocio.categoria.id = value
                    }
                    
                    if let value = item["categoria"]["nombre"].string {
                        unNegocio.categoria.nombre = value
                    }
                    
                    if let value = item["categoria"]["imagen"].string {
                        unNegocio.categoria.imagen = URL(string:value)
                    }
                }
                
                if let value = item["principal"].string {
                    if value == "1" {
                        unNegocio.esPrincipal = true
                    }
                }
                
                unNegocio.jsonData = item.rawString()
                
                tempArray.append(unNegocio)
            }
        }
        
        return tempArray
    }
    
    func jsonToCategoriaArray(elJson: JSON)->[Categoria]{
        var tempArray = [Categoria]()
        
        if elJson["data"]["categoriasServicio"].exists() {
            for item in elJson["data"]["categoriasServicio"].array! {
                let unaCategoria = Categoria()
                
                unaCategoria.id = ""
                unaCategoria.nombre = ""
                unaCategoria.imagen = URL(string:"")
                
                if let value = item["id"].string {
                    unaCategoria.id = value
                }
                
                if let value = item["nombre"].string {
                    unaCategoria.nombre = value
                }
                
                if let value = item["imagen"].string {
                    unaCategoria.imagen = URL(string:value)
                }
                
                tempArray.append(unaCategoria)
            }
        }
        
        return tempArray
    }
    
    func jsonToProfesionalArray(elJson: JSON, paraQuien: String)->[Profesional]{
        var tempArray = [Profesional]()
        var nodoArrayRaiz : JSON!
        var valorSeteado = false
        if paraQuien == "main" {
            if elJson["data"]["profesionales"].exists() {
                nodoArrayRaiz = elJson["data"]["profesionales"]
                valorSeteado = true
            }
        }
        else if paraQuien == "categoria" {
            if elJson["data"].exists() {
                nodoArrayRaiz = elJson["data"]
                valorSeteado = true
            }
        } else if paraQuien == "perfil" {
            nodoArrayRaiz = elJson
            valorSeteado = true
        }
        
        if valorSeteado {
            for item in nodoArrayRaiz.array! {
                let unProfesional = Profesional()
                
                unProfesional.id = ""
                unProfesional.nombre = ""
                unProfesional.apellidos = ""
                unProfesional.telefono = ""
                unProfesional.email = ""
                unProfesional.tinRfc = ""
                unProfesional.tipoLogin = tipoLoginUsuario()
                unProfesional.tipo = tipoUsuario()
                unProfesional.estatus = estatusUsuario()
                unProfesional.wantCashback = false
                unProfesional.amountCashback = 0
                unProfesional.avatar = URL(string:"")
                unProfesional.imagen = URL(string: "")
                unProfesional.distancia = ""
                unProfesional.razonSocial = ""
                unProfesional.descripcion = ""
                unProfesional.calificacion = ""
                unProfesional.esPrincipal = false
                unProfesional.categoria = Categoria()
                unProfesional.cuentaPaypal = ""
                unProfesional.direcciones = [Direccion]()
                unProfesional.jsonData = ""
                
                if let value = item["id"].string {
                    unProfesional.id = value
                }
                
                if let value = item["nombre"].string {
                    unProfesional.nombre = value
                }
                
                if let value = item["apellidos"].string {
                    unProfesional.apellidos = value
                }
                
                if let value = item["telefono"].string {
                    unProfesional.telefono = value
                }
                
                if let value = item["email"].string {
                    unProfesional.email = value
                }
                
                if let value = item["tinRfc"].string {
                    unProfesional.tinRfc = value
                }
                
                unProfesional.tipoLogin.id = ""
                unProfesional.tipoLogin.descripcion = ""
                if item["login"].exists() {
                    if let value = item["login"]["id"].string {
                        unProfesional.tipoLogin.id = value
                    }
                    if let value = item["login"]["descr"].string {
                        unProfesional.tipoLogin.descripcion = value
                    }
                }
                
                unProfesional.tipo.id = ""
                unProfesional.tipo.descripcion = ""
                if item["tipo"].exists() {
                    if let value = item["tipo"]["id"].string {
                        unProfesional.tipo.id = value
                    }
                    if let value = item["tipo"]["descr"].string {
                        unProfesional.tipo.descripcion = value
                    }
                }
                
                unProfesional.estatus.id = ""
                unProfesional.estatus.descripcion = ""
                if item["estatus"].exists() {
                    if let value = item["estatus"]["id"].string {
                        unProfesional.estatus.id = value
                    }
                    if let value = item["estatus"]["descr"].string {
                        unProfesional.estatus.descripcion = value
                    }
                }
                
                if let value = item["cashback"].string {
                    if value == "1" {
                        unProfesional.wantCashback = true
                    }
                }
                
                //monto cashback
                
                if let value = item["avatar"].string {
                    unProfesional.avatar = URL(string:value)
                }
                
                if let value = item["imagen"].string {
                    unProfesional.imagen = URL(string:value)
                }
                
                if item["direcciones"].exists() {
                    unProfesional.direcciones = self.jsonToArrayDireccion(elJson: item["direcciones"])
                }
                
                if let value = item["distancia"].string {
                    unProfesional.distancia = value
                }
                
                if let value = item["razonSocial"].string {
                    unProfesional.razonSocial = value
                }
                
                if let value = item["descripcion"].string {
                    unProfesional.descripcion = value
                }
                
                if let value = item["calificacion"].string {
                    unProfesional.calificacion = value
                }
                
                if let value = item["principal"].string {
                    if value == "1" {
                        unProfesional.esPrincipal = true
                    }
                }
                
                unProfesional.categoria.id = ""
                unProfesional.categoria.nombre = ""
                unProfesional.categoria.imagen = URL(string:"")
                if item["categoria"].exists() {
                    if let value = item["categoria"]["id"].string {
                        unProfesional.categoria.id = value
                    }
                    if let value = item["categoria"]["nombre"].string {
                        unProfesional.categoria.nombre = value
                    }
                    if let value = item["categoria"]["imagen"].string {
                        unProfesional.categoria.imagen = URL(string:value)
                    }
                }
                
                unProfesional.jsonData = item.rawString()
                
                tempArray.append(unProfesional)
            }
        } //hasta aqui para main
        
        return tempArray
    }
    
    func jsonToProfesional(elJson: JSON)->Profesional{
        let unProfesional = Profesional()
        
        //Propiedades comunes de Usuario
        var unUsuario = Usuario()
        unUsuario = jsonToUsuario(elJson: elJson)
        unProfesional.deviceId = unUsuario.deviceId
        unProfesional.id = unUsuario.id
        unProfesional.nombre = unUsuario.nombre
        unProfesional.apellidos = unUsuario.apellidos
        unProfesional.telefono = unUsuario.telefono
        unProfesional.email = unUsuario.email
        unProfesional.tinRfc = unUsuario.tinRfc
        unProfesional.afiliador = unUsuario.afiliador
        unProfesional.tipoLogin = unUsuario.tipoLogin
        unProfesional.tipo = unUsuario.tipo
        unProfesional.estatus = unUsuario.estatus
        unProfesional.wantCashback = unUsuario.wantCashback
        unProfesional.amountCashback = unUsuario.amountCashback
        unProfesional.avatar = unUsuario.avatar
        unProfesional.cuentaPaypal = unUsuario.cuentaPaypal
        unProfesional.direcciones = unUsuario.direcciones
        unProfesional.afiliacion = unUsuario.afiliacion
        unProfesional.pais = unUsuario.pais
        unProfesional.deviceId = unUsuario.deviceId
        
        //Propiedades del profesional
        unProfesional.imagen = URL(string: "")
        unProfesional.distancia = ""
        unProfesional.razonSocial = ""
        unProfesional.descripcion = ""
        unProfesional.calificacion = ""
        unProfesional.esPrincipal = false
        unProfesional.categoria = Categoria()
        unProfesional.jsonData = ""
        
        ///>
        
        if let value = elJson["imagen"].string {
            unProfesional.imagen = URL(string:value)
        }
        
        if let value = elJson["distancia"].string {
            unProfesional.distancia = value
        }
        
        if let value = elJson["razonSocial"].string {
            unProfesional.razonSocial = value
        }
        
        if let value = elJson["descripcion"].string {
            unProfesional.descripcion = value
        }
        
        if let value = elJson["calificacion"].string {
            unProfesional.calificacion = value
        }
        
        if let value = elJson["principal"].string {
            if value == "1" {
                unProfesional.esPrincipal = true
            }
        }
        
        unProfesional.categoria.id = ""
        unProfesional.categoria.nombre = ""
        unProfesional.categoria.imagen = URL(string:"")
        if elJson["categoria"].exists() {
            if let value = elJson["categoria"]["id"].string {
                unProfesional.categoria.id = value
            }
            if let value = elJson["categoria"]["nombre"].string {
                unProfesional.categoria.nombre = value
            }
            if let value = elJson["categoria"]["imagen"].string {
                unProfesional.categoria.imagen = URL(string:value)
            }
        }
        
        unProfesional.jsonData = elJson.rawString()
        
        return unProfesional
    }
    
    func jsonToUsuario(elJson: JSON, paraPerfil: Bool = false)->Usuario{
        let unUsuario = Usuario()
        
        var jsonRoot = elJson
    
        if paraPerfil {
            jsonRoot = elJson["data"]["usuario"]
        }
        //Intenta iniciar con el nodo "data"
        else if (elJson["data"].exists()) {
            jsonRoot = elJson["data"]
        }
        
        unUsuario.id = ""
        if let value = jsonRoot["id"].string {
            unUsuario.id = value
        }
        
        if unUsuario.id == "" {
            if let value = jsonRoot["id"].int {
                unUsuario.id = String(value)
            }
        }
        
        unUsuario.email = ""
        if let value = jsonRoot["email"].string {
            unUsuario.email = value
        }
        
        unUsuario.nombre = ""
        if let value = jsonRoot["nombre"].string {
            unUsuario.nombre = value
        }
        
        unUsuario.apellidos = ""
        if let value = jsonRoot["apellidos"].string {
            unUsuario.apellidos = value
        }
        
        unUsuario.telefono = ""
        if let value = jsonRoot["telefono"].string {
            unUsuario.telefono = value
        }
        
        unUsuario.cuentaPaypal = ""
        if let value = jsonRoot["cuentaPaypal"].string {
            unUsuario.cuentaPaypal = value
        }
        
        unUsuario.avatar = URL(string: "") //no debe ser hardcoded
        if let value = jsonRoot["avatar"].string {
            if value != "" {
                unUsuario.avatar = URL(string: value)
            } else {
                unUsuario.avatar = self.getUserAvatar()
            }
            
        }
        
        unUsuario.tinRfc = ""
        if let value = jsonRoot["tinRfc"].string {
            unUsuario.tinRfc = value
        }
        
        unUsuario.wantCashback = false
        unUsuario.amountCashback = 0
        if let value = jsonRoot["cashback"].string {
            if value == "1" {
                unUsuario.wantCashback = true
            }
        }
        
        unUsuario.tipoLogin = tipoLoginUsuario()
        unUsuario.tipoLogin.id = "0"
        unUsuario.tipoLogin.descripcion = ""
        if jsonRoot["login"].exists() {
            unUsuario.tipoLogin.id = jsonRoot["login"]["id"].string
            unUsuario.tipoLogin.descripcion = jsonRoot["login"]["descr"].string
        }
        
        unUsuario.tipo = tipoUsuario()
        unUsuario.tipo.id = "0"
        unUsuario.tipo.descripcion = ""
        if jsonRoot["tipo"].exists() {
            unUsuario.tipo.id = jsonRoot["tipo"]["id"].string
            unUsuario.tipo.descripcion = jsonRoot["tipo"]["descr"].string
        }
        
        unUsuario.estatus = estatusUsuario()
        unUsuario.estatus.id = "0"
        unUsuario.estatus.descripcion = ""
        if jsonRoot["estatus"].exists() {
            unUsuario.estatus.id = jsonRoot["estatus"]["id"].string
            unUsuario.estatus.descripcion = jsonRoot["estatus"]["descr"].string
        }
        
        unUsuario.direcciones = [Direccion]()
        if jsonRoot["direcciones"].exists() {
            if let _ = jsonRoot["direcciones"].array {
                unUsuario.direcciones = self.jsonToArrayDireccion(elJson: jsonRoot["direcciones"])
            }
        }
        
        //datos afiliacion
        unUsuario.afiliacion = Afiliacion()
        unUsuario.afiliacion.id = ""
        unUsuario.afiliacion.intentos = 0
        unUsuario.afiliacion.statusEnum = EstatusAfiliacion.noAfiliado
        unUsuario.afiliacion.estatus = EstructuraBase()
        unUsuario.afiliacion.estatus.id = ""
        unUsuario.afiliacion.estatus.descripcion = ""
        
        if jsonRoot["afiliacion"].exists() {
            if let value = jsonRoot["afiliacion"]["id"].string {
                unUsuario.afiliacion.id = value
            }
            
            if let value = jsonRoot["afiliacion"]["intentos"].string {
                if value != "" {
                    unUsuario.afiliacion.intentos = Int(value)
                }
                
            }
            
            if let value = jsonRoot["afiliacion"]["estatus"]["id"].string {
                if value != "" {
                    unUsuario.afiliacion.estatus.id = value
                    if let value2 = EstatusAfiliacion(rawValue: value){
                        unUsuario.afiliacion.statusEnum = value2
                    }
                    
                }
                
            }
            
            if let value = jsonRoot["afiliacion"]["estatus"]["descr"].string {
                if value != "" {
                    unUsuario.afiliacion.estatus.descripcion = value
                }
            }
        }
        
        //Pais
        if jsonRoot["pais"].exists() {
            unUsuario.pais = Pais()
            
            if let value = jsonRoot["pais"]["id"].string {
                unUsuario.pais.id = value
            }
            
            if let value = jsonRoot["pais"]["nombre"].string {
                unUsuario.pais.descripcion = value
            }
            
            if let value = jsonRoot["pais"]["abrev"].string {
                unUsuario.pais.abreviacion = value
            }
            DDLogDebug("PAIS: \(unUsuario.pais)")
        }
        
        //DeviceID
        unUsuario.deviceId = ""
        if let value = elJson["deviceId"].string {
            unUsuario.deviceId = value
        }
        
        //Divisa
        if jsonRoot["divisa"].exists() {
            let divisa = Divisa()
            divisa.pais = Pais()
            
            if let value = jsonRoot["divisa"]["pais"]["id"].string {
                divisa.pais.id = value
            }
            
            if let value = jsonRoot["divisa"]["pais"]["descr"].string {
                divisa.pais.descripcion = value
            }
            
            if let value = jsonRoot["divisa"]["id"].string {
                divisa.id = value
            }
            
            if let value = jsonRoot["divisa"]["moneda"].string {
                divisa.moneda = value
            }
            
            if let value = jsonRoot["divisa"]["abrev"].string {
                divisa.abreviacion = value
            }
            unUsuario.divisa = divisa
        }
        
        //Codigo Afiliador
        if let codigoAfiliador = jsonRoot["codigoAfiliador"].string {
            unUsuario.codigoAfiliador = codigoAfiliador
        }
        
        //afiliador
        
        /*
         var afiliador : Usuario?
         */
        return unUsuario
    }
    
    func jsonToArrayDireccion(elJson : JSON)->[Direccion]{
        var arrayDirecciones = [Direccion]()
        
        for item in elJson.array! {
            let unaDireccion = Direccion()
            
            unaDireccion.id = ""
            unaDireccion.descripcion = ""
            unaDireccion.alias = ""
            unaDireccion.latitud = ""
            unaDireccion.longitud = ""
            
            if let value = item["id"].string {
                unaDireccion.id = value
            }
            
            if let value = item["descr"].string {
                unaDireccion.descripcion = value
            }
            
            if let value = item["alias"].string {
                unaDireccion.alias = value
            }
            
            if let value = item["lat"].string {
                unaDireccion.latitud = value
            }
            
            if let value = item["lng"].string {
                unaDireccion.longitud = value
            }
            
            if let value = item["predeterminado"].string {
                if value == "1" {
                    unaDireccion.esPredeterminado = true
                }
            }
            
            arrayDirecciones.append(unaDireccion)
            
        }
        
        return arrayDirecciones
    }
    
    func jsonToArrayMensajes(json : JSON)->[JSQMessage] {
        var tempArray = [JSQMessage]()
        
        for item in json.array! {
            var senderID = ""
            var senderName = ""
            var fecha = Date()
            var mensaje = ""
            
            if let value = item["usuario"]["id"].string {
                senderID = value
            }
            
            if let value = item["usuario"]["nombre"].string {
                senderName = value
            }
            
            if let value = item["msj"].string {
                mensaje = value
            }
            
            if let value = item["fecha"].string {
                fecha = self.stringToDate(elString: value)
            }
            
            let unMensaje = JSQMessage(senderId: senderID, senderDisplayName: senderName, date: fecha, text: mensaje)
            tempArray.append(unMensaje!)
        }
        
        return tempArray
    }
    
    func jsonToArrayServicio(elJson : JSON)->[Servicio]{
        //json debe llegar al nodo correspondiente -> Activo/historico
        
        var arrayServicios = [Servicio]()
        
        if elJson.array != nil {
            for item in elJson.array! {
                let unServicio = Servicio()
                unServicio.id = ""
                unServicio.usuario = Usuario()
                unServicio.tituloSolicitud = ""
                unServicio.descripcion = ""
                unServicio.categoria = Categoria()
                unServicio.estatus = EstatusServicio()
                unServicio.presupuesto = ""
                //divisa
                unServicio.fechaServicio = Date()
                unServicio.direccion = ""
                unServicio.latitude = ""
                unServicio.longitute = ""
                unServicio.fechaSolicitud = Date()
                unServicio.propuestas = [Propuesta]()
                
                if let value = item["id"].string {
                    unServicio.id = value
                }
                
                //Usuario.
                let usuario = self.jsonToUsuario(elJson: item["usuario"])
                unServicio.usuario = usuario
                
                if let value = item["titulo"].string {
                    unServicio.tituloSolicitud = value
                }
                
                if let value = item["descr"].string {
                    unServicio.descripcion = value
                }
                
                unServicio.categoria.id = ""
                unServicio.categoria.nombre = ""
                unServicio.categoria.imagen = URL(string: "")
                if let value = item["categoria"]["id"].string {
                    unServicio.categoria.id = value
                }
                if let value = item["categoria"]["nombre"].string {
                    unServicio.categoria.nombre = value
                }
                if let value = item["categoria"]["imagen"].string {
                    unServicio.categoria.imagen = URL(string:value)
                }
                
                unServicio.estatus.id = ""
                unServicio.estatus.descripcion = ""
                if let value = item["estatus"]["id"].string {
                    unServicio.estatus.id = value
                }
                if let value = item["estatus"]["descr"].string {
                    unServicio.estatus.descripcion = value
                }
                
                if let value = item["presupuesto"].string {
                    unServicio.presupuesto = value
                }
                
                //Pais
                let pais = Pais()
                let paisDict = item["divisa"]["pais"].dictionaryValue
                if let id = paisDict["id"]?.string {
                    pais.id = id
                }
                
                if let descr = paisDict["descr"]?.string {
                    pais.descripcion = descr
                }
                
                //Divisa
                let divisa = Divisa(id: item["divisa"]["id"].string!,
                                    pais: pais,
                                    moneda: item["divisa"]["moneda"].string!,
                                    abreviacion: item["divisa"]["abrev"].string!)
                
                unServicio.divisa = divisa
                
                if let value = item["fechaServicio"].string {
                    unServicio.fechaServicio = self.stringToDate(formato: "yyyy-MM-dd HH:mm:ss", elString: value)
                }
                
                if let value = item["direccion"].string {
                    unServicio.direccion = value
                }
                
                if let value = item["lat"].string {
                    unServicio.latitude = value
                }
                
                if let value = item["lng"].string {
                    unServicio.longitute = value
                }
                
                if let value = item["fechaSolicitud"].string {
                    unServicio.fechaSolicitud = self.stringToDate(formato: "yyyy-MM-dd HH:mm:ss", elString: value)
                }
                
                //Nodo propuestas
                if item["propuestas"].exists() {
                    unServicio.propuestas = self.jsonToArrayPropuesta(elJson: item["propuestas"])
                }
                
                //Bandera para saber si el Profesional fue calificado por el cliente.
                if item["profesionalCalificado"].exists() {
                    unServicio.profesionalCalificado = item["profesionalCalificado"].boolValue
                }

                arrayServicios.append(unServicio)
            }
        }
        
        return arrayServicios
        
    }
    
    func jsonToServicio(elJson : JSON)->Servicio {
        //json debe llegar al nodo correspondiente -> Activo/historico
        
        
        let unServicio = Servicio()
        unServicio.id = ""
        unServicio.usuario = Usuario()
        unServicio.descripcion = ""
        unServicio.categoria = Categoria()
        unServicio.estatus = EstatusServicio()
        unServicio.presupuesto = ""
        //divisa
        unServicio.fechaServicio = Date()
        unServicio.direccion = ""
        unServicio.latitude = ""
        unServicio.longitute = ""
        unServicio.fechaSolicitud = Date()
        unServicio.propuestas = [Propuesta]()
        unServicio.profesionalCalificado = false
        
        if let value = elJson["id"].string {
            unServicio.id = value
        }
        
        //Usuario.
        let usuario = self.jsonToUsuario(elJson: elJson["usuario"])
        unServicio.usuario = usuario
        
        if let value = elJson["descr"].string {
            unServicio.descripcion = value
        }
        
        unServicio.categoria.id = ""
        unServicio.categoria.nombre = ""
        unServicio.categoria.imagen = URL(string: "")
        if let value = elJson["categoria"]["id"].string {
            unServicio.categoria.id = value
        }
        if let value = elJson["categoria"]["nombre"].string {
            unServicio.categoria.nombre = value
        }
        if let value = elJson["categoria"]["imagen"].string {
            unServicio.categoria.imagen = URL(string:value)
        }
        
        unServicio.estatus.id = ""
        unServicio.estatus.descripcion = ""
        if let value = elJson["estatus"]["id"].string {
            unServicio.estatus.id = value
        }
        if let value = elJson["estatus"]["descr"].string {
            unServicio.estatus.descripcion = value
        }
        
        if let value = elJson["presupuesto"].string {
            unServicio.presupuesto = value
        }
        
        //Pais
        let pais = Pais()
        let paisDict = elJson["divisa"]["pais"].dictionaryValue
        if let id = paisDict["id"]?.string {
            pais.id = id
        }
        
        if let descr = paisDict["descr"]?.string {
            pais.descripcion = descr
        }
        
        //Divisa
        let divisa = Divisa(id: elJson["divisa"]["id"].string!,
                            pais: pais,
                            moneda: elJson["divisa"]["moneda"].string!,
                            abreviacion: elJson["divisa"]["abrev"].string!)
        
        unServicio.divisa = divisa
        
        
        if let value = elJson["fechaServicio"].string {
            unServicio.fechaServicio = self.stringToDate(formato: "yyyy-MM-dd HH:mm:ss", elString: value)
        }
        
        if let value = elJson["direccion"].string {
            unServicio.direccion = value
        }
        
        if let value = elJson["lat"].string {
            unServicio.latitude = value
        }
        
        if let value = elJson["lng"].string {
            unServicio.longitute = value
        }
        
        if let value = elJson["fechaSolicitud"].string {
            unServicio.fechaSolicitud = self.stringToDate(formato: "yyyy-MM-dd HH:mm:ss", elString: value)
        }
        
        if elJson["propuestas"].exists() {
            unServicio.propuestas = self.jsonToArrayPropuesta(elJson: elJson["propuestas"])
        }
        
        //Bandera para saber si el Profesional fue calificado por el cliente.
        if elJson["profesionalCalificado"].exists() {
            unServicio.profesionalCalificado = elJson["profesionalCalificado"].boolValue
        }
        
        return unServicio
        
    }
    
    func jsonToArrayPropuesta(elJson : JSON)->[Propuesta]{
        
        var arrayPropuesta = [Propuesta]()
        
        if elJson.array != nil {
            for item in elJson.array! {
                let unaPropuesta = Propuesta()
                unaPropuesta.id = ""
                unaPropuesta.idServicio = ""
                unaPropuesta.datosProfesional = Profesional()
                unaPropuesta.descripcion = ""
                unaPropuesta.presupuesto = ""
                unaPropuesta.estatus = EstatusPropuesta()
                
                if let value = item["id"].string {
                    unaPropuesta.id = value
                }
                
                if let value = item["servicio"]["id"].string {
                    unaPropuesta.idServicio = value
                }
                
                //Nodo Profesional
                if item["profesional"].exists() {
                    unaPropuesta.datosProfesional = self.jsonToProfesional(elJson: item["profesional"])
                }
                
                if let value = item["descr"].string {
                    unaPropuesta.descripcion = value
                }
                
                if let value = item["presupuesto"].string {
                    unaPropuesta.presupuesto = value
                }
                
                unaPropuesta.estatus.id = ""
                unaPropuesta.estatus.descripcion = ""
                if let value = item["estatus"]["id"].string {
                    unaPropuesta.estatus.id = value
                }
                if let value = item["estatus"]["descr"].string {
                    unaPropuesta.estatus.descripcion = value
                }
                
                //Pais
                let pais = Pais()
                let paisDict = item["divisa"]["pais"].dictionaryValue
                if let id = paisDict["id"]?.string {
                    pais.id = id
                }
                
                if let descr = paisDict["descr"]?.string {
                    pais.descripcion = descr
                }
                
                //Divisa
                let divisa = Divisa(id: item["divisa"]["id"].string!,
                                    pais: pais,
                                    moneda: item["divisa"]["moneda"].string!,
                                    abreviacion: item["divisa"]["abrev"].string!)
                
                unaPropuesta.divisa = divisa

                
                arrayPropuesta.append(unaPropuesta)
            }
        }
        
        return arrayPropuesta
    }
    
    func jsonToEmpresa(elJson: JSON)->Empresa{
        
        let unNegocio = Empresa()
        
        unNegocio.id = ""
        unNegocio.imagen = URL(string:"")
        unNegocio.cash_back = ""
        unNegocio.distancia = ""
        unNegocio.razonSocial = ""
        unNegocio.tin_rfc = ""
        unNegocio.descripcion = ""
        unNegocio.calificacion = 0
        unNegocio.categoria = Categoria()
        unNegocio.esPrincipal = false
        unNegocio.direcciones = [Direccion]()
        unNegocio.jsonData = ""
        
        if let value = elJson["id"].string {
            unNegocio.id = value
        }
        
        if let value = elJson["imagen"].string {
            unNegocio.imagen = URL(string:value)
        }
        
        if elJson["direcciones"].exists() {
            unNegocio.direcciones = self.jsonToArrayDireccion(elJson: elJson["direcciones"])
        }
        
        if let value = elJson["cashback"].string {
            unNegocio.cash_back = value
        }
        
        if let value = elJson["distancia"].string {
            unNegocio.distancia = value
        }
        
        if let value = elJson["razonSocial"].string {
            unNegocio.razonSocial = value
        }
        
        if let value = elJson["tin_rfc"].string {
            unNegocio.tin_rfc = value
        }
        
        if let value = elJson["descripcion"].string {
            unNegocio.descripcion = value
        }
        
        if let value = elJson["calificacion"].string {
            unNegocio.calificacion = Float(value)
        }
        
        unNegocio.categoria.id = ""
        unNegocio.categoria.nombre = ""
        unNegocio.categoria.imagen = URL(string:"")
        if elJson["categoria"].exists() {
            if let value = elJson["categoria"]["id"].string {
                unNegocio.categoria.id = value
            }
            
            if let value = elJson["categoria"]["nombre"].string {
                unNegocio.categoria.nombre = value
            }
            
            if let value = elJson["categoria"]["imagen"].string {
                unNegocio.categoria.imagen = URL(string:value)
            }
        }
        
        if let value = elJson["principal"].string {
            if value == "1" {
                unNegocio.esPrincipal = true
            }
        }
        
        unNegocio.jsonData = elJson.rawString()
        
        return unNegocio
    }
    
    func jsonToDatosCashBack(elJson: JSON)->DatosCashback {
        let datoscb = DatosCashback()
        datoscb.id = ""
        datoscb.idUsuario = ""
        datoscb.cashbackPendiente = ""
        datoscb.cashbackAfiliados = ""
        datoscb.cashbackCobrado = ""
        datoscb.fechaUltimoCobro = ""
        datoscb.afiliados = [Empresa_Profesional]()
        
        if let value = elJson["id"].string {
            datoscb.id = value
        }
        
        if let value = elJson["idUsuario"].string {
            datoscb.idUsuario = value
        }
        
        if let value = elJson["cashbackPendiente"].string {
            datoscb.cashbackPendiente = value
        }
        
        if let value = elJson["cashbackAfiliados"].string {
            datoscb.cashbackAfiliados = value
        }
        
        if let value = elJson["cashbackCobrado"].string {
            datoscb.cashbackCobrado = value
        }
        
        if let value = elJson["fechaUltimoCobro"].string {
            datoscb.fechaUltimoCobro = value
        }
        
        datoscb.afiliados = self.jsonToArrayAfiliados(elJson: elJson["afiliados"])
        
        for elJson in elJson["afiliadosProfesionales"].array! {
            datoscb.afiliados.append(self.jsonToEmpresa(elJson: elJson))
        }
        
        for elJson in elJson["afiliadosEmpresas"].array! {
            datoscb.afiliados.append(self.jsonToEmpresa(elJson: elJson))
        }
        
        return datoscb
        
    }
    
    func jsonToArrayAfiliados(elJson: JSON)->[Usuario]{
        var arrayAfiliados = [Usuario]()
        
        if elJson.array != nil {
            
            for item in elJson.array! {
                let unUsuario = Usuario()
                unUsuario.id = ""
                if let value = item["id"].string {
                    unUsuario.id = value
                }
                
                if unUsuario.id == "" {
                    if let value = item["id"].int {
                        unUsuario.id = String(value)
                    }
                }
                
                unUsuario.email = ""
                if let value = item["email"].string {
                    unUsuario.email = value
                }
                
                unUsuario.nombre = ""
                if let value = item["nombre"].string {
                    unUsuario.nombre = value
                }
                
                unUsuario.apellidos = ""
                if let value = item["apellidos"].string {
                    unUsuario.apellidos = value
                }
                
                unUsuario.telefono = ""
                if let value = item["telefono"].string {
                    unUsuario.telefono = value
                }
                
                unUsuario.avatar = URL(string: "") //no debe ser hardcoded
                if let value = item["avatar"].string {
                    if value != "" {
                        unUsuario.avatar = URL(string: value)
                    } else {
                        unUsuario.avatar = self.getUserAvatar()
                    }
                    
                }
                
                unUsuario.tinRfc = ""
                if let value = item["tinRfc"].string {
                    unUsuario.tinRfc = value
                }
                
                unUsuario.wantCashback = false
                unUsuario.amountCashback = 0
                if let value = item["cashback"].string {
                    if value == "1" {
                        unUsuario.wantCashback = true
                    }
                }
                
                unUsuario.tipoLogin = tipoLoginUsuario()
                unUsuario.tipoLogin.id = "0"
                unUsuario.tipoLogin.descripcion = ""
                if item["login"].exists() {
                    unUsuario.tipoLogin.id = item["login"]["id"].string
                    unUsuario.tipoLogin.descripcion = item["login"]["descr"].string
                }
                
                unUsuario.tipo = tipoUsuario()
                unUsuario.tipo.id = "0"
                unUsuario.tipo.descripcion = ""
                if item["tipo"].exists() {
                    unUsuario.tipo.id = item["tipo"]["id"].string
                    unUsuario.tipo.descripcion = item["tipo"]["descr"].string
                }
                
                unUsuario.estatus = estatusUsuario()
                unUsuario.estatus.id = "0"
                unUsuario.estatus.descripcion = ""
                if item["estatus"].exists() {
                    unUsuario.estatus.id = item["estatus"]["id"].string
                    unUsuario.estatus.descripcion = item["estatus"]["descr"].string
                }
                
                unUsuario.direcciones = [Direccion]()
                if item["direcciones"].exists() {
                    if let _ = item["direcciones"].array {
                        unUsuario.direcciones = self.jsonToArrayDireccion(elJson: item["direcciones"])
                    }
                }
                
                //datos afiliacion
                unUsuario.afiliacion = Afiliacion()
                unUsuario.afiliacion.id = ""
                unUsuario.afiliacion.intentos = 0
                unUsuario.afiliacion.statusEnum = EstatusAfiliacion.noAfiliado
                unUsuario.afiliacion.estatus = EstructuraBase()
                unUsuario.afiliacion.estatus.id = ""
                unUsuario.afiliacion.estatus.descripcion = ""
                
                if item["afiliacion"].exists() {
                    if let value = item["afiliacion"]["id"].string {
                        unUsuario.afiliacion.id = value
                    }
                    
                    if let value = item["afiliacion"]["intentos"].string {
                        if value != "" {
                            unUsuario.afiliacion.intentos = Int(value)
                        }
                        
                    }
                    
                    
                    
                    if let value = item["afiliacion"]["estatus"]["id"].string {
                        if value != "" {
                            unUsuario.afiliacion.estatus.id = value
                            if let value2 = EstatusAfiliacion(rawValue: value){
                                unUsuario.afiliacion.statusEnum = value2
                            }
                            
                        }
                        
                    }
                    
                    if let value = item["afiliacion"]["estatus"]["descr"].string {
                        if value != "" {
                            unUsuario.afiliacion.estatus.descripcion = value
                        }
                        
                    }
                }
                
                //afiliador
                
                /*
                 var afiliador : Usuario?
                 */
                arrayAfiliados.append(unUsuario)
            }
            
        } //fin if nil
        
        
        return arrayAfiliados
    }
    
    func jsonToArrayOpiniones(json: JSON) -> [Opinion]? {
        var arrayOpiniones = [Opinion]()
        
        if let xjson = json.array {
            
            for item in xjson {
                let opinion = Opinion()
                
                opinion.calificacion = ""
                opinion.nombre = ""
                opinion.opinion = ""
                opinion.urlImg = ""
                
                if let value = item["calificacion"].string {
                    opinion.calificacion = value
                }
                
                var nombreCompleto = ""
                if let value = item["usuarioCalifica"]["nombre"].string {
                    nombreCompleto = value
                }
                
                if let value = item["usuarioCalifica"]["apellidos"].string {
                    nombreCompleto.append(" \(value)")
                }
                
                opinion.nombre = nombreCompleto
                
                if let value = item["comentario"].string {
                    opinion.opinion = value
                }
                
                if let value = item["usuarioCalifica"]["avatar"].string {
                    opinion.urlImg = value
                }
                
                arrayOpiniones.append(opinion)
            }
        }
        return arrayOpiniones
    }
    
    func jsonToCashback(json: JSON) -> Cashback {
        var unCashback = Cashback()
        
        if let value = json["tipo"].string {
            unCashback.tipo = unCashback.tipoCashbackFromString(string: value)
        }
        
        if let value = json["id"].string {
            unCashback.id = value
        }
        
        if let value = json["usuario.id"].string {
            unCashback.usuarioId = value
        }
        
        //Deudor
        var deudor = Deudor()
        if let value = json["deudor.id"].string {
            deudor.id = value
        }
        
        if let value = json["deudor.nombre"].string {
            deudor.nombre = value
        }
        unCashback.deudor = deudor
        
        if let value = json["monto"].string {
            unCashback.monto = value
        }
        
        if let value = json["fecha"].string {
            unCashback.fecha = value
        }
        
        if let value = json["cashbackCliente"].string {
            unCashback.cashbackCliente = value
        }
        
        //Estatus cashback
        let estatusCashback = EstatusCashback()
        if let value = json["estatus.id"].string {
            estatusCashback.id = value
        }
        
        if let value = json["estatus.descr"].string {
            estatusCashback.descripcion = value
        }
        unCashback.estatus = estatusCashback
        
        //Divisa
        let divisa = Divisa()
        if let value = json["divisa.id"].string {
            divisa.id = value
        }
        
        if let value = json["divisa.abrev"].string {
            divisa.abreviacion = value
        }
        unCashback.divisa = divisa
        
        //Cargo
        if let value = json["cargo.estatus.id"].string {
            let estatusCargo = EstatusCargo()
            estatusCargo.id = value
            
            if let value = json["cargo.estatus.descr"].string {
                estatusCargo.descripcion = value
            }
            
            var cargo = Cargo()
            if let value = json["cargo.fecha"].string {
                cargo.fecha = value
            }
            
            if let value = json["cargo.pago.id"].string {
                cargo.pago = value
            }
            
            cargo.estatus = estatusCargo
            unCashback.cargo = cargo
        }
        
        return unCashback
    }
    
    func jsonToArrayCashbacks(json: JSON) -> [Cashback]? {
        var arrayCashback = [Cashback]()
        
        if let xjson = json.array {
            for item in xjson {
                let cashback = jsonToCashback(json: item)
                arrayCashback.append(cashback)
            }
        }
        return arrayCashback
    }
    
    //MARK: - GET/SET USER DATA
    func saveUserData(elUsuario: Usuario, borrarFavoritos : Bool = false){
        self.borraTodoCierraSesion(borrarFavoritos: borrarFavoritos)
        
        self.userDefaults.set(true, forKey: "userLogged")
        self.userDefaults.set(elUsuario.id, forKey: "idUsr")
        self.userDefaults.set(elUsuario.email, forKey: "emailUsr")
        self.userDefaults.set(elUsuario.nombre, forKey: "nombreUsr")
        self.userDefaults.set(elUsuario.apellidos, forKey: "apellidosUsr")
        self.userDefaults.set(elUsuario.telefono, forKey: "telUsr")
        self.userDefaults.set(elUsuario.avatar, forKey: "avatar")
        self.userDefaults.set(elUsuario.tipoLogin.id, forKey: "tipoLoginId")
        self.userDefaults.set(elUsuario.tipoLogin.descripcion, forKey: "tipoLoginDesc")
        self.userDefaults.set(elUsuario.tipo.id, forKey: "tipoUsuarioId")
        self.userDefaults.set(elUsuario.tipo.descripcion, forKey: "tipoUsuarioDesc")
        self.userDefaults.set(elUsuario.estatus.id, forKey: "estatusUsuarioId")
        self.userDefaults.set(elUsuario.estatus.descripcion, forKey: "estatusUsuarioDesc")
        self.userDefaults.set(elUsuario.wantCashback, forKey: "wantCashbackUsr")
        self.userDefaults.set(elUsuario.amountCashback, forKey: "montoCashbackUsr")
        self.userDefaults.set(elUsuario.afiliacion.intentos, forKey: "numIntentos")
        self.userDefaults.set(elUsuario.afiliacion.statusEnum.rawValue, forKey: "afiliacionIdEstatus")
        self.userDefaults.set(elUsuario.cuentaPaypal, forKey: "cuentaPaypal")
        self.userDefaults.set(elUsuario.codigoAfiliador, forKey: "codigoAfiliador")
        
        let paisData = NSKeyedArchiver.archivedData(withRootObject: elUsuario.pais)
        self.userDefaults.set(paisData, forKey: "pais")
        
        let direccionesData = NSKeyedArchiver.archivedData(withRootObject: elUsuario.direcciones)
        self.userDefaults.set(direccionesData, forKey: "direcciones")
        
        let divisaData = NSKeyedArchiver.archivedData(withRootObject: elUsuario.divisa)
        self.userDefaults.set(divisaData, forKey: "divisa")
    }
    
    func borraTodoCierraSesion(borrarFavoritos : Bool = false){
        self.userDefaults.removeObject(forKey: "userLogged")
        self.userDefaults.removeObject(forKey: "idUsr")
        self.userDefaults.removeObject(forKey: "emailUsr")
        self.userDefaults.removeObject(forKey: "nombreUsr")
        self.userDefaults.removeObject(forKey: "apellidosUsr")
        self.userDefaults.removeObject(forKey: "telUsr")
        self.userDefaults.removeObject(forKey: "avatar")
        self.userDefaults.removeObject(forKey: "tipoLoginId")
        self.userDefaults.removeObject(forKey: "tipoLoginDesc")
        self.userDefaults.removeObject(forKey: "tipoUsuarioId")
        self.userDefaults.removeObject(forKey: "tipoUsuarioDesc")
        self.userDefaults.removeObject(forKey: "estatusUsuarioId")
        self.userDefaults.removeObject(forKey: "estatusUsuarioDesc")
        self.userDefaults.removeObject(forKey: "montoCashbackUsr")
        self.userDefaults.removeObject(forKey: "cashbackUsr")
        self.userDefaults.removeObject(forKey: "numIntentos")
        self.userDefaults.removeObject(forKey: "afiliacionIdEstatus")
        self.userDefaults.removeObject(forKey: "cuentaPaypal")
        self.userDefaults.removeObject(forKey: "paisNombre")
        self.userDefaults.removeObject(forKey: "paisId")
        self.userDefaults.removeObject(forKey: "direcciones")
        self.userDefaults.removeObject(forKey: "divisa")
        self.userDefaults.removeObject(forKey: "codigoAfiliador")
        
        if borrarFavoritos {
            self.userDefaults.removeObject(forKey: "dicFavoritosEmpresa")
            self.userDefaults.removeObject(forKey: "dicFavoritosProfesional")
        }
    }
    
    func getUserId()->String{
        var xid = "0"
        
        if let did = self.userDefaults.string(forKey: "idUsr") {
            xid = did
        }
        return xid
    }
    
    func getUserName(onlyName: Bool = true, onlyLastName: Bool = true)->String {
        var name = ""
        
        if onlyName {
            if let value = self.userDefaults.string(forKey: "nombreUsr") {
                name = value
            }
        }
        
        if onlyLastName {
            if let value = self.userDefaults.string(forKey: "apellidosUsr") {
                name = name + " " + value
            }
        }
        
        return name
    }
    
    func getUserAvatar()->URL? {
        var result = URL(string: "")
        
        if let value = self.userDefaults.url(forKey: "avatar") {
                result = value
        }
        DDLogDebug("Avatar: \(String(describing: result))")
        return result
    }
    
    func getUserPayPal()->String {
        var resultado = ""
        
        if let value = self.userDefaults.string(forKey: "cuentaPaypal") {
            resultado = value
        }
        
        return resultado
    }
    
    func getUserPais() -> Pais {
        var pais = Pais()
        
        if let paisData = self.userDefaults.data(forKey: "pais") {
            let unPais = NSKeyedUnarchiver.unarchiveObject(with: paisData) as! Pais
            pais = unPais
        }
        DDLogDebug("GET PAIS: \(pais.descripcion)")
        return pais
    }
    
    func getUserDivisa() -> Divisa {
        var divisa = Divisa()
        
        if let divisaData = self.userDefaults.data(forKey: "divisa") {
            let unaDivisa = NSKeyedUnarchiver.unarchiveObject(with: divisaData) as! Divisa
            divisa = unaDivisa
        }
        DDLogDebug("GET DIVISA: \(divisa.moneda)")
        return divisa
    }

    func getUserEmail()->String {
        var resultado = ""
        
        if let value = self.userDefaults.string(forKey: "emailUsr") {
            resultado = value
        }
        
        return resultado
    }
    
    func isUserLoggedIn()->Bool{
        
        if(self.userDefaults.bool(forKey: "userLogged")) {
            DDLogDebug("Usuario logueado")
        }
        else {
            DDLogDebug("Usuario no logueado")
        }
        
        return self.userDefaults.bool(forKey: "userLogged")
    }
    
    func getDataUsuario()-> Usuario {
        /*
         self.userDefaults.set(numIntentos, forKey: "numIntentos")
         self.userDefaults.set(afiliacionIdEstatus.rawValue, forKey: "afiliacionIdEstatus")
         -------------------------------
         var afiliador : Usuario?
         var direcciones : [Direccion]!
         var afiliacion : Afiliacion!
         */
        
        let unUsuario = Usuario()
        unUsuario.id = ""
        unUsuario.email = ""
        unUsuario.nombre = ""
        unUsuario.apellidos = ""
        unUsuario.telefono = ""
        unUsuario.avatar = URL(string: "")
        unUsuario.tipoLogin = tipoLoginUsuario()
        unUsuario.tipoLogin.id = ""
        unUsuario.tipoLogin.descripcion = ""
        unUsuario.tipo = tipoUsuario()
        unUsuario.tipo.id = ""
        unUsuario.tipo.descripcion = ""
        unUsuario.estatus = estatusUsuario()
        unUsuario.estatus.id = ""
        unUsuario.estatus.descripcion = ""
        unUsuario.cuentaPaypal = ""
        unUsuario.afiliacion = Afiliacion()
        unUsuario.afiliacion.id = ""
        unUsuario.afiliacion.intentos = 0
        unUsuario.afiliacion.statusEnum = EstatusAfiliacion.noAfiliado
        unUsuario.afiliacion.estatus = EstructuraBase()
        unUsuario.afiliacion.estatus.id = ""
        unUsuario.afiliacion.estatus.descripcion = ""
        unUsuario.direcciones = [Direccion]()
        unUsuario.pais = Pais()
        
        
        if let value = self.userDefaults.string(forKey: "idUsr") {
            unUsuario.id = value
        }
        if let value = self.userDefaults.string(forKey: "emailUsr") {
            unUsuario.email = value
        }
        if let value = self.userDefaults.string(forKey: "nombreUsr") {
            unUsuario.nombre = value
        }
        if let value = self.userDefaults.string(forKey: "apellidosUsr") {
            unUsuario.apellidos = value
        }
        if let value = self.userDefaults.string(forKey: "telUsr") {
            unUsuario.telefono = value
        }
        if let value = self.userDefaults.url(forKey: "avatar") {
            unUsuario.avatar = value
        }
        
        if let value = self.userDefaults.string(forKey: "tipoLoginId") {
            unUsuario.tipoLogin.id = value
            
        }
        
        if let value = self.userDefaults.string(forKey: "tipoLoginDesc") {
            unUsuario.tipoLogin.descripcion = value
            
        }
        
        if let value = self.userDefaults.string(forKey: "tipoUsuarioId") {
            unUsuario.tipo.id = value
            
        }
        
        if let value = self.userDefaults.string(forKey: "tipoUsuarioDesc") {
            unUsuario.tipo.descripcion = value
            
        }
        
        if let value = self.userDefaults.string(forKey: "estatusUsuarioId") {
            unUsuario.estatus.id = value
            
        }
        
        if let value = self.userDefaults.string(forKey: "estatusUsuarioDesc") {
            unUsuario.estatus.descripcion = value
            
        }
        if let value = self.userDefaults.string(forKey: "cuentaPaypal") {
            unUsuario.cuentaPaypal = value
        }
        
        if let value = self.userDefaults.string(forKey: "afiliacionIdEstatus") {
            
            unUsuario.afiliacion.estatus.id = value
            if let value2 = EstatusAfiliacion(rawValue: value){
                unUsuario.afiliacion.statusEnum = value2
            }
        }
        
        //Pais
        if let paisData = self.userDefaults.data(forKey: "pais") {
            let xpais = NSKeyedUnarchiver.unarchiveObject(with: paisData) as! Pais
            unUsuario.pais = xpais
        }
        
        //Direcciones
        if let direccionesData = self.userDefaults.data(forKey: "direcciones") {
            let arrayDirecciones = NSKeyedUnarchiver.unarchiveObject(with: direccionesData) as! [Direccion]
            unUsuario.direcciones = arrayDirecciones
        }
        
        //Codigo afiliador
        if let codigoAfiliador = self.userDefaults.string(forKey: "codigoAfiliador") {
            unUsuario.codigoAfiliador = codigoAfiliador
        }
        
        unUsuario.wantCashback = self.userDefaults.bool(forKey: "wantCashbackUsr")
        unUsuario.amountCashback = self.userDefaults.float(forKey: "montoCashbackUsr")
        unUsuario.afiliacion.intentos = self.userDefaults.integer(forKey: "numIntentos")

        return unUsuario
    }
    
    
    func getDiccFavoritos(cual : tipoDiccFavorito)->[String:String] {
        var dicFavoritos = [String : String]()
        
        var laKey = "dicFavoritosEmpresa"  //dicFavoritosEmpresa / dicFavoritosProfesional
        
        if cual == .profesional {
            laKey = "dicFavoritosProfesional"
        }
        
        if let dicF = self.userDefaults.value(forKey: laKey) as? NSData {
            let newDic = NSKeyedUnarchiver.unarchiveObject(with: dicF as Data) as! [String : String]
            dicFavoritos = newDic
        }
        
        return dicFavoritos
    }
    
    func existeIdEnFavoritos(id: String, donde: tipoDiccFavorito)-> Bool {
        
        let dicFavoritos = self.getDiccFavoritos(cual: donde)
        
        if let _ = dicFavoritos[id] {
            return true
        } else {
            return false
        }
    }
    
    func agregarQuitarFavorito(id: String, elJson: String, donde: tipoDiccFavorito)-> String {
        var mensaje = "Ocurrio un problema al intentar marcar/desmarcar como favorito"
        
        var dicFavoritos = self.getDiccFavoritos(cual: donde)
        
        if let _ = dicFavoritos[id] { //si existe se elimina
            dicFavoritos.removeValue(forKey: id)
            mensaje = "Desmarcado como favorito"
        } else { //si no existe se agrega
            dicFavoritos[id] = elJson
            mensaje = "Marcado como favorito"
        }
        
        var laKey = "dicFavoritosEmpresa"  //dicFavoritosEmpresa / dicFavoritosProfesional
        
        if donde == .profesional {
            laKey = "dicFavoritosProfesional"
        }
        
        let data = NSKeyedArchiver.archivedData(withRootObject: dicFavoritos)
        self.userDefaults.set(data, forKey: laKey)
        
        return mensaje
    }
    
    func getUsrEstatusAfiliacion()->EstatusAfiliacion {
        var elEstatus = EstatusAfiliacion.noAfiliado
        
        if let value = self.userDefaults.string(forKey: "afiliacionIdEstatus") {
            if let value2 = EstatusAfiliacion(rawValue: value){
                elEstatus = value2
            }
        }
        return elEstatus
    }
        
    func getFavoritosObjs(completionHandler: @escaping (_ array:[Empresa_Profesional]) ->()) {
        var arrayFavs = [Empresa_Profesional]()
        
        DispatchQueue.global(qos: .userInteractive).async {
            var dicFav = self.getDiccFavoritos(cual: .empresa)
            
            for jsonString in dicFav.values {
                let elJson = JSON(parseJSON: jsonString)
                arrayFavs.append(self.jsonToEmpresa(elJson: elJson))
            }
            
            dicFav = self.getDiccFavoritos(cual: .profesional)
            
            for jsonString in dicFav.values {
                let elJson = JSON(parseJSON: jsonString)
                arrayFavs.append(self.jsonToProfesional(elJson: elJson))
            }
            completionHandler(arrayFavs)
        }
    }
    
    //MARK: - UTILIDADES
    
    func getImageScaledToWidth (sourceImage:UIImage, scaledToWidth: CGFloat) -> UIImage {
        let oldWidth = sourceImage.size.width
        let scaleFactor = scaledToWidth / oldWidth
        
        let newHeight = sourceImage.size.height * scaleFactor
        let newWidth = oldWidth * scaleFactor
        
        UIGraphicsBeginImageContext(CGSize(width:newWidth, height:newHeight))
        sourceImage.draw(in: CGRect(x:0, y:0, width:newWidth, height:newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    func scaleImageToSize (sourceImage:UIImage, scaledToSize: CGSize) -> UIImage {
        UIGraphicsBeginImageContext(scaledToSize)
        sourceImage.draw(in: CGRect(x:0, y:0, width:scaledToSize.width, height:scaledToSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    func randRange (lower: UInt32 , upper: UInt32) -> UInt32 {
        return lower + arc4random_uniform(upper - lower + 1)
    }
    
    func backtoMain(){
        let xappDelegate = UIApplication.shared.delegate as! AppDelegate
        xappDelegate.gotoMainScreen()
    }
    
    func saveSideMenuOptionSelected(name: String){
        DDLogDebug("Guarda opcion: \(name)")
        
        self.userDefaults.set(name, forKey: "SideMenuIndex")
    }
    
    func getSideMenuOptionSelected()->String? {
        return self.userDefaults.string(forKey: "SideMenuIndex")
    }
    
    //PAra resize de la imagen
    func scaleAndCropImage(image:UIImage, toSize size: CGSize) -> UIImage {
        // Sanity check; make sure the image isn't already sized.
        if image.size.equalTo(size) {
            return image
        }
        
        let widthFactor = size.width / image.size.width
        let heightFactor = size.height / image.size.height
        var scaleFactor: CGFloat = 0.0
        
        scaleFactor = heightFactor
        
        if widthFactor > heightFactor {
            scaleFactor = widthFactor
        }
        
        var thumbnailOrigin = CGPoint.zero
        let scaledWidth  = image.size.width * scaleFactor
        let scaledHeight = image.size.height * scaleFactor
        
        if widthFactor > heightFactor {
            thumbnailOrigin.y = (size.height - scaledHeight) / 2.0
        }
            
        else if widthFactor < heightFactor {
            thumbnailOrigin.x = (size.width - scaledWidth) / 2.0
        }
        
        var thumbnailRect = CGRect.zero
        thumbnailRect.origin = thumbnailOrigin
        thumbnailRect.size.width  = scaledWidth
        thumbnailRect.size.height = scaledHeight
        
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        image.draw(in: thumbnailRect)
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return scaledImage
    }
    
    func getEmpresasPrincipales(arrayEmpresas:[Empresa])->[Empresa]{
        var arrayReturn = [Empresa]()
        if arrayEmpresas.count > 0 {
            for item in arrayEmpresas {
                if item.esPrincipal {
                    arrayReturn.append(item)
                }
            }
        }
        return arrayReturn
    }
    
    func dateToString(formato: String = "dd/MM/yyyy", laFecha: Date)->String{
        let formatter = DateFormatter()
        formatter.dateFormat = formato
        return formatter.string(from: laFecha)
    }
    
    func stringToDate(formato: String = "dd-MM-yyyy HH:mm:ss", elString: String)->Date {
        let formatter = DateFormatter()
        formatter.dateFormat = formato
        var returnDate = Date()
        if let xvalue = formatter.date(from: "01-01-1990 00:00:00") {
            returnDate = xvalue
        }
        if let xvalue = formatter.date(from: elString) {
            returnDate = xvalue
        }
        return returnDate
    }
    
    //MARK: - DUMMYS PARA PRUEBAS
    
    func fillArrayNegocio(limit : Int = 10)->[Empresa]{
        var testArray = [Empresa]()
        for index in 0..<limit {
            let unNegocio = Empresa()
            unNegocio.razonSocial = "GREEN SUPER GYM \(index)"
            unNegocio.cash_back = String(Functions().randRange(lower: 3, upper: 10))
            unNegocio.distancia = "\(1 * (Double(index + 1) * 0.2 ))"
            
            testArray.append(unNegocio)
        }
        return testArray
    }
    
    func testGetDummyImageServNego(index: Int, tipo: String)-> UIImage{
        var laImagen = UIImage()
        if tipo == "serv" {
            switch index {
            case 1: laImagen = Asset.CarruselServicios.carruselServ1.image; break;
            case 2: laImagen = Asset.CarruselServicios.carruselServ2.image; break;
            case 3: laImagen = Asset.CarruselServicios.carruselServ3.image; break;
            case 4: laImagen = Asset.CarruselServicios.carruselServ4.image; break;
            case 5: laImagen = Asset.CarruselServicios.carruselServ5.image; break;
            default: break
            }
        } else {
            switch index {
            case 1: laImagen = Asset.CarruselDummys.carrousel1.image; break;
            case 2: laImagen = Asset.CarruselDummys.carrousel2.image; break;
            case 3: laImagen = Asset.CarruselDummys.carrousel3.image; break;
            case 4: laImagen = Asset.CarruselDummys.carrousel4.image; break;
            default: break
            }
        }
        
        return laImagen
    }
    
    func testGetDummyFace(index: Int)-> UIImage {
        var laImagen = UIImage()
        switch index {
        case 1: laImagen = Asset.FacesDummys.face1.image; break;
        case 2: laImagen = Asset.FacesDummys.face2.image; break;
        case 3: laImagen = Asset.FacesDummys.face3.image; break;
        case 4: laImagen = Asset.FacesDummys.face4.image; break;
        case 5: laImagen = Asset.FacesDummys.face5.image; break;
        default:
            break
        }
        return laImagen
    }
    
    func getStringDummy(index: Int)->String {
        return Constantes.dummyOpiniones[index - 1]
    }
    
    func getStringNombreServicio(index: Int)->String {
        return Constantes.dummyNombreServicios[index]
    }
    
    func getArrayNotificacion()->[Notificacion] {
        var testArray = [Notificacion]()
        let limit = self.randRange(lower: 5, upper: 10)
        
        for _ in 0..<limit {
            let unaNotif = Notificacion()
            unaNotif.titulo = Functions().getStringDummy(index: Int(Functions().randRange(lower: 1, upper: 4)))
            unaNotif.fecha = "10:00 pm - 12/Ago/2017"
            unaNotif.testImg = Functions().testGetDummyFace(index: Int(Functions().randRange(lower: 1, upper: 5)))
            testArray.append(unaNotif)
        }
        
        return testArray
    }
}
