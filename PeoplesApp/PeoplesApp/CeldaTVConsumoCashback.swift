//
//  CeldaTVConsumoCashback.swift
//  PeoplesApp
//
//  Created by Jesús García on 15/02/18.
//  Copyright © 2018 firewallsoluciones. All rights reserved.
//

import UIKit

class CeldaTVConsumoCashback: UITableViewCell {
    var lblDeudorNombre = UILabel()
    var lblMontoConsumo = UILabel()
    var lblFechaConsumo = UILabel()
    var viewStatus = UIView()
    var lblStatus = UILabel()
    
    var anchoCelda : CGFloat = 0
    var altoCelda : CGFloat = 0
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.buildInterface()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func buildInterface() {
        let marginGuide = contentView.layoutMarginsGuide

        //View status
        contentView.addSubview(viewStatus)
        viewStatus.translatesAutoresizingMaskIntoConstraints = false
        viewStatus.backgroundColor = Constantes.paletaColores.verdeCompletado
        viewStatus.layer.cornerRadius = 5
        viewStatus.clipsToBounds = true
        viewStatus.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor).isActive = true
        viewStatus.widthAnchor.constraint(equalToConstant: 130).isActive = true
        viewStatus.heightAnchor.constraint(equalToConstant: 35).isActive = true
        viewStatus.centerYAnchor.constraint(equalTo: marginGuide.centerYAnchor).isActive = true

        //Label status
        viewStatus.addSubview(lblStatus)
        lblStatus.translatesAutoresizingMaskIntoConstraints = false
        lblStatus.textColor = UIColor.white
        lblStatus.centerXAnchor.constraint(equalTo: viewStatus.centerXAnchor).isActive = true
        lblStatus.centerYAnchor.constraint(equalTo: viewStatus.centerYAnchor).isActive = true
        
        //Deudor nombre
        contentView.addSubview(lblDeudorNombre)
        lblDeudorNombre.translatesAutoresizingMaskIntoConstraints = false
        lblDeudorNombre.textColor = Constantes.paletaColores.verdePeoples
        lblDeudorNombre.font = Constantes.currentFonts.NunitoBold.base
        lblDeudorNombre.numberOfLines = 0
        lblDeudorNombre.lineBreakMode = .byWordWrapping
        
        lblDeudorNombre.topAnchor.constraint(equalTo: marginGuide.topAnchor).isActive = true
        lblDeudorNombre.heightAnchor.constraint(greaterThanOrEqualToConstant: 25).isActive = true
        lblDeudorNombre.leadingAnchor.constraint(equalTo: marginGuide.leadingAnchor).isActive = true
        lblDeudorNombre.trailingAnchor.constraint(equalTo: viewStatus.leadingAnchor, constant: -5).isActive = true
        
        ///test
//        lblDeudorNombre.bottomAnchor.constraint(equalTo: marginGuide.bottomAnchor).isActive = true
        
        //Monto consumo
        contentView.addSubview(lblMontoConsumo)
        lblMontoConsumo.translatesAutoresizingMaskIntoConstraints = false
        lblMontoConsumo.font = Constantes.currentFonts.NunitoRegular.base
        lblMontoConsumo.numberOfLines = 0
        lblMontoConsumo.lineBreakMode = .byWordWrapping
        
        lblMontoConsumo.topAnchor.constraint(equalTo: lblDeudorNombre.bottomAnchor, constant: 2).isActive = true
        lblMontoConsumo.leadingAnchor.constraint(equalTo: marginGuide.leadingAnchor).isActive = true
        lblMontoConsumo.trailingAnchor.constraint(equalTo: lblDeudorNombre.trailingAnchor).isActive = true
        lblMontoConsumo.bottomAnchor.constraint(equalTo: marginGuide.bottomAnchor).isActive = true

        //Fecha de consumo
//        contentView.addSubview(lblFechaConsumo)
//        lblFechaConsumo.translatesAutoresizingMaskIntoConstraints = false
//        lblFechaConsumo.font = Constantes.currentFonts.NunitoRegular.base
//        lblFechaConsumo.backgroundColor = UIColor.blue
//        lblFechaConsumo.topAnchor.constraint(equalTo: lblDeudorNombre.bottomAnchor, constant: 2).isActive = true
//        lblFechaConsumo.leadingAnchor.constraint(equalTo: marginGuide.leadingAnchor).isActive = true
//        lblFechaConsumo.trailingAnchor.constraint(equalTo: viewStatus.leadingAnchor, constant: -5).isActive = true
//        lblFechaConsumo.bottomAnchor.constraint(equalTo: marginGuide.bottomAnchor, constant: 2).isActive = true
        
    }

}
