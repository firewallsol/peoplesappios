//
//  AfiliarNegocioVC.swift
//  PeoplesApp
//
//  Created by Jesús García on 08/03/18.
//  Copyright © 2018 firewallsoluciones. All rights reserved.
//

import UIKit
import CocoaLumberjack

class AfiliarNegocioVC: UIViewController {
    //MARK: - Outlets
    var lblTitulo = UILabel()
    var lblDescripcion = UILabel()
    var lblDescCodigo = UILabel()
    var lblCodigoAfiliador = CopyableLabel()
    var btnLink = UIButton()
    
    //MARK: - Vars
    var usuario = Usuario()

    //MARK: - View
    override func viewDidLoad() {
        super.viewDidLoad()
        
        usuario = Functions().getDataUsuario()
        buildInterface()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - Custom
    func buildInterface() {
        let marginGuide = view.layoutMarginsGuide
        
        //View
        view.backgroundColor = UIColor.white
        
        //Titulo
        view.addSubview(lblTitulo)
        lblTitulo.translatesAutoresizingMaskIntoConstraints = false
        lblTitulo.textColor = Constantes.paletaColores.verdePeoples
        lblTitulo.font = Constantes.currentFonts.NunitoBold.base
        lblTitulo.textAlignment = .center
        
        lblTitulo.topAnchor.constraint(equalTo: marginGuide.topAnchor, constant: 10).isActive = true
        lblTitulo.leadingAnchor.constraint(equalTo: marginGuide.leadingAnchor).isActive = true
        lblTitulo.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor).isActive = true

        //Descripcion
        view.addSubview(lblDescripcion)
        lblDescripcion.translatesAutoresizingMaskIntoConstraints = false
        lblDescripcion.font = Constantes.currentFonts.NunitoRegular.base
        lblDescripcion.textAlignment = .center
        lblDescripcion.numberOfLines = 0
        lblDescripcion.lineBreakMode = .byWordWrapping
        
        lblDescripcion.topAnchor.constraint(equalTo: lblTitulo.layoutMarginsGuide.bottomAnchor, constant: 20).isActive = true
        lblDescripcion.leadingAnchor.constraint(equalTo: marginGuide.leadingAnchor).isActive = true
        lblDescripcion.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor).isActive = true
        
        //Utiliza codigo
        view.addSubview(lblDescCodigo)
        lblDescCodigo.translatesAutoresizingMaskIntoConstraints = false
        lblDescCodigo.textColor = Constantes.paletaColores.verdePeoples
        lblDescCodigo.font = Constantes.currentFonts.NunitoBold.base
        lblDescCodigo.textAlignment = .center
        
        lblDescCodigo.topAnchor.constraint(equalTo: lblDescripcion.layoutMarginsGuide.bottomAnchor, constant: 40).isActive = true
        lblDescCodigo.leadingAnchor.constraint(equalTo: marginGuide.leadingAnchor).isActive = true
        lblDescCodigo.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor).isActive = true
        
        //Codigo afiliador
        view.addSubview(lblCodigoAfiliador)
        lblCodigoAfiliador.translatesAutoresizingMaskIntoConstraints = false
        lblCodigoAfiliador.font = Constantes.currentFonts.NunitoBold.base
        lblCodigoAfiliador.textAlignment = .center
        
        lblCodigoAfiliador.topAnchor.constraint(equalTo: lblDescCodigo.layoutMarginsGuide.bottomAnchor, constant: 25).isActive = true
        lblCodigoAfiliador.leadingAnchor.constraint(equalTo: marginGuide.leadingAnchor).isActive = true
        lblCodigoAfiliador.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor).isActive = true
        
        //Boton sitio
        view.addSubview(btnLink)
        btnLink.translatesAutoresizingMaskIntoConstraints = false
        btnLink.titleLabel?.font = Constantes.currentFonts.NunitoBold.base
        btnLink.backgroundColor = Constantes.paletaColores.verdePeoples
        btnLink.layer.cornerRadius = 5
        btnLink.addTarget(self, action: #selector(seleccionaBotonIrAlSitio), for: .touchUpInside)
        
        btnLink.topAnchor.constraint(equalTo: lblCodigoAfiliador.layoutMarginsGuide.bottomAnchor, constant: 40).isActive = true
        btnLink.heightAnchor.constraint(equalToConstant: 44).isActive = true
        btnLink.leadingAnchor.constraint(equalTo: marginGuide.leadingAnchor, constant: 30).isActive = true
        btnLink.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor, constant: -30).isActive = true
    }
    
    func setData() {
        lblTitulo.text = NSLocalizedString("Afilia un negocio", comment: "").uppercased()
        
        lblDescripcion.text = NSLocalizedString("Si deseas afiliar a una empresa o negocio, utiliza tu código de afiliación el cual te ayudará a recibir un cashback por cada afiliación que realices vinculada a ese código.", comment: "")
        
        lblDescCodigo.text = NSLocalizedString("Utiliza tu código de afiliador:", comment: "")
        
        var codigoAfiliador = NSLocalizedString("Sin código", comment: "")
        if let codigo = usuario.codigoAfiliador {
            codigoAfiliador = codigo
        }
        lblCodigoAfiliador.text = codigoAfiliador.uppercased()
        btnLink.setTitle(NSLocalizedString("Ir al sitio", comment: "").uppercased(), for: .normal)
    }
    
    //MARK: - Events
    @objc func seleccionaBotonIrAlSitio() {
        if let link = URL(string: Constantes.URLS.afiliarNegocio) {
            
            if UIApplication.shared.canOpenURL(link) {
                DDLogInfo("Abriendo sitio: \(link)")
                UIApplication.shared.openURL(link)
            } else {
                DDLogError("Error al abrir sitio: \(link)")
                self.mensajeSimple(mensaje: NSLocalizedString("No se pudo abrir el sitio.", comment: ""))
            }
        }
    }
    
}
