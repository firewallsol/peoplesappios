//
//  AfiliateIntroVC.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 14/09/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit

class AfiliateIntroVC: UIViewController {
    
    var scrollView : UIScrollView!
    
    var statusActual = EstatusAfiliacion.noAfiliado

    //MARK: - View
    override func viewDidLoad() {
        super.viewDidLoad()

        self.automaticallyAdjustsScrollViewInsets = false
        self.view.backgroundColor = UIColor.white
        
        self.setBarMenu()
        self.buildInterface()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let(success, message) = self.validaStatusAfiliacion(status: statusActual)
        if !success {
            DispatchQueue.main.async {
                self.muestraAlerta(mensaje: message)
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationItem.title = ""
    }
    
    //MARK: - Events
    func buildInterface(){
        
        var xframe = CGRect.zero
        
        self.scrollView = UIScrollView(frame: self.view.frame)
        
        //logo
        xframe.origin = CGPoint(x: 10, y: 10)
        let altoImg = Constantes.tamaños.anchoPantalla * 0.2
        let anchoImg = altoImg * 3.0125
        xframe.size = CGSize(width: anchoImg, height: altoImg)
        let imgLogo = UIImageView(frame: xframe)
        imgLogo.image = Asset.pplsAppLogo.image
        self.scrollView.addSubview(imgLogo)
        
        //parrafo 1
        xframe.origin = CGPoint(x: 10, y: imgLogo.frame.maxY + 20)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla * 0.9, height: 30)
        let parrafo1 = UILabel(frame: xframe)
        parrafo1.numberOfLines = 0
        parrafo1.textAlignment = .justified
        parrafo1.text = FWS.lblJoinParraph1
        parrafo1.font = Constantes.currentFonts.NunitoRegular.base
        parrafo1.sizeToFit()
        parrafo1.center.x = Constantes.tamaños.centroHorizontal
        self.scrollView.addSubview(parrafo1)
        
        //es facil
        xframe.origin = CGPoint(x: 10, y: parrafo1.frame.maxY + 20)
        let esFacil = UILabel(frame: xframe)
        esFacil.numberOfLines = 0
        esFacil.textAlignment = .justified
        esFacil.text = FWS.lblJoinItsEasy
        esFacil.font = Constantes.currentFonts.NunitoBold.base
        esFacil.sizeToFit()
        esFacil.center.x = Constantes.tamaños.centroHorizontal
        self.scrollView.addSubview(esFacil)
        
        //paso 1
        //num
        xframe.origin = CGPoint(x: 20, y: esFacil.frame.maxY + 20)
        let num1 = UILabel(frame: xframe)
        num1.text = "1"
        num1.textColor = UIColor.gray
        num1.font = Constantes.currentFonts.NunitoBold.plus.LabelFontSize_4
        num1.sizeToFit()
        self.scrollView.addSubview(num1)
        
        //text
        xframe.origin = CGPoint(x: num1.frame.maxX + 15, y: esFacil.frame.maxY + 20)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla * 0.8, height: 30)
        let paso1txt = UILabel(frame: xframe)
        paso1txt.text = FWS.lblJoinStep1
        paso1txt.font = Constantes.currentFonts.NunitoRegular.minus.LabelFontSize_3
        paso1txt.sizeToFit()
        paso1txt.center.y = num1.frame.minY + (num1.frame.size.height / 2)
        self.scrollView.addSubview(paso1txt)
        
        
        //paso 2
        //num
        xframe.origin = CGPoint(x: 20, y: num1.frame.maxY + 15)
        let num2 = UILabel(frame: xframe)
        num2.text = "2"
        num2.textColor = UIColor.gray
        num2.font = Constantes.currentFonts.NunitoBold.plus.LabelFontSize_4
        num2.sizeToFit()
        self.scrollView.addSubview(num2)
        
        //text
        xframe.origin = CGPoint(x: num2.frame.maxX + 15, y: num1.frame.maxY + 20)
        let paso2txt = UILabel(frame: xframe)
        paso2txt.numberOfLines = 0
        paso2txt.text = FWS.lblJoinStep2
        paso2txt.font = Constantes.currentFonts.NunitoRegular.minus.LabelFontSize_3
        paso2txt.sizeToFit()
        paso2txt.center.y = num2.frame.minY + (num2.frame.size.height / 2)
        self.scrollView.addSubview(paso2txt)
        
        //paso 3
        //num
        xframe.origin = CGPoint(x: 20, y: num2.frame.maxY + 15)
        let num3 = UILabel(frame: xframe)
        num3.text = "3"
        num3.textColor = UIColor.gray
        num3.font = Constantes.currentFonts.NunitoBold.plus.LabelFontSize_4
        num3.sizeToFit()
        self.scrollView.addSubview(num3)
        
        //text
        xframe.origin = CGPoint(x: num3.frame.maxX + 15, y: num2.frame.maxY + 20)
        let paso3txt = UILabel(frame: xframe)
        paso3txt.numberOfLines = 0
        paso3txt.text = FWS.lblJoinStep3
        paso3txt.font = Constantes.currentFonts.NunitoRegular.minus.LabelFontSize_3
        paso3txt.sizeToFit()
        paso3txt.center.y = num3.frame.minY + (num3.frame.size.height / 2)
        self.scrollView.addSubview(paso3txt)
        
        self.view.addSubview(self.scrollView)
        
        let padding = (self.navigationController?.navigationBar.frame.size.height)! + Constantes.tamaños.altoStatusBar + (Constantes.tamaños.altoBotonTop)
        
        xframe.origin = CGPoint(x: 0, y: Constantes.tamaños.altoPantalla - padding)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: Constantes.tamaños.altoBotonTop)
        let btnContinuar = UIView(frame: xframe)
        btnContinuar.backgroundColor = Constantes.paletaColores.verdePeoples
        
        let lblBtnCont = UILabel(frame: xframe)
        lblBtnCont.text = "CONTINUAR"
        lblBtnCont.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_1
        lblBtnCont.textColor = UIColor.white
        btnContinuar.addSubview(lblBtnCont)
        lblBtnCont.sizeToFit()
        lblBtnCont.center.x = Constantes.tamaños.anchoPantalla / 2
        lblBtnCont.center.y = btnContinuar.frame.size.height / 2
        let tapBtnCont = UITapGestureRecognizer(target: self, action: #selector(self.gotoRegistro))
        btnContinuar.addGestureRecognizer(tapBtnCont)
        btnContinuar.isUserInteractionEnabled = true
        self.view.addSubview(btnContinuar)
        
        self.scrollView.contentSize.height = num3.frame.maxY + padding + 10
        
        if self.statusActual == .rechazada {
            self.mensajeSimple(mensaje: "Su proceso de afiliación esta en estatus: RECHAZADA. Para mayor información revise su email. ", dismiss: false)
        }
    }
    
    @objc func gotoRegistro(){
        if Functions().isUserLoggedIn() {
            
            //Valida que el usuario tenga un país de origen.
            let pais = Functions().getUserPais()
            if pais.descripcion != "" {
                let elVC = AfiliateRegistroVC()
                self.navigationController?.pushViewController(elVC, animated: true)
            }
            else {
                let mensaje = NSLocalizedString("AlertShowEditProfileOnProfileIncomplete", comment: "")
                let titulo = NSLocalizedString("AppName", comment: "")
                let viewController = EditarPerfilVC()
                self.alertaRedirige(viewController: viewController, mensaje: mensaje, titulo: titulo)
            }
        }
        else {
            let mensaje = NSLocalizedString("AlertShowLoginOnRequestService", comment: "")
            let titulo = NSLocalizedString("AppName", comment: "")
            let viewController = LoginVC()
            
            self.alertaRedirige(viewController: viewController, mensaje: mensaje, titulo: titulo)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func validaStatusAfiliacion(status: EstatusAfiliacion) ->(Bool, String) {
        var elBool = false
        var mensaje = ""
        
        switch status {
        case .noAfiliado:
            elBool = true
            break
        case .solicitada:
            mensaje = "Su proceso de afiliación esta en estatus: SOLICITADA. Para mayor información revise su email."
            break
        case .preaprobada:
            mensaje = "Su proceso de afiliación esta en estatus: PREAPROBADA. Para mayor información revise su email."
            break
        case .rechazada:
            elBool = true
            break
        case .completada:
            mensaje = "Su proceso de afiliación esta en estatus: COMPLETADA. Para mayor información revise su email."
            break
        case .denegada:
            mensaje = "Su proceso de afiliación esta en estatus: DENEGADA. Para mayor información revise su email."
            break
        }
        return (elBool, mensaje)
    }

    func muestraAlerta(mensaje: String) {
        let alerta = UIAlertController(title: NSLocalizedString("AppName", comment: ""),
                                       message: mensaje,
                                       preferredStyle: .actionSheet)
        
        let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""),
                                     style: .default) { (action) in
                                        Functions().backtoMain()
        }
        
        alerta.addAction(okAction)
        self.present(alerta, animated: true, completion: nil)
    }
    
    
}
