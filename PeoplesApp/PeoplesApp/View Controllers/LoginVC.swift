//
//  LoginVC.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 28/07/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import FacebookLogin
import FacebookCore
import CocoaLumberjack

class LoginVC: UIViewController, GIDSignInDelegate, GIDSignInUIDelegate {
    
    
    //MARK: - Components
    var txtEmail : UITextField!
    var txtPassword : UITextField!
    var btnIngresar : UIView!
    var btnOlvideContra : UIView!
    var btnCrearCuenta : UIView!
    var btnFacebook : UIView!
    var btnGooglePlus : UIView!
    var scrollView : UIScrollView!
    
    var showSideMenu = false
    
    //MARK: - View
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        self.buildInterface()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isTranslucent = false
        
        self.navigationController?.navigationBar.tintColor = Constantes.paletaColores.verdePeoples
        
        self.navigationItem.title = NSLocalizedString("Login", comment: "")
        
        if self.showSideMenu {
            self.setBarMenu()
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationItem.title = ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Keyboard
    @objc func keyboardWillShow(sender: NSNotification) {
        var userInfo = sender.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height
        self.scrollView.contentInset = contentInset
        
        var visibleRect = self.view.frame
        visibleRect.size.height -= keyboardFrame.size.height
        
        var elOrigin = CGPoint.zero
        
        let scroll = false
        
        if self.txtEmail.isFirstResponder {
            visibleRect.size.height -= ((self.txtEmail.inputAccessoryView?.frame.size.height)! * 2)
            elOrigin = self.txtEmail.frame.origin
            //scroll = true
        }
        
        if self.txtPassword.isFirstResponder {
            visibleRect.size.height -= ((self.txtPassword.inputAccessoryView?.frame.size.height)! * 2)
            elOrigin = self.txtPassword.frame.origin
            //scroll = true
        }
        
        if ((scroll) && !(visibleRect.contains(elOrigin))){
            let scrollPoint = CGPoint(x: 0, y: elOrigin.y - visibleRect.size.height)
            self.scrollView.setContentOffset(scrollPoint, animated: true)
        }
    }
    
    @objc func keyboardWillHide(sender: NSNotification) {
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        self.scrollView.contentInset = contentInset
    }
    
    //MARK: - Events
    func buildInterface(){
        self.scrollView = UIScrollView(frame: self.view.frame)
        self.scrollView.backgroundColor = UIColor.white
        
        var xframe = CGRect()
        xframe.origin = CGPoint(x: 10, y: 10)
        xframe.size = CGSize(width: 10, height: 30)
        
        let lblingresarconemail = UILabel(frame: xframe)
        lblingresarconemail.font = UIFont.boldSystemFont(ofSize: lblingresarconemail.font.pointSize)
        lblingresarconemail.textColor = Constantes.paletaColores.verdePeoples
        lblingresarconemail.text = "INGRESAR CON E-MAIL"
        lblingresarconemail.font = Constantes.currentFonts.NunitoRegular.base
        lblingresarconemail.sizeToFit()
        self.scrollView.addSubview(lblingresarconemail)
        
        lblingresarconemail.center.x = Constantes.tamaños.centroHorizontal
        
        //txt email
        xframe.origin = CGPoint(x: 10, y: lblingresarconemail.frame.maxY + 20)
        xframe.size = CGSize(width: Constantes.tamaños.anchoTextfield, height: Constantes.tamaños.altoTextfield)
        self.txtEmail = UITextField(frame: xframe)
        self.txtEmail.layer.borderColor = Constantes.paletaColores.bordeTextfield.cgColor
        self.txtEmail.layer.borderWidth = Constantes.tamaños.sizeBordeTextfield
        self.txtEmail.layer.cornerRadius = 3
        self.txtEmail.center.x = Constantes.tamaños.centroHorizontal
        self.txtEmail.placeholder = NSLocalizedString("Email", comment: "")
        self.txtEmail.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0)
        self.txtEmail.keyboardType = .emailAddress
        self.txtEmail.autocapitalizationType = .none
        self.txtEmail.autocorrectionType = .no
        self.txtEmail.font = Constantes.currentFonts.NunitoRegular.base
        self.addToolBar(textField: self.txtEmail)
        self.scrollView.addSubview(self.txtEmail)
        
        //txt passwd
        xframe.origin = CGPoint(x: 10, y: self.txtEmail.frame.maxY + 10)
        self.txtPassword = UITextField(frame: xframe)
        self.txtPassword.layer.borderColor = Constantes.paletaColores.bordeTextfield.cgColor
        self.txtPassword.layer.borderWidth = Constantes.tamaños.sizeBordeTextfield
        self.txtPassword.layer.cornerRadius = 3
        self.txtPassword.center.x = Constantes.tamaños.centroHorizontal
        self.txtPassword.placeholder = "Contraseña"
        self.txtPassword.isSecureTextEntry = true
        self.txtPassword.autocapitalizationType = .none
        self.txtPassword.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0)
        self.txtPassword.font = Constantes.currentFonts.NunitoRegular.base
        self.addToolBar(textField: self.txtPassword)
        self.scrollView.addSubview(self.txtPassword)
        
        //btn ingresar
        xframe.origin = CGPoint(x: 10, y: self.txtPassword.frame.maxY + 20)
        xframe.size = CGSize(width: Constantes.tamaños.anchoTextfield / 1.5, height: Constantes.tamaños.altoTextfield)
        self.btnIngresar = UIView(frame: xframe)
        self.btnIngresar.backgroundColor = Constantes.paletaColores.verdePeoples
        self.btnIngresar.layer.cornerRadius = 10
        self.btnIngresar.center.x = Constantes.tamaños.centroHorizontal
        self.scrollView.addSubview(self.btnIngresar)
        //label
        let lblBtnIngresar = UILabel(frame: xframe)
        lblBtnIngresar.text = "INGRESAR"
        lblBtnIngresar.textColor = UIColor.white
        lblBtnIngresar.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_1
        lblBtnIngresar.sizeToFit()
        self.btnIngresar.addSubview(lblBtnIngresar)
        lblBtnIngresar.center.x = self.btnIngresar.frame.size.width / 2
        lblBtnIngresar.center.y = self.btnIngresar.frame.size.height / 2
        let tapBtnIngresar = UITapGestureRecognizer(target: self, action: #selector(self.btnLoginTapped))
        self.btnIngresar.isUserInteractionEnabled = true
        self.btnIngresar.addGestureRecognizer(tapBtnIngresar)
        
        //btnolvide contra
        //label
        let lblBtnOlvidePsswd = UILabel(frame: xframe)
        lblBtnOlvidePsswd.text = "¿Olvidaste tu contraseña?"
        lblBtnOlvidePsswd.textColor = UIColor.black
        lblBtnOlvidePsswd.font = Constantes.currentFonts.NunitoRegular.minus.LabelFontSize_2
        lblBtnOlvidePsswd.sizeToFit()
        xframe.origin = CGPoint(x: 10, y: self.btnIngresar.frame.maxY + 1)
        xframe.size = CGSize(width: lblBtnOlvidePsswd.frame.size.width + 10, height: Constantes.tamaños.altoTextfield)
        self.btnOlvideContra = UIView(frame: xframe)
        self.btnOlvideContra.center.x = Constantes.tamaños.centroHorizontal
        self.btnOlvideContra.backgroundColor = UIColor.white
        let tapGesture2 = UITapGestureRecognizer(target: self, action: #selector(seleccionaBotonRecuperarContrasenia))
        self.btnOlvideContra.addGestureRecognizer(tapGesture2)
        self.btnOlvideContra.isUserInteractionEnabled = true
        
        self.btnOlvideContra.addSubview(lblBtnOlvidePsswd)
        lblBtnOlvidePsswd.center.x = self.btnOlvideContra.frame.size.width / 2
        lblBtnOlvidePsswd.center.y = self.btnOlvideContra.frame.size.height / 2
        self.scrollView.addSubview(self.btnOlvideContra)
        
        //crear cuenta nueva
        let lblBtnCrearCuenta = UILabel(frame: xframe)
        lblBtnCrearCuenta.text = "Crear una cuenta nueva"
        lblBtnCrearCuenta.textColor = UIColor.black
        lblBtnCrearCuenta.font = Constantes.currentFonts.NunitoBold.base
        lblBtnCrearCuenta.sizeToFit()
        xframe.origin = CGPoint(x: 10, y: self.btnOlvideContra.frame.maxY + 20)
        xframe.size = CGSize(width: lblBtnCrearCuenta.frame.size.width + 10, height: Constantes.tamaños.altoTextfield)
        self.btnCrearCuenta = UIView(frame: xframe)
        self.btnCrearCuenta.center.x = Constantes.tamaños.centroHorizontal
        self.btnCrearCuenta.backgroundColor = UIColor.white
        self.btnCrearCuenta.addSubview(lblBtnCrearCuenta)
        lblBtnCrearCuenta.center.x = self.btnCrearCuenta.frame.size.width / 2
        lblBtnCrearCuenta.center.y = self.btnCrearCuenta.frame.size.height / 2
        self.scrollView.addSubview(self.btnCrearCuenta)
        
        let tapCrearCuenta = UITapGestureRecognizer(target: self, action: #selector(self.gotoCrearCuenta))
        self.btnCrearCuenta.isUserInteractionEnabled = true
        self.btnCrearCuenta.addGestureRecognizer(tapCrearCuenta)
        
        //static label ingresa con
        xframe.origin = CGPoint(x: 10, y: self.btnCrearCuenta.frame.maxY + 30)
        let lblIngresaCon = UILabel(frame: xframe)
        lblIngresaCon.text = "INGRESA CON"
        lblIngresaCon.font = Constantes.currentFonts.NunitoBold.base
        lblIngresaCon.sizeToFit()
        lblIngresaCon.textColor = Constantes.paletaColores.verdePeoples
        lblIngresaCon.center.x = Constantes.tamaños.centroHorizontal
        self.scrollView.addSubview(lblIngresaCon)
        
        //btn facebook
        xframe.origin = CGPoint(x: 10, y: lblIngresaCon.frame.maxY + 20)
        xframe.size = CGSize(width: Constantes.tamaños.anchoTextfield / 1.5, height: Constantes.tamaños.altoTextfield)
        self.btnFacebook = UIView(frame: xframe)
        self.btnFacebook.backgroundColor = Constantes.paletaColores.azulFacebook
        self.btnFacebook.layer.cornerRadius = 10
        self.btnFacebook.center.x = Constantes.tamaños.centroHorizontal
        self.scrollView.addSubview(self.btnFacebook)
        //icono
        xframe.origin = CGPoint(x: 5, y: 5)
        xframe.size = CGSize(width: Constantes.tamaños.altoTextfield * 0.6, height: Constantes.tamaños.altoTextfield * 0.6)
        let icbtnFacebook = UIImageView(frame: xframe)
        icbtnFacebook.contentMode = .scaleAspectFit
        icbtnFacebook.backgroundColor = UIColor.white
        icbtnFacebook.center.y = self.btnFacebook.frame.size.height / 2
        icbtnFacebook.image = Asset.Icons.icFacebookBtn64.image
        self.btnFacebook.addSubview(icbtnFacebook)
        
        //label
        let lblBtnFacebook = UILabel(frame: xframe)
        lblBtnFacebook.text = "Facebook"
        lblBtnFacebook.font = Constantes.currentFonts.NunitoRegular.base
        lblBtnFacebook.textColor = UIColor.white
        lblBtnFacebook.sizeToFit()
        self.btnFacebook.addSubview(lblBtnFacebook)
        lblBtnFacebook.center.y = self.btnFacebook.frame.size.height / 2
        lblBtnFacebook.center.x = (self.btnFacebook.frame.size.width / 2) + 10
        
        let tapBtnFacebook = UITapGestureRecognizer(target: self, action: #selector(seleccionaBotonLoginFacebook))
        self.btnFacebook.isUserInteractionEnabled = true
        self.btnFacebook.addGestureRecognizer(tapBtnFacebook)
        
        //btn google plus
        xframe.origin = CGPoint(x: 10, y: self.btnFacebook.frame.maxY + 10)
        xframe.size = CGSize(width: Constantes.tamaños.anchoTextfield / 1.5, height: Constantes.tamaños.altoTextfield)
        self.btnGooglePlus = UIView(frame: xframe)
        self.btnGooglePlus.backgroundColor = Constantes.paletaColores.rojoGoogle
        self.btnGooglePlus.layer.cornerRadius = 10
        self.btnGooglePlus.center.x = Constantes.tamaños.centroHorizontal
        
        let tapGesture = UITapGestureRecognizer()
        tapGesture.addTarget(self, action: #selector(seleccionaBotonLoginGoogle))
        btnGooglePlus.addGestureRecognizer(tapGesture)
        btnGooglePlus.isUserInteractionEnabled = true
        
        self.scrollView.addSubview(self.btnGooglePlus)
        
        //icono
        xframe.origin = CGPoint(x: 5, y: 5)
        xframe.size = CGSize(width: Constantes.tamaños.altoTextfield * 0.7, height: Constantes.tamaños.altoTextfield * 0.7)
        let icbtnGoogleP = UIImageView(frame: xframe)
        icbtnGoogleP.contentMode = .scaleAspectFill
        icbtnGoogleP.center.y = self.btnGooglePlus.frame.size.height / 2
        icbtnGoogleP.image = Asset.Icons.icGoogleplusBtn64.image
        self.btnGooglePlus.addSubview(icbtnGoogleP)
        
        //label
        let lblBtnGoogleP = UILabel(frame: xframe)
        lblBtnGoogleP.text = "Google"
        lblBtnGoogleP.font = Constantes.currentFonts.NunitoRegular.base
        lblBtnGoogleP.textColor = UIColor.white
        lblBtnGoogleP.sizeToFit()
        self.btnGooglePlus.addSubview(lblBtnGoogleP)
        lblBtnGoogleP.center.y = self.btnFacebook.frame.size.height / 2
        lblBtnGoogleP.center.x = (self.btnFacebook.frame.size.width / 2) + 10
        
        self.view.addSubview(self.scrollView)
        
        self.scrollView.contentSize.height = self.btnGooglePlus.frame.maxY + ((self.navigationController?.navigationBar.frame.size.height)! + Constantes.tamaños.altoStatusBar) + 10
    }
    
    @objc func gotoCrearCuenta(){
        let elVC = RegistroVC()
        self.navigationController?.pushViewController(elVC, animated: true)
    }
    
    func validaForma()->Bool{
        
        var mensaje = ""
        
        //nombre
        if (self.txtEmail.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == ""){
            mensaje = "El campo \"Email\", es obligatorio"
        }
        
        if !String.isValidEmailAddress(emailAddressString: txtEmail.text!) {
            mensaje = "El formato de correo electrónico es incorrecto"
        }
        
        //nombre
        if (mensaje == "" && self.txtPassword.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == ""){
            mensaje = "El campo \"Password\", es obligatorio"
            
        }
        
        if mensaje != "" {
            
            self.mensajeSimple(mensaje: mensaje)
            
            return false
        }
        
        
        return true
    }
    
    @objc func btnLoginTapped(){
        if !self.validaForma() {
            return
        }
        
        self.showLoader()
        
        //Id de dispositivo en OneSignal
        let idDispositivo = PushNotifications.obtienePlayerID()
        let correo = self.txtEmail.text?.lowercased().trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let passwd = self.txtPassword.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        Functions().loginUserEmail(username: correo!,
                                   password: passwd!,
                                   idDispositivo: idDispositivo) { (exito, mensaje, error) in
                                    
                                    if let xerror = error {
                                        DDLogError(xerror.localizedDescription)
                                        self.hideLoader()
                                        self.mensajeSimple(mensaje: "Ocurrio un error al realizar el login de usuario")
                                    } else {
                                        if exito {
                                            Functions().backtoMain()
                                            self.hideLoader()
                                        } else {
                                            DDLogError(mensaje)
                                            self.hideLoader()
                                            self.mensajeSimple(mensaje: mensaje)
                                        }
                                    }
        }
    }
    
    func loginOrRegister(nombre: String,
                         apellidos: String,
                         correo: String,
                         password: String,
                         loginType: tipoRegistroLogin,
                         avatar: String,
                         origen: String = "") {
        
        DDLogInfo("Ingresando...")
        
        //Intenta Login.
        let idDispositivo = PushNotifications.obtienePlayerID()
        Functions().loginUserEmail(username: correo,
                                   password: password,
                                   idDispositivo: idDispositivo) { (exito, mensaje, error) in
                                    
                                    if let xerror = error {
                                        DDLogError(xerror.localizedDescription)
                                        self.hideLoader()
                                        self.mensajeSimple(mensaje: "Ocurrio un error al realizar el login de usuario")
                                    } else {
                                        if exito {
                                            self.hideLoader()
                                            Functions().backtoMain()
                                        } else {
                                            DDLogError(mensaje)
                                            //Usuario no existe, intenta crearlo.
                                            Functions().registerUser(rnombre: nombre,
                                                                     rapellidos: apellidos,
                                                                     rcorreo: correo,
                                                                     rpasswd: password,
                                                                     rtipo: loginType,
                                                                     avatar: avatar) { (exito, mensaje, error) in
                                                                        
                                                                        if let xerror = error {
                                                                            DDLogError(xerror.localizedDescription)
                                                                            self.hideLoader()
                                                                            self.mensajeSimple(mensaje: "Ocurrio un error realizar el registro de usuario")
                                                                        } else {
                                                                            if exito {
                                                                                DDLogInfo("Usuario creado correctamente!")
                                                                                Functions().backtoMain()
                                                                                self.hideLoader()
                                                                                
                                                                            }
                                                                            else {
                                                                                DDLogError(mensaje)
                                                                                self.hideLoader()
                                                                                self.mensajeSimple(mensaje: mensaje)
                                                                            }
                                                                        }
                                            }
                                        }
                                    }
        }
    }
    
    @objc func seleccionaBotonLoginFacebook() {
        DDLogInfo("Intenta login con Facebook...")
        showLoader()
        
        //cerrar cualquier sesion abierta anteriormente
        Functions().borraTodoCierraSesion()
        
        let lmanager = LoginManager()
        lmanager.logOut()
        
        lmanager.logIn(readPermissions:[.publicProfile, .email], viewController: self) { loginResult in
            switch loginResult {
            case .failed(let error):
                DDLogError(error.localizedDescription)
                self.hideLoader()
                self.mensajeSimple(mensaje: "Ocurrió un error en la comunicación con Facebook")
                
            case .cancelled:
                self.hideLoader()
                DDLogInfo("User cancelled login")
            case .success(let grantedPermissions, let declinedPermissions, _):
                DDLogInfo("Login on Facebook, with success")
                if declinedPermissions.contains("email") {
                    self.hideLoader()
                }
                if grantedPermissions.contains("email"){
                    
                    self.getFacebookProfileInfo()
                    
                } else {
                    self.hideLoader()
                    self.mensajeSimple(mensaje: "No ha otorgado los permisos a la aplicación", dismiss: false)
                }
            }
        }
    }
    
    @objc func seleccionaBotonLoginGoogle() {
        DDLogInfo("Intenta login con Google...")
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        
        GIDSignIn.sharedInstance().signIn()
    }
    
    @objc func seleccionaBotonRecuperarContrasenia() {
        let recuperarVC = RecuperarContraseniaVC()
        self.present(recuperarVC, animated: true, completion: nil)
    }
    
    //MARK: - Google SignIn
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        DDLogInfo("Ingresando...")
        
        if (error == nil) {
            let userId = user.userID                  // For client-side use only!
            let givenName = user.profile.givenName
            let familyName = user.profile.familyName
            let email = user.profile.email
            let avatar = user.profile.imageURL(withDimension: 250).absoluteString
            
            self.loginOrRegister(nombre: givenName!,
                                 apellidos: familyName!,
                                 correo: email!,
                                 password: userId!,
                                 loginType: tipoRegistroLogin.google,
                                 avatar: avatar)
            
        } else {
            DDLogError(error.localizedDescription)
        }
    }
    
    //MARK: - Google SignInUI
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        showLoader()
        self.present(viewController, animated: true) {
            self.hideLoader()
        }
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        showLoader()
        self.dismiss(animated: true) {
            self.hideLoader()
        }
    }
    
    //MARK: - Facebook
    func getFacebookProfileInfo() {
        let graphRequest = GraphRequest(graphPath: "/me",
                                        parameters: ["fields":"id, last_name, first_name, email, picture.type(large)"],
                                        accessToken: AccessToken.current,
                                        httpMethod: .GET,
                                        apiVersion: .defaultVersion)
        
        graphRequest.start { (response, result) in
            switch result {
            case .success(let response):
                
                if let dictionary = response.dictionaryValue {
                    var userId = ""
                    var firstName = ""
                    var lastName = ""
                    var email = ""
                    var picture = ""
                    
                    if let value = dictionary["id"] as? String {
                        userId = value
                    }
                    
                    if let value = dictionary["first_name"] as? String {
                        firstName = value
                    }
                    
                    if let value = dictionary["last_name"] as? String {
                        lastName = value
                    }
                    
                    if let value = dictionary["email"] as? String {
                        email = value
                    }
                    
                    if let value = dictionary["picture"] as? NSDictionary,
                        let data = value["data"] as? NSDictionary,
                        let url = data["url"] as? String {
                        picture = url
                    }
                    
                    DDLogVerbose(userId)
                    DDLogVerbose(firstName)
                    DDLogVerbose(lastName)
                    DDLogVerbose(email)
                    DDLogVerbose(picture)
                    
                    self.loginOrRegister(nombre: firstName,
                                         apellidos: lastName,
                                         correo: email,
                                         password: userId,
                                         loginType: tipoRegistroLogin.facebook,
                                         avatar: picture)
                }
                
            case .failed(let error):
                DDLogError("Error al obtener datos de usuario en Facebook Graph: \(error.localizedDescription)")
            }
        }
    }
    
    
    
    
    
    
    
}
