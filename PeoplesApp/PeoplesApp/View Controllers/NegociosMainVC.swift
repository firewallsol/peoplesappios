//
//  NegociosMainVC.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 02/08/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import Kingfisher
import CoreLocation

class NegociosMainVC: UIViewController, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate  {
    
    var tableNegocios : UITableView!
    var topContainer : UIView!
    var btnAfilia : UIView!
    
    var nombreCeldaNegocios = "celdaNegocios"
    var bottomPadding : CGFloat = 0
    var arrayEmpresas = [Empresa]()
    var showLoaderBool = true
    var lblNegociosCerca : UILabel!
    
    var locationManager = CLLocationManager()

    //MARK: - View
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Inicializa CoreLocation.
        self.configuraCoreLocation()
        
        self.automaticallyAdjustsScrollViewInsets = false
        self.view.backgroundColor = UIColor.white
        self.buildInterface()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setBarMenu()
        
        if self.showLoaderBool {
            self.showLoader()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationItem.title = ""
    }
    
    func setNegociosLabelData(){
        self.lblNegociosCerca.text = "\(self.arrayEmpresas.count) negocios cerca de ti"
        self.lblNegociosCerca.sizeToFit()
        self.lblNegociosCerca.frame.origin.x = Constantes.tamaños.anchoPantalla - (self.lblNegociosCerca.frame.size.width + 10)
        self.lblNegociosCerca.center.y = self.topContainer.frame.size.height / 2
    }
    
    func buildInterface(){
        var xframe = CGRect.zero
        
        //top container
        xframe.origin = CGPoint(x: 0, y: 0)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: Constantes.tamaños.altoBotonTop)
        self.topContainer = UIView(frame: xframe)
        self.view.addSubview(self.topContainer)
        
        //label
        self.lblNegociosCerca = UILabel(frame: xframe)
        self.lblNegociosCerca.text = "\(self.arrayEmpresas.count) negocios cerca de ti"
        self.lblNegociosCerca.font = Constantes.currentFonts.NunitoRegular.base
        self.lblNegociosCerca.textColor = Constantes.paletaColores.verdePeoples
        self.lblNegociosCerca.sizeToFit()
        self.topContainer.addSubview(self.lblNegociosCerca)
        self.lblNegociosCerca.frame.origin.x = Constantes.tamaños.anchoPantalla - (self.lblNegociosCerca.frame.size.width + 10)
        self.lblNegociosCerca.center.y = self.topContainer.frame.size.height / 2
        
        //table negocios
        xframe.origin = CGPoint(x: 0, y: self.topContainer.frame.maxY)
        let altoTable = Constantes.tamaños.altoPantalla - Constantes.tamaños.altoBotonTop
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: altoTable)
        self.tableNegocios = UITableView(frame: xframe)
        self.tableNegocios.delegate = self
        self.tableNegocios.dataSource = self
        self.tableNegocios.register(CeldaTVNegociosMain.self, forCellReuseIdentifier: self.nombreCeldaNegocios)
        self.tableNegocios.tableFooterView = UIView(frame: CGRect.zero)
        self.view.addSubview(self.tableNegocios)
        
        //boton
        self.bottomPadding = (Constantes.tamaños.altoBotonTop + Constantes.tamaños.altoStatusBar) + (self.navigationController?.navigationBar.frame.size.height)!
        
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: Constantes.tamaños.altoBotonTop)
        xframe.origin = CGPoint(x: 0, y: Constantes.tamaños.altoPantalla - bottomPadding)
        self.btnAfilia = UIView(frame: xframe)
        
        self.btnAfilia.backgroundColor = Constantes.paletaColores.verdePeoples
        self.view.addSubview(self.btnAfilia)
        let lblBtnafiliar = UILabel(frame: xframe)
        lblBtnafiliar.text = NSLocalizedString("Afilia un negocio", comment: "").uppercased()
        lblBtnafiliar.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_1
        lblBtnafiliar.sizeToFit()
        lblBtnafiliar.textColor = UIColor.white
        self.btnAfilia.addSubview(lblBtnafiliar)
        lblBtnafiliar.center.x = Constantes.tamaños.anchoPantalla / 2
        lblBtnafiliar.center.y = self.btnAfilia.frame.size.height / 2
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(seleccionaBotonAfiliarNegocio))
        lblBtnafiliar.addGestureRecognizer(tapGesture)
        lblBtnafiliar.isUserInteractionEnabled = true
        
        NSLog("termino el build interface")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Table view datasource y delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableView.validaExistenciaDatos(datos: self.arrayEmpresas as AnyObject)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayEmpresas.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constantes.tamaños.altoCeldaNegociosMain
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.nombreCeldaNegocios) as! CeldaTVNegociosMain
        cell.frame.size = CGSize(width: tableView.frame.size.width, height: cell.frame.size.height)
        let cellSizeHeight = cell.frame.size.height
        let cellSizeWidth = cell.frame.size.width
        
        let unNegocio = self.arrayEmpresas[indexPath.row]
        
        cell.imgNegocio.kf.setImage(with: unNegocio.imagen)
        
        
        cell.lblNombreNegocio.text = unNegocio.razonSocial
        let altolbl = cell.lblNombreNegocio.heightForView()
        cell.lblNombreNegocio.frame.size.height = altolbl
        
        cell.lblDirecNegocio.text = unNegocio.direcciones.first?.descripcion
        //let anchoLabelDirec = cellSizeWidth - (cell.imgNegocio.frame.size.width + 5)
        //cell.lblDirecNegocio.frame.size.width = anchoLabelDirec * 0.9
        cell.lblDirecNegocio.sizeToFit()
        cell.lblDirecNegocio.frame.origin.y = cell.lblNombreNegocio.frame.maxY + 3
        
        cell.lblCashBack.text = "Obtén Cash back"
        cell.lblCashBack.sizeToFit()
        var posycashback = cellSizeHeight - (cell.lblDirecNegocio.frame.maxY)
        posycashback = cell.lblDirecNegocio.frame.maxY + (posycashback / 2)
        cell.lblCashBack.center.y = posycashback
        
        cell.lblDistancia.text = "\(unNegocio.distancia!) km"
        cell.lblDistancia.sizeToFit()
        cell.lblDistancia.frame.origin.x = cellSizeWidth - (cell.lblDistancia.frame.size.width + 5)
        
        if Functions().existeIdEnFavoritos(id: unNegocio.id, donde: .empresa) {
            cell.imgLike.image = Asset.Icons.icFav.image
            cell.imgLike.tintColor = .red
        }
        
        cell.imgShare.frame.origin.x = cellSizeWidth - (cell.imgShare.frame.size.width + 5)
        cell.imgShare.isUserInteractionEnabled = true
        cell.imgShare.tag = indexPath.row
        let shareTap = UITapGestureRecognizer(target: self, action: #selector(seleccionaBotonCompartir(sender:)))
        cell.imgShare.addGestureRecognizer(shareTap)
        
        cell.imgLike.frame.origin.x = cell.imgShare.frame.minX - (cell.imgLike.frame.size.width + 5)
        cell.imgLike.tag = indexPath.row
        let likeTap = UITapGestureRecognizer(target: self, action: #selector(self.likePressed(sender:)))
        cell.imgLike.isUserInteractionEnabled = true
        cell.imgLike.addGestureRecognizer(likeTap)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let elVC = DetalleComercioVC()
        elVC.currentEmpresa = self.arrayEmpresas[indexPath.row]
        self.navigationController?.pushViewController(elVC, animated: true)
    }
    
    //table view datasource y delegate - fin
    
    @objc func likePressed(sender: UITapGestureRecognizer){
        
        let tagSender = (sender.view?.tag)!
        let unNegocio = self.arrayEmpresas[tagSender]
        
        if Functions().isUserLoggedIn() {
            let mensaje = Functions().agregarQuitarFavorito(id: unNegocio.id, elJson: unNegocio.jsonData, donde: .empresa)
            self.mensajeSimple(mensaje: mensaje)
            let xindexPath = IndexPath(row: tagSender, section: 0)
            self.tableNegocios.reloadRows(at: [xindexPath], with: UITableViewRowAnimation.automatic)
        }
    }

    //MARK: - Location Manager
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //
        print("UPDATE LOCATION")
        let location = locations.last
        manager.stopUpdatingLocation()
        
        
        let latitude = (location?.coordinate.latitude.description)!
        let longitude = (location?.coordinate.longitude.description)!
        
        print(latitude)
        print(longitude)
        
        actualizarUbicacion(latitud: latitude, longitud: longitude)
    }

    //MARK: - Custom
    func configuraCoreLocation() {
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
    }
    
    func actualizarUbicacion(latitud: String, longitud: String) {
        
        Functions().getMainData(latitud: latitud, longitud: longitud) {
            exito, categorias, negocios, profesionales, error in
            self.showLoaderBool = false
            
            if self.isLoaderAnimating() {
                self.hideLoader()
            }
            
            if let xerror = error {
                print("GET datos main - Ocurrio un error \(xerror)")
                self.mensajeSimple(mensaje: "Error de red", dismiss: false)
            }
            else {
                if exito {
                    self.arrayEmpresas = negocios
                    
                    print("NEGOCIOS: \(negocios.count)")
                    
                    DispatchQueue.main.async(execute: {
                        self.tableNegocios.reloadData()
                        self.setNegociosLabelData()
                        
                        self.tableNegocios.contentSize.height += self.bottomPadding + 5
                        self.btnAfilia.frame.origin.y = Constantes.tamaños.altoPantalla - self.bottomPadding
                    })
                }
                else {
                    self.mensajeSimple(mensaje: "Error desde servicio", dismiss: false)
                }
            }
        }
    }
    
    @objc func seleccionaBotonCompartir(sender: UITapGestureRecognizer) {
        let tagSender = (sender.view?.tag)!
        let unNegocio = self.arrayEmpresas[tagSender]
        
        let cell = tableNegocios.cellForRow(at: IndexPath(row: tagSender, section: 0)) as! CeldaTVNegociosMain
        
        //        self.showLoader()
        //        DispatchQueue.global(qos:.userInteractive).async {
        var imagen = UIImage()
        var finalString = String()
        
        if let image = cell.imgNegocio.image {
            imagen = image
        }
        
        if let xnombre = unNegocio.razonSocial {
            finalString += "\(xnombre)\n"
        }
        
        if let xdireccion = unNegocio.direcciones.first?.descripcion {
            finalString += "\(xdireccion)"
        }
        
        let activityView = UIActivityViewController(activityItems: [imagen, finalString],
                                                    applicationActivities: nil)
        
        activityView.excludedActivityTypes = [.airDrop, .openInIBooks,
                                              .print, .addToReadingList,
                                              .postToFlickr, .postToVimeo,
                                              .postToWeibo, .postToTencentWeibo]
        
        //            DispatchQueue.main.async {
        self.present(activityView, animated: true)
        //                {
        //                    self.hideLoader()
        //                }
        //            }
        //        }
    }
    
    @objc func seleccionaBotonAfiliarNegocio() {
        if Functions().isUserLoggedIn() {
            let viewController = AfiliarNegocioVC()
            self.navigationController?.pushViewController(viewController, animated: true)
        } else {
            self.mensajeSimple(mensaje: NSLocalizedString("Se necesita hacer login a People's App", comment: ""))
        }
    }


}
