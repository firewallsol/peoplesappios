//
//  AyudaVC.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 11/09/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit

class AyudaVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    var tableAyuda : UITableView!
    
    var preguntasFrecCell = UITableViewCell()
    var llamanosCell = UITableViewCell()
    var emailCell = UITableViewCell()
    var facebookCell = UITableViewCell()
    var instagramCell = UITableViewCell()
    var twitterCell = UITableViewCell()
    var terminosCondCell = UITableViewCell()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor.white
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        self.buildInterface()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "Ayuda"
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationItem.title = ""
    }
    
    func buildInterface(){
        var xframe = CGRect.zero
        //tableview
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: 300)
        self.tableAyuda = UITableView(frame: xframe)
        self.tableAyuda.backgroundColor = UIColor.white
        self.tableAyuda.delegate = self
        self.tableAyuda.dataSource = self
        self.tableAyuda.isScrollEnabled = false
        self.tableAyuda.tableFooterView = UIView(frame: CGRect.zero)
        
        //preguntas frec
        self.preguntasFrecCell.textLabel?.text = "Preguntas frecuentes"
        self.preguntasFrecCell.textLabel?.textColor = Constantes.paletaColores.verdePeoples
        self.preguntasFrecCell.imageView?.image = Functions().scaleImageToSize(sourceImage: Asset.Icons.payPal.image, scaledToSize: CGSize(width: 20, height: 20))
        self.preguntasFrecCell.textLabel?.font = Constantes.currentFonts.NunitoRegular.base
        
        self.preguntasFrecCell.imageView?.contentMode = .scaleAspectFill
        self.preguntasFrecCell.imageView?.clipsToBounds = true
        self.preguntasFrecCell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        self.preguntasFrecCell.selectionStyle = .none
        
        //llamanos
        self.llamanosCell.textLabel?.text = "Llámanos"
        self.llamanosCell.textLabel?.textColor = Constantes.paletaColores.verdePeoples
        self.llamanosCell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        self.llamanosCell.selectionStyle = .none
        self.llamanosCell.imageView?.image = Functions().scaleImageToSize(sourceImage: Asset.Icons.icClock48.image, scaledToSize: CGSize(width: 20, height: 20))
        self.llamanosCell.textLabel?.font = Constantes.currentFonts.NunitoRegular.base
        
        self.llamanosCell.imageView?.contentMode = .scaleAspectFill
        self.llamanosCell.imageView?.clipsToBounds = true
        
        //email
        self.emailCell.textLabel?.text = "Escríbenos por E-mail"
        self.emailCell.textLabel?.textColor = Constantes.paletaColores.verdePeoples
        self.emailCell.imageView?.backgroundColor = UIColor.black
        self.emailCell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        self.emailCell.selectionStyle = .none
        self.emailCell.imageView?.image = Functions().scaleImageToSize(sourceImage: Asset.Icons.payPal.image, scaledToSize: CGSize(width: 20, height: 20))
        self.emailCell.textLabel?.font = Constantes.currentFonts.NunitoRegular.base
        
        self.emailCell.imageView?.contentMode = .scaleAspectFill
        self.emailCell.imageView?.clipsToBounds = true
        
        //facebook
        self.facebookCell.textLabel?.text = "Escríbenos por Facebook"
        self.facebookCell.textLabel?.textColor = Constantes.paletaColores.verdePeoples
        self.facebookCell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        self.facebookCell.selectionStyle = .none
        self.facebookCell.imageView?.image = Functions().scaleImageToSize(sourceImage: Asset.Icons.icClock48.image, scaledToSize: CGSize(width: 20, height: 20))
        self.facebookCell.textLabel?.font = Constantes.currentFonts.NunitoRegular.base
        
        self.facebookCell.imageView?.contentMode = .scaleAspectFill
        self.facebookCell.imageView?.clipsToBounds = true
        
        
        //instagram
        self.instagramCell.textLabel?.text = "Escríbenos por Instagram"
        self.instagramCell.textLabel?.textColor = Constantes.paletaColores.verdePeoples
        self.instagramCell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        self.instagramCell.selectionStyle = .none
        self.instagramCell.imageView?.image = Functions().scaleImageToSize(sourceImage: Asset.Icons.payPal.image, scaledToSize: CGSize(width: 20, height: 20))
        self.instagramCell.textLabel?.font = Constantes.currentFonts.NunitoRegular.base
        
        self.instagramCell.imageView?.contentMode = .scaleAspectFill
        self.instagramCell.imageView?.clipsToBounds = true
        
        //twitter
        self.twitterCell.textLabel?.text = "Escríbenos por Twitter"
        self.twitterCell.textLabel?.textColor = Constantes.paletaColores.verdePeoples
        self.twitterCell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        self.twitterCell.selectionStyle = .none
        self.twitterCell.imageView?.image = Functions().scaleImageToSize(sourceImage: Asset.Icons.icClock48.image, scaledToSize: CGSize(width: 20, height: 20))
        self.twitterCell.textLabel?.font = Constantes.currentFonts.NunitoRegular.base
        
        self.twitterCell.imageView?.contentMode = .scaleAspectFill
        self.twitterCell.imageView?.clipsToBounds = true
        
        //terminos
        self.terminosCondCell.textLabel?.text = "Términos y condiciones de uso"
        self.terminosCondCell.textLabel?.textColor = Constantes.paletaColores.verdePeoples
        self.terminosCondCell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        self.terminosCondCell.selectionStyle = .none
        self.terminosCondCell.imageView?.image = Functions().scaleImageToSize(sourceImage: Asset.Icons.payPal.image, scaledToSize: CGSize(width: 20, height: 20))
        self.terminosCondCell.textLabel?.font = Constantes.currentFonts.NunitoRegular.base
        
        self.terminosCondCell.imageView?.contentMode = .scaleAspectFill
        self.terminosCondCell.imageView?.clipsToBounds = true
        
        
        self.view.addSubview(self.tableAyuda)
        
        self.tableAyuda.reloadData()
        self.tableAyuda.layoutIfNeeded()
    }
    
    //table view delegate datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0: return self.preguntasFrecCell
        case 1: return self.llamanosCell
        case 2: return self.emailCell
        case 3: return self.facebookCell
        case 4: return self.instagramCell
        case 5: return self.twitterCell
        case 6: return self.terminosCondCell
        default: fatalError("La celda no corresponde")
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0: break; //preguntas
        case 1: break; //llamanos
        case 2: break; //email
        case 3: break; //facebook
        case 4: break; //instagram
        case 5: break; //twitter
        case 6: break; //terminos
        default:
            break
        }
    }
    
    //table view delegate datasource - fin

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
