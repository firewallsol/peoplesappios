//
//  AfiliateDocumentosVC.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 14/09/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import BEMCheckBox
import TPPDF
import IRLDocumentScanner

class AfiliateDocumentosVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, IRLScannerViewControllerDelegate {
    
    var scrollView : UIScrollView!
    
    var chkId : BEMCheckBox!
    var chkDom : BEMCheckBox!
    var chkRfc : BEMCheckBox!
    var chkPaypal : BEMCheckBox!
    
    var btnFotoId : UIImageView!
    var btnFotoDom : UIImageView!
    var btnFotoRfc : UIImageView!
    var btnAddPaypal : UIImageView!
    
    var btnQID : UIImageView!
    var btnQDom : UIImageView!
    var btnQRfc : UIImageView!
    var btnQPaypal : UIImageView!
    
    var img1 : UIImageView!
    var img2 : UIImageView!
    var img3 : UIImageView!
    
    var lblImg1 : UILabel!
    var lblImg2 : UILabel!
    var lblImg3 : UILabel!
    var btnEnviarDatos : UIView!

    var imgsOriginales = [Int : UIImage?]()
    
    
    
    var datosUsuario : Usuario!
    var codigoAfiliador: String!
    var currentImgSender = 0

    //MARK: - View
    override func viewDidLoad() {
        super.viewDidLoad()

        self.automaticallyAdjustsScrollViewInsets = false
        self.view.backgroundColor = UIColor.white
        
        self.buildInterface()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationItem.title = "Afiliate - Paso 2"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationItem.title = ""
    }
    
    //MARK: - Events
    func buildInterface(){
        var xframe = CGRect.zero
        
        let checkAltoAncho = Constantes.tamaños.anchoPantalla * 0.06
        let anchoLabel = Constantes.tamaños.anchoPantalla * 0.55
        let anchoQMark = Constantes.tamaños.anchoPantalla * 0.06
        let altoAddPhoto = Constantes.tamaños.anchoPantalla * 0.09
        let posXAddPhoto = Constantes.tamaños.anchoPantalla - (altoAddPhoto + 10)
        
        self.scrollView = UIScrollView(frame: self.view.frame)
        
        //titulo
        xframe.origin = CGPoint(x: 10, y: 10)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla * 0.95, height: 30)
        let parrafo1 = UILabel(frame: xframe)
        parrafo1.numberOfLines = 0
        parrafo1.textAlignment = .justified
        parrafo1.text = FWS.lblUploadDocs
        parrafo1.font = Constantes.currentFonts.NunitoBold.base
        parrafo1.sizeToFit()
        parrafo1.center.x = Constantes.tamaños.centroHorizontal
        self.scrollView.addSubview(parrafo1)
        
        //identificacion
        //check
        xframe.origin = CGPoint(x: 10, y: parrafo1.frame.maxY + 20)
        xframe.size = CGSize(width: checkAltoAncho, height: checkAltoAncho)
        self.chkId = BEMCheckBox(frame: xframe)
        self.chkId.tintColor = Constantes.paletaColores.verdePeoples
        self.chkId.onCheckColor = UIColor.white
        self.chkId.onFillColor = Constantes.paletaColores.verdePeoples
        self.chkId.onTintColor = Constantes.paletaColores.verdePeoples
        self.chkId.onAnimationType = .fill
        self.chkId.offAnimationType = .fill
        self.chkId.isUserInteractionEnabled = false
        self.chkId.on = false
        self.scrollView.addSubview(self.chkId)
        
        //label
        xframe.origin = CGPoint(x: self.chkId.frame.maxX + 10, y: parrafo1.frame.maxY + 20)
        xframe.size = CGSize(width: anchoLabel, height: 25)
        let lblId = UILabel(frame: xframe)
        lblId.text = FWS.lblJoinIdDoc
        lblId.font = Constantes.currentFonts.NunitoRegular.base
        lblId.adjustsFontSizeToFitWidth = true
        lblId.center.y = self.chkId.frame.minY + (self.chkId.frame.size.height / 2)
        self.scrollView.addSubview(lblId)
        lblId.sizeToFit()
        
        //q mark
        xframe.origin = CGPoint(x: lblId.frame.maxX + 10, y: parrafo1.frame.maxY + 20)
        xframe.size = CGSize(width: anchoQMark, height: anchoQMark)
        self.btnQID = UIImageView(frame: xframe)
        self.btnQID.image = Asset.Icons.icQuestion64.image
        self.btnQID.contentMode = .scaleAspectFill
        self.scrollView.addSubview(self.btnQID)
        self.btnQID.clipsToBounds = true
        self.btnQID.center.y = lblId.center.y
        
        
        //add photo
        
        xframe.origin = CGPoint(x: posXAddPhoto, y: parrafo1.frame.maxY + 20)
        xframe.size = CGSize(width: altoAddPhoto, height: altoAddPhoto)
        self.btnFotoId = UIImageView(frame: xframe)
        self.btnFotoId.image = Asset.Icons.icCameraPlus64.image
        self.btnFotoId.contentMode = .scaleAspectFill
        self.scrollView.addSubview(self.btnFotoId)
        self.btnFotoId.center.y = self.btnQID.center.y
        self.btnFotoId.tag = 1
        self.btnFotoId.isUserInteractionEnabled = true
        let fotoIdTap = UITapGestureRecognizer(target: self, action: #selector(self.abreGaleria(sender:)))
        self.btnFotoId.addGestureRecognizer(fotoIdTap)
        
        
        //comprobante
        //check
        xframe.origin = CGPoint(x: 10, y: self.chkId.frame.maxY + 20)
        xframe.size = CGSize(width: checkAltoAncho, height: checkAltoAncho)
        self.chkDom = BEMCheckBox(frame: xframe)
        self.chkDom.tintColor = Constantes.paletaColores.verdePeoples
        self.chkDom.onCheckColor = UIColor.white
        self.chkDom.onFillColor = Constantes.paletaColores.verdePeoples
        self.chkDom.onTintColor = Constantes.paletaColores.verdePeoples
        self.chkDom.onAnimationType = .fill
        self.chkDom.offAnimationType = .fill
        self.chkDom.isUserInteractionEnabled = false
        self.chkDom.on = false
        self.scrollView.addSubview(self.chkDom)
        
        //label
        xframe.origin = CGPoint(x: self.chkDom.frame.maxX + 10, y: self.chkId.frame.maxY + 20)
        xframe.size = CGSize(width: anchoLabel, height: 25)
        let lblDom = UILabel(frame: xframe)
        lblDom.text = FWS.lblJoinProofOfAddres
        lblDom.font = Constantes.currentFonts.NunitoRegular.base
        lblDom.adjustsFontSizeToFitWidth = true
        lblDom.center.y = self.chkDom.frame.minY + (self.chkDom.frame.size.height / 2)
        self.scrollView.addSubview(lblDom)
        lblDom.sizeToFit()
        
        //q mark
        xframe.origin = CGPoint(x: lblDom.frame.maxX + 10, y: self.chkId.frame.maxY + 20)
        xframe.size = CGSize(width: anchoQMark, height: anchoQMark)
        self.btnQDom = UIImageView(frame: xframe)
        self.btnQDom.image = Asset.Icons.icQuestion64.image
        self.btnQDom.contentMode = .scaleAspectFill
        self.scrollView.addSubview(self.btnQDom)
        self.btnQDom.clipsToBounds = true
        self.btnQDom.center.y = lblDom.center.y
        
        
        //add photo
        xframe.origin = CGPoint(x: posXAddPhoto, y: self.chkId.frame.maxY + 20)
        xframe.size = CGSize(width: altoAddPhoto, height: altoAddPhoto)
        self.btnFotoDom = UIImageView(frame: xframe)
        self.btnFotoDom.image = Asset.Icons.icCameraPlus64.image
        self.btnFotoDom.contentMode = .scaleAspectFill
        self.scrollView.addSubview(self.btnFotoDom)
        self.btnFotoDom.center.y = self.btnQDom.center.y
        self.btnFotoDom.isUserInteractionEnabled = true
        self.btnFotoDom.tag = 2
        let fotoDomTap = UITapGestureRecognizer(target: self, action: #selector(self.abreGaleria(sender:)))
        self.btnFotoDom.addGestureRecognizer(fotoDomTap)
        
        
        //rfc
        //check
        xframe.origin = CGPoint(x: 10, y: self.chkDom.frame.maxY + 20)
        xframe.size = CGSize(width: checkAltoAncho, height: checkAltoAncho)
        self.chkRfc = BEMCheckBox(frame: xframe)
        self.chkRfc.tintColor = Constantes.paletaColores.verdePeoples
        self.chkRfc.onCheckColor = UIColor.white
        self.chkRfc.onFillColor = Constantes.paletaColores.verdePeoples
        self.chkRfc.onTintColor = Constantes.paletaColores.verdePeoples
        self.chkRfc.onAnimationType = .fill
        self.chkRfc.offAnimationType = .fill
        self.chkRfc.isUserInteractionEnabled = false
        self.chkRfc.on = false
        self.scrollView.addSubview(self.chkRfc)
        
        //label
        xframe.origin = CGPoint(x: self.chkRfc.frame.maxX + 10, y: self.chkDom.frame.maxY + 20)
        xframe.size = CGSize(width: anchoLabel, height: 25)
        let lblRfc = UILabel(frame: xframe)
        lblRfc.text = FWS.lblJoinRFC
        lblRfc.font = Constantes.currentFonts.NunitoRegular.base
        lblRfc.adjustsFontSizeToFitWidth = true
        lblRfc.center.y = self.chkRfc.frame.minY + (self.chkRfc.frame.size.height / 2)
        self.scrollView.addSubview(lblRfc)
        lblRfc.sizeToFit()
        
        //q mark
        xframe.origin = CGPoint(x: lblRfc.frame.maxX + 10, y: self.chkDom.frame.maxY + 20)
        xframe.size = CGSize(width: anchoQMark, height: anchoQMark)
        self.btnQRfc = UIImageView(frame: xframe)
        self.btnQRfc.image = Asset.Icons.icQuestion64.image
        self.btnQRfc.contentMode = .scaleAspectFill
        self.scrollView.addSubview(self.btnQRfc)
        self.btnQRfc.clipsToBounds = true
        self.btnQRfc.center.y = lblRfc.center.y
        
        //add photo
        xframe.origin = CGPoint(x: posXAddPhoto, y: self.chkDom.frame.maxY + 20)
        xframe.size = CGSize(width: altoAddPhoto, height: altoAddPhoto)
        self.btnFotoRfc = UIImageView(frame: xframe)
        self.btnFotoRfc.image = Asset.Icons.icCameraPlus64.image
        self.btnFotoRfc.contentMode = .scaleAspectFill
        self.scrollView.addSubview(self.btnFotoRfc)
        self.btnFotoRfc.center.y = self.btnQRfc.center.y
        self.btnFotoRfc.isUserInteractionEnabled = true
        self.btnFotoRfc.tag = 3
        let fotoRfcTap = UITapGestureRecognizer(target: self, action: #selector(self.abreGaleria(sender:)))
        self.btnFotoRfc.addGestureRecognizer(fotoRfcTap)
        
        
        //paypal
        //check
        xframe.origin = CGPoint(x: 10, y: self.chkRfc.frame.maxY + 20)
        xframe.size = CGSize(width: checkAltoAncho, height: checkAltoAncho)
        self.chkPaypal = BEMCheckBox(frame: xframe)
        self.chkPaypal.tintColor = Constantes.paletaColores.verdePeoples
        self.chkPaypal.onCheckColor = UIColor.white
        self.chkPaypal.onFillColor = Constantes.paletaColores.verdePeoples
        self.chkPaypal.onTintColor = Constantes.paletaColores.verdePeoples
        self.chkPaypal.onAnimationType = .fill
        self.chkPaypal.offAnimationType = .fill
        self.chkPaypal.isUserInteractionEnabled = false
        self.chkPaypal.on = false
        self.scrollView.addSubview(self.chkPaypal)
        
        //label
        xframe.origin = CGPoint(x: self.chkPaypal.frame.maxX + 10, y: self.chkRfc.frame.maxY + 20)
        xframe.size = CGSize(width: anchoLabel, height: 25)
        let lblPayPal = UILabel(frame: xframe)
        lblPayPal.text = FWS.lblJoinAddPaypalAccount
        lblPayPal.font = Constantes.currentFonts.NunitoRegular.base
        lblPayPal.adjustsFontSizeToFitWidth = true
        lblPayPal.center.y = self.chkPaypal.frame.minY + (self.chkPaypal.frame.size.height / 2)
        self.scrollView.addSubview(lblPayPal)
        lblPayPal.sizeToFit()
        
        //q mark
        xframe.origin = CGPoint(x: lblPayPal.frame.maxX + 10, y: self.chkRfc.frame.maxY + 20)
        xframe.size = CGSize(width: anchoQMark, height: anchoQMark)
        self.btnQPaypal = UIImageView(frame: xframe)
        self.btnQPaypal.image = Asset.Icons.icQuestion64.image
        self.btnQPaypal.contentMode = .scaleAspectFill
        self.scrollView.addSubview(self.btnQPaypal)
        self.btnQPaypal.clipsToBounds = true
        self.btnQPaypal.center.y = lblPayPal.center.y
        
        //add paypal
        xframe.origin = CGPoint(x: posXAddPhoto, y: self.chkRfc.frame.maxY + 20)
        xframe.size = CGSize(width: altoAddPhoto, height: altoAddPhoto)
        self.btnAddPaypal = UIImageView(frame: xframe)
        self.btnAddPaypal.image = Asset.Icons.payPal.image
        self.btnAddPaypal.contentMode = .scaleAspectFill
        self.scrollView.addSubview(self.btnAddPaypal)
        self.btnAddPaypal.center.y = self.btnQPaypal.center.y
        let tapPayPal = UITapGestureRecognizer(target: self, action: #selector(self.addPayPal))
        self.btnAddPaypal.isUserInteractionEnabled = true
        self.btnAddPaypal.addGestureRecognizer(tapPayPal)
        
        //image views
        self.addImgsViews()
        
        self.view.addSubview(self.scrollView)
        
        //continuar
        let padding = (Constantes.tamaños.altoBotonTop + (self.navigationController?.navigationBar.frame.size.height)!) + Constantes.tamaños.altoStatusBar
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: Constantes.tamaños.altoBotonTop)
        xframe.origin = CGPoint(x: 0, y: Constantes.tamaños.altoPantalla - padding)
        self.btnEnviarDatos = UIView(frame: xframe)
        self.btnEnviarDatos.backgroundColor = Constantes.paletaColores.verdePeoples
        self.view.addSubview(self.btnEnviarDatos)
        
        let lblBtnCont = UILabel(frame: xframe)
        lblBtnCont.text = "CONTINUAR"
        lblBtnCont.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_1
        lblBtnCont.textColor = UIColor.white
        self.btnEnviarDatos.addSubview(lblBtnCont)
        lblBtnCont.sizeToFit()
        lblBtnCont.center.x = Constantes.tamaños.anchoPantalla / 2
        lblBtnCont.center.y = btnEnviarDatos.frame.size.height / 2
        let tapBtnSend = UITapGestureRecognizer(target: self, action: #selector(self.sendData))
        self.btnEnviarDatos.addGestureRecognizer(tapBtnSend)
        self.btnEnviarDatos.isUserInteractionEnabled = true
        
        self.scrollView.contentSize.height = self.img1.frame.maxY + padding + (Constantes.tamaños.anchoPantalla * 0.1)
        
    }
    
    func isFormValid()->Bool{
        var mensaje = ""
        
        //documento id
        if (!self.chkId.on){
            mensaje = FWS.alertSnapshotRequired(FWS.lblJoinIdDoc)
        }
        
        //comprobante dom
        if (mensaje == "" && !self.chkDom.on){
            mensaje = FWS.alertSnapshotRequired(FWS.lblJoinProofOfAddres)
        }

        
        //rfc
        if (mensaje == "" && !self.chkRfc.on){
            mensaje = FWS.alertSnapshotRequired(FWS.lblJoinRFC)
        }
        
        //paypal
        if (mensaje == "" && !self.chkPaypal.on){
            mensaje = FWS.alertFieldRequired(FWS.lblJoinAddPaypalAccount)
        }
        
        if mensaje != "" {
            
            self.mensajeSimple(mensaje: mensaje)
            
            return false
        }
        
        
        return true

    }
    
    @objc func sendData(){
        
        if !self.isFormValid(){
            return
        }
        
        self.showLoader()
        
        Functions().sendDataAfiliate(unUsuario: self.datosUsuario, codigoAfiliador: codigoAfiliador,
                                     urlPdf: self.generatePDF()){ exito, mensaje, error in
            self.hideLoader()
            if let xerror = error {
                print("envia docs afiliate - Ocurrio un error \(xerror)")
                self.mensajeSimple(mensaje: mensaje, dismiss: false)
            } else {
                if exito {
                    let alert = UIAlertController(title: FWS.appName,
                                                  message: mensaje,
                                                  preferredStyle: UIAlertControllerStyle.alert)
                    
                    let okButton = UIAlertAction(title: FWS.buttonOK, style: UIAlertActionStyle.default, handler: {(
                        alert: UIAlertAction!) in
                        Functions().backtoMain()
                    })
                    
                    alert.addAction(okButton)
                    
                    self.present(alert, animated: true, completion: nil)
                } else {
                    self.mensajeSimple(mensaje: mensaje, dismiss: false)
                }
            }
        }
        
    }
    
    func generatePDF()->URL{
        let pdf = PDFGenerator(format: .a4)
        pdf.addText(.headerCenter, text: "DOCUMENTACION PARA EL USUARIO: Inserte aqui su nombre de usuario")
        pdf.addText(.footerRight, text: "03/Octubre/2017 14:10 pm")
        
        pdf.addImage(image: self.imgsOriginales[1]!!)
        pdf.addText(.contentCenter, text: FWS.lblJoinIdDoc, lineSpacing: 0.5)
        pdf.createNewPage()
        pdf.addImage(image: self.imgsOriginales[2]!!)
        pdf.addText(.contentCenter, text: FWS.lblJoinProofOfAddres, lineSpacing: 0.5)
        pdf.createNewPage()
        pdf.addImage(image: self.imgsOriginales[3]!!)
        pdf.addText(.contentCenter, text: FWS.lblJoinRFC, lineSpacing: 0.5)
        return pdf.generatePDFfile("documentos")
    }
    
    func previewPDF(pdfPath: URL){
        let elVC = PDFViewer()
        elVC.loadurl(laUrl: pdfPath)
        self.navigationController?.pushViewController(elVC, animated: true)
        
    }
    
    func addImgsViews(){
        var xframe = CGRect.zero
        
        let anchoAltoImg = CGSize(width: Constantes.tamaños.anchoPantalla * 0.3, height: Constantes.tamaños.anchoPantalla * 0.3)
        let separacion = Constantes.tamaños.anchoPantalla * 0.025
        var origPoint = CGPoint(x: separacion, y: self.chkPaypal.frame.maxY + 35)
        
        
        xframe.origin = origPoint
        xframe.size = anchoAltoImg
        
        //img1 - ID
        self.img1 = UIImageView(frame: xframe)
        self.img1.layer.borderWidth = 0.7
        self.img1.layer.borderColor = Constantes.paletaColores.verdePeoples.cgColor
        self.img1.layer.cornerRadius = 10
        self.img1.setNeedsDisplay()
        self.img1.clipsToBounds = true
        self.img1.isUserInteractionEnabled = true
        
        //boton delete
        let anchoAltoBtnDel = self.img1.frame.size.width * 0.3
        //agregar el boton de delete
        xframe.size = CGSize(width: anchoAltoBtnDel, height: anchoAltoBtnDel)
        let xdelbtn = self.img1.frame.size.width - (anchoAltoBtnDel + 5)
        xframe.origin = CGPoint(x: xdelbtn, y: 5)
        let btnDelete = UIImageView(frame: xframe)
        btnDelete.image = Asset.Icons.icDelete48.image
        btnDelete.contentMode = .scaleToFill
        btnDelete.tag = 1
        let tapBtnDelete1 = UITapGestureRecognizer(target: self, action: #selector(self.deleteImage(sender:)))
        btnDelete.isUserInteractionEnabled = true
        btnDelete.addGestureRecognizer(tapBtnDelete1)
        self.img1.addSubview(btnDelete)
        self.scrollView.addSubview(self.img1)
        
        // label id
        xframe.origin = CGPoint(x: self.img1.frame.minX, y: self.img1.frame.maxY + 5)
        xframe.size = CGSize(width: self.img1.frame.size.width, height: 25)
        self.lblImg1 = UILabel(frame: xframe)
        self.lblImg1.text = "Identificacion"
        self.lblImg1.font = Constantes.currentFonts.NunitoRegular.base
        self.lblImg1.adjustsFontSizeToFitWidth = true
        self.lblImg1.textAlignment = .center
        self.scrollView.addSubview(self.lblImg1)
        
        //img 2 - dom
        origPoint = CGPoint(x: self.img1.frame.maxX + separacion, y: self.img1.frame.minY)
        xframe.origin = origPoint
        xframe.size = anchoAltoImg
        self.img2 = UIImageView(frame: xframe)
        self.img2.layer.borderWidth = 0.7
        self.img2.layer.borderColor = Constantes.paletaColores.verdePeoples.cgColor
        self.img2.layer.cornerRadius = 10
        self.img2.setNeedsDisplay()
        self.img2.clipsToBounds = true
        self.img2.isUserInteractionEnabled = true
        
        //agregar el boton de delete
        xframe.size = CGSize(width: anchoAltoBtnDel, height: anchoAltoBtnDel)
        let xdelbtn2 = self.img2.frame.size.width - (anchoAltoBtnDel + 5)
        xframe.origin = CGPoint(x: xdelbtn2, y: 5)
        let btnDelete2 = UIImageView(frame: xframe)
        btnDelete2.image = Asset.Icons.icDelete48.image
        btnDelete2.contentMode = .scaleToFill
        btnDelete2.tag = 2
        let tapBtnDelete2 = UITapGestureRecognizer(target: self, action: #selector(self.deleteImage(sender:)))
        btnDelete2.isUserInteractionEnabled = true
        btnDelete2.addGestureRecognizer(tapBtnDelete2)
        self.img2.addSubview(btnDelete2)
        self.scrollView.addSubview(self.img2)
        
        // label dom
        xframe.origin = CGPoint(x: self.img2.frame.minX, y: self.img2.frame.maxY + 5)
        xframe.size = CGSize(width: self.img2.frame.size.width, height: 25)
        self.lblImg2 = UILabel(frame: xframe)
        self.lblImg2.text = "Comprobante"
        self.lblImg2.font = Constantes.currentFonts.NunitoRegular.base
        self.lblImg2.textAlignment = .center
        self.lblImg2.adjustsFontSizeToFitWidth = true
        self.scrollView.addSubview(self.lblImg2)
        
        //img 3 - rfc
        origPoint = CGPoint(x: self.img2.frame.maxX + separacion, y: self.img1.frame.minY)
        xframe.origin = origPoint
        xframe.size = anchoAltoImg
        self.img3 = UIImageView(frame: xframe)
        self.img3.layer.borderWidth = 0.7
        self.img3.layer.borderColor = Constantes.paletaColores.verdePeoples.cgColor
        self.img3.layer.cornerRadius = 10
        self.img3.setNeedsDisplay()
        self.img3.clipsToBounds = true
        self.img3.isUserInteractionEnabled = true
        
        //agregar el boton de delete
        xframe.size = CGSize(width: anchoAltoBtnDel, height: anchoAltoBtnDel)
        let xdelbtn3 = self.img3.frame.size.width - (anchoAltoBtnDel + 5)
        xframe.origin = CGPoint(x: xdelbtn3, y: 5)
        let btnDelete3 = UIImageView(frame: xframe)
        btnDelete3.image = Asset.Icons.icDelete48.image
        btnDelete3.contentMode = .scaleToFill
        btnDelete3.tag = 3
        let tapBtnDelete3 = UITapGestureRecognizer(target: self, action: #selector(self.deleteImage(sender:)))
        btnDelete3.isUserInteractionEnabled = true
        btnDelete3.addGestureRecognizer(tapBtnDelete3)
        self.img3.addSubview(btnDelete3)
        self.scrollView.addSubview(self.img3)
        
        // label id
        xframe.origin = CGPoint(x: self.img3.frame.minX, y: self.img3.frame.maxY + 5)
        xframe.size = CGSize(width: self.img3.frame.size.width, height: 25)
        self.lblImg3 = UILabel(frame: xframe)
        self.lblImg3.text = "Registro"
        self.lblImg3.font = Constantes.currentFonts.NunitoRegular.base
        self.lblImg3.adjustsFontSizeToFitWidth = true
        self.lblImg3.textAlignment = .center
        self.scrollView.addSubview(self.lblImg3)
        
        self.img1.isHidden = true
        self.img2.isHidden = true
        self.img3.isHidden = true
        
        self.lblImg1.isHidden = true
        self.lblImg2.isHidden = true
        self.lblImg3.isHidden = true
    }
    
    //MARK: - Image Picker
    @objc func abreGaleria(sender: UITapGestureRecognizer){
        showLoader()
        self.currentImgSender = (sender.view?.tag)!
        let camera = DSCameraHandler(delegate_: self)
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        optionMenu.popoverPresentationController?.sourceView = self.view
        
        let takePhoto = UIAlertAction(title: "Take Photo", style: .default) { (alert : UIAlertAction!) in
            //camera.getCameraOn(self, canEdit: false)
            let scanner = IRLScannerViewController.standardCameraView(with: self)
            scanner.showControls = true
            scanner.showAutoFocusWhiteRectangle = true
            self.present(scanner, animated: true, completion: nil)
        }
        
        let sharePhoto = UIAlertAction(title: "Photo Library", style: .default) { (alert : UIAlertAction!) in
            camera.getPhotoLibraryOn(self, canEdit: false)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (alert : UIAlertAction!) in
        }
        optionMenu.addAction(takePhoto)
        optionMenu.addAction(sharePhoto)
        optionMenu.addAction(cancelAction)
        
        self.present(optionMenu, animated: true) {
            self.hideLoader()
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let chosenImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            let nuevaImg = Functions().scaleAndCropImage(image: chosenImage, toSize: CGSize(width: 100, height: 100))
            
            
            self.addNuevaImagen(laImagen: nuevaImg, imgOrig: chosenImage)
            
            
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - IRLScannerViewControllerDelegate
    func pageSnapped(_ page_image: UIImage, from controller: IRLScannerViewController) {
        controller.dismiss(animated: true) { () -> Void in
            let nuevaImg = Functions().scaleAndCropImage(image: page_image, toSize: CGSize(width: 100, height: 100))
            
            
            self.addNuevaImagen(laImagen: nuevaImg, imgOrig: page_image)
        }
    }
    
    func didCancel(_ cameraView: IRLScannerViewController) {
        cameraView.dismiss(animated: true) {}
    }
    
    //para image picker e imagen
    func addNuevaImagen(laImagen: UIImage, imgOrig: UIImage){
        
        self.imgsOriginales[self.currentImgSender] = imgOrig
        
        UIView.animate(withDuration: 0.50, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 0, options: [], animations: {
            
            switch self.currentImgSender {
            case 1:
                self.chkId.setOn(true, animated: true)
                self.img1.image = laImagen
                self.img1.setNeedsDisplay()
                self.img1.isHidden = false
                self.lblImg1.isHidden = false
            case 2:
                self.chkDom.setOn(true, animated: true)
                self.img2.image = laImagen
                self.img2.setNeedsDisplay()
                self.img2.isHidden = false
                self.lblImg2.isHidden = false
            case 3:
                self.chkRfc.setOn(true, animated: true)
                self.img3.image = laImagen
                self.img3.setNeedsDisplay()
                self.img3.isHidden = false
                self.lblImg3.isHidden = false
            default: break
            }
            
        }, completion: nil)
    }
    
    @objc func deleteImage(sender: UITapGestureRecognizer){
        let tag = (sender.view?.tag)!
        
        self.imgsOriginales[tag] = nil
        
        UIView.animate(withDuration: 0.50, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 0, options: [], animations: {
            
            switch tag {
            case 1:
                self.chkId.setOn(false, animated: true)
                self.img1.image = nil
                self.img1.setNeedsDisplay()
                self.img1.isHidden = true
                self.lblImg1.isHidden = true
            case 2:
                self.chkDom.setOn(false, animated: true)
                self.img2.image = nil
                self.img2.setNeedsDisplay()
                self.img2.isHidden = true
                self.lblImg2.isHidden = true
            case 3:
                self.chkRfc.setOn(false, animated: true)
                self.img3.image = nil
                self.img3.setNeedsDisplay()
                self.img3.isHidden = true
                self.lblImg3.isHidden = true
            default: break
            }
            
        }, completion: nil)
    }
    
    @objc func addPayPal(){
        var inputTextField: UITextField?
        let passwordPrompt = UIAlertController(title: FWS.appName, message: "Por favor ingrese su cuenta de registro en Paypal.", preferredStyle: UIAlertControllerStyle.alert)
        passwordPrompt.addAction(UIAlertAction(title: FWS.buttonCancel, style: UIAlertActionStyle.default, handler: nil))
        passwordPrompt.addAction(UIAlertAction(title: FWS.buttonOK, style: UIAlertActionStyle.default, handler: { (action) -> Void in
            if let value = (inputTextField?.text) {
                if value != "" {
                    self.datosUsuario.cuentaPaypal = value
                    self.chkPaypal.setOn(true, animated: true)
                }
            }
            
        }))
        passwordPrompt.addTextField(configurationHandler: {(textField: UITextField!) in
            textField.placeholder = "Email Paypal"
            textField.keyboardType = .emailAddress
            inputTextField = textField
        })
        
        present(passwordPrompt, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
