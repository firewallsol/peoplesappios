//
//  AjustesVC.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 11/09/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit

class AjustesVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var tableAjustes : UITableView!
    
    var notifEmailCell : CeldaTVAjustes!
    var notifSMSCell : CeldaTVAjustes!
    var notifPushCell : CeldaTVAjustes!
    var GPSCell : CeldaTVAjustes!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor.white
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        self.buildInterface()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "Ajustes"
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationItem.title = ""
    }
    
    func buildInterface(){
        var xframe = CGRect.zero
        //tableview
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: 300)
        self.tableAjustes = UITableView(frame: xframe)
        self.tableAjustes.backgroundColor = UIColor.white
        self.tableAjustes.delegate = self
        self.tableAjustes.dataSource = self
        self.tableAjustes.isScrollEnabled = false
        self.tableAjustes.tableFooterView = UIView(frame: CGRect.zero)
        
        //email
        self.notifEmailCell = CeldaTVAjustes(style: .default, reuseIdentifier: "cell")
        self.notifEmailCell.textLabel?.text = "Notificaciones vía EMAIL"
        self.notifEmailCell.textLabel?.textColor = UIColor.gray
        self.notifEmailCell.btnSwitch.isOn = true
        
        
        //sms
        self.notifSMSCell = CeldaTVAjustes(style: .default, reuseIdentifier: "cell")
        self.notifSMSCell.textLabel?.text = "Notificaciones vía SMS"
        self.notifSMSCell.textLabel?.textColor = UIColor.gray
        self.notifSMSCell.btnSwitch.isOn = true
        
        //push
        self.notifPushCell = CeldaTVAjustes(style: .default, reuseIdentifier: "cell")
        self.notifPushCell.textLabel?.text = "Notificaciones PUSH"
        self.notifPushCell.textLabel?.textColor = UIColor.gray
        self.notifPushCell.btnSwitch.isOn = true
        
        //gps
        self.GPSCell = CeldaTVAjustes(style: .default, reuseIdentifier: "cell")
        self.GPSCell.textLabel?.text = "Localizacion GPS"
        self.GPSCell.textLabel?.textColor = UIColor.gray
        self.GPSCell.btnSwitch.isOn = true
        
        self.view.addSubview(self.tableAjustes)
        
        self.tableAjustes.reloadData()
        self.tableAjustes.layoutIfNeeded()
        
    }
    
    //table view delegate datasource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 3
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            switch indexPath.row {
            case 0: return self.notifEmailCell
            case 1: return self.notifSMSCell
            case 2: return self.notifPushCell
            default: fatalError("La celda no corresponde")
            }
        } else {
            return self.GPSCell
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "NOTIFICACIONES"
        } else {
            return "LOCALIZACION"
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = Constantes.paletaColores.verdePeoples
    }
    
    //table view delegate datasource - fin

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
