//
//  PerfilProfesionalVC.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 30/06/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import Kingfisher
import Cosmos
import CocoaLumberjack

class DetalleProfesionalVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: - Components
    var imgFondo : UIImageView!
    var imgProfesional : UIImageView!
    var lblNombreProfesional : UILabel!
    var lblEspecialidad : UILabel!
    var ratingStars : CosmosView!
    var imgLike : UIImageView!
    var imgShare : UIImageView!
    var lblDescripcion : UILabel!
    var imgLocation : UIImageView!
    var lblDireccion : UILabel!
    var tblOpiniones : UITableView!
    var btnSolicitarServicio : UIView!
    var scrollView : UIScrollView!
    
    //MARK: - Vars
    var navTintColor = UIColor()
    var currentPro : Profesional!
    var nombreCeldaOpinion = "celdaOpinion"
    var arrayOpiniones = [Opinion]()

    //MARK: - View
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.fillArrayOpinion()
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        self.scrollView = UIScrollView(frame: self.view.frame)
        self.scrollView.backgroundColor = UIColor.white
        self.view.addSubview(self.scrollView)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navTintColor = (self.navigationController?.navigationBar.tintColor)!
        self.navigationController?.navigationBar.tintColor = Constantes.paletaColores.verdePeoples
        
        self.title = ""
        
        self.buildInterface(){ finished in
            self.tblOpiniones.reloadData()
            let when = DispatchTime.now() + 1
            DispatchQueue.main.asyncAfter(deadline: when){
                self.Table_Auto_Height()
            }
            
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isTranslucent = false
        
        self.navigationController?.navigationBar.tintColor = Constantes.paletaColores.verdePeoples
        /*
        self.navigationController?.navigationBar.tintColor = self.navTintColor
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        self.navigationController?.navigationBar.shadowImage = nil
         */
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func buildInterface(completionHandler: @escaping (Bool) -> ()){
        var xframe = CGRect.zero
        
        //imgFondo
        let altoFondoImg = Constantes.tamaños.altoPantalla * 0.41
        xframe.origin = CGPoint(x: 0, y: 0)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: altoFondoImg)
        self.imgFondo = UIImageView(frame: xframe)
        self.imgFondo.kf.indicatorType = .activity
        self.imgFondo.kf.setImage(with: self.currentPro.imagen)
        self.imgFondo.contentMode = .scaleToFill
        self.imgFondo.clipsToBounds = true
        self.scrollView.addSubview(self.imgFondo)
        
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.imgFondo.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.imgFondo.addSubview(blurEffectView)
        
        //imagen pro
        let anchoAltoImagen = altoFondoImg * 0.5
        xframe.origin = CGPoint(x: 0, y: self.imgFondo.frame.minY + Constantes.tamaños.altoStatusBar)
        xframe.size = CGSize(width: anchoAltoImagen, height: anchoAltoImagen)
        self.imgProfesional = UIImageView(frame: xframe)
        self.imgProfesional.layer.cornerRadius = self.imgProfesional.frame.size.width / 2
        self.imgProfesional.clipsToBounds = true
        self.imgProfesional.kf.indicatorType = .activity
        self.imgProfesional.kf.setImage(with: self.currentPro.imagen)
        self.imgProfesional.center.x = Constantes.tamaños.anchoPantalla / 2
        self.imgProfesional.contentMode = .scaleAspectFill
        self.scrollView.addSubview(self.imgProfesional)
        
        //nombre pro
        xframe.origin = CGPoint(x: 0, y: self.imgProfesional.frame.maxY + 10)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla * 0.9, height: 50)
        self.lblNombreProfesional = UILabel(frame: xframe)
        self.lblNombreProfesional.font = Constantes.currentFonts.NunitoBold.plus.LabelFontSize_1
        self.lblNombreProfesional.text = self.currentPro.razonSocial
        self.lblNombreProfesional.textColor = UIColor.white
        self.lblNombreProfesional.textAlignment = .center
        self.lblNombreProfesional.lineBreakMode = .byTruncatingTail
        self.lblNombreProfesional.center.x = Constantes.tamaños.anchoPantalla / 2
        var altolabel = self.lblNombreProfesional.heightForView()
        self.lblNombreProfesional.frame.size.height = altolabel
        self.lblNombreProfesional.adjustsFontSizeToFitWidth = true
        
        self.scrollView.addSubview(self.lblNombreProfesional)
        
        //especialidad
        xframe.origin = CGPoint(x: 0, y: self.lblNombreProfesional.frame.maxY + 1)
        self.lblEspecialidad = UILabel(frame: xframe)
        self.lblEspecialidad.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_1
        self.lblEspecialidad.textColor = Constantes.paletaColores.verdePeoples
        self.lblEspecialidad.text = self.currentPro.categoria.nombre
        self.lblEspecialidad.textAlignment = .center
        self.lblEspecialidad.lineBreakMode = .byTruncatingTail
        self.lblEspecialidad.center.x = Constantes.tamaños.anchoPantalla / 2
        altolabel = self.lblEspecialidad.heightForView()
        self.lblEspecialidad.frame.size.height = altolabel
        
        self.scrollView.addSubview(self.lblEspecialidad)
        
        //rating
        let anchoRating = Constantes.tamaños.anchoPantalla * 0.4
        let altoRating = altoFondoImg * 0.2
        xframe.origin = CGPoint(x: 10, y: self.lblEspecialidad.frame.maxY + 5)
        xframe.size = CGSize(width: anchoRating, height: altoRating)
        self.ratingStars = CosmosView(frame: xframe)
        self.ratingStars.settings.updateOnTouch = false
        self.ratingStars.settings.totalStars = 5
        self.ratingStars.settings.fillMode = .precise
        self.ratingStars.settings.starSize = Double(Constantes.tamaños.anchoPantalla * 0.08)
        self.ratingStars.settings.textColor = UIColor.white
        self.ratingStars.settings.textFont = Constantes.currentFonts.NunitoRegular.minus.LabelFontSize_1!
        
        let calificacion = Double(self.currentPro.calificacion)!
        self.ratingStars.rating = calificacion
        self.ratingStars.text = String(format: "(%.2f)", calificacion)
        self.scrollView.addSubview(self.ratingStars)
        
        // center stars en y
        let centerystars = self.lblEspecialidad.frame.maxY + ((self.imgFondo.frame.maxY - self.lblEspecialidad.frame.maxY) / 2)
        self.ratingStars.center.y = centerystars
        
        let anchoAltoIcons = Constantes.tamaños.anchoPantalla * 0.07
        
        //Share
        xframe.origin = CGPoint(x: Constantes.tamaños.anchoPantalla - (anchoAltoIcons + 10), y: self.imgFondo.frame.maxY - (anchoAltoIcons + 10))
        xframe.size = CGSize(width: anchoAltoIcons, height: anchoAltoIcons)
        self.imgShare = UIImageView(frame: xframe)
        self.imgShare.contentMode = .scaleAspectFit
        self.imgShare.image = Asset.Icons.icShare48.image
        self.imgShare.tintColor = Constantes.paletaColores.verdePeoples
        
        let tapGesture = UITapGestureRecognizer()
        tapGesture.numberOfTapsRequired = 1
        tapGesture.addTarget(self, action: #selector(seleccionaBotonCompartir))
        self.imgShare.isUserInteractionEnabled = true
        self.imgShare.addGestureRecognizer(tapGesture)
        self.scrollView.addSubview(self.imgShare)
        
        //img like
        xframe.origin = CGPoint(x: self.imgShare.frame.minX - (15 + anchoAltoIcons), y: self.imgFondo.frame.maxY - (anchoAltoIcons + 10))
        xframe.size = CGSize(width: anchoAltoIcons, height: anchoAltoIcons)
        self.imgLike = UIImageView(frame: xframe)
        self.imgLike.contentMode = .scaleAspectFit
        
        if Functions().existeIdEnFavoritos(id: self.currentPro.id, donde: .profesional){
            self.imgLike.image = Asset.Icons.icFav.image
            self.imgLike.tintColor = .red
        } else {
            self.imgLike.image = Asset.Icons.icFav.image
            self.imgLike.tintColor = .lightGray
        }
        
        self.scrollView.addSubview(self.imgLike)
        let likeTap = UITapGestureRecognizer(target: self, action: #selector(self.likePressed(sender:)))
        self.imgLike.isUserInteractionEnabled = true
        self.imgLike.addGestureRecognizer(likeTap)
        
        
        //descrip
        xframe.origin = CGPoint(x: 10, y: self.imgFondo.frame.maxY + 15)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla * 0.9, height: 30)
        self.lblDescripcion = UILabel(frame: xframe)
        self.lblDescripcion.numberOfLines = 0
        self.lblDescripcion.font = Constantes.currentFonts.NunitoRegular.minus.LabelFontSize_2
        self.lblDescripcion.textColor = UIColor.lightGray
        self.lblDescripcion.textAlignment = .justified
        self.lblDescripcion.text = self.currentPro.descripcion
        self.lblDescripcion.sizeToFit()
        self.scrollView.addSubview(self.lblDescripcion)
        
        //img ubicacion
        xframe.origin = CGPoint(x: 10, y: self.lblDescripcion.frame.maxY + 10)
        let anchoAltoImgLoc = Constantes.tamaños.anchoPantalla * 0.07
        xframe.size = CGSize(width: anchoAltoImgLoc, height: anchoAltoImgLoc)
        self.imgLocation = UIImageView(frame: xframe)
        self.imgLocation.image = Asset.Icons.icLocation.image
        self.imgLocation.contentMode = .scaleAspectFill
        self.scrollView.addSubview(self.imgLocation)
        
        //lbl ubicacion
        xframe.origin = CGPoint(x: self.imgLocation.frame.maxX + 10, y: self.lblDescripcion.frame.maxY + 10)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla * 0.8, height: 30)
        self.lblDireccion = UILabel(frame: xframe)
        self.lblDireccion.numberOfLines = 0
        self.lblDireccion.font = Constantes.currentFonts.NunitoRegular.minus.LabelFontSize_3
        self.lblDireccion.text = self.currentPro.direcciones.first?.descripcion
        self.lblDireccion.sizeToFit()
        self.scrollView.addSubview(self.lblDireccion)
        
        //center img ubicacion con label
        let centerImgLocation = self.lblDireccion.frame.minY + (self.lblDireccion.frame.size.height / 2)
        self.imgLocation.center.y = centerImgLocation
        
        //opiniones
        xframe.origin = CGPoint(x: 10, y: self.lblDireccion.frame.maxY + 15)
        let lblTituloOpiniones = UILabel(frame: xframe)
        lblTituloOpiniones.text = "OPINIONES"
        lblTituloOpiniones.font = Constantes.currentFonts.NunitoBold.base
        lblTituloOpiniones.textColor = Constantes.paletaColores.verdePeoples
        self.scrollView.addSubview(lblTituloOpiniones)
        
        //table de opiniones
        xframe.origin = CGPoint(x: 0, y: lblTituloOpiniones.frame.maxY + 10)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla * 0.9, height: 2000)
        self.tblOpiniones = UITableView(frame: xframe)
        self.scrollView.addSubview(self.tblOpiniones)
        self.tblOpiniones.delegate = self
        self.tblOpiniones.dataSource = self
        self.tblOpiniones.register(CeldaTVOpinion.self, forCellReuseIdentifier: self.nombreCeldaOpinion)
        self.tblOpiniones.estimatedRowHeight = Constantes.tamaños.altoCeldaOpinion
        self.tblOpiniones.rowHeight = UITableViewAutomaticDimension
        self.tblOpiniones.center.x = Constantes.tamaños.anchoPantalla / 2
        
        self.view.addSubview(self.scrollView)
        
        //btn
        let padding = (Constantes.tamaños.altoBotonTop)
        xframe.origin = CGPoint(x: 0, y: Constantes.tamaños.altoPantalla - padding)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: Constantes.tamaños.altoBotonTop)
        self.btnSolicitarServicio = UIView(frame: xframe)
        self.btnSolicitarServicio.backgroundColor = Constantes.paletaColores.verdePeoples
        
        let lblBtnsolicitar = UILabel(frame: xframe)
        lblBtnsolicitar.text = NSLocalizedString("RequestService", comment: "").uppercased()
        lblBtnsolicitar.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_1
        lblBtnsolicitar.sizeToFit()
        lblBtnsolicitar.textColor = UIColor.white
        self.btnSolicitarServicio.addSubview(lblBtnsolicitar)
        lblBtnsolicitar.center.x = Constantes.tamaños.anchoPantalla / 2
        lblBtnsolicitar.center.y = self.btnSolicitarServicio.frame.size.height / 2
        let tapSolicitar = UITapGestureRecognizer(target: self, action: #selector(self.gotoSolicitarServicio(tap:)))
        self.btnSolicitarServicio.addGestureRecognizer(tapSolicitar)
        self.btnSolicitarServicio.isUserInteractionEnabled = true
        
        self.view.addSubview(self.btnSolicitarServicio)
        
        completionHandler(true)
    }
    
    //MARK: - Table view
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableView.validaExistenciaDatos(datos:self.arrayOpiniones as AnyObject)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var alto : CGFloat = 0
        
        if let cell = tableView.cellForRow(at: indexPath) as? CeldaTVOpinion {
            var tempAlto = cell.imgOpinion.frame.size.height + 15
            tempAlto += cell.lblOpinion.frame.size.height + 10
            alto = tempAlto
        } else {
            alto = Constantes.tamaños.altoCeldaOpinion
        }
        
        return alto
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayOpiniones.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.nombreCeldaOpinion, for: indexPath) as! CeldaTVOpinion
        cell.frame.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: cell.frame.size.height)
        
        let laOpinion = self.arrayOpiniones[indexPath.row]
                
        let url = URL(string: laOpinion.urlImg)
        
        cell.imgOpinion.kf.setImage(with: url)
        cell.imgOpinion.setNeedsDisplay()
        
        cell.lblNombrePersona.frame.size.width = cell.frame.size.width * 0.8
        cell.lblNombrePersona.text = laOpinion.nombre
        cell.starsView.rating = Double(laOpinion.calificacion!)!
        cell.lblOpinion.frame.origin.x = cell.imgOpinion.frame.minX
        cell.lblOpinion.frame.size.width = Constantes.tamaños.anchoPantalla * 0.85
        cell.lblOpinion.text = laOpinion.opinion
        cell.lblOpinion.sizeToFit()
        
        return cell
    }
    
    //MARK: - Custom
    func fillArrayOpinion(){
        self.arrayOpiniones.removeAll()
        
        Functions().obtenerValoracionDeProfesional(idProfesional: currentPro.id) {
            (success, opiniones, mensaje, error) in
            if let xerror = error {
                DDLogError(xerror.localizedDescription)
                self.mensajeSimple(mensaje: NSLocalizedString("ErrorOnRequestingOpinions", comment: ""))
                return
            }
            
            if success {
                self.arrayOpiniones = opiniones!
                self.tblOpiniones.reloadData()
            }
            else {
                DDLogError(mensaje)
                self.mensajeSimple(mensaje: mensaje)
            }
        }
    }
    
    func Table_Auto_Height()
    {
        let padding = (Constantes.tamaños.altoBotonTop) + 5
        var frame: CGRect = self.tblOpiniones.frame
        frame.size.height = self.tblOpiniones.contentSize.height
        self.tblOpiniones.frame = frame
        
        self.scrollView.contentSize.height = self.tblOpiniones.frame.maxY + padding
        
    }
    
    @objc func gotoSolicitarServicio(tap:UITapGestureRecognizer){
        var elVC = UIViewController()
        if Functions().isUserLoggedIn() {
            
            //Valida que el usuario tenga un país de origen.
            let pais = Functions().getUserPais()
            if pais.descripcion != "" {
                elVC = SolicitarServicioVC()
                (elVC as! SolicitarServicioVC).currentPro = self.currentPro
                (elVC as! SolicitarServicioVC).sender = .detallePro
                self.navigationController?.pushViewController(elVC, animated: true)
            }
            else {
                let mensaje = NSLocalizedString("AlertShowEditProfileOnProfileIncomplete", comment: "")
                let titulo = NSLocalizedString("AppName", comment: "")
                let viewController = EditarPerfilVC()
                
                self.alertaRedirige(viewController: viewController, mensaje: mensaje, titulo: titulo)
            }
        }
        else {
            let mensaje = NSLocalizedString("AlertShowLoginOnRequestService", comment: "")
            let titulo = NSLocalizedString("AppName", comment: "")
            let viewController = LoginVC()
            
            self.alertaRedirige(viewController: viewController, mensaje: mensaje, titulo: titulo)
        }
    }
    
    @objc func likePressed(sender: UITapGestureRecognizer){
        
        if Functions().isUserLoggedIn() {
            let mensaje = Functions().agregarQuitarFavorito(id: self.currentPro.id,
                                                            elJson: self.currentPro.jsonData,
                                                            donde: .profesional)
            self.mensajeSimple(mensaje: mensaje)
            if Functions().existeIdEnFavoritos(id: self.currentPro.id, donde: .profesional){
                self.imgLike.image = Asset.Icons.icFav.image
                self.imgLike.tintColor = .red
            }
                
            else {
                self.imgLike.image = Asset.Icons.icFav.image
                self.imgLike.tintColor = .lightGray
            }
        }
    }
    
    @objc func seleccionaBotonCompartir() {
        self.showLoader()
        DispatchQueue.global(qos:.userInteractive).async {
            var imagen = UIImage()
            var finalString = String()
            
            if let ximagen = self.imgProfesional.image {
                imagen = ximagen
            }
            if let xnombre = self.lblNombreProfesional.text {
                finalString += "\(xnombre)\n"
            }
            if let xespecialidad = self.lblEspecialidad.text {
                finalString += "\(xespecialidad)\n"
            }
            if let xdireccion = self.lblDireccion.text {
                finalString += "\(xdireccion)"
            }
            
            let activityView = UIActivityViewController(activityItems: [imagen,finalString],
                                                        applicationActivities: nil)
            
            activityView.excludedActivityTypes = [.airDrop, .openInIBooks,
                                                  .print, .addToReadingList,
                                                  .postToFlickr, .postToVimeo,
                                                  .postToWeibo, .postToTencentWeibo]
            
            DispatchQueue.main.async {
                self.present(activityView, animated: true) {
                    self.hideLoader()
                }
            }
        }
    }


    
}
