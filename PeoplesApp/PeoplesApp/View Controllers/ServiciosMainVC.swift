//
//  ServiciosMainVC.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 29/06/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import CoreLocation

class ServiciosMainVC: UIViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, CLLocationManagerDelegate {
    
    var collectionServicios : UICollectionView!
    var btnSolicitar : UIView!
    var nombreCeldaServicio = "celdaServicio"
    var bottomPadding : CGFloat = 0
    var showLoaderBool = true
    var arrayServicios = [Categoria]()
    var locationManager = CLLocationManager()
    var ubicacionActual: CLLocationCoordinate2D!

    //MARK: - View
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Inicializa CoreLocation.
        self.configuraCoreLocation()
        
        self.automaticallyAdjustsScrollViewInsets = false
        self.view.backgroundColor = UIColor.white
        
        self.setBarMenu()
        self.buildInterface(){ finished in
            //
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if self.showLoaderBool {
            self.showLoader()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationItem.title = ""
    }
    
    func buildInterface(completionHandler: @escaping (Bool) -> ()){ 
        var xframe = CGRect.zero
        xframe.origin = CGPoint(x: 0, y: 0)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: Constantes.tamaños.altoPantalla)
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        self.collectionServicios = UICollectionView(frame: xframe, collectionViewLayout: layout)
        self.collectionServicios.delegate = self
        self.collectionServicios.dataSource = self
        self.collectionServicios.backgroundColor = UIColor.white
        self.collectionServicios.register(CeldaCVServicioMain.self, forCellWithReuseIdentifier: self.nombreCeldaServicio)
        self.view.addSubview(self.collectionServicios)
        
        //boton
        self.bottomPadding = (self.navigationController?.navigationBar.frame.size.height)! + (Constantes.tamaños.altoBotonTop + Constantes.tamaños.altoStatusBar)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: Constantes.tamaños.altoBotonTop)
        xframe.origin = CGPoint(x: 0, y: Constantes.tamaños.altoPantalla - self.bottomPadding)
        self.btnSolicitar = UIView(frame: xframe)
        self.btnSolicitar.backgroundColor = Constantes.paletaColores.verdePeoples
        self.view.addSubview(self.btnSolicitar)
        let lblBtnsolicitar = UILabel(frame: xframe)
        lblBtnsolicitar.text = NSLocalizedString("RequestService", comment: "").uppercased()
        lblBtnsolicitar.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_1
        lblBtnsolicitar.sizeToFit()
        lblBtnsolicitar.textColor = UIColor.white
        self.btnSolicitar.addSubview(lblBtnsolicitar)
        lblBtnsolicitar.center.x = Constantes.tamaños.anchoPantalla / 2
        lblBtnsolicitar.center.y = self.btnSolicitar.frame.size.height / 2
        let tapbtnsolicitar = UITapGestureRecognizer(target: self, action: #selector(self.btnSolicitarTap))
        self.btnSolicitar.isUserInteractionEnabled = true
        self.btnSolicitar.addGestureRecognizer(tapbtnsolicitar)
        
        completionHandler(true)
    }
    
    @objc func btnSolicitarTap(){
        if Functions().isUserLoggedIn() {
            
            //Valida que el usuario tenga un país de origen.
            let pais = Functions().getUserPais()
            if pais.descripcion != "" {
                let elVC = SolicitarServicioVC()
                elVC.sender = .Main
                elVC.arrayServicios = self.arrayServicios
                self.navigationController?.pushViewController(elVC, animated: true)
            }
            else {
                let mensaje = NSLocalizedString("AlertShowEditProfileOnProfileIncomplete", comment: "")
                let titulo = NSLocalizedString("AppName", comment: "")
                let viewController = EditarPerfilVC()
                
                self.alertaRedirige(viewController: viewController, mensaje: mensaje, titulo: titulo)
            }
        }
        else {
            let mensaje = NSLocalizedString("AlertShowLoginOnRequestService", comment: "")
            let titulo = NSLocalizedString("AppName", comment: "")
            let viewController = LoginVC()
            
            self.alertaRedirige(viewController: viewController, mensaje: mensaje, titulo: titulo)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Collection view datasource,  delegate y flow layout
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrayServicios.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: self.nombreCeldaServicio, for: indexPath as IndexPath) as! CeldaCVServicioMain
        
        let unaCategoria = self.arrayServicios[indexPath.item]
        
        var xframe = CGRect.zero
        xframe.origin = CGPoint(x: 10, y: 10)
        xframe.size = CGSize(width: cell.frame.size.height * 0.65, height: cell.frame.size.height * 0.65)
        cell.imgServicio.frame = xframe
        cell.imgServicio.center.x = cell.frame.size.width / 2
        cell.imgServicio.layer.cornerRadius = cell.imgServicio.frame.size.width / 2
        cell.imgServicio.clipsToBounds = true
        cell.imgServicio.kf.indicatorType = .activity
        cell.imgServicio.kf.setImage(with: unaCategoria.imagen)
        
        xframe.origin = CGPoint(x: 10, y: cell.imgServicio.frame.maxY + 5)
        xframe.size = CGSize(width: cell.frame.size.width * 0.95, height: 30)
        cell.lblServicio.frame = xframe
        cell.lblServicio.text = unaCategoria.nombre
        cell.lblServicio.sizeToFit()
        cell.lblServicio.center.x = cell.frame.size.width / 2
        let yposcenterServ = cell.imgServicio.frame.maxY + ((cell.frame.size.height - cell.imgServicio.frame.maxY) / 2)
        cell.lblServicio.center.y =  yposcenterServ
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        var elTamaño = CGSize.zero
        
        let anchoCollView = collectionView.frame.size.width - 20
        
        elTamaño = CGSize(width: anchoCollView / 2.07, height: anchoCollView / 2.07)
        
        return elTamaño
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        
        return UIEdgeInsets(top: 5, left: 10, bottom: self.bottomPadding + 10, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let elVC = ListaProfesionalesVC()
        elVC.currentCategoria = self.arrayServicios[indexPath.item]
        elVC.ubicacionActual = ubicacionActual
        self.navigationController?.pushViewController(elVC, animated: true)
    }
    
    //MARK: - Location Manager
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //
        print("UPDATE LOCATION")
        let location = locations.last
        manager.stopUpdatingLocation()
        
        ubicacionActual = location?.coordinate
        let latitude = (ubicacionActual.latitude.description)
        let longitude = (ubicacionActual.longitude.description)
        
        print(latitude)
        print(longitude)
        
        actualizarUbicacion(latitud: latitude, longitud: longitude)
    }
    
    //MARK: - Custom
    func configuraCoreLocation() {
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
    }
    
    func actualizarUbicacion(latitud: String, longitud: String) {
        
        Functions().getMainData(latitud: latitud, longitud: longitud) {
            exito, categorias, negocios, profesionales, error in
            self.showLoaderBool = false
            
            if self.isLoaderAnimating() {
                self.hideLoader()
            }
            
            if let xerror = error {
                print("GET datos main - Ocurrio un error \(xerror)")
                self.mensajeSimple(mensaje: "Error de red", dismiss: false)
            }
            else {
                if exito {
                    self.arrayServicios = categorias
                    DispatchQueue.main.async(execute: {
                        self.collectionServicios.reloadData()
                    })
                }
                else {
                    self.mensajeSimple(mensaje: "Error desde servicio", dismiss: false)
                }
            }
        }
    }


}
