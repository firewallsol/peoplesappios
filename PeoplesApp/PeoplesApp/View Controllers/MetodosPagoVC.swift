//
//  MetodosPagoVC.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 11/09/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit

class MetodosPagoVC: UIViewController {
    
    var lblID : UILabel!
    var imgTipoPago : UIImageView!
    var txtUsrPago : UITextField!
    
    var btnAñadir : UIView!
    
    var usuarioActual : Usuario!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if usuarioActual == nil {
            usuarioActual = Functions().getDataUsuario()
        }
        
        
        self.automaticallyAdjustsScrollViewInsets = false
        self.view.backgroundColor = UIColor.white

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "Método de pago"
        
        self.buildInterface(){ finished in
            //
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationItem.title = ""
    }
    
    func buildInterface(completionHandler: @escaping (Bool) -> ()){
        
        var xframe = CGRect.zero
        
        //añade
        //ico
        let imgAnchoAlto = Constantes.tamaños.anchoPantalla * 0.1
        xframe.origin = CGPoint(x: 10, y: 20)
        xframe.size = CGSize(width: imgAnchoAlto, height: imgAnchoAlto)
        let imgAñadirPago = UIImageView(frame: xframe)
        imgAñadirPago.image = Asset.Icons.icPaymentMethod64.image
        imgAñadirPago.contentMode = .scaleAspectFill
        self.view.addSubview(imgAñadirPago)
        
        //label
        xframe.origin = CGPoint(x: imgAñadirPago.frame.maxX + 10, y: 20)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla * 0.8, height: 25)
        let lblAñadirPago = UILabel(frame: xframe)
        lblAñadirPago.numberOfLines = 1
        lblAñadirPago.adjustsFontSizeToFitWidth = true
        lblAñadirPago.text = "Añade o actualiza un método de pago"
        lblAñadirPago.textColor = Constantes.paletaColores.verdePeoples
        lblAñadirPago.font = Constantes.currentFonts.NunitoRegular.base
        self.view.addSubview(lblAñadirPago)
        lblAñadirPago.center.y = imgAñadirPago.frame.minY + (imgAnchoAlto / 2)
        
        //ID
        xframe.origin = CGPoint(x: 10, y: imgAñadirPago.frame.maxY + 20)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla * 0.8, height: 25)
        self.lblID = UILabel(frame: xframe)
        self.lblID.textColor = Constantes.paletaColores.verdePeoples
        self.lblID.text = "ID. \(self.usuarioActual.id!)"
        self.lblID.font = Constantes.currentFonts.NunitoBold.base
        self.lblID.sizeToFit()
        self.view.addSubview(self.lblID)
        
        //datos
        //icono
        xframe.size = CGSize(width: imgAnchoAlto, height: imgAnchoAlto)
        xframe.origin = CGPoint(x: 10, y: self.lblID.frame.maxY + 20)
        self.imgTipoPago = UIImageView(frame: xframe)
        self.imgTipoPago.image = Asset.Icons.payPal.image
        self.imgTipoPago.contentMode = .scaleAspectFill
        self.view.addSubview(self.imgTipoPago)
        
        //txtdato
        let anchoCampo = Constantes.tamaños.anchoPantalla * 0.75
        xframe.origin = CGPoint(x: self.imgTipoPago.frame.maxX + 5, y: self.lblID.frame.maxY + 20)
        xframe.size = CGSize(width: anchoCampo, height: Constantes.tamaños.altoTextfield)
        self.txtUsrPago = UITextField(frame: xframe)
        self.txtUsrPago.text = self.usuarioActual.cuentaPaypal!
        self.txtUsrPago.placeholder = "Cuenta Paypal"
        self.txtUsrPago.layer.borderWidth = Constantes.tamaños.sizeBordeTextfield
        self.txtUsrPago.layer.borderColor = Constantes.paletaColores.bordeTextfield.cgColor
        self.txtUsrPago.layer.cornerRadius = 3
        self.txtUsrPago.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0)
        self.txtUsrPago.rightViewMode = .always
        self.txtUsrPago.autocorrectionType = .no
        self.txtUsrPago.rightView = UIView()
        self.txtUsrPago.font = Constantes.currentFonts.NunitoRegular.base
        self.view.addSubview(self.txtUsrPago)
        self.txtUsrPago.center.y = self.imgTipoPago.frame.minY + (imgAnchoAlto / 2)
        self.addToolBar(textField: self.txtUsrPago, textView: nil)
        self.txtUsrPago.keyboardType = .emailAddress
        
        //label principal
        
        //btnañadir
        let xpadding =  (Constantes.tamaños.altoBotonTop) + (self.navigationController?.navigationBar.frame.size.height)! + Constantes.tamaños.altoStatusBar
        xframe.origin = CGPoint(x: 0, y: Constantes.tamaños.altoPantalla - xpadding)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: Constantes.tamaños.altoBotonTop)
        self.btnAñadir = UIView(frame: xframe)
        self.btnAñadir.backgroundColor = Constantes.paletaColores.verdePeoples
        
        let lblBtnAñadir = UILabel(frame: xframe)
        lblBtnAñadir.text = "ACTUALIZAR"
        lblBtnAñadir.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_1
        lblBtnAñadir.textColor = UIColor.white
        self.btnAñadir.addSubview(lblBtnAñadir)
        lblBtnAñadir.sizeToFit()
        lblBtnAñadir.center.x = Constantes.tamaños.anchoPantalla / 2
        lblBtnAñadir.center.y = self.btnAñadir.frame.size.height / 2
        let tapMetodoPago = UITapGestureRecognizer(target: self, action: #selector(self.actualizaFormaPago))
        self.btnAñadir.addGestureRecognizer(tapMetodoPago)
        self.btnAñadir.isUserInteractionEnabled = true
        self.view.addSubview(self.btnAñadir)
        
        completionHandler(true)
    }
    
    @objc func actualizaFormaPago(){
        let payPalData = self.txtUsrPago.text?.trimmed()
        if (self.usuarioActual.cuentaPaypal == payPalData!){
            self.mensajeSimple(mensaje: "No ha realizado cambios a su cuenta de PayPal")
            return
        }
        
        self.showLoader()
        
        Functions().updateFormaPago(paypal: payPalData!){ exito, mensaje, error  in
            self.hideLoader()
            if let xerror = error {
                print("GET datos cashback - Ocurrio un error \(xerror)")
                self.mensajeSimple(mensaje: "Error de red, revise su conexion", dismiss: false)
            } else {
                if exito {
                    let alert = UIAlertController(title: "People's App", message: mensaje, preferredStyle: UIAlertControllerStyle.alert)
                    
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: {(
                        alert: UIAlertAction!) in
                        _ = self.navigationController?.popViewController(animated: true)
                    })
                    )
                    
                    self.present(alert, animated: true, completion: nil)
                } else {
                    self.mensajeSimple(mensaje: mensaje, dismiss: false)
                }
            }
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
