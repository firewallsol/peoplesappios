//
//  NotificacionesVC.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 12/09/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit

class NotificacionesVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var tableNotificaciones : UITableView!
    
    var arrayNotificaciones = [Notificacion]()
    
    var cellIdentifier = "celdaNotificacion"

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.arrayNotificaciones = Functions().getArrayNotificacion() //quité datos dummy's

        self.view.backgroundColor = UIColor.white
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        self.setBarMenu()
        
        self.buildInterface()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setTableSize()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationItem.title = ""
    }
    
    func buildInterface(){
        var xframe = CGRect.zero
        //tableview
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: Constantes.tamaños.altoPantalla)
        self.tableNotificaciones = UITableView(frame: xframe)
        self.tableNotificaciones.backgroundColor = UIColor.white
        self.tableNotificaciones.delegate = self
        self.tableNotificaciones.dataSource = self
        self.tableNotificaciones.tableFooterView = UIView(frame: CGRect.zero)
        self.tableNotificaciones.register(CeldaTVNotificacion.self, forCellReuseIdentifier: self.cellIdentifier)
        
        self.view.addSubview(self.tableNotificaciones)
        
        self.tableNotificaciones.reloadData()
    }
    
    func setTableSize(){
        let padding = (self.navigationController?.navigationBar.frame.size.height)! + Constantes.tamaños.altoStatusBar + 5
        var xframe = self.tableNotificaciones.frame
        xframe.size.height = Constantes.tamaños.altoPantalla - padding
        self.tableNotificaciones.frame = xframe
    }
    
    //table view delegate datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayNotificaciones.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constantes.tamaños.altoCeldaPedidos
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.cellIdentifier) as! CeldaTVNotificacion
        cell.frame.size = CGSize(width: tableView.frame.size.width, height: cell.frame.size.height)
        
        //var xframe = CGRect.zero
        
        let laNotif = self.arrayNotificaciones[indexPath.row]
        
        cell.imgNotif.image = laNotif.testImg
        cell.imgNotif.setNeedsLayout()
        
        cell.lblFecha.text = laNotif.fecha
        cell.lblFecha.sizeToFit()
        cell.lblFecha.frame.origin.y = Constantes.tamaños.altoCeldaPedidos - (cell.lblFecha.frame.size.height + 5)
        
        cell.lblTitulo.text = laNotif.titulo
        cell.lblTitulo.sizeToFit()
        cell.lblTitulo.center.y = cell.lblFecha.frame.minY / 2
        
        cell.lblFecha.frame.origin.x = cell.lblTitulo.frame.minX
        
        
        return cell
    }
    
    //table view delegate datasource - fin

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
