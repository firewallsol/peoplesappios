//
//  CalificarServVC.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 01/09/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import Cosmos
import Kingfisher

class CalificarServVC: UIViewController {
    
    var lblFecha : UILabel!
    var imgProfesional : UIImageView!
    var lblNomberePro : UILabel!
    var lblEspecialidad : UILabel!
    var rateStars : CosmosView!
    
    var btnTiempoLlegada : BotonProblemaCalificar!
    var btnCalidad : BotonProblemaCalificar!
    var btnPresentacion : BotonProblemaCalificar!
    
    var btnTiempoAsignar : BotonProblemaCalificar!
    var btnCobro : BotonProblemaCalificar!
    var btnActitud : BotonProblemaCalificar!
    
    var txtOpinion : UITextView!
    var btnCalificar : UIView!
    
    var scrollView : UIScrollView!
    
    var currentPropuesta : Propuesta!
    
    var indexSatisfaccion = 0

    //MARK: - View
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        self.view.backgroundColor = UIColor.white
        self.automaticallyAdjustsScrollViewInsets = false
        
        self.buildInterface(){ finished in 
            //
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = "Calificar servicio"
//        self.navigationItem.title = "Calificar servicio"
    }
    
    //MARK: - Keyboard
    @objc func keyboardWillShow(sender: NSNotification) {
        var userInfo = sender.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height
        self.scrollView.contentInset = contentInset
        
        var visibleRect = self.view.frame
        visibleRect.size.height -= keyboardFrame.size.height
        
        //var elOrigin = CGPoint.zero
        
        //var scroll = false
        
        /*
        if self.txtOpinion.isFirstResponder {
            visibleRect.size.height -= ((self.txtOpinion.inputAccessoryView?.frame.size.height)!)
            elOrigin = self.txtOpinion.frame.origin
            scroll = true
        }
        
        if ((scroll) && !(visibleRect.contains(elOrigin))){
            let scrollPoint = CGPoint(x: 0, y: elOrigin.y - visibleRect.size.height)
            self.scrollView.setContentOffset(scrollPoint, animated: true)
        }
        */
    }
    
    @objc func keyboardWillHide(sender: NSNotification) {
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        self.scrollView.contentInset = contentInset
    }
    
    //MARK: - Events
    func buildInterface(completionHandler: @escaping (Bool) -> ()){
        var xframe = CGRect.zero
        
        self.scrollView = UIScrollView(frame: self.view.frame)
        
        //fecha
        let ypos = Constantes.tamaños.altoStatusBar + 10
        xframe.origin = CGPoint(x: 10, y: ypos)
        self.lblFecha = UILabel(frame: xframe)
        self.lblFecha.text = Functions().dateToString(laFecha: Date())
        self.lblFecha.textColor = UIColor.gray
        self.lblFecha.font = Constantes.currentFonts.NunitoRegular.base
        self.lblFecha.sizeToFit()
        self.scrollView.addSubview(self.lblFecha)
        
        //imagen profesional
        let anchoAltoImagen = Constantes.tamaños.anchoPantalla * 0.3
        xframe.origin = CGPoint(x: 10, y: self.lblFecha.frame.maxY + 10)
        xframe.size = CGSize(width: anchoAltoImagen, height: anchoAltoImagen)
        self.imgProfesional = UIImageView(frame: xframe)
        self.imgProfesional.layer.cornerRadius = self.imgProfesional.frame.size.width / 2
        self.imgProfesional.clipsToBounds = true
        self.imgProfesional.kf.indicatorType = .activity
        self.imgProfesional.kf.setImage(with: self.currentPropuesta.datosProfesional.imagen)
        self.imgProfesional.contentMode = .scaleAspectFill
        self.scrollView.addSubview(self.imgProfesional)
        
        //especialidad
        xframe.origin = CGPoint(x: self.imgProfesional.frame.maxX + 10, y: 20)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla * 0.6, height: 20)
        self.lblEspecialidad = UILabel(frame: xframe)
        self.lblEspecialidad.adjustsFontSizeToFitWidth = true
        self.lblEspecialidad.font = Constantes.currentFonts.NunitoRegular.minus.LabelFontSize_2
        self.lblEspecialidad.text = self.currentPropuesta.datosProfesional.categoria.nombre
        self.scrollView.addSubview(self.lblEspecialidad)
        self.lblEspecialidad.textAlignment = .center
        self.lblEspecialidad.center.y = self.imgProfesional.frame.minY + (self.imgProfesional.frame.size.height / 2)
        self.lblEspecialidad.textColor = Constantes.paletaColores.verdePeoples
        
        //nombre
        let posynombre = self.lblEspecialidad.frame.minY - (5 + 20)
        xframe.origin = CGPoint(x: self.imgProfesional.frame.maxX + 10, y: posynombre)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla * 0.6, height: 25)
        self.lblNomberePro = UILabel(frame: xframe)
        self.lblNomberePro.adjustsFontSizeToFitWidth = true
        self.lblNomberePro.font = Constantes.currentFonts.NunitoBold.base
        self.lblNomberePro.text = self.currentPropuesta.datosProfesional.razonSocial
        self.scrollView.addSubview(self.lblNomberePro)
        self.lblNomberePro.textAlignment = .center
        
        //rating
        xframe.origin = CGPoint(x: self.imgProfesional.frame.maxX + 10, y: self.lblEspecialidad.frame.maxY + 10)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla * 0.5, height: Constantes.tamaños.anchoPantalla * 0.25)
        self.rateStars = CosmosView(frame: xframe)
        self.rateStars.settings.totalStars = 5
        self.rateStars.settings.fillMode = .full
        self.rateStars.settings.starSize = Double(Constantes.tamaños.anchoPantalla * 0.11)
        self.rateStars.rating = 5
        self.scrollView.addSubview(self.rateStars)
        
        //algo salio mal
        xframe.origin = CGPoint(x: 10, y: self.rateStars.frame.maxY + 20)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla * 0.5, height: Constantes.tamaños.anchoPantalla * 0.25)
        let lblWrong = UILabel(frame: xframe)
        lblWrong.text = "¿ALGO SALIÓ MAL?"
        lblWrong.font = Constantes.currentFonts.NunitoBold.base
        self.scrollView.addSubview(lblWrong)
        lblWrong.sizeToFit()
        
        let anchoBoton = Constantes.tamaños.anchoPantalla * 0.45
        let altoBoton = Constantes.tamaños.altoBotonTop * 1.5
        let separacionBtns = Constantes.tamaños.anchoPantalla * 0.03
        
        //botones derecho
        //tiempo
        xframe.origin = CGPoint(x: separacionBtns, y: lblWrong.frame.maxY + 15)
        xframe.size = CGSize(width: anchoBoton, height: altoBoton)
        self.btnTiempoLlegada = BotonProblemaCalificar(frame: xframe)
        self.btnTiempoLlegada.icono.image = Asset.Icons.icClock48.image
        self.btnTiempoLlegada.titulo.text = "Tiempo de llegada del contratista"
        self.scrollView.addSubview(self.btnTiempoLlegada)
        self.btnTiempoLlegada.isUserInteractionEnabled = true
        let tapTiempoLLegada = UITapGestureRecognizer(target: self, action: #selector(self.marcaDesmarcaBotones(sender:)))
        self.btnTiempoLlegada.addGestureRecognizer(tapTiempoLLegada)
        self.btnTiempoLlegada.tag = 1
        
        
        //calidad
        xframe.origin = CGPoint(x: separacionBtns, y: self.btnTiempoLlegada.frame.maxY + (separacionBtns / 2))
        self.btnCalidad = BotonProblemaCalificar(frame: xframe)
        self.btnCalidad.icono.image = Asset.Icons.icClock48.image
        self.btnCalidad.titulo.text = "Calidad del servicio"
        self.scrollView.addSubview(self.btnCalidad)
        self.btnCalidad.isUserInteractionEnabled = true
        let tapBtnCalidad = UITapGestureRecognizer(target: self, action: #selector(self.marcaDesmarcaBotones(sender:)))
        self.btnCalidad.addGestureRecognizer(tapBtnCalidad)
        self.btnCalidad.tag = 3
        
        
        //presentacion
        xframe.origin = CGPoint(x: separacionBtns, y: self.btnCalidad.frame.maxY + (separacionBtns / 2))
        self.btnPresentacion = BotonProblemaCalificar(frame: xframe)
        self.btnPresentacion.icono.image = Asset.Icons.icClock48.image
        self.btnPresentacion.titulo.text = "Presentacion del contratista"
        self.scrollView.addSubview(self.btnPresentacion)
        self.btnPresentacion.isUserInteractionEnabled = true
        let tapBtnPresentacion = UITapGestureRecognizer(target: self, action: #selector(self.marcaDesmarcaBotones(sender:)))
        self.btnPresentacion.addGestureRecognizer(tapBtnPresentacion)
        self.btnPresentacion.tag = 5
        
        //botones izquierdo
        let xposbtnsizq = self.btnTiempoLlegada.frame.maxX + separacionBtns
        //demora asignar
        xframe.origin = CGPoint(x: xposbtnsizq, y: self.btnTiempoLlegada.frame.minY)
        self.btnTiempoAsignar = BotonProblemaCalificar(frame: xframe)
        self.btnTiempoAsignar.icono.image = Asset.Icons.icClock48.image
        self.btnTiempoAsignar.titulo.text = "Demora en asignacion del servicio"
        self.scrollView.addSubview(self.btnTiempoAsignar)
        self.btnTiempoAsignar.isUserInteractionEnabled = true
        let tapBtnTiempoAsignar = UITapGestureRecognizer(target: self, action: #selector(self.marcaDesmarcaBotones(sender:)))
        self.btnTiempoAsignar.addGestureRecognizer(tapBtnTiempoAsignar)
        self.btnTiempoAsignar.tag = 2
        
        //cobro de mas
        xframe.origin = CGPoint(x: xposbtnsizq, y: self.btnCalidad.frame.minY)
        self.btnCobro = BotonProblemaCalificar(frame: xframe)
        self.btnCobro.icono.image = Asset.Icons.icClock48.image
        self.btnCobro.titulo.text = "Me cobraron de mas"
        self.scrollView.addSubview(self.btnCobro)
        self.btnCobro.isUserInteractionEnabled = true
        let tapBtnCobro = UITapGestureRecognizer(target: self, action: #selector(self.marcaDesmarcaBotones(sender:)))
        self.btnCobro.addGestureRecognizer(tapBtnCobro)
        self.btnCobro.tag = 4
        
        //actitud
        xframe.origin = CGPoint(x: xposbtnsizq, y: self.btnPresentacion.frame.minY)
        self.btnActitud = BotonProblemaCalificar(frame: xframe)
        self.btnActitud.icono.image = Asset.Icons.icClock48.image
        self.btnActitud.titulo.text = "Actitud del contratista"
        self.scrollView.addSubview(self.btnActitud)
        self.btnActitud.isUserInteractionEnabled = true
        let tapBtnActitud = UITapGestureRecognizer(target: self, action: #selector(self.marcaDesmarcaBotones(sender:)))
        self.btnActitud.addGestureRecognizer(tapBtnActitud)
        self.btnActitud.tag = 6
        
        
        //Comparte tu opinion
        xframe.origin = CGPoint(x: 10, y: self.btnActitud.frame.maxY + 20)
        xframe.size = CGSize(width: (anchoBoton * 2) + separacionBtns , height: Constantes.tamaños.anchoPantalla * 0.35)
        self.txtOpinion = UITextView(frame: xframe)
        self.txtOpinion.layer.borderColor = UIColor.gray.cgColor
        self.txtOpinion.layer.borderWidth = Constantes.tamaños.sizeBordeTextfield
        self.txtOpinion.layer.cornerRadius = 10
        self.scrollView.addSubview(self.txtOpinion)
        self.txtOpinion.center.x = Constantes.tamaños.centroHorizontal
        self.txtOpinion.font = Constantes.currentFonts.NunitoRegular.base
        self.addToolBar(textField: nil, textView: self.txtOpinion)
        
        self.view.addSubview(self.scrollView)
        
        let paddingBtn = Constantes.tamaños.altoBotonTop
        
        xframe.origin = CGPoint(x: 0, y: Constantes.tamaños.altoPantalla - paddingBtn)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: Constantes.tamaños.altoBotonTop)
        self.btnCalificar = UIView(frame: xframe)
        self.btnCalificar.backgroundColor = Constantes.paletaColores.verdePeoples
        
        let lblBtncalificar = UILabel(frame: xframe)
        lblBtncalificar.text = "CALIFICAR"
        lblBtncalificar.textAlignment = .center
        lblBtncalificar.textColor = UIColor.white
        lblBtncalificar.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_1
        self.btnCalificar.addSubview(lblBtncalificar)
        lblBtncalificar.center.x = Constantes.tamaños.anchoPantalla / 2
        lblBtncalificar.center.y = self.btnCalificar.frame.size.height / 2

        let tapCalificar = UITapGestureRecognizer(target: self, action: #selector(self.confirmaCalificar))
        self.btnCalificar.addGestureRecognizer(tapCalificar)
        self.btnCalificar.isUserInteractionEnabled = true
        
        self.view.addSubview(self.btnCalificar)
        
        let padding = 10 + paddingBtn
        
        self.scrollView.contentSize.height = self.txtOpinion.frame.maxY + padding
        
        completionHandler(true)
    }
    
    @objc func confirmaCalificar() {
        let alert = UIAlertController(title: "People's App", message: "¿Esta seguro que desea calificar esta propuesta?", preferredStyle: UIAlertControllerStyle.alert)
        
        let okButton = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: {(
            alert: UIAlertAction!) in
            self.showLoader()
            self.calificaServicio()
        })
        
        let cancelButton = UIAlertAction(title: "CANCEL", style: UIAlertActionStyle.cancel, handler: nil)
        
        alert.addAction(cancelButton)
        alert.addAction(okButton)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func calificaServicio(){
        Functions().calificarServicio(idProfesional: self.currentPropuesta.datosProfesional.id, idServicio: self.currentPropuesta.idServicio, satisfaccion: "\(self.indexSatisfaccion)", calificacion: "\(self.rateStars.rating)", comentario: self.txtOpinion.text){ exito, mensaje, error in
            
            self.hideLoader()
            if let xerror = error {
                print("responder propuesta - Ocurrio un error \(xerror)")
                self.mensajeSimple(mensaje: "Error de red", dismiss: false)
            } else {
                if exito {
                    self.alertAndDismissWith(message: mensaje, buttonTitle: NSLocalizedString("OK", comment: ""))
                } else {
                    self.mensajeSimple(mensaje: mensaje, dismiss: false)
                }
            }
            
        }
    }
    
    @objc func marcaDesmarcaBotones(sender: UITapGestureRecognizer){
        self.desmarcaTodos(sender: sender)
        let boton = sender.view as! BotonProblemaCalificar
        if boton.isSelected {
            self.indexSatisfaccion = 0
            boton.setUnSelected()
        } else {
            boton.setSelected()
            self.indexSatisfaccion = boton.tag
        }
    }
    
    func desmarcaTodos(sender: UITapGestureRecognizer){
        let indexTag = sender.view?.tag
        
        if indexTag != self.btnTiempoLlegada.tag {
            self.btnTiempoLlegada.setUnSelected()
        }
        
        if indexTag != self.btnCalidad.tag {
            self.btnCalidad.setUnSelected()
        }
        
        if indexTag != self.btnPresentacion.tag {
            self.btnPresentacion.setUnSelected()
        }
        
        if indexTag != self.btnTiempoAsignar.tag {
            self.btnTiempoAsignar.setUnSelected()
        }
        
        if indexTag != self.btnCobro.tag {
            self.btnCobro.setUnSelected()
        }
        
        if indexTag != self.btnActitud.tag {
            self.btnActitud.setUnSelected()
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

}
