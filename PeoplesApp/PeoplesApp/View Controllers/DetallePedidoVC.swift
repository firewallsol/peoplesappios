//
//  DetallePedidoVC.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 03/08/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import Kingfisher
import CocoaLumberjack

class DetallePedidoVC: UIViewController {
    
    //MARK: - Components
    var imgProfesional : UIImageView!
    var lblNombreProfesional : UILabel!
    var lblEspecialidad : UILabel!
    var segmentEstatus : UISegmentedControl!
    var lblFechaServicio : UILabel!
    var lblPrecio : UILabel!
    var lblCashBack : UILabel!
    var lblDetallesServicio : UILabel!
    var btnCancelar : UIButton!
    var btnChat : UIButton!
    var scrollView : UIScrollView!
    var lblDescTituloSolicitud: UILabel!
    
    
    //MARK: - Vars
    var itemsSegment = [String]()
    var currentServicio : Servicio!
    
    enum SegmentedOptions: String {
        case asignado = "Asignado"
        case completada = "Completada"
        case cancelada = "Cancelada"
    }
    
    
    //MARK: - View
    override func viewDidLoad() {
        super.viewDidLoad()
        
        itemsSegment = [SegmentedOptions.asignado.rawValue,
                        SegmentedOptions.completada.rawValue,
                        SegmentedOptions.cancelada.rawValue]
        
        self.automaticallyAdjustsScrollViewInsets = false
        self.view.backgroundColor = UIColor.white
        self.buildInterface()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationItem.title = "Pedidos"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationItem.title = ""
    }
    
    func buildInterface(){
        
        var xframe = CGRect.zero
        
        self.scrollView = UIScrollView(frame: self.view.frame)
        
        //img usuario
        let anchoAltoImagen = Constantes.tamaños.anchoPantalla * 0.3
        xframe.origin = CGPoint(x: 10, y: 20)
        xframe.size = CGSize(width: anchoAltoImagen, height: anchoAltoImagen)
        self.imgProfesional = UIImageView(frame: xframe)
        self.imgProfesional.layer.cornerRadius = self.imgProfesional.frame.size.width / 2
        self.imgProfesional.clipsToBounds = true
        self.imgProfesional.kf.indicatorType = .activity
        self.imgProfesional.kf.setImage(with: currentServicio.propuestas.first?.datosProfesional.imagen)
        self.imgProfesional.contentMode = .scaleAspectFill
        self.scrollView.addSubview(self.imgProfesional)
        
        //especialidad
        xframe.origin = CGPoint(x: self.imgProfesional.frame.maxX + 10, y: 20)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla * 0.6, height: 20)
        self.lblEspecialidad = UILabel(frame: xframe)
        self.lblEspecialidad.adjustsFontSizeToFitWidth = true
        self.lblEspecialidad.font = Constantes.currentFonts.NunitoRegular.minus.LabelFontSize_2
        self.lblEspecialidad.text = currentServicio.propuestas.first?.datosProfesional.categoria.nombre
        self.scrollView.addSubview(self.lblEspecialidad)
        self.lblEspecialidad.textAlignment = .center
        self.lblEspecialidad.center.y = self.imgProfesional.frame.minY + (self.imgProfesional.frame.size.height / 2) + 20
        self.lblEspecialidad.textColor = Constantes.paletaColores.verdePeoples
        
        //nombre profesional
        let posynombre = self.lblEspecialidad.frame.minY - (5 + 20)
        xframe.origin = CGPoint(x: self.imgProfesional.frame.maxX + 10, y: posynombre)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla * 0.6, height: 25)
        self.lblNombreProfesional = UILabel(frame: xframe)
        self.lblNombreProfesional.adjustsFontSizeToFitWidth = true
        self.lblNombreProfesional.font = Constantes.currentFonts.NunitoBold.base
        self.lblNombreProfesional.text = currentServicio.propuestas.first?.datosProfesional.razonSocial
        self.scrollView.addSubview(self.lblNombreProfesional)
        self.lblNombreProfesional.textAlignment = .center
        
        //segment status
        xframe.origin = CGPoint(x: 0, y: self.imgProfesional.frame.maxY + 10)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: Constantes.tamaños.altoBotonTop)
        self.segmentEstatus = UISegmentedControl(items: self.itemsSegment)
        self.segmentEstatus.frame = xframe
        self.scrollView.addSubview(self.segmentEstatus)
        self.segmentEstatus.tintColor = Constantes.paletaColores.verdePeoples
        let font = Constantes.currentFonts.NunitoBold.base
        self.segmentEstatus.setTitleTextAttributes([NSAttributedStringKey.font: font!], for: .normal)
        self.segmentEstatus.isUserInteractionEnabled = false
        self.segmentEstatus.selectedSegmentIndex = (Int(self.currentServicio.estatus.id)!) - 2 //el 0 no cuenta y el 1 no puede aparecer aqui
        
        let separacionCampos = Constantes.tamaños.anchoPantalla * 0.07
        
        
        //fecha de servicio
        //titulo
        xframe.origin = CGPoint(x: 10, y: self.segmentEstatus.frame.maxY + (Constantes.tamaños.anchoPantalla * 0.1))
        let tituloFechaServicio = UILabel(frame: xframe)
        tituloFechaServicio.textColor = Constantes.paletaColores.verdePeoples
        tituloFechaServicio.text = "FECHA DE SERVICIO"
        tituloFechaServicio.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_1
        tituloFechaServicio.sizeToFit()
        self.scrollView.addSubview(tituloFechaServicio)
        
        //value fecha servicio
        xframe.origin = CGPoint(x: 40, y: tituloFechaServicio.frame.minY)
        self.lblFechaServicio = UILabel(frame: xframe)
        self.scrollView.addSubview(self.lblFechaServicio)
        self.lblFechaServicio.textColor = UIColor.gray
        self.lblFechaServicio.text = Functions().dateToString(laFecha: self.currentServicio.fechaServicio)
        self.lblFechaServicio.font = Constantes.currentFonts.NunitoRegular.minus.LabelFontSize_3
        self.lblFechaServicio.sizeToFit()
        self.lblFechaServicio.frame.origin.x = Constantes.tamaños.anchoPantalla - (self.lblFechaServicio.frame.size.width + 5)
        
        
        //precio
        //titulo
        xframe.origin = CGPoint(x: 10, y: tituloFechaServicio.frame.maxY + separacionCampos)
        let tituloPrecio = UILabel(frame: xframe)
        tituloPrecio.textColor = Constantes.paletaColores.verdePeoples
        tituloPrecio.text = "PRECIO"
        tituloPrecio.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_1
        tituloPrecio.sizeToFit()
        self.scrollView.addSubview(tituloPrecio)
        
        //value precio
        xframe.origin = CGPoint(x: 30, y: tituloPrecio.frame.minY)
        self.lblPrecio = UILabel(frame: xframe)
        self.scrollView.addSubview(self.lblPrecio)
        self.lblPrecio.textColor = UIColor.red
        let precio = currentServicio.propuestas.first?.presupuesto!
        self.lblPrecio.text = precio?.formatoPrecio()
        self.lblPrecio.font = Constantes.currentFonts.NunitoRegular.base
        self.lblPrecio.sizeToFit()
        self.lblPrecio.frame.origin.x = Constantes.tamaños.anchoPantalla - (self.lblPrecio.frame.size.width + 50)
        
        //Divisa
        xframe.origin = CGPoint(x: lblPrecio.frame.maxX + 3, y: lblPrecio.frame.minY)
        let lblDivisa = UILabel(frame: xframe)
        lblDivisa.text = currentServicio.divisa.abreviacion
        lblDivisa.textColor = UIColor.red
        lblDivisa.sizeToFit()
        lblDivisa.center.y = lblPrecio.center.y
        
        self.scrollView.addSubview(lblDivisa)
        lblDivisa.frame.origin.x = Constantes.tamaños.anchoPantalla - (lblDivisa.frame.size.width + 5)

        
        
        //cashback
        //titulo
        xframe.origin = CGPoint(x: 10, y: tituloPrecio.frame.maxY + separacionCampos)
        let tituloCashback = UILabel(frame: xframe)
        tituloCashback.textColor = Constantes.paletaColores.verdeCompletado
        tituloCashback.text = "CASHBACK"
        tituloCashback.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_1
        tituloCashback.sizeToFit()
        self.scrollView.addSubview(tituloCashback)
        
        //value cashback
        xframe.origin = CGPoint(x: 40, y: tituloCashback.frame.minY)
        self.lblCashBack = UILabel(frame: xframe)
        self.scrollView.addSubview(self.lblCashBack)
        self.lblCashBack.textColor = Constantes.paletaColores.verdeCompletado
        let amountCash = (currentServicio.propuestas.first?.datosProfesional.amountCashback)!
        self.lblCashBack.text = "+\(amountCash) %"
        self.lblCashBack.font = Constantes.currentFonts.NunitoRegular.base
        self.lblCashBack.sizeToFit()
        self.lblCashBack.frame.origin.x = Constantes.tamaños.anchoPantalla - (self.lblCashBack.frame.size.width + 5)
        
        //TITULO DE SOLICITUD
        xframe.origin = CGPoint(x: 10, y:tituloCashback.frame.maxY + separacionCampos)
        let lblTituloSolicitud = UILabel(frame: xframe)
        lblTituloSolicitud.text = "TÍTULO DE SOLICITUD"
        lblTituloSolicitud.textColor = Constantes.paletaColores.verdePeoples
        lblTituloSolicitud.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_1
        self.scrollView.addSubview(lblTituloSolicitud)
        
        xframe.origin = CGPoint(x: 10, y:lblTituloSolicitud.frame.maxY)
        lblDescTituloSolicitud = UILabel(frame: xframe)
        lblDescTituloSolicitud.text = currentServicio.tituloSolicitud
        lblDescTituloSolicitud.numberOfLines = 0
        lblDescTituloSolicitud.font = Constantes.currentFonts.NunitoRegular.minus.LabelFontSize_1
        lblDescTituloSolicitud.sizeToFit()
        
        self.scrollView.addSubview(lblDescTituloSolicitud)
        
        //detalles del servicio
        //titulo
        xframe.origin = CGPoint(x: 10, y: lblDescTituloSolicitud.frame.maxY + separacionCampos)
        let tituloDetalles = UILabel(frame: xframe)
        tituloDetalles.textColor = Constantes.paletaColores.verdePeoples
        tituloDetalles.text = "DETALLES DEL SERVICIO"
        tituloDetalles.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_1
        tituloDetalles.sizeToFit()
        self.scrollView.addSubview(tituloDetalles)
        
        //value detalles servicio
        xframe.origin = CGPoint(x: 10, y: tituloDetalles.frame.maxY + (separacionCampos / 2))
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla * 0.95, height: 30)
        self.lblDetallesServicio = UILabel(frame: xframe)
        self.lblDetallesServicio.numberOfLines = 0
        self.lblDetallesServicio.text = self.currentServicio.descripcion
        self.lblDetallesServicio.font = Constantes.currentFonts.NunitoRegular.minus.LabelFontSize_1
        self.lblDetallesServicio.sizeToFit()
        self.lblDetallesServicio.center.x = Constantes.tamaños.centroHorizontal
        self.scrollView.addSubview(self.lblDetallesServicio)
        
        //botones
        
        let anchoBoton = Constantes.tamaños.anchoPantalla * 0.3
        
        //cancelar
        xframe.origin = CGPoint(x: 30, y: self.lblDetallesServicio.frame.maxY + separacionCampos)
        xframe.size = CGSize(width: anchoBoton, height: Constantes.tamaños.altoBotonTop)
        self.btnCancelar = UIButton(frame: xframe)
        self.btnCancelar.setTitle("Cancelar", for: .normal)
        self.btnCancelar.backgroundColor = UIColor.lightGray
        self.btnCancelar.setTitleColor(UIColor.white, for: .normal)
        self.btnCancelar.layer.cornerRadius = 10
        self.btnCancelar.titleLabel?.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_1
        self.scrollView.addSubview(self.btnCancelar)
        //        self.btnCancelar.center.x = self.btnTerminar.frame.minX / 2
        self.btnCancelar.addTarget(self, action: #selector(self.confirmaCancelarServicio), for: .touchUpInside)
        
        //chat
        xframe.origin = CGPoint(x: 30, y: self.lblDetallesServicio.frame.maxY + separacionCampos)
        xframe.size = CGSize(width: anchoBoton, height: Constantes.tamaños.altoBotonTop)
        self.btnChat = UIButton(frame: xframe)
        self.btnChat.setTitle("Chat", for: .normal)
        self.btnChat.backgroundColor = Constantes.paletaColores.verdeCompletado
        self.btnChat.setTitleColor(UIColor.white, for: .normal)
        self.btnChat.layer.cornerRadius = 10
        self.btnChat.titleLabel?.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_1
        self.scrollView.addSubview(self.btnChat)
        let centerXBtnChat = self.btnCancelar.frame.maxX + ((Constantes.tamaños.anchoPantalla - self.btnCancelar.frame.maxX) / 2)
        self.btnChat.center.x = centerXBtnChat
        self.btnChat.addTarget(self, action: #selector(self.gotoChat), for: .touchUpInside)
        
        
        //padding
        let padding = (self.navigationController?.navigationBar.frame.size.height)! + Constantes.tamaños.altoStatusBar + 10
        self.scrollView.contentSize.height = self.btnChat.frame.maxY + padding
        
        self.view.addSubview(self.scrollView)
        
        if ((self.currentServicio.estatus.id != "1") &&
            (self.currentServicio.estatus.id != "2")){
            self.btnCancelar.isHidden = true
            self.btnChat.isHidden = true
        }
    }
    
    //MARK: -  Events
    @objc func confirmaTerminarServicio(){
        let alert = UIAlertController(title: "People's App",
                                      message: "¿Esta seguro que desea marcar este servicio como TERMINADO?",
                                      preferredStyle: UIAlertControllerStyle.alert)
        
        let okButton = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: {(
            alert: UIAlertAction!) in
            self.showLoader()
            self.actualizarServicio(estatus: "COMPLETADA")
            
        })
        
        let cancelButton = UIAlertAction(title: "CANCEL", style: UIAlertActionStyle.cancel, handler: nil)
        
        alert.addAction(cancelButton)
        alert.addAction(okButton)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func confirmaCancelarServicio(){
        let alert = UIAlertController(title: "People's App",
                                      message: "¿Esta seguro que desea marcar este servicio como CANCELADO?",
                                      preferredStyle: UIAlertControllerStyle.alert)
        
        let okButton = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: {(
            alert: UIAlertAction!) in
            self.showLoader()
            self.actualizarServicio(estatus: "CANCELADA")
        })
        
        let cancelButton = UIAlertAction(title: "CANCEL", style: UIAlertActionStyle.cancel, handler: nil)
        
        alert.addAction(cancelButton)
        alert.addAction(okButton)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func actualizarServicio(estatus: String){
        Functions().actualizaServicio(idServicio: self.currentServicio.id, estatus: estatus) { exito, mensaje, error in
            self.hideLoader()
            if let xerror = error {
                print("actualizarServicio - Ocurrio un error \(xerror)")
                self.mensajeSimple(mensaje: "Error de red", dismiss: false)
            } else {
                if exito {
                    let alert = UIAlertController(title: "People's App", message: mensaje, preferredStyle: UIAlertControllerStyle.alert)
                    
                    let okButton = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: {(
                        alert: UIAlertAction!) in
                        if estatus == "COMPLETADA" {
//                            self.gotoCalificar()
                        } else {
                            _ = self.navigationController?.popViewController(animated: true)
                            NotificationCenter.default.post(name: Constantes.notificationNames.backToListadoPedidos, object: nil)
                        }
                    })
                    
                    alert.addAction(okButton)
                    
                    self.present(alert, animated: true, completion: nil)
                } else {
                    self.mensajeSimple(mensaje: mensaje, dismiss: false)
                }
            }
        }
        
    }
    
//    func gotoCalificar(){
//        let elVC = CalificarServVC()
//        elVC.currentPropuesta = self.currentServicio.propuestas.first
//        self.navigationController?.present(elVC, animated: true, completion: nil)
//        
//    }
    
    @objc func gotoChat(){
        let elVC = ChatVC()
        elVC.currentPropuesta = self.currentServicio.propuestas.first
        self.navigationController?.pushViewController(elVC, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
