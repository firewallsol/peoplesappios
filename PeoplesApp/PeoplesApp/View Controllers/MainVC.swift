//
//  MainVC.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 29/06/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import SideMenu
import ImageSlideshow
import Kingfisher
import CoreLocation
import CocoaLumberjack

class MainVC: UIViewController, UITableViewDelegate, UITableViewDataSource,
UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, CLLocationManagerDelegate {
    
    //MARK: - Components
    var scrollView : UIScrollView!
    var imgCarrousel : ImageSlideshow!
    var btnNegocios : UIView!
    var btnServicio : UIView!
    var btnAfiliaSolicita : UIView!
    var lblBtnAfiliaSolicita : UILabel!
    var tableNegocios : UITableView!
    var collectionServicios : UICollectionView!
    
    //MARK: - Vars
    var arrayEmpresas = [Empresa]()
    var arrayServicios = [Categoria]()
    var arrayEmpresasPrincipales = [Empresa]()
    var arrayProfesionalesPrincipales = [Profesional]()
    var screenActual = OptionsSegmented.negocios.rawValue
    var  showLoaderBool = true
    var locationManager : CLLocationManager!
    var ubicacionActual: CLLocationCoordinate2D!
    var isDataLoaded = false
    
    enum OptionsSegmented: Int {
        case negocios = 1, servicios
    }
    
    enum CellIdentifier: String {
        case negocio = "celdaNegocios"
        case servicio = "celdaServicio"
    }
    
    //MARK: - View
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        
        self.view.backgroundColor = UIColor.white
        
        self.setBarMenu(setLogo: true)
        self.buildInterface(){ finished in
            //
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if self.showLoaderBool {
            self.showLoader()
        }
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.title = ""
        
        if Functions().isUserLoggedIn() {
            //Llamado para actualizar Estatus de Afiliacion localmente.
            //Se usa para traer los datos de Usuario del server, ahi contiene el estatus de afiliacion.
            DispatchQueue.global(qos: .default).async {
                Functions().getInfoCashBack { (success, message, usuario, datosCashback, error) in
                    //code
                }
            }
        }
    }
    
    //MARK: - Location Manager
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("actualice mi ubicacion = \(locations)")
        self.locationManager.stopUpdatingLocation()
        
        ubicacionActual = locations.last?.coordinate
        
        if !isDataLoaded {
            self.isDataLoaded = true
            Functions().getMainData(latitud: ubicacionActual.latitude.description,
                                    longitud: ubicacionActual.longitude.description) {
                                        exito, categorias, negocios, profesionales, error in
                                        self.showLoaderBool = false
                                        
                                        if error != nil {
                                            self.hideLoader()
                                            self.mensajeSimple(mensaje: "Error de red", dismiss: false)
                                        }
                                        else {
                                            if exito {
                                                self.arrayEmpresas = negocios
                                                self.arrayEmpresasPrincipales = Functions().getEmpresasPrincipales(arrayEmpresas: negocios)
                                                self.arrayServicios = categorias
                                                self.arrayProfesionalesPrincipales = profesionales
                                                
                                                //Completion
                                                self.getImageInputs(){ lasImagenes in
                                                    DDLogDebug("Descargó \(lasImagenes.count) imagenes para carrusel")

                                                    //Set principal business or logo
                                                    if lasImagenes.count > 0 {
                                                        self.imgCarrousel.setImageInputs(lasImagenes)
                                                        self.imgCarrousel.contentScaleMode = .scaleAspectFill
                                                    }
//                                                    else {
//                                                        self.imgCarrousel.setImageInputs([ImageSource(image: UIImage(named: "pplsAppLogo")!)])
//                                                        self.imgCarrousel.contentScaleMode = .scaleAspectFit
//                                                    }
                                                    
                                                    self.collectionServicios.reloadData()
                                                    
                                                    //Reload with completion
                                                    self.tableNegocios.reloadData({
                                                        self.TableCollection_Auto_Height()
                                                        self.hideLoader()
                                                    })
                                                }
                                                
                                            } else {
                                                self.hideLoader()
                                                self.mensajeSimple(mensaje: "Error desde servicio", dismiss: false)
                                            }
                                        }
            }
        }
    }
    
    //MARK:  - Table view datasource y delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableView.validaExistenciaDatos(datos: arrayEmpresas as AnyObject)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayEmpresas.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constantes.tamaños.altoCeldaNegociosMain
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.negocio.rawValue,
                                                 for: indexPath) as! CeldaTVNegociosMain
        
        cell.frame.size = CGSize(width: tableView.frame.size.width, height: cell.frame.size.height)
        let cellSizeHeight = cell.frame.size.height
        let cellSizeWidth = cell.frame.size.width
        
        let unNegocio = self.arrayEmpresas[indexPath.row]
        
        cell.imgNegocio.kf.indicatorType = .activity
        cell.imgNegocio.kf.setImage(with: unNegocio.imagen)
        
        cell.lblNombreNegocio.text = unNegocio.razonSocial
        let altolbl = cell.lblNombreNegocio.heightForView()
        cell.lblNombreNegocio.frame.size.height = altolbl
        
        cell.lblDirecNegocio.text = unNegocio.direcciones.first?.descripcion
        let anchoLabelDirec = cellSizeWidth - (cell.imgNegocio.frame.size.width + 5)
        cell.lblDirecNegocio.frame.size.width = anchoLabelDirec * 0.9
        cell.lblDirecNegocio.sizeToFit()
        cell.lblDirecNegocio.frame.origin.y = cell.lblNombreNegocio.frame.maxY + 3
        
        cell.lblCashBack.text = "Obtén Cash back"
        cell.lblCashBack.sizeToFit()
        var posycashback = cellSizeHeight - (cell.lblDirecNegocio.frame.maxY)
        posycashback = cell.lblDirecNegocio.frame.maxY + (posycashback / 2)
        cell.lblCashBack.center.y = posycashback
        
        cell.lblDistancia.text = "\(unNegocio.distancia!) km"
        cell.lblDistancia.sizeToFit()
        cell.lblDistancia.frame.origin.x = cellSizeWidth - (cell.lblDistancia.frame.size.width + 5)
        
        cell.imgShare.frame.origin.x = cellSizeWidth - (cell.imgShare.frame.size.width + 7)
        let shareTap = UITapGestureRecognizer(target: self, action: #selector(self.seleccionaBotonCompartir(sender:)))
        cell.imgShare.isUserInteractionEnabled = true
        cell.imgShare.addGestureRecognizer(shareTap)
        
        if Functions().existeIdEnFavoritos(id: unNegocio.id, donde: .empresa) {
            cell.imgLike.image = Asset.Icons.icFav.image
            cell.imgLike.tintColor = .red
        }
        
        cell.imgLike.frame.origin.x = cell.imgShare.frame.minX - (cell.imgLike.frame.size.width + 5)
        cell.imgLike.tag = indexPath.row
        let likeTap = UITapGestureRecognizer(target: self, action: #selector(self.likePressed(sender:)))
        cell.imgLike.isUserInteractionEnabled = true
        cell.imgLike.addGestureRecognizer(likeTap)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let elVC = DetalleComercioVC()
        elVC.currentEmpresa = self.arrayEmpresas[indexPath.row]
        self.navigationController?.pushViewController(elVC, animated: true)
    }
    
    //MARK: -  Collection view datasource,  delegate y flow layout
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrayServicios.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifier.servicio.rawValue,
                                                      for: indexPath as IndexPath) as! CeldaCVServicioMain
        
        let unServicio = self.arrayServicios[indexPath.item]
        
        var xframe = CGRect.zero
        xframe.origin = CGPoint(x: 10, y: 5)
        let anchoAltoImg = cell.frame.size.height * 0.7
        xframe.size = CGSize(width: anchoAltoImg, height: anchoAltoImg)
        cell.imgServicio.frame = xframe
        cell.imgServicio.center.x = cell.frame.size.width / 2
        cell.imgServicio.layer.cornerRadius = cell.imgServicio.frame.size.width / 2
        cell.imgServicio.clipsToBounds = true
        cell.imgServicio.kf.indicatorType = .activity
        cell.imgServicio.kf.setImage(with: unServicio.imagen)
        cell.imgServicio.setNeedsDisplay()
        
        xframe.origin = CGPoint(x: 10, y: cell.imgServicio.frame.maxY + 5)
        xframe.size = CGSize(width: cell.frame.size.width * 0.95, height: 30)
        cell.lblServicio.frame = xframe
        cell.lblServicio.text = unServicio.nombre
        cell.lblServicio.sizeToFit()
        cell.lblServicio.center.x = cell.frame.size.width / 2
        let yposcenterServ = cell.imgServicio.frame.maxY + ((cell.frame.size.height - cell.imgServicio.frame.maxY) / 2)
        cell.lblServicio.center.y =  yposcenterServ
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        var elTamaño = CGSize.zero
        
        let anchoCollView = collectionView.frame.size.width - 20
        
        elTamaño = CGSize(width: anchoCollView / 2.07, height: anchoCollView / 2.07)
        
        return elTamaño
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        
        return UIEdgeInsets(top: 5, left: 10, bottom: 10, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let elVC = ListaProfesionalesVC()
        elVC.ubicacionActual = ubicacionActual
        elVC.currentCategoria = self.arrayServicios[indexPath.item]
        self.navigationController?.pushViewController(elVC, animated: true)
        
    }
    
    //MARK: - Events
    @objc func tapBtnAfiliaSolicita(tap: UITapGestureRecognizer){
        
        if Functions().isUserLoggedIn() {
            if self.screenActual == OptionsSegmented.servicios.rawValue {
                
                //Valida que el usuario tenga un país de origen.
                let pais = Functions().getUserPais()
                if pais.descripcion != "" {
                    let elVC = SolicitarServicioVC()
                    elVC.sender = .Main
                    elVC.arrayServicios = self.arrayServicios
                    self.navigationController?.pushViewController(elVC, animated: true)
                }
                else {
                    let mensaje = NSLocalizedString("AlertShowEditProfileOnProfileIncomplete", comment: "")
                    let titulo = NSLocalizedString("AppName", comment: "")
                    let viewController = EditarPerfilVC()
                    
                    self.alertaRedirige(viewController: viewController, mensaje: mensaje, titulo: titulo)
                }
            }
        }
        else {
            let mensaje = NSLocalizedString("AlertShowLoginOnRequestService", comment: "")
            let titulo = NSLocalizedString("AppName", comment: "")
            let viewController = LoginVC()
            
            self.alertaRedirige(viewController: viewController, mensaje: mensaje, titulo: titulo)
        }
    }
    
    @objc func likePressed(sender: UITapGestureRecognizer){
        let tagSender = (sender.view?.tag)!
        let unNegocio = self.arrayEmpresas[tagSender]
        
        if Functions().isUserLoggedIn() {
            let mensaje = Functions().agregarQuitarFavorito(id: unNegocio.id, elJson: unNegocio.jsonData, donde: .empresa)
            self.mensajeSimple(mensaje: mensaje)
            let xindexPath = IndexPath(row: tagSender, section: 0)
            self.tableNegocios.reloadRows(at: [xindexPath], with: UITableViewRowAnimation.automatic)
        }
    }
    
    @objc func seleccionaBotonCompartir(sender: UITapGestureRecognizer) {
        let tagSender = (sender.view?.tag)!
        let unNegocio = self.arrayEmpresas[tagSender]
        
        let cell = tableNegocios.cellForRow(at: IndexPath(row: tagSender, section: 0)) as! CeldaTVNegociosMain

//        self.showLoader()
//        DispatchQueue.global(qos:.userInteractive).async {
            var imagen = UIImage()
            var finalString = String()
            
            if let image = cell.imgNegocio.image {
                imagen = image
            }
        
            if let xnombre = unNegocio.razonSocial {
                finalString += "\(xnombre)\n"
            }
            
            if let xdireccion = unNegocio.direcciones.first?.descripcion {
                finalString += "\(xdireccion)"
            }
            
            let activityView = UIActivityViewController(activityItems: [imagen, finalString],
                                                        applicationActivities: nil)
            
            activityView.excludedActivityTypes = [.airDrop, .openInIBooks,
                                                  .print, .addToReadingList,
                                                  .postToFlickr, .postToVimeo,
                                                  .postToWeibo, .postToTencentWeibo]
            
//            DispatchQueue.main.async {
                self.present(activityView, animated: true)
//                {
//                    self.hideLoader()
//                }
//            }
//        }
    }
    
    @objc func topButtonTapped(sender: UITapGestureRecognizer) {
        let tag = (sender.view?.tag)!
        switch tag {
        case OptionsSegmented.negocios.rawValue:
            if self.screenActual != OptionsSegmented.negocios.rawValue {
                self.screenActual = OptionsSegmented.negocios.rawValue
                self.changeInterface()
            }
            break
        case OptionsSegmented.servicios.rawValue:
            if self.screenActual != OptionsSegmented.servicios.rawValue {
                self.screenActual = OptionsSegmented.servicios.rawValue
                self.changeInterface()
            }
            break
            
        default:
            DDLogWarn("La opción seleccionada no existe.")
            break
        }
    }
    
    
    
    //MARK: - Custom
    func getImageInputs(completionHandler: @escaping ([AlamofireSource]) ->()) {
        var inputs = [AlamofireSource]()
        //    var contador = 0
        if self.screenActual == OptionsSegmented.negocios.rawValue {
            
            if self.arrayEmpresasPrincipales.count > 0 {
                for item in self.arrayEmpresasPrincipales {
                    
                    if let image = item.imagen {
                        let placeholder = UIImage(named: "pplsAppLogo")
                        let source = AlamofireSource(url: image, placeholder: placeholder)
                        inputs.append(source)
                    }
                }
                completionHandler(inputs)
            }
            else {
                completionHandler(inputs)
            }
            
        } else {
            if self.arrayProfesionalesPrincipales.count > 0 {
                for item in self.arrayProfesionalesPrincipales {
                    if let image = item.imagen {
                        let placeholder = UIImage(named: "pplsAppLogo")
                        let source = AlamofireSource(url: image, placeholder: placeholder)
                        inputs.append(source)
                    }
                }
                completionHandler(inputs)
            }
            else {
                completionHandler(inputs)
            }
        }
    }
    
    func changeInterface(){
        self.getImageInputs(){ lasImagenes in
            //Set principal business or logo
            if lasImagenes.count > 0 {
                self.imgCarrousel.setImageInputs(lasImagenes)
            }
//            else {
//                self.imgCarrousel.setImageInputs([ImageSource(image: UIImage(named: "pplsAppLogo")!)])
//            }
        }
        
        if self.screenActual == OptionsSegmented.negocios.rawValue {
            UIView.animate(withDuration: 0.4, animations: {
                self.btnNegocios.backgroundColor = Constantes.paletaColores.verdePeoples
                (self.btnNegocios.subviews[0] as! UILabel).textColor = UIColor.white
                self.btnServicio.backgroundColor = UIColor.white
                (self.btnServicio.subviews[0] as! UILabel).textColor = Constantes.paletaColores.verdePeoples
                
            })
            
            self.lblBtnAfiliaSolicita.text = "AFILIA UN NEGOCIO"
            self.lblBtnAfiliaSolicita.sizeToFit()
            self.lblBtnAfiliaSolicita.center.x = Constantes.tamaños.anchoPantalla / 2
            self.lblBtnAfiliaSolicita.center.y = self.btnAfiliaSolicita.frame.size.height / 2
            self.btnAfiliaSolicita.isHidden = true
            
            self.collectionServicios.isHidden = true
            self.scrollView.sendSubview(toBack: self.collectionServicios)
            self.tableNegocios.isHidden = false
            self.tableNegocios.isScrollEnabled = false
            self.scrollView.bringSubview(toFront: self.tableNegocios)
            
        } else {
            UIView.animate(withDuration: 0.4, animations: {
                self.btnServicio.backgroundColor = Constantes.paletaColores.verdePeoples
                (self.btnServicio.subviews[0] as! UILabel).textColor = UIColor.white
                self.btnNegocios.backgroundColor = UIColor.white
                (self.btnNegocios.subviews[0] as! UILabel).textColor = Constantes.paletaColores.verdePeoples
                
            })
            
            self.lblBtnAfiliaSolicita.text = NSLocalizedString("RequestService", comment: "")
            self.lblBtnAfiliaSolicita.sizeToFit()
            self.lblBtnAfiliaSolicita.center.x = Constantes.tamaños.anchoPantalla / 2
            self.lblBtnAfiliaSolicita.center.y = self.btnAfiliaSolicita.frame.size.height / 2
            self.btnAfiliaSolicita.isHidden = false
            
            self.tableNegocios.isHidden = true
            
            self.collectionServicios.isHidden = false
            self.scrollView.sendSubview(toBack: self.tableNegocios)
            self.tableNegocios.isHidden = true
            self.scrollView.bringSubview(toFront: self.collectionServicios)
        }
        
        self.TableCollection_Auto_Height()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func TableCollection_Auto_Height() {
        let padding = (Constantes.tamaños.altoBotonTop + (self.navigationController?.navigationBar.frame.size.height)!) + Constantes.tamaños.altoStatusBar + 5
        if self.screenActual == OptionsSegmented.negocios.rawValue {
            
            if(self.tableNegocios.contentSize.height > self.tableNegocios.frame.height){
                var frame: CGRect = self.tableNegocios.frame;
                frame.size.height = self.tableNegocios.contentSize.height;
                self.tableNegocios.frame = frame;
            }
            self.scrollView.contentSize.height = self.tableNegocios.frame.maxY + padding
        }
        else {
            if(self.collectionServicios.contentSize.height > self.collectionServicios.frame.height){
                var frame: CGRect = self.collectionServicios.frame;
                frame.size.height = self.collectionServicios.contentSize.height;
                self.collectionServicios.frame = frame;
            }
            self.scrollView.contentSize.height = self.collectionServicios.frame.maxY + padding
        }
    }
    
    @objc func carrouselTapped(sender: UITapGestureRecognizer){
        if arrayEmpresasPrincipales.count > 0 {
            if self.screenActual == OptionsSegmented.negocios.rawValue {
                let elVC = DetalleComercioVC()
                elVC.currentEmpresa = self.arrayEmpresasPrincipales[self.imgCarrousel.currentPage]
                self.navigationController?.pushViewController(elVC, animated: true)
            } else {
                let elVC = DetalleProfesionalVC()
                elVC.currentPro = self.arrayProfesionalesPrincipales[self.imgCarrousel.currentPage]
                self.navigationController?.pushViewController(elVC, animated: true)
            }
        }
    }
    
    func buildInterface(completionHandler: @escaping (Bool) -> ()){
        var xframe = CGRect.zero
        
        //let yposInicial = (self.navigationController?.navigationBar.frame.size.height)! + Constantes.tamaños.altoStatusBar
        
        self.scrollView = UIScrollView(frame: self.view.frame)
        
        //carrusel
        xframe.origin = CGPoint(x: 0, y: 0)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: Constantes.tamaños.anchoPantalla * 0.5)
        self.imgCarrousel = ImageSlideshow(frame: xframe)
        self.scrollView.addSubview(self.imgCarrousel)
        self.imgCarrousel.slideshowInterval = 5
        self.imgCarrousel.activityIndicator = DefaultActivityIndicator()
        //Initial image for slideshow
        self.imgCarrousel.setImageInputs([ImageSource(image: UIImage(named: "pplsAppLogo")!)])
        self.imgCarrousel.contentScaleMode = .scaleAspectFit
        
        let tapCarrousel = UITapGestureRecognizer(target: self, action: #selector(self.carrouselTapped(sender:)))
        self.imgCarrousel.isUserInteractionEnabled = true
        self.imgCarrousel.addGestureRecognizer(tapCarrousel)
        
        //botones negocio servicio
        let anchoBotonTop = Constantes.tamaños.anchoPantalla / 2
        
        //negocios
        xframe.origin = CGPoint(x: 0, y: self.imgCarrousel.frame.maxY)
        xframe.size = CGSize(width: anchoBotonTop, height: Constantes.tamaños.altoBotonTop)
        self.btnNegocios = UIView(frame: xframe)
        self.btnNegocios.backgroundColor = Constantes.paletaColores.verdePeoples
        self.btnNegocios.layer.borderColor = Constantes.paletaColores.verdePeoples.cgColor
        self.btnNegocios.layer.borderWidth = 0.5
        self.btnNegocios.tag = 1
        self.scrollView.addSubview(self.btnNegocios)
        let tapBtnNegocio = UITapGestureRecognizer(target: self, action: #selector(self.topButtonTapped(sender:)))
        self.btnNegocios.isUserInteractionEnabled = true
        self.btnNegocios.addGestureRecognizer(tapBtnNegocio)
        //label
        let lblBtnNegocios = UILabel(frame: xframe)
        lblBtnNegocios.text = NSLocalizedString("Business", comment: "")
        lblBtnNegocios.textColor = UIColor.white
        lblBtnNegocios.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_2
        lblBtnNegocios.sizeToFit()
        self.btnNegocios.addSubview(lblBtnNegocios)
        lblBtnNegocios.center.x = self.btnNegocios.frame.size.width / 2
        lblBtnNegocios.center.y = self.btnNegocios.frame.size.height / 2
        
        //servicio
        xframe.origin = CGPoint(x: self.btnNegocios.frame.maxX, y: self.imgCarrousel.frame.maxY)
        xframe.size = CGSize(width: anchoBotonTop, height: Constantes.tamaños.altoBotonTop)
        self.btnServicio = UIView(frame: xframe)
        self.btnServicio.layer.borderColor = Constantes.paletaColores.verdePeoples.cgColor
        self.btnServicio.layer.borderWidth = 0.5
        self.btnServicio.tag = 2
        self.scrollView.addSubview(self.btnServicio)
        let tapBtnServicio = UITapGestureRecognizer(target: self, action: #selector(self.topButtonTapped(sender:)))
        self.btnServicio.isUserInteractionEnabled = true
        self.btnServicio.addGestureRecognizer(tapBtnServicio)
        //label
        let lblBtnServicio = UILabel(frame: xframe)
        lblBtnServicio.text = NSLocalizedString("Services", comment: "")
        lblBtnServicio.textColor = Constantes.paletaColores.verdePeoples
        lblBtnServicio.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_2
        lblBtnServicio.sizeToFit()
        self.btnServicio.addSubview(lblBtnServicio)
        lblBtnServicio.center.x = self.btnServicio.frame.size.width / 2
        lblBtnServicio.center.y = self.btnServicio.frame.size.height / 2
        
        //tableview
        xframe.origin = CGPoint(x: 0, y: self.btnNegocios.frame.maxY)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: 300)
        self.tableNegocios = UITableView(frame: xframe)
        self.scrollView.addSubview(self.tableNegocios)
        self.tableNegocios.delegate = self
        self.tableNegocios.dataSource = self
        self.tableNegocios.register(CeldaTVNegociosMain.self,
                                    forCellReuseIdentifier: CellIdentifier.negocio.rawValue)
        
        self.tableNegocios.tableFooterView = UIView(frame: CGRect.zero)
        
        //collection servicios
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        self.collectionServicios = UICollectionView(frame: xframe, collectionViewLayout: layout)
        self.collectionServicios.delegate = self
        self.collectionServicios.dataSource = self
        self.collectionServicios.backgroundColor = UIColor.white
        self.collectionServicios.register(CeldaCVServicioMain.self,
                                          forCellWithReuseIdentifier: CellIdentifier.servicio.rawValue)
        self.collectionServicios.isHidden = true
        self.scrollView.addSubview(self.collectionServicios)
        self.scrollView.sendSubview(toBack: self.collectionServicios)
        
        self.view.addSubview(self.scrollView)
        
        //btn afilia negocio o pide servicio
        let padding = (Constantes.tamaños.altoBotonTop + (self.navigationController?.navigationBar.frame.size.height)!) + Constantes.tamaños.altoStatusBar
        xframe.origin = CGPoint(x: 0, y: self.view.frame.maxY - padding)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: Constantes.tamaños.altoBotonTop)
        self.btnAfiliaSolicita = UIView(frame: xframe)
        self.btnAfiliaSolicita.backgroundColor = Constantes.paletaColores.verdePeoples
        self.lblBtnAfiliaSolicita = UILabel(frame: xframe)
        self.lblBtnAfiliaSolicita.text = "AFILIA UN NEGOCIO"
        self.lblBtnAfiliaSolicita.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_1
        self.lblBtnAfiliaSolicita.sizeToFit()
        self.lblBtnAfiliaSolicita.textColor = UIColor.white
        self.btnAfiliaSolicita.addSubview(self.lblBtnAfiliaSolicita)
        self.lblBtnAfiliaSolicita.center.x = Constantes.tamaños.anchoPantalla / 2
        self.lblBtnAfiliaSolicita.center.y = self.btnAfiliaSolicita.frame.size.height / 2
        let tapSolicitar = UITapGestureRecognizer(target: self, action: #selector(self.tapBtnAfiliaSolicita(tap:)))
        self.btnAfiliaSolicita.addGestureRecognizer(tapSolicitar)
        self.btnAfiliaSolicita.isUserInteractionEnabled = true
        self.btnAfiliaSolicita.isHidden = true
        
        self.view.addSubview(self.btnAfiliaSolicita)
        
        completionHandler(true)
    }
    
}
