//
//  PedidosMainVC.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 02/08/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import Kingfisher
import CocoaLumberjack

class PedidosMainVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //var btnActivo : UIView!
    //var btnHistorial : UIView!
    let btnItems = ["Solicitado","Activo","Historial"]
    var activoHistorialSegment : UISegmentedControl!
    var tablaPedidos : UITableView!
    
    var nombreCeldaPedidos = "celdaPedidos"
    
    var arraySolicitado = [Servicio]()
    var arrayActivos = [Servicio]()
    var arrayHistorico = [Servicio]()
    var currentArray = [Servicio]()
    
    var refreshControl = UIRefreshControl()
    
    enum OpcionSegmento: String {
        case solicitado = "Solicitado"
        case activo = "Activo"
        case historial = "Historial"
    }

    //MARK: - View
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.white
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        self.refreshControl.addTarget(self, action: #selector(self.loadData), for: UIControlEvents.valueChanged)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.backToHere), name: Constantes.notificationNames.backToListadoPedidos, object: nil)
        
        self.setBarMenu()
        
        self.buildInterface(){ finished in
            self.tablaPedidos.addSubview(self.refreshControl)
            self.showLoader()
        }
        
        self.loadData()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationItem.title = ""
    }
    
    @objc func backToHere(){
        //_ = self.navigationController?.popViewController(animated: true)
        self.loadData()
    }
    
    
    func buildInterface(completionHandler: @escaping (Bool) -> ()){
        
        var xframe = CGRect.zero
        
        //boton activo
        xframe.origin = CGPoint(x: 0, y: 5)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: Constantes.tamaños.altoBotonTop)
        self.activoHistorialSegment = UISegmentedControl(items: self.btnItems)
        self.activoHistorialSegment.frame = xframe
        self.view.addSubview(self.activoHistorialSegment)
        self.activoHistorialSegment.selectedSegmentIndex = 0
        self.activoHistorialSegment.tintColor = Constantes.paletaColores.verdePeoples
        let font = Constantes.currentFonts.NunitoBold.base
        self.activoHistorialSegment.setTitleTextAttributes([NSAttributedStringKey.font: font!], for: .normal)
        self.activoHistorialSegment.addTarget(self, action: #selector(self.segmentChanged(sender:)), for: .valueChanged)
        
        //tabla
        xframe.origin = CGPoint(x: 0, y: self.activoHistorialSegment.frame.size.height + 5)
        let altoTable = Constantes.tamaños.altoPantalla - Constantes.tamaños.altoBotonTop
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: altoTable)
        self.tablaPedidos = UITableView(frame: xframe)
        self.tablaPedidos.delegate = self
        self.tablaPedidos.dataSource = self
        self.tablaPedidos.register(CeldaTVPedidos.self, forCellReuseIdentifier: self.nombreCeldaPedidos)
        self.view.addSubview(self.tablaPedidos)
        self.tablaPedidos.tableFooterView = UIView(frame: CGRect.zero)
        
        completionHandler(true)
    }
    
    //MARK: - TableView datasource y delegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constantes.tamaños.altoCeldaPedidos
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableView.validaExistenciaDatos(datos:self.currentArray as AnyObject)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.currentArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.nombreCeldaPedidos, for: indexPath) as! CeldaTVPedidos
        cell.frame.size = CGSize(width: tableView.frame.size.width, height: cell.frame.size.height)
        let cellSizeHeight = cell.frame.size.height
        let cellSizeWidth = cell.frame.size.width
        
        let elPedido = self.currentArray[indexPath.row]
        
        cell.imgProfesional.kf.indicatorType = .activity
        
        cell.imgProfesional.kf.setImage(with: elPedido.categoria.imagen)
        
        cell.lblNombreServicio.text = elPedido.tituloSolicitud
        cell.lblNombreServicio.frame.size.width = cellSizeWidth * 0.35
        cell.lblNombreServicio.sizeToFit()
        
        let baseNombreServ = (cellSizeHeight / 2) - (cell.lblNombreServicio.frame.size.height)
        cell.lblNombreServicio.frame.origin.y = baseNombreServ
        
        cell.lblNombreProfesional.text = "\(elPedido.propuestas.count) propuesta(s)"
        cell.lblNombreProfesional.frame.size.width = cellSizeWidth * 0.35
        cell.lblNombreProfesional.sizeToFit()
        cell.lblNombreProfesional.frame.origin.y = cell.lblNombreServicio.frame.maxY + 3
        cell.lblNombreProfesional.frame.origin.x = cell.lblNombreServicio.frame.minX
        
        let posView = cellSizeWidth - (cell.estatusView.frame.size.width + 5)
        cell.estatusView.frame.origin.x = posView
        
        if (elPedido.estatus != nil)  {
            cell.estatusView.backgroundColor = Constantes.paletaColores.verdeCompletado
        }
        cell.lblEstatus.text = elPedido.estatus.descripcion
        cell.lblEstatus.sizeToFit()
        cell.lblEstatus.center.x = cell.estatusView.frame.size.width / 2
        cell.lblEstatus.center.y = cell.estatusView.frame.size.height / 2 
        
        cell.lblFecha.text = Functions().dateToString(laFecha: elPedido.fechaServicio)
        cell.lblFecha.sizeToFit()
        cell.lblFecha.frame.origin.y = cell.estatusView.frame.maxY + 3
        let centerFecha = cell.estatusView.frame.minX + (cell.estatusView.frame.size.width / 2)
        cell.lblFecha.center.x = centerFecha
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var elVC = UIViewController()
        let servicio = self.currentArray[indexPath.row]
        
        if servicio.propuestas.count > 0 {
            switch self.activoHistorialSegment.selectedSegmentIndex {
            case OpcionSegmento.solicitado.hashValue:
                //Mostrará listado de propuestas.
                elVC = ListaPropuestasVC()
                (elVC as! ListaPropuestasVC).currentServicio = servicio
                break
                
            case OpcionSegmento.activo.hashValue, OpcionSegmento.historial.hashValue:
                elVC = DetallePedidoVC()
                (elVC as! DetallePedidoVC).currentServicio = servicio
                break
                
            default:
                DDLogWarn("No se localizó el segmento seleccionado.")
                break
            }
            
            self.navigationController?.pushViewController(elVC, animated: true)
        }
        else {
            let mensaje = NSLocalizedString("Aún no se tienen propuestas para esta solicitud.", comment: "")
            self.mensajeSimple(mensaje: mensaje)
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        if (self.activoHistorialSegment.selectedSegmentIndex == 0) || (self.activoHistorialSegment.selectedSegmentIndex == 1) {
            return true
        }
        return false
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "Cancel"
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath){
        
        if editingStyle == .delete {
            if (self.activoHistorialSegment.selectedSegmentIndex == 0) || (self.activoHistorialSegment.selectedSegmentIndex == 1) {
                self.confirmaCancel(row: indexPath.row)
            }
        }
        
    }
    
    //MARK: - Events
    @objc func loadData(){
        
        Functions().getListadosolicitudes(index: 0){ exito, serviciosSolicitados, serviciosActivos, serviciosHistoricos, error in
            
            self.hideLoader()
            
            if self.refreshControl.isRefreshing {
                self.refreshControl.endRefreshing()
            }
            
            if let xerror = error {
                print("GET listado solicitudes - Ocurrio un error \(xerror)")
                self.mensajeSimple(mensaje: "Error de red", dismiss: false)
            } else {
                if exito {
                    self.arraySolicitado = serviciosSolicitados
                    self.arrayActivos = serviciosActivos
                    self.arrayHistorico = serviciosHistoricos
                    
                    let serviciosPorEvaluar = self.obtieneServiciosCompletadosPorEvaluar(servicios: self.arrayHistorico)
                    if serviciosPorEvaluar.count > 0 {
                        let servicio = serviciosPorEvaluar.first
                        self.muestraEvaluacion(propuesta: (servicio?.propuestas.first)!)
                    }
                    self.segmentChanged(sender: self.activoHistorialSegment)
                }
                else {
                    self.mensajeSimple(mensaje: "Error desde servicio", dismiss: false)
                }
            }
        }
    }

    
    func confirmaCancel(row: Int){
        
        let alert = UIAlertController(title: "People's App", message: "¿Esta seguro que desea marcar este servicio como CANCELADO?", preferredStyle: UIAlertControllerStyle.alert)
        
        let okButton = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: {(
            alert: UIAlertAction!) in
            self.showLoader()
            self.actualizarServicio(idServicio: self.currentArray[row].id,estatus: "CANCELADA")
        })
        
        let cancelButton = UIAlertAction(title: "CANCEL", style: UIAlertActionStyle.cancel, handler: nil)
        
        alert.addAction(cancelButton)
        alert.addAction(okButton)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func actualizarServicio(idServicio: String, estatus: String){
        Functions().actualizaServicio(idServicio: idServicio, estatus: estatus) { exito, mensaje, error in
            
            if let xerror = error {
                self.hideLoader()
                DDLogError(xerror.localizedDescription)
                self.mensajeSimple(mensaje: "Error de red", dismiss: false)
            } else {
                if exito {
                    self.mensajeSimple(mensaje: mensaje)
                    self.loadData()
                } else {
                    self.hideLoader()
                    DDLogError(mensaje)
                    self.mensajeSimple(mensaje: mensaje, dismiss: false)
                }
            }
        }
        
    }
    
    @objc func segmentChanged(sender: UISegmentedControl){
        self.currentArray.removeAll()
        
        switch sender.selectedSegmentIndex {
        case 0: self.currentArray = self.arraySolicitado; break;
        case 1: self.currentArray = self.arrayActivos; break;
        case 2: self.currentArray = self.arrayHistorico; break;
        default:
            break
        }
        
        self.tablaPedidos.reloadData()
        DispatchQueue.main.async(execute: {
            self.tablaPedidos.contentSize.height = self.tablaPedidos.contentSize.height + (Constantes.tamaños.altoBotonTop + Constantes.tamaños.altoStatusBar + (self.navigationController?.navigationBar.frame.size.height)!) //separaciones
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func obtieneServiciosCompletadosPorEvaluar(servicios:[Servicio])-> [Servicio] {
        var arrayPorEvaluar = [Servicio]()
        if servicios.count > 0 {
            //Revisa si no está evaluado y está completado, se debe evaluar.
            arrayPorEvaluar = servicios.filter({ (servicio) -> Bool in
                if !servicio.profesionalCalificado &&
                    servicio.estatus.descripcion == EstatusServicio.EstatusDelServicio.completada.rawValue {
                    return true
                }
                return false
            })
            
            //            if arrayPorEvaluar.count > 0 {
            //                let servicio = arrayPorEvaluar.first
            //                self.muestraEvaluacion(propuesta: (servicio?.propuestas.first)!)
            //            }
        }
        print("ARRAY POR EVALUAR: \(arrayPorEvaluar.count)")
        return arrayPorEvaluar
    }
    
    func muestraEvaluacion(propuesta: Propuesta){
        let elVC = CalificarServVC()
        elVC.currentPropuesta = propuesta
        self.navigationController?.present(elVC, animated: true, completion: nil)
    }
}
