//
//  RegistroVC.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 31/07/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import BEMCheckBox

class RegistroVC: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    //MARK: - Components
    var txtNombre : UITextField!
    var txtApellidos : UITextField!
    var txtCelular : UITextField!
    var txtEmail : UITextField!
    var txtContra : UITextField!
    var txtRepiteContra : UITextField!
    var txtPais: UITextField!
    var picker: UIPickerView!
    var checkTerminos : BEMCheckBox!
    var btnContinuar : UIView!
    var scrollView : UIScrollView!
    
    //MARK: - Vars
    var arrayPaises = [Pais]()
    var paisSelected = Pais()
    
    //MARK: - View
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //WARNING: Usa paises hardcoded.
        arrayPaises = Constantes.arrayPaises
        
        picker = UIPickerView()
        picker.delegate = self
        picker.dataSource = self
        
        let tapGesture = UITapGestureRecognizer()
        tapGesture.addTarget(self, action: #selector(tapToDismiss))
        self.view.isUserInteractionEnabled = true
        self.view.addGestureRecognizer(tapGesture)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        self.automaticallyAdjustsScrollViewInsets = false
        self.buildInterface()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isTranslucent = false
        
        self.navigationController?.navigationBar.tintColor = Constantes.paletaColores.verdePeoples
        
        self.navigationItem.title = NSLocalizedString("Registry", comment: "")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Keyboard
    @objc func keyboardWillShow(sender: NSNotification) {
        
        var userInfo = sender.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height
        self.scrollView.contentInset = contentInset
        
        var visibleRect = self.view.frame
        visibleRect.size.height -= keyboardFrame.size.height
        
        var elOrigin = CGPoint.zero
        
        var scroll = false
        
        if self.txtCelular.isFirstResponder {
            visibleRect.size.height -= ((self.txtCelular.inputAccessoryView?.frame.size.height)! * 2)
            elOrigin = self.txtCelular.frame.origin
            scroll = true
        }
        
        if self.txtEmail.isFirstResponder {
            visibleRect.size.height -= ((self.txtEmail.inputAccessoryView?.frame.size.height)! * 2)
            elOrigin = self.txtEmail.frame.origin
            scroll = true
        }
        
        if self.txtContra.isFirstResponder {
            visibleRect.size.height -= ((self.txtContra.inputAccessoryView?.frame.size.height)! * 2)
            elOrigin = self.txtContra.frame.origin
            scroll = true
        }
        
        if self.txtRepiteContra.isFirstResponder {
            visibleRect.size.height -= ((self.txtRepiteContra.inputAccessoryView?.frame.size.height)! * 2)
            elOrigin = self.txtRepiteContra.frame.origin
            scroll = true
        }
        
        //        if self.txtPais.isFirstResponder {
        //            visibleRect.size.height -= ((self.txtPais.inputAccessoryView?.frame.size.height)! * 2)
        //            elOrigin = self.txtPais.frame.origin
        //            scroll = true
        //        }
        
        if ((scroll) && !(visibleRect.contains(elOrigin))){
            let scrollPoint = CGPoint(x: 0, y: elOrigin.y - visibleRect.size.height)
            self.scrollView.setContentOffset(scrollPoint, animated: true)
        }
    }
    
    @objc func keyboardWillHide(sender: NSNotification) {
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        self.scrollView.contentInset = contentInset
    }
    
    //MARK: - Picker
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayPaises.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let pais = arrayPaises[row]
        return pais.descripcion
    }
    
//    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
//        let pais = arrayPaises[row]
//        paisSelected = pais
//        
//        txtPais.text = pais.descripcion
//    }
    
    //MARK: - Custom
    func buildInterface(){
        self.scrollView = UIScrollView(frame: self.view.frame)
        self.scrollView.backgroundColor = UIColor.white
        var xframe = CGRect.zero
        let separacionCampos : CGFloat = 10
        let cornerCampos : CGFloat = 3
        
        //nombre
        xframe.size = CGSize(width: Constantes.tamaños.anchoTextfield, height: Constantes.tamaños.altoTextfield)
        xframe.origin = CGPoint(x: 10, y: 10)
        self.txtNombre = UITextField(frame: xframe)
        self.txtNombre.placeholder = NSLocalizedString("FirstName", comment: "")
        self.txtNombre.font = Constantes.currentFonts.NunitoRegular.base
        self.txtNombre.layer.borderWidth = Constantes.tamaños.sizeBordeTextfield
        self.txtNombre.layer.borderColor = Constantes.paletaColores.bordeTextfield.cgColor
        self.txtNombre.layer.cornerRadius = cornerCampos
        self.txtNombre.center.x = Constantes.tamaños.centroHorizontal
        self.txtNombre.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0)
        self.txtNombre.keyboardType = .alphabet
        self.addToolBar(textField: self.txtNombre)
        self.scrollView.addSubview(self.txtNombre)
        
        //apellidos
        xframe.origin = CGPoint(x: 10, y: self.txtNombre.frame.maxY + separacionCampos)
        self.txtApellidos = UITextField(frame: xframe)
        self.txtApellidos.placeholder = NSLocalizedString("LastName", comment: "")
        self.txtApellidos.font = Constantes.currentFonts.NunitoRegular.base
        self.txtApellidos.layer.borderWidth = Constantes.tamaños.sizeBordeTextfield
        self.txtApellidos.layer.borderColor = Constantes.paletaColores.bordeTextfield.cgColor
        self.txtApellidos.layer.cornerRadius = cornerCampos
        self.txtApellidos.center.x = Constantes.tamaños.centroHorizontal
        self.txtApellidos.keyboardType = .alphabet
        self.txtApellidos.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0)
        self.addToolBar(textField: self.txtApellidos)
        self.scrollView.addSubview(self.txtApellidos)
        
        //celular
        xframe.origin = CGPoint(x: 10, y: self.txtApellidos.frame.maxY + separacionCampos)
        self.txtCelular = UITextField(frame: xframe)
        self.txtCelular.placeholder = NSLocalizedString("Cellphone", comment: "")
        self.txtCelular.font = Constantes.currentFonts.NunitoRegular.base
        self.txtCelular.layer.borderWidth = Constantes.tamaños.sizeBordeTextfield
        self.txtCelular.layer.borderColor = Constantes.paletaColores.bordeTextfield.cgColor
        self.txtCelular.layer.cornerRadius = cornerCampos
        self.txtCelular.center.x = Constantes.tamaños.centroHorizontal
        self.txtCelular.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0)
        self.txtCelular.keyboardType = .numberPad
        self.addToolBar(textField: self.txtCelular)
        self.scrollView.addSubview(self.txtCelular)
        
        //Pais
        xframe.origin = CGPoint(x: 10, y: self.txtCelular.frame.maxY + separacionCampos)
        self.txtPais = UITextField(frame: xframe)
        self.txtPais.placeholder = NSLocalizedString("Country", comment: "")
        self.txtPais.font = Constantes.currentFonts.NunitoRegular.base
        self.txtPais.layer.borderWidth = Constantes.tamaños.sizeBordeTextfield
        self.txtPais.layer.borderColor = Constantes.paletaColores.bordeTextfield.cgColor
        self.txtPais.layer.cornerRadius = cornerCampos
        self.txtPais.center.x = Constantes.tamaños.centroHorizontal
        self.txtPais.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0)
        self.txtPais.inputView = picker
        self.txtPais.inputAccessoryView = createToolbarPais()
        self.scrollView.addSubview(self.txtPais)
        
        //email
        xframe.origin = CGPoint(x: 10, y: self.txtPais.frame.maxY + separacionCampos)
        self.txtEmail = UITextField(frame: xframe)
        self.txtEmail.placeholder = NSLocalizedString("Email", comment: "")
        self.txtEmail.font = Constantes.currentFonts.NunitoRegular.base
        self.txtEmail.layer.borderWidth = Constantes.tamaños.sizeBordeTextfield
        self.txtEmail.layer.borderColor = Constantes.paletaColores.bordeTextfield.cgColor
        self.txtEmail.layer.cornerRadius = cornerCampos
        self.txtEmail.center.x = Constantes.tamaños.centroHorizontal
        self.txtEmail.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0)
        self.txtEmail.keyboardType = .emailAddress
        self.txtEmail.autocorrectionType = .no
        self.txtEmail.autocapitalizationType = .none
        self.addToolBar(textField: self.txtEmail)
        self.scrollView.addSubview(self.txtEmail)
        
        //contra 
        xframe.origin = CGPoint(x: 10, y: self.txtEmail.frame.maxY + separacionCampos)
        self.txtContra = UITextField(frame: xframe)
        self.txtContra.placeholder = NSLocalizedString("Password", comment: "")
        self.txtContra.font = Constantes.currentFonts.NunitoRegular.base
        self.txtContra.layer.borderWidth = Constantes.tamaños.sizeBordeTextfield
        self.txtContra.layer.borderColor = Constantes.paletaColores.bordeTextfield.cgColor
        self.txtContra.layer.cornerRadius = cornerCampos
        self.txtContra.isSecureTextEntry = true
        self.txtContra.center.x = Constantes.tamaños.centroHorizontal
        self.txtContra.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0)
        self.addToolBar(textField: self.txtContra)
        self.scrollView.addSubview(self.txtContra)
        
        //repite contra 
        xframe.origin = CGPoint(x: 10, y: self.txtContra.frame.maxY + separacionCampos)
        self.txtRepiteContra = UITextField(frame: xframe)
        self.txtRepiteContra.placeholder = NSLocalizedString("RepeatPassword", comment: "")
        self.txtRepiteContra.font = Constantes.currentFonts.NunitoRegular.base
        self.txtRepiteContra.layer.borderWidth = Constantes.tamaños.sizeBordeTextfield
        self.txtRepiteContra.layer.borderColor = Constantes.paletaColores.bordeTextfield.cgColor
        self.txtRepiteContra.layer.cornerRadius = cornerCampos
        self.txtRepiteContra.isSecureTextEntry = true
        self.txtRepiteContra.center.x = Constantes.tamaños.centroHorizontal
        self.txtRepiteContra.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0)
        self.addToolBar(textField: self.txtRepiteContra)
        self.scrollView.addSubview(self.txtRepiteContra)
        
        //check terminos
        xframe.size = CGSize(width: 20, height: 20)
        xframe.origin = CGPoint(x: self.txtRepiteContra.frame.minX, y: self.txtRepiteContra.frame.maxY + 30)
        self.checkTerminos = BEMCheckBox(frame: xframe)
        self.checkTerminos.tintColor = Constantes.paletaColores.verdePeoples
        self.checkTerminos.onCheckColor = UIColor.white
        self.checkTerminos.onFillColor = Constantes.paletaColores.verdePeoples
        self.checkTerminos.onTintColor = Constantes.paletaColores.verdePeoples
        self.checkTerminos.onAnimationType = .fill
        self.checkTerminos.offAnimationType = .fill
        self.scrollView.addSubview(self.checkTerminos)
        
        //label terminos
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla * 0.8, height: 25)
        xframe.origin = CGPoint(x: self.checkTerminos.frame.maxX + separacionCampos, y: self.checkTerminos.frame.minY)
        let lblTerminos = UILabel(frame: xframe)
        
        let underlineAttribute = [NSAttributedStringKey.underlineStyle: NSUnderlineStyle.styleSingle.rawValue]
        let underlineAttributedString = NSAttributedString(string:  NSLocalizedString("AcceptTerms", comment: ""),
                                                           attributes: underlineAttribute)
        lblTerminos.attributedText = underlineAttributedString
        
        lblTerminos.font = Constantes.currentFonts.NunitoRegular.base
        lblTerminos.adjustsFontSizeToFitWidth = true
        lblTerminos.textColor = UIColor.gray
        lblTerminos.center.y = self.checkTerminos.frame.minY + (self.checkTerminos.frame.size.height / 2)
        
        //gesture for label.
        let tapGesture = UITapGestureRecognizer()
        tapGesture.addTarget(self, action: #selector(seleccionaTerminosCondiciones))
        lblTerminos.addGestureRecognizer(tapGesture)
        lblTerminos.isUserInteractionEnabled = true
        
        self.scrollView.addSubview(lblTerminos)
        
        self.view.addSubview(self.scrollView)
        
        //btn continuar
        let padding = (Constantes.tamaños.altoBotonTop + (self.navigationController?.navigationBar.frame.size.height)!) + Constantes.tamaños.altoStatusBar
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: Constantes.tamaños.altoBotonTop)
        xframe.origin = CGPoint(x: 0, y: Constantes.tamaños.altoPantalla - padding)
        self.btnContinuar = UIView(frame: xframe)
        self.btnContinuar.backgroundColor = Constantes.paletaColores.verdePeoples
        self.view.addSubview(self.btnContinuar)
        let lblBtncontinuar = UILabel(frame: xframe)
        lblBtncontinuar.text = NSLocalizedString("ButtonContinue", comment: "").uppercased()
        lblBtncontinuar.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_1
        lblBtncontinuar.sizeToFit()
        lblBtncontinuar.textColor = UIColor.white
        self.btnContinuar.addSubview(lblBtncontinuar)
        lblBtncontinuar.center.x = Constantes.tamaños.anchoPantalla / 2
        lblBtncontinuar.center.y = self.btnContinuar.frame.size.height / 2
        let tapBtnContinuar = UITapGestureRecognizer(target: self, action: #selector(self.btnContinuarTapped))
        self.btnContinuar.isUserInteractionEnabled = true
        self.btnContinuar.addGestureRecognizer(tapBtnContinuar)
        
        self.scrollView.contentSize.height = self.checkTerminos.frame.maxY + padding + 30
    }
    
    func validaForma()->Bool{
        
        var mensaje = ""
        
        //nombre
        if (self.txtNombre.text?.trimmed() == ""){
            mensaje = "El campo \"Nombre\", es obligatorio"
            
        }
        //nombre
        if (mensaje == "" && self.txtApellidos.text?.trimmed() == ""){
            mensaje = "El campo \"Apellidos\", es obligatorio"
            
        }
        //correo
        if (mensaje == "" && (self.txtEmail.text?.trimmed() == "")){
            mensaje = "El campo \"Correo\", es obligatorio"
            
        }
        //celular
        if (mensaje == "" && (self.txtCelular.text?.trimmed() == "")){
            mensaje = "El campo \"Celular\", es obligatorio"
            
        }
        //pais
        if (mensaje == "" && (self.txtPais.text?.trimmed() == "")){
            mensaje = "El campo \"País\", es obligatorio"
        }
        
        //contra
        if (mensaje == "" && (self.txtContra.text?.trimmed() == "")){
            mensaje = "El campo \"Contraseña\", es obligatorio"
            
        }
        
        //contra
        if (mensaje == "" && ((self.txtContra.text?.trimmed().count)! < 5 )){
            mensaje = "El campo \"Contraseña\", requiere al menos 5 caracteres"
        }
        
        //conf contra
        if (mensaje == "" && (self.txtContra.text?.trimmed() != self.txtRepiteContra.text?.trimmed()) ){
            mensaje = "Las contraseñas no coinciden"
        }
        
        //terminos y condiciones
        if (mensaje == "" && !self.checkTerminos.on){
            mensaje = "Debe aceptar los términos y condiciones"
        }
        
        if mensaje != "" {
            
            self.mensajeSimple(mensaje: mensaje)
            
            return false
        }
        
        
        return true
    }
    
    func createToolbarPais() ->UIToolbar {
        let toolbarPickerFecha = UIToolbar()
        toolbarPickerFecha.barStyle = UIBarStyle.default
        toolbarPickerFecha.isTranslucent = true
        toolbarPickerFecha.sizeToFit()
        
        let okBtnPickF = UIBarButtonItem(title: NSLocalizedString("OK", comment:""),
                                         style: UIBarButtonItemStyle.plain,
                                         target: self,
                                         action: #selector(self.selectKeyboardButton(sender:)))
        
        let spaceBtnPickF = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace,
                                            target: nil,
                                            action: nil)
        
        let cancelBtnPickF = UIBarButtonItem(title: NSLocalizedString("Cancel", comment: ""),
                                             style: UIBarButtonItemStyle.plain,
                                             target: self,
                                             action: #selector(self.selectKeyboardButton(sender:)))
        
        toolbarPickerFecha.setItems([cancelBtnPickF, spaceBtnPickF, okBtnPickF], animated: false)
        toolbarPickerFecha.isUserInteractionEnabled = true
        return toolbarPickerFecha
    }
    
    
    //MARK: - Events
    @objc func seleccionaTerminosCondiciones() {
        let vc = AvisoPrivacidadVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func tapToDismiss() {
        self.view.endEditing(true)
    }
    
    @objc func btnContinuarTapped(){
        //let xappDelegate = UIApplication.shared.delegate as! AppDelegate
        //xappDelegate.gotoMainScreen()
        if !self.validaForma() {
            return
        }
        
        self.showLoader()
        
        let nombre = self.txtNombre.text?.trimmed()
        let apellidos = self.txtApellidos.text?.trimmed()
        let correo = self.txtEmail.text?.lowercased().trimmed()
        let celular = self.txtCelular.text?.trimmed()
        let passwd = self.txtContra.text?.trimmed()
        
        Functions().registerUser(rnombre: nombre!, rapellidos: apellidos!,
                                 rcorreo: correo!, rpasswd: passwd!, rtipo: tipoRegistroLogin.email,
                                 telefono: celular!, rcashback: "", paisId: paisSelected.id ){ exito, mensaje, error in
                                    
                                    self.hideLoader()
                                    
                                    if let xerror = error {
                                        print("Ocurrio un error \(xerror)")
                                        self.mensajeSimple(mensaje: "Ocurrio un error realizar el registro de usuario")
                                    } else {
                                        if exito {
                                            let alert = UIAlertController(title: "People's App", message: "Usuario creado correctamente", preferredStyle: UIAlertControllerStyle.alert)
                                            
                                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: {(
                                                alert: UIAlertAction!) in
                                                Functions().backtoMain()
                                            })
                                            )
                                            
                                            self.present(alert, animated: true, completion: nil)
                                        } else {
                                            self.mensajeSimple(mensaje: "Ocurrio un error realizar el registro de usuario")
                                        }
                                    }
        }
    }
    
    @objc func selectKeyboardButton(sender: UIBarButtonItem) {
        if sender.title == NSLocalizedString("OK", comment: "") {
            
            let idSelected = picker.selectedRow(inComponent: 0)
            let pais = self.arrayPaises[idSelected]
            self.txtPais.text = pais.descripcion
            paisSelected = pais
        }
        self.view.endEditing(true)
    }
    
    
}
