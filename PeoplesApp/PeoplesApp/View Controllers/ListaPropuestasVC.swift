//
//  ListaPropuestasVC.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 18/10/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import Kingfisher

class ListaPropuestasVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var tablePropuestas : UITableView!
    
    let nombreCelda = "celdaProfesional"
    
    var currentServicio : Servicio!
    
    var tituloVC = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.automaticallyAdjustsScrollViewInsets = false
        self.view.backgroundColor = UIColor.white
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.backToPedidos), name: Constantes.notificationNames.backToListadoPropuesta, object: nil)
        
        self.buildInterface {
            DispatchQueue.main.async {
                //Add height compensation for navigation bar in table.
                let navBarSize = (((self.navigationController?.navigationBar.frame.size.height) != nil) ? (self.navigationController?.navigationBar.frame.size.height)! : CGFloat(44))
                self.tablePropuestas.contentSize.height = self.tablePropuestas.contentSize.height + Constantes.tamaños.altoStatusBar + navBarSize
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isTranslucent = false
        
        self.navigationController?.navigationBar.tintColor = Constantes.paletaColores.verdePeoples
        
        self.navigationItem.title = self.tituloVC
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationItem.title = ""
    }
    
    @objc func backToPedidos(){
        NotificationCenter.default.post(name: Constantes.notificationNames.backToListadoPedidos, object: nil)
        _ = self.navigationController?.popViewController(animated: true)
        
    }
    
    func buildInterface(completion:@escaping()->()){
        self.tablePropuestas = UITableView(frame: self.view.frame)
        self.view.addSubview(self.tablePropuestas)
        self.tablePropuestas.delegate = self
        self.tablePropuestas.dataSource = self
        self.tablePropuestas.register(CeldaTVPropuestaProfesional.self, forCellReuseIdentifier: self.nombreCelda)
        self.tablePropuestas.tableFooterView = UIView(frame: CGRect.zero)
        completion()
    }
    
    //MARK: - TableView datasource y delegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constantes.tamaños.altoCeldaNegociosMain
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableView.validaExistenciaDatos(datos: self.currentServicio.propuestas as AnyObject)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.currentServicio.propuestas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.nombreCelda, for: indexPath) as! CeldaTVPropuestaProfesional
        
        let laPropuesta = self.currentServicio.propuestas[indexPath.row]
        
        cell.imgProfesional.kf.indicatorType = .activity
        cell.imgProfesional.kf.setImage(with: laPropuesta.datosProfesional.imagen)
        
        cell.lblNombreProfesional.text = laPropuesta.datosProfesional.razonSocial
        let altolabelNombre = cell.lblNombreProfesional.heightForView()
        cell.lblNombreProfesional.frame.size.height = altolabelNombre
        
        
        cell.lblProfesion.text = laPropuesta.datosProfesional.categoria.nombre
        let altolabelProfesion = cell.lblProfesion.heightForView()
        cell.lblProfesion.frame.size.height = altolabelProfesion
        
        cell.lblProfesion.center.y = cell.frame.size.height / 2
        cell.lblNombreProfesional.frame.origin.y = cell.lblProfesion.frame.minY - (1 + cell.lblNombreProfesional.frame.size.height)
        
        var precio = laPropuesta.presupuesto!
        precio = precio.formatoPrecio()
        
        let divisa = laPropuesta.divisa.abreviacion!
        precio = "\(precio) \(divisa)"
        
        cell.lblPresupuesto.text = precio
        cell.lblPresupuesto.sizeToFit()
        cell.lblPresupuesto.frame.origin.x = cell.frame.size.width - (cell.lblPresupuesto.frame.size.width + 10)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let elVC = DetallePropuestaVC()
        elVC.fechaServicio = self.currentServicio.fechaServicio
        elVC.currentPropuesta = self.currentServicio.propuestas[indexPath.row]
        self.navigationController?.pushViewController(elVC, animated: true)
    }
    
    
    //tableview delegates y datasource -- fin
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
