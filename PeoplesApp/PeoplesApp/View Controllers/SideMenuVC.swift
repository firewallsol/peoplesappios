//
//  SideMenuVC.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 29/06/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import Kingfisher
import FacebookLogin
import CocoaLumberjack
import SideMenu

class SideMenuVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    //MARK: - Components
    var topView : UIView!
    var imgLogo : UIImageView!
    var imgUsuario : UIImageView!
    var lblNombreUsr : UILabel!
    var tableOpciones : UITableView!
    
    //MARK: - Vars
    var arrayOpciones = [String]()
    var navBarHeight : CGFloat = 0
    var optionSelected: String!
    
    //Opciones para menú.
    let inicio = NSLocalizedString("Home", comment: "") //"OptionHome".localized //
    let afiliate = NSLocalizedString("Join", comment: "") //"OptionJoin".localized //
    let negocios = NSLocalizedString("Business", comment: "") //"OptionBusiness".localized //
    let servicios = NSLocalizedString("Services", comment: "") //"OptionServices".localized //
    let pedidos = NSLocalizedString("Orders", comment: "") //"OptionOrders".localized //
    let perfil = NSLocalizedString("Profile", comment: "") //"OptionProfile".localized //
    let notificaciones = NSLocalizedString("Notifications", comment: "") //"OptionNotifications".localized //
    let configuracion = NSLocalizedString("Configurations", comment: "") //"OptionConfigurations".localized //
    let cerrarSesion = NSLocalizedString("Logout", comment: "") //"OptionLogout".localized //
    
    enum CellIdentifier: String {
        case sideMenu = "SideMenuCell"
    }
    
    //MARK: - View
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Previene llamar dos veces la misma vista...
        SideMenuManager.default.menuAllowPushOfSameClassTwice = false

        
        if optionSelected == nil {
            Functions().saveSideMenuOptionSelected(name: inicio)
        }
        
        self.buildInterface()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        self.navigationController?.navigationBar.tintColor = Constantes.paletaColores.verdePeoples
        self.navigationController?.navigationBar.clipsToBounds = true
        
        self.title = ""
        
        self.view.backgroundColor = UIColor.white
        
        if Functions().isUserLoggedIn() {
            
            arrayOpciones = [
                inicio,
                afiliate,
                negocios,
                servicios,
                pedidos,
                notificaciones,
                configuracion,
                cerrarSesion
            ]
            
            let nombreCompleto = Functions().getUserName()
            
            if nombreCompleto != "" {
                if self.lblNombreUsr.isHidden {
                    self.lblNombreUsr.isHidden = false
                }
                self.lblNombreUsr.text = nombreCompleto
            }
            
            if Functions().getUserAvatar()?.absoluteString != "" {
                if self.imgUsuario.isHidden {
                    self.imgUsuario.isHidden = false
                }
                
                self.imgUsuario.kf.setImage(with: Functions().getUserAvatar())
            }
            
        }
        else {
            arrayOpciones = [
                inicio,
                negocios,
                servicios,
                configuracion
            ]
            
            lblNombreUsr.text = NSLocalizedString("LoginOrSignup", comment: "")
            imgUsuario.image = UIImage(named: "avatarDefault")
            imgUsuario.tintColor = Constantes.paletaColores.verdePeoples
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func buildInterface(){
        var xframe = CGRect.zero
        let padding = Constantes.tamaños.altoStatusBar
        let anchoItems = Constantes.tamaños.anchoPantalla * 0.6
        
        xframe.origin = CGPoint(x: 0, y: padding)
        xframe.size = CGSize(width: anchoItems, height: Constantes.tamaños.anchoPantalla * 0.35)
        self.topView = UIView(frame: xframe)
        self.view.addSubview(self.topView)
        
        //logo
        xframe.origin = CGPoint(x: 0, y: 5)
        let anchoImagen = anchoItems * 0.4
        xframe.size = CGSize(width: anchoImagen, height: anchoImagen * 0.35)
        self.imgLogo = UIImageView(frame: xframe)
        self.imgLogo.image = Asset.pplsAppLogo.image
        self.topView.addSubview(self.imgLogo)
        self.imgLogo.center.x = self.topView.frame.size.width / 2
        
        //Tap Gesture.
        let tapGestureLbl = UITapGestureRecognizer()
        tapGestureLbl.addTarget(self, action: #selector(seleccionaNombreAvatar))
        tapGestureLbl.numberOfTapsRequired = 1
        
        let tapGestureImg = UITapGestureRecognizer()
        tapGestureImg.addTarget(self, action: #selector(seleccionaNombreAvatar))
        tapGestureImg.numberOfTapsRequired = 1
        
        
        //Avatar
        xframe.origin = CGPoint(x: 5, y: self.imgLogo.frame.maxY + 5)
        xframe.size = CGSize(width: anchoItems * 0.25, height: anchoItems * 0.25)
        imgUsuario = UIImageView(frame: xframe)
        imgUsuario.kf.indicatorType = .activity
        imgUsuario.contentMode = .scaleAspectFit
        imgUsuario.layer.cornerRadius = self.imgUsuario.frame.size.width / 2
        imgUsuario.clipsToBounds = true
        imgUsuario.isUserInteractionEnabled = true
        imgUsuario.addGestureRecognizer(tapGestureImg)
        self.topView.addSubview(imgUsuario)
        
        //nombre
        xframe.origin = CGPoint(x: self.imgUsuario.frame.maxX + 5, y: self.imgLogo.frame.maxY + 5)
        xframe.size = CGSize(width: anchoItems * 0.7, height: 50)
        self.lblNombreUsr = UILabel(frame: xframe)
        self.lblNombreUsr.text = ""
        self.lblNombreUsr.numberOfLines = 0
        self.lblNombreUsr.font = Constantes.currentFonts.NunitoRegular.base
        self.lblNombreUsr.lineBreakMode = .byWordWrapping
        self.lblNombreUsr.adjustsFontSizeToFitWidth = true
        self.lblNombreUsr.addGestureRecognizer(tapGestureLbl)
        self.lblNombreUsr.isUserInteractionEnabled = true
        self.topView.addSubview(self.lblNombreUsr)
        self.lblNombreUsr.center.y = self.imgUsuario.frame.minY + (self.imgUsuario.frame.size.height / 2)
        
        let altoOpciones = Constantes.tamaños.altoPantalla - (padding + self.topView.frame.size.height + 10)
        xframe.origin = CGPoint(x: 0, y: self.topView.frame.maxY)
        xframe.size = CGSize(width: anchoItems, height: altoOpciones)
        self.tableOpciones = UITableView(frame: xframe)
        self.view.addSubview(self.tableOpciones)
        self.tableOpciones.dataSource = self
        self.tableOpciones.delegate = self
        self.tableOpciones.register(UITableViewCell.self, forCellReuseIdentifier: CellIdentifier.sideMenu.rawValue)
        
        
        self.tableOpciones.tableFooterView = UIView(frame: CGRect.zero)
    }
    
    //MARK: - Table View
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayOpciones.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.sideMenu.rawValue,
                                                 for: indexPath)
        
        let name = self.arrayOpciones[indexPath.row]
        
        cell.textLabel?.text = NSLocalizedString(name, comment: "")
        cell.textLabel?.font = Constantes.currentFonts.NunitoRegular.base
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if optionSelected == arrayOpciones[indexPath.row] {
            self.dismiss(animated: true, completion: nil)
            return
        }
        
        let cellSelected = tableView.cellForRow(at: indexPath)
        optionSelected = (cellSelected?.textLabel?.text)!
        
        let elVC = viewControllerFor(optionSelected: optionSelected)
        self.navigationController?.pushViewController(elVC, animated: true)
    }
    
    //MARK: - Events
    @objc func seleccionaNombreAvatar() {
        if Functions().getSideMenuOptionSelected() == perfil {
            self.dismiss(animated: true, completion: nil)
            return
        }
        
        var viewController = UIViewController()
        viewController = viewControllerFor(optionSelected: perfil)
        optionSelected = perfil
        
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func seleccionaCerrarSesion() {
        if Functions().isUserLoggedIn(){
            self.dismiss(animated: true, completion: nil)
            
            //Cerrar sesión con Google.
            GIDSignIn.sharedInstance().signOut()
            
            //Cerrar sesión con Facebook.
            let lmanager = LoginManager()
            lmanager.logOut()

            
            Functions().borraTodoCierraSesion(borrarFavoritos: true)
            Functions().backtoMain()
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func viewControllerFor(optionSelected: String) -> UIViewController {
        var viewController = UIViewController()
        
        switch optionSelected {
        case inicio:
            viewController = MainVC()
            viewController.title = inicio
            break
        case afiliate:
            viewController = AfiliateIntroVC()
            viewController.title = afiliate
            (viewController as! AfiliateIntroVC).statusActual = Functions().getUsrEstatusAfiliacion()
            break
        case negocios:
            viewController = NegociosMainVC()
            viewController.title = negocios
            break
        case servicios:
            viewController = ServiciosMainVC()
            viewController.title = servicios
            break
        case pedidos:
            viewController = PedidosMainVC()
            viewController.title = pedidos
            break
        case perfil:
            if Functions().isUserLoggedIn() {
                viewController = PerfilVC()
                viewController.title = perfil
            } else {
                viewController = LoginVC()
                (viewController as! LoginVC).showSideMenu = true
            }
            break
        case notificaciones:
            viewController = NotificacionesVC()
            viewController.title = notificaciones
            break
        case configuracion:
            viewController = ConfiguracionVC()
            viewController.title = configuracion
            break
        case cerrarSesion:
            seleccionaCerrarSesion()
            break
        default:
            DDLogWarn("Opción seleccionada no existe.")
            break
        }
        
        Functions().saveSideMenuOptionSelected(name: optionSelected)
        
        return viewController
    }
}
