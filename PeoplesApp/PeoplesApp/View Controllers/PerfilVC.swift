//
//  PerfilVC.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 05/09/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import Kingfisher
import CocoaLumberjack

class PerfilVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    enum Cells: Int {
        case reembolso
        case cashbackPendiente
        case cashbackPagado
        case afiliacionesPendientes
        case afiliacionesPagadas
    }
    
    enum SegmentName {
        case cuenta
        case favoritos
    }
    
    //MARK: - Components
    var imgUsuario : UIImageView!
    var lblNombreUsr : UILabel!
    var lblCiudad : UILabel!
    var lblIDUsr : CopyableLabel!
    var segmentPerfil : UISegmentedControl!
    var reembolsosCell :  CeldaTVReembolsoPerfil!
    var cashbackPendienteCell : CeldaTVDetailStylePerfil!
    var cashbackPagadoCell : CeldaTVDetailStylePerfil!
    var afiliacionesPendientesCell : CeldaTVDetailStylePerfil!
    var afiliacionesPagadasCell: CeldaTVDetailStylePerfil!
    var tableDatos : UITableView!
    var btnCobrar : UIView!
    var lblBtnCobrar : UILabel!
    var scrollView : UIScrollView!
    
    //MARK: - Vars
    var arrayEmpresas = [Empresa_Profesional]()
    let itemsSegment = [NSLocalizedString("Cuenta", comment: ""),
                        NSLocalizedString("Favoritos", comment: "")]
    let cellIdentifier = "cell"
    var usuarioActual : Usuario!
    var arrayCashbacks = [Cashback]()
    
    //MARK: - View
    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
        self.view.backgroundColor = UIColor.white
        
        self.usuarioActual = Functions().getDataUsuario()
        
        self.buildInterface(){ finished in
            //
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setBarMenu()
        
        let rightbbitem = UIBarButtonItem(title: "Editar", style: .plain, target: self, action: #selector(self.gotoEditarPerfil))
        
        self.navigationItem.rightBarButtonItem = rightbbitem
        self.navigationController?.navigationBar.tintColor = Constantes.paletaColores.verdePeoples
        
        self.showLoader()
        getInfoCashback()
        listadoCashback()
        
        //Actualiza favoritos.
        if(segmentPerfil.selectedSegmentIndex == SegmentName.favoritos.hashValue) {
            self.segmentValueTap(sender: segmentPerfil)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationItem.title = ""
    }
    
    func setUserData(){
        //img
        self.imgUsuario.kf.setImage(with: self.usuarioActual.avatar)
        
        //nombre
        self.lblNombreUsr.text = "\(self.usuarioActual.nombre!) \(self.usuarioActual.apellidos!)"
        self.lblNombreUsr.center.x = Constantes.tamaños.centroHorizontal
        
        //direccion
        self.lblCiudad.text = self.usuarioActual.direcciones.first?.descripcion
        self.lblCiudad.sizeToFit()
        self.lblCiudad.center.x = Constantes.tamaños.centroHorizontal
        
        //Codigo afiliador
        let stringCodigoAfiliador = NSLocalizedString("Código de afiliador:", comment: "")
        var codigoAfiliador = NSLocalizedString("Sin código", comment: "")
        if let codigo = self.usuarioActual.codigoAfiliador {
            codigoAfiliador = codigo
        }
        
        self.lblIDUsr.text = "\(stringCodigoAfiliador) \(codigoAfiliador)"
        self.lblIDUsr.sizeToFit()
        self.lblIDUsr.center.x = Constantes.tamaños.centroHorizontal
        
        self.centerLblCobrar()
        
        self.tableDatos.reloadData {
            self.setTableHeight()
            self.hideLoader()
        }
    }
    
    func buildInterface(completionHandler: @escaping (Bool) -> ()){
        
        var xframe = CGRect.zero
        
        self.scrollView = UIScrollView(frame: self.view.frame)
        
        //imagen
        let anchoAltoImagen = Constantes.tamaños.anchoPantalla * 0.3
        xframe.origin = CGPoint(x: 10, y: 10)
        xframe.size = CGSize(width: anchoAltoImagen, height: anchoAltoImagen)
        self.imgUsuario = UIImageView(frame: xframe)
        self.imgUsuario.layer.cornerRadius = self.imgUsuario.frame.size.width / 2
        self.imgUsuario.clipsToBounds = true
        self.imgUsuario.kf.indicatorType = .activity
        self.imgUsuario.kf.setImage(with: self.usuarioActual.avatar)
        self.imgUsuario.contentMode = .scaleAspectFill
        self.scrollView.addSubview(self.imgUsuario)
        self.imgUsuario.center.x = Constantes.tamaños.centroHorizontal
        
        //nombre
        xframe.origin = CGPoint(x: 10, y: self.imgUsuario.frame.maxY + 3)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla * 0.8, height: 30)
        self.lblNombreUsr = UILabel(frame: xframe)
        self.lblNombreUsr.font = Constantes.currentFonts.NunitoBold.base
        self.lblNombreUsr.textColor = Constantes.paletaColores.verdePeoples
        self.scrollView.addSubview(self.lblNombreUsr)
        self.lblNombreUsr.text = "\(self.usuarioActual.nombre!) \(self.usuarioActual.apellidos!)"
        self.lblNombreUsr.textAlignment = .center
        self.lblNombreUsr.sizeToFit()
        self.lblNombreUsr.center.x = Constantes.tamaños.centroHorizontal
        
        //ciudad
        xframe.origin = CGPoint(x: 25, y: self.lblNombreUsr.frame.maxY + 3)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla * 0.5, height: 30)
        self.lblCiudad = UILabel(frame: xframe)
        self.lblCiudad.font = Constantes.currentFonts.NunitoRegular.minus.LabelFontSize_4
        self.lblCiudad.textColor = UIColor.gray
        self.lblCiudad.lineBreakMode = .byWordWrapping
        self.lblCiudad.numberOfLines = 2
        self.scrollView.addSubview(self.lblCiudad)
        self.lblCiudad.text = ""
        self.lblCiudad.center.x = Constantes.tamaños.centroHorizontal
        
        //ID
        xframe.origin = CGPoint(x: 10, y: self.lblCiudad.frame.maxY + 5)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla * 0.8, height: 30)
        self.lblIDUsr = CopyableLabel(frame: xframe)
        self.lblIDUsr.font = Constantes.currentFonts.NunitoBold.base
        self.lblIDUsr.textColor = Constantes.paletaColores.verdePeoples
        self.scrollView.addSubview(self.lblIDUsr)
        self.lblIDUsr.text = NSLocalizedString("Código de afiliador:", comment: "")
        self.lblIDUsr.textAlignment = .center
        self.lblIDUsr.sizeToFit()
        self.lblIDUsr.center.x = Constantes.tamaños.centroHorizontal
        
        //segment
        xframe.origin = CGPoint(x: 0, y: self.lblIDUsr.frame.maxY + 10)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: Constantes.tamaños.altoBotonTop)
        self.segmentPerfil = UISegmentedControl(items: self.itemsSegment)
        self.segmentPerfil.frame = xframe
        self.scrollView.addSubview(self.segmentPerfil)
        self.segmentPerfil.selectedSegmentIndex = 0
        self.segmentPerfil.tintColor = Constantes.paletaColores.verdePeoples
        
        let font = Constantes.currentFonts.NunitoBold.base!
        self.segmentPerfil.setTitleTextAttributes([NSAttributedStringKey.font: font], for: .normal)
        self.segmentPerfil.addTarget(self, action: #selector(self.segmentValueTap(sender:)), for: .valueChanged)
        
        //table datos
        xframe.origin = CGPoint(x: 0, y: self.segmentPerfil.frame.maxY + 10)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: 400)
        self.tableDatos = UITableView(frame: xframe)
        self.tableDatos.dataSource = self
        self.tableDatos.delegate = self
        self.tableDatos.isScrollEnabled = false
        self.scrollView.addSubview(self.tableDatos)
        
        //celdas
        //reembolso
        self.reembolsosCell = CeldaTVReembolsoPerfil(style: .default, reuseIdentifier: self.cellIdentifier)
        self.reembolsosCell.imgIcono.image = Asset.Icons.icClock48.image
        
        //cashback
        self.cashbackPendienteCell = CeldaTVDetailStylePerfil(style: .subtitle, reuseIdentifier: self.cellIdentifier)
        self.cashbackPendienteCell.textLabel?.text = NSLocalizedString("Cashback pendiente", comment:"")
        self.cashbackPendienteCell.detailTextLabel?.text = ""
        self.cashbackPendienteCell.detailTextLabel?.textColor = UIColor.gray
        self.cashbackPendienteCell.lblMonto.text = "$0"
        
        
        //canjes
        self.cashbackPagadoCell = CeldaTVDetailStylePerfil(style: .subtitle, reuseIdentifier: self.cellIdentifier)
        self.cashbackPagadoCell.textLabel?.text = NSLocalizedString("Cashback pagado", comment: "")
        self.cashbackPagadoCell.detailTextLabel?.text = ""
        self.cashbackPagadoCell.detailTextLabel?.textColor = UIColor.gray
        
        self.cashbackPagadoCell.lblMonto.text = "$0"
        
        //afiliados
        ///TEST
        DDLogDebug("ESTATUS AFILIACION:\(usuarioActual.afiliacion.statusEnum.rawValue)")
        usuarioActual.afiliacion.statusEnum = .completada
        DDLogDebug("ESTATUS AFILIACION:\(usuarioActual.afiliacion.statusEnum.rawValue)")
        
        if usuarioActual.afiliacion.statusEnum == .completada {
            
            self.afiliacionesPagadasCell = CeldaTVDetailStylePerfil(style: .subtitle, reuseIdentifier: self.cellIdentifier)
            self.afiliacionesPagadasCell.textLabel?.text = NSLocalizedString("Afiliaciones pagadas", comment: "")
            self.afiliacionesPagadasCell.detailTextLabel?.text = ""
            self.afiliacionesPagadasCell.detailTextLabel?.textColor = UIColor.gray
            self.afiliacionesPagadasCell.lblMonto.text = "$0"

            self.afiliacionesPendientesCell = CeldaTVDetailStylePerfil(style: .subtitle, reuseIdentifier: self.cellIdentifier)
            self.afiliacionesPendientesCell.textLabel?.text = NSLocalizedString("Afiliaciones pendientes", comment: "")
            self.afiliacionesPendientesCell.detailTextLabel?.text = ""
            self.afiliacionesPendientesCell.detailTextLabel?.textColor = UIColor.gray
            self.afiliacionesPendientesCell.lblMonto.text = "$0"            
        }
        
        self.view.addSubview(self.scrollView)
        
        //boton
        let padding = (self.navigationController?.navigationBar.frame.size.height)! + (Constantes.tamaños.altoBotonTop + Constantes.tamaños.altoStatusBar)
        xframe.origin = CGPoint(x: 0, y: Constantes.tamaños.altoPantalla - padding)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: Constantes.tamaños.altoBotonTop)
        self.btnCobrar = UIView(frame: xframe)
        self.btnCobrar.backgroundColor = Constantes.paletaColores.verdePeoples
        
        self.lblBtnCobrar = UILabel(frame: xframe)
        self.lblBtnCobrar.text = NSLocalizedString("COBRAR ", comment: "")
        self.lblBtnCobrar.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_1
        self.lblBtnCobrar.textColor = UIColor.white
        self.btnCobrar.addSubview(lblBtnCobrar)
        self.centerLblCobrar()
        //let tapSolicitar = UITapGestureRecognizer(target: self, action: #selector(self.gotoSolicitarServicio(tap:)))
        //self.btnSolicitarServicio.addGestureRecognizer(tapSolicitar)
        //self.btnSolicitarServicio.isUserInteractionEnabled = true
        self.view.addSubview(self.btnCobrar)
        self.btnCobrar.isHidden = true
        
        completionHandler(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Table View
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch self.segmentPerfil.selectedSegmentIndex {
        case 0: return Constantes.tamaños.altoCeldaPerfilDefault
        case 1: return Constantes.tamaños.altoCeldaNegociosMain
        //        case 2: return Constantes.tamaños.altoCeldaNegociosMain
        default:
            return Constantes.tamaños.altoCeldaPerfilDefault
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.segmentPerfil.selectedSegmentIndex == 0 {
            
            ///TEST
            DDLogDebug("ESTATUS AFILIACION:\(usuarioActual.afiliacion.statusEnum.rawValue)")
            usuarioActual.afiliacion.statusEnum = .completada
            DDLogDebug("ESTATUS AFILIACION:\(usuarioActual.afiliacion.statusEnum.rawValue)")
            
            
            if usuarioActual.afiliacion.statusEnum == .completada {
                return 5
            } else {
                return 3
            }
        } else {
            return self.arrayEmpresas.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        ///Cuenta
        if self.segmentPerfil.selectedSegmentIndex == SegmentName.cuenta.hashValue {
            switch indexPath.row {
            case Cells.reembolso.hashValue:
                return self.reembolsosCell

            case Cells.cashbackPendiente.hashValue:
                let total = totalCashbackPor(estatusCargo: EstatusCargo.Keys.pendiente, tipoCashback: .consumo)
                cashbackPendienteCell.lblMonto.text = String(total).formatoPrecio()
                cashbackPendienteCell.lblMonto.sizeToFit()
                self.cashbackPendienteCell.lblMonto.frame.origin.x = Constantes.tamaños.anchoPantalla - (self.cashbackPendienteCell.lblMonto.frame.size.width + 15)
                return cashbackPendienteCell

            case Cells.cashbackPagado.hashValue:
                let total = totalCashbackPor(estatusCargo: EstatusCargo.Keys.enviado, tipoCashback: .consumo)
                cashbackPagadoCell.lblMonto.text = String(total).formatoPrecio()

                self.cashbackPagadoCell.lblMonto.sizeToFit()
                self.cashbackPagadoCell.lblMonto.frame.origin.x = Constantes.tamaños.anchoPantalla - (self.cashbackPagadoCell.lblMonto.frame.size.width + 15)
                return cashbackPagadoCell

            case Cells.afiliacionesPendientes.hashValue:
                let total = totalCashbackPor(estatusCargo: EstatusCargo.Keys.pendiente, tipoCashback: .afiliacion)
                afiliacionesPendientesCell.lblMonto.text = String(total).formatoPrecio()
                
                afiliacionesPendientesCell.lblMonto.sizeToFit()
                afiliacionesPendientesCell.lblMonto.frame.origin.x = Constantes.tamaños.anchoPantalla - (self.afiliacionesPendientesCell.lblMonto.frame.size.width + 15)

                return afiliacionesPendientesCell
            
            case Cells.afiliacionesPagadas.hashValue:
                let total = totalCashbackPor(estatusCargo: EstatusCargo.Keys.enviado, tipoCashback: .afiliacion)
                afiliacionesPagadasCell.lblMonto.text = String(total).formatoPrecio()
                
                afiliacionesPagadasCell.lblMonto.sizeToFit()
                afiliacionesPagadasCell.lblMonto.frame.origin.x = Constantes.tamaños.anchoPantalla - (self.afiliacionesPagadasCell.lblMonto.frame.size.width + 15)
                
                return afiliacionesPagadasCell
                
            default: fatalError("La celda no corresponde")
            }
        }
            ///Favoritos
        else {
            return self.getCellNegocio(tableView: tableView, cellForRowAt: indexPath)
        }
        return UITableViewCell()
    }
    
    func getCellNegocio(tableView: UITableView, cellForRowAt indexPath: IndexPath)->CeldaTVNegociosMain {
        var cell = CeldaTVNegociosMain()
        if let xcell = tableView.dequeueReusableCell(withIdentifier: self.cellIdentifier) as? CeldaTVNegociosMain {
            cell = xcell
        }
        cell.frame.size = CGSize(width: tableView.frame.size.width, height: cell.frame.size.height)
        let cellSizeHeight = cell.frame.size.height
        let cellSizeWidth = cell.frame.size.width
        
        var unaEmpresa = Empresa()
        var unProfesional = Profesional()
        var unUsuario = Usuario()
        var esEmpresaOPro = tipoDiccFavorito.empresa
        
        
        if let value = self.arrayEmpresas[indexPath.row] as? Empresa {
            unaEmpresa = value
        } else if let value2 = self.arrayEmpresas[indexPath.row] as? Profesional {
            unProfesional = value2
            esEmpresaOPro = tipoDiccFavorito.profesional
        } else if let value3 = self.arrayEmpresas[indexPath.row] as? Usuario {
            unUsuario = value3
            esEmpresaOPro = tipoDiccFavorito.usuario
        }
        
        var cashtext = ""
        
        if esEmpresaOPro == .empresa {
            
            cell.imgNegocio.kf.setImage(with: unaEmpresa.imagen)
            cell.lblNombreNegocio.text = unaEmpresa.razonSocial!
            cell.lblDirecNegocio.text = unaEmpresa.direcciones.first?.descripcion
            cashtext = "+ \(unaEmpresa.cash_back!)% Cash back"
            
            
        } else if esEmpresaOPro == .profesional {
            cell.imgNegocio.kf.setImage(with: unProfesional.imagen)
            cell.lblNombreNegocio.text = unProfesional.razonSocial!
            cell.lblDirecNegocio.text = unProfesional.direcciones.first?.descripcion
            
        } else {
            cell.imgNegocio.kf.setImage(with: unUsuario.avatar)
            cell.lblNombreNegocio.text = "\(unUsuario.nombre!) \(unUsuario.apellidos!)"
            cell.lblDirecNegocio.text = ""
        }
        cell.lblNombreNegocio.sizeToFit()
        
        
        let anchoLabelDirec = cellSizeWidth - (cell.imgNegocio.frame.size.width + 5)
        cell.lblDirecNegocio.frame.size.width = anchoLabelDirec * 0.9
        cell.lblDirecNegocio.sizeToFit()
        cell.lblDirecNegocio.frame.origin.y = cell.lblNombreNegocio.frame.maxY + 3
        
        
        
        cell.lblCashBack.text = cashtext
        cell.lblCashBack.sizeToFit()
        var posycashback = cellSizeHeight - (cell.lblDirecNegocio.frame.maxY)
        posycashback = cell.lblDirecNegocio.frame.maxY + (posycashback / 2)
        cell.lblCashBack.center.y = posycashback
        
        cell.lblDistancia.isHidden = true
        cell.imgShare.isHidden = true
        cell.imgLike.isHidden = true
        
        return cell
    }
    
    @objc func setTableHeight(){
        var frame: CGRect = self.tableDatos.frame
        self.tableDatos.isScrollEnabled = false
        if self.segmentPerfil.selectedSegmentIndex == SegmentName.cuenta.hashValue {
            frame.size.height = self.tableDatos.contentSize.height
        } else {
            self.tableDatos.isScrollEnabled = true
            //frame.size.height = (self.tableDatos.contentSize.height > 300) ? 300 : self.tableDatos.contentSize.height
            frame.size.height = self.tableDatos.contentSize.height
        }
        
        self.tableDatos.frame = frame
        
        self.scrollView.contentSize.height = self.tableDatos.frame.maxY + (Constantes.tamaños.altoBotonTop ) +
            (self.navigationController?.navigationBar.frame.size.height)! + Constantes.tamaños.altoStatusBar + 5
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedSegment = segmentPerfil.selectedSegmentIndex
        
        switch selectedSegment {
        case SegmentName.cuenta.hashValue:
            let consumosCashbackVC = preparaVC(index: indexPath.row)
            self .navigationController?.pushViewController(consumosCashbackVC, animated: true)
            break
            
        case SegmentName.favoritos.hashValue:
            if let value = self.arrayEmpresas[indexPath.row] as? Empresa {
                let elVC = DetalleComercioVC()
                elVC.currentEmpresa = value
                self.navigationController?.pushViewController(elVC, animated: true)
            }
            else if let value2 = self.arrayEmpresas[indexPath.row] as? Profesional {
                let elVC = DetalleProfesionalVC()
                elVC.currentPro = value2
                self.navigationController?.pushViewController(elVC, animated: true)
            }
            
            break
        default:
            DDLogWarn("Invalid option: \(selectedSegment)")
        }
    }
    
    //MARK: - Events
    @objc func segmentValueTap(sender: UISegmentedControl){
        let selectedSegment = sender.selectedSegmentIndex
        
        switch selectedSegment {
        case SegmentName.cuenta.hashValue:
            self.arrayEmpresas.removeAll()
            self.tableDatos.reloadData()
            self.setTableHeight()
            break
            
        case SegmentName.favoritos.hashValue:
            Functions().getFavoritosObjs(completionHandler: { (array) in
                DispatchQueue.main.async(execute: {
                    self.arrayEmpresas = array
                    self.tableDatos.reloadData({
                        self.lblBtnCobrar.text = NSLocalizedString("AFILIAR NEGOCIO", comment: "")
                        self.setTableHeight()
                    })
                })
            })
            
            break
        default:
            break
        }
        self.centerLblCobrar()
    }
    
    func centerLblCobrar(){
        self.lblBtnCobrar.sizeToFit()
        self.lblBtnCobrar.center.x = Constantes.tamaños.anchoPantalla / 2
        self.lblBtnCobrar.center.y = self.btnCobrar.frame.size.height / 2
    }
    
    @objc func gotoEditarPerfil(){
        let elVC = EditarPerfilVC()
        elVC.datosUsuario = self.usuarioActual
        self.navigationController?.pushViewController(elVC, animated: true)
    }
    
    //MARK: - Custom
    func listadoCashback() {
        showLoader()
        Functions().listadoCashbacks { (success, message, arrayCashbacks, error) in
            if error != nil {
                self.hideLoader()
                self.mensajeSimple(mensaje: NSLocalizedString("Ocurrió un error al obtener listado.", comment: ""))
                return
            }
            
            if success {
                if let xarrayCashback = arrayCashbacks {
                    self.arrayCashbacks = xarrayCashback
                    self.hideLoader()
                }
            }
            else {
                self.hideLoader()
                self.mensajeSimple(mensaje: message)
            }
        }
    }
    
    func getInfoCashback() {
        Functions().getInfoCashBack(){ exito, mensaje, xusuario, xdatosCashBack, error in
            if let _ = error {
                self.hideLoader()
                self.mensajeSimple(mensaje: "Error de red, revise su conexion", dismiss: false)
            } else {
                if exito {
                    self.usuarioActual = xusuario
                    self.setUserData()
                } else {
                    self.hideLoader()
                    self.mensajeSimple(mensaje: mensaje, dismiss: false)
                }
            }
        }
    }
    
    func totalCashbackPor(estatusCargo: EstatusCargo.Keys, tipoCashback: TipoCashback) -> Double {
        var total = 0.0
        
        if arrayCashbacks.count > 0 {
            let filteredArray = filtrarCashbacksPor(estatusCargo: estatusCargo, tipoCashback: tipoCashback)
            total = filteredArray.reduce(0, {$0 + Double($1.cashbackCliente)!})
        }
        
        DDLogInfo("EstatusCargo: \(estatusCargo.rawValue), TipoCashback: \(tipoCashback.rawValue), Total: \(total)")
        return total
    }
    
    func filtrarCashbacksPor(estatusCargo: EstatusCargo.Keys, tipoCashback: TipoCashback) -> [Cashback] {
        var array = [Cashback]()
        if arrayCashbacks.count > 0 {
            array = arrayCashbacks.filter({
                if let unCargo = $0.cargo {
                    if unCargo.estatus.id == estatusCargo.rawValue {
                        
                        switch tipoCashback {
                        case .consumo, .servicio:
                            if $0.tipo == .consumo || $0.tipo == .servicio {
                                return true
                            }

                        case .afiliacion:
                            if $0.tipo == .afiliacion {
                                return true
                            }
                        }
                    }
                }
                return false
            })
        }
        DDLogDebug("Items: \(array.count)")
        return array
    }
    
    func preparaVC(index: Int) -> ConsumosCashbackVC {
        let viewController = ConsumosCashbackVC()
        var estatusCargo = EstatusCargo.Keys.pagado
        var tipoCashback = TipoCashback.consumo
        
        let cellSelected = tableDatos.cellForRow(at: IndexPath(item: index, section: 0))
        viewController.title = cellSelected?.textLabel?.text
        
        switch index {
        case Cells.reembolso.rawValue:
            print("reembolso")
            viewController.arrayCashbacks = arrayCashbacks
            return viewController
            
        case Cells.cashbackPendiente.rawValue:
            print("cashback pendiente")
            estatusCargo = EstatusCargo.Keys.pendiente
            
        case Cells.cashbackPagado.rawValue:
            print("cashback pagado")
            estatusCargo = EstatusCargo.Keys.enviado //Estatus considerado como Pagado para Peoples: Enviado.
            
        case Cells.afiliacionesPendientes.rawValue:
            print("afiliaciones pendientes")
            estatusCargo = EstatusCargo.Keys.pendiente
            tipoCashback = TipoCashback.afiliacion
            
            
        case Cells.afiliacionesPagadas.rawValue:
            print("afiliaciones pagadas")
            estatusCargo = EstatusCargo.Keys.enviado //Estatus considerado como Pagado para Peoples: Enviado.
            tipoCashback = TipoCashback.afiliacion
            
            
        default:
            DDLogWarn("Invalid option: \(index)")
        }
        

        viewController.arrayCashbacks = filtrarCashbacksPor(estatusCargo: estatusCargo, tipoCashback: tipoCashback)
        
        return viewController
    }
    
    
    
}
