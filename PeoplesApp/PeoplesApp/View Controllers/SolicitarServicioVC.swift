//
//  SolicitarServicioVC.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 17/07/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import GooglePlacePicker

class SolicitarServicioVC: UIViewController, UIPickerViewDataSource,
    UIPickerViewDelegate, UITextFieldDelegate,
GMSPlacePickerViewControllerDelegate {

    
    
    var txtTipoServicio : UITextField!
    var pickerTipoServicio = UIPickerView()
    var pickerFecha = UIDatePicker()
    var txtDetalleServicio : UITextView!
    var txtCantPresupuesto : UITextField!
    var lblTipoMoneda : UILabel!
    var txtFechaServicio : UITextField!
    var btnSolicitar : UIView!
    var txtDireccion : UITextField!
    var txtTituloSolicitud : UITextField!
    var scrollView : UIScrollView!
    
    var servLatitud = ""
    var servLongitud = ""
    var servDireccionCompleta = ""
    var fechaHoraSend = ""
    var currentPro : Profesional!
    var sender = senderSolicitaServicio.Main
    var categoriaSelected = Categoria()
    var divisa: Divisa!
    
    //Variable para poder crear Solicitudes sin Propuesta, desde Main.
    var idCategoriaSelected = ""
    var arrayServicios = [Categoria]()
    
    
    //MARK: - View
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Load divisa from userdefaults.
        divisa = Functions().getUserDivisa()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        self.buildInterface()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isTranslucent = false
        
        self.navigationController?.navigationBar.tintColor = Constantes.paletaColores.verdePeoples
        
        self.navigationItem.title = "Solicitar Servicio"
        
        switch self.sender {
        case .Main:
            if txtTipoServicio.text == "" {
                if let categoria = self.arrayServicios.first {
                    self.txtTipoServicio.text = categoria.nombre
                    self.idCategoriaSelected = categoria.id
                }
            }
            self.txtTipoServicio.isUserInteractionEnabled = true
            break
            
        case .listaPro:
            self.txtTipoServicio.text = categoriaSelected.nombre
            self.idCategoriaSelected = categoriaSelected.id
            break
            
        case .detallePro:
            self.txtTipoServicio.text = self.currentPro.categoria.nombre
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationItem.title = ""
    }
    
    //MARK: - Keyboard
    @objc func keyboardWillShow(sender: NSNotification) {
        var userInfo = sender.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height
        self.scrollView.contentInset = contentInset
        
        var visibleRect = self.view.frame
        visibleRect.size.height -= keyboardFrame.size.height
        
        var elOrigin = CGPoint.zero
        
        var scroll = false
        
        if self.txtTituloSolicitud.isFirstResponder {
            visibleRect.size.height -= ((self.txtTituloSolicitud.inputAccessoryView?.frame.size.height)! * 2)
            elOrigin = self.txtTituloSolicitud.frame.origin
            scroll = true
        }
        
        if self.txtDetalleServicio.isFirstResponder {
            visibleRect.size.height -= ((self.txtDetalleServicio.inputAccessoryView?.frame.size.height)! * 2)
            elOrigin = self.txtDetalleServicio.frame.origin
            scroll = true
        }
        
        if self.txtCantPresupuesto.isFirstResponder {
            visibleRect.size.height -= ((self.txtCantPresupuesto.inputAccessoryView?.frame.size.height)! * 2)
            elOrigin = self.txtCantPresupuesto.frame.origin
            scroll = true
        }
        
        if self.txtFechaServicio.isFirstResponder {
            visibleRect.size.height -= ((self.txtFechaServicio.inputAccessoryView?.frame.size.height)! * 2)
            elOrigin = self.txtFechaServicio.frame.origin
            scroll = true
        }
        
        if ((scroll) && !(visibleRect.contains(elOrigin))){
            let scrollPoint = CGPoint(x: 0, y: elOrigin.y - visibleRect.size.height)
            self.scrollView.setContentOffset(scrollPoint, animated: true)
        }
    }
    
    @objc func keyboardWillHide(sender: NSNotification) {
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        self.scrollView.contentInset = contentInset
    }
    
    
    //MARK: - picker view delegate
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if pickerView == pickerTipoServicio {
            return self.arrayServicios.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == pickerTipoServicio {
            let servicio = self.arrayServicios[row]
            return servicio.nombre
        }
        return ""
    }
    
    @objc func donePickerServ(sender: UIBarButtonItem){
        self.txtTipoServicio.text = self.arrayServicios[pickerTipoServicio.selectedRow(inComponent: 0)].nombre
        self.idCategoriaSelected = self.arrayServicios[pickerTipoServicio.selectedRow(inComponent: 0)].id
        self.view.endEditing(true)
    }
    
    @objc func donePickerFecha(sender: UIBarButtonItem){
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MMMM/yyyy HH:mm"
        let selectedDate: String = dateFormatter.string(from: self.pickerFecha.date)
        self.txtFechaServicio.text = selectedDate
        //para envio
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:00"
        self.fechaHoraSend = dateFormatter.string(from: self.pickerFecha.date)
        
        self.view.endEditing(true)
    }
    
    @objc func seleccionaCancelar() {
        self.view.endEditing(true)
    }
    
    //MARK: - Text field
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.txtDireccion {
            textField.resignFirstResponder()
            let config = GMSPlacePickerConfig(viewport: nil)
            let placePickerVC = GMSPlacePickerViewController(config: config)
            placePickerVC.delegate = self
            present(placePickerVC, animated: true, completion: nil)
            return false
        }
        else if (textField == self.txtTipoServicio) {
            //picker
            pickerTipoServicio.delegate = self
            self.txtTipoServicio.inputView = pickerTipoServicio
            let toolbarPickerServ = self.createToolbarTipoServicio()
            self.txtTipoServicio.inputAccessoryView = toolbarPickerServ
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var success = true
        
        if textField == txtTituloSolicitud {
            
            let maxLength = 50
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            success =  newString.length <= maxLength
        }
        return success
    }
    
    
    //MARK: - Google place picker delegate
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        
        print("Place name \(place.name)")
        print("Place address \(String(describing: place.formattedAddress))")
        print("Place coordenates \(place.coordinate)")
        
        self.txtDireccion.text = place.name
        self.servDireccionCompleta = place.formattedAddress!
        self.servLatitud = "\(place.coordinate.latitude)"
        self.servLongitud = "\(place.coordinate.longitude)"
        
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        
        print("No place selected")
    }
    
    //MARK: - Events
    func buildInterface(){
        
        self.scrollView = UIScrollView(frame: self.view.frame)
        self.scrollView.backgroundColor = UIColor.white
        
        var xframe = CGRect.zero
        let anchoCampo = Constantes.tamaños.anchoPantalla * 0.9
        let altoCampo = Constantes.tamaños.anchoPantalla * 0.1
        let anchoBorder : CGFloat = 0.5
        let colorBorde = UIColor.black.cgColor
        
        //titulo tipo servicio
        xframe.origin = CGPoint(x: 10, y: 10)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: 30)
        let tituloTipoServicio = UILabel(frame: xframe)
        tituloTipoServicio.text = "TIPO DE SERVICIO"
        tituloTipoServicio.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_2
        tituloTipoServicio.textColor = Constantes.paletaColores.verdePeoples
        tituloTipoServicio.sizeToFit()
        self.scrollView.addSubview(tituloTipoServicio)
        
        //textfield tipo servicio
        xframe.origin = CGPoint(x: 10, y: tituloTipoServicio.frame.maxY + 10)
        xframe.size = CGSize(width: anchoCampo, height: altoCampo)
        self.txtTipoServicio = UITextField(frame: xframe)
        self.txtTipoServicio.layer.borderWidth = anchoBorder
        self.txtTipoServicio.layer.borderColor = colorBorde
        self.txtTipoServicio.layer.cornerRadius = 5
        self.txtTipoServicio.textAlignment = .center
        self.txtTipoServicio.center.x = Constantes.tamaños.centroHorizontal
        self.txtTipoServicio.font = Constantes.currentFonts.NunitoRegular.base
        self.txtTipoServicio.isUserInteractionEnabled = false
        self.txtTipoServicio.delegate = self
        self.txtTipoServicio.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0)
        self.addToolBar(textField:  self.txtTipoServicio)
        self.scrollView.addSubview(self.txtTipoServicio)
        
        //Titulo de la solicitud.
        xframe.origin = CGPoint(x: 10, y: self.txtTipoServicio.frame.maxY + 10)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: 30)
        let lblTituloSolicitud = UILabel(frame: xframe)
        lblTituloSolicitud.text = "TITULO DE LA SOLICITUD"
        lblTituloSolicitud.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_2
        lblTituloSolicitud.textColor = Constantes.paletaColores.verdePeoples
        lblTituloSolicitud.sizeToFit()
        self.scrollView.addSubview(lblTituloSolicitud)
        
        xframe.origin = CGPoint(x: 10, y:lblTituloSolicitud.frame.maxY + 10)
        xframe.size = CGSize(width: anchoCampo, height: altoCampo)
        txtTituloSolicitud = UITextField(frame: xframe)
        txtTituloSolicitud.layer.borderWidth = anchoBorder
        txtTituloSolicitud.layer.borderColor = colorBorde
        txtTituloSolicitud.layer.cornerRadius = 5
        txtTituloSolicitud.textAlignment = .center
        txtTituloSolicitud.center.x = Constantes.tamaños.centroHorizontal
        txtTituloSolicitud.font = Constantes.currentFonts.NunitoRegular.base
        txtTituloSolicitud.delegate = self
        self.addToolBar(textField: txtTituloSolicitud)
        self.scrollView.addSubview(txtTituloSolicitud)
        
        
        //titulo detalles de servicio
        xframe.origin = CGPoint(x: 10, y: self.txtTituloSolicitud.frame.maxY + 10)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: 30)
        let tituloDetalleServicio = UILabel(frame: xframe)
        tituloDetalleServicio.text = "DETALLES DEL SERVICIO"
        tituloDetalleServicio.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_2
        tituloDetalleServicio.textColor = Constantes.paletaColores.verdePeoples
        tituloDetalleServicio.sizeToFit()
        self.scrollView.addSubview(tituloDetalleServicio)
        
        //text detalle de servicio
        xframe.origin = CGPoint(x: 10, y: tituloDetalleServicio.frame.maxY + 10)
        xframe.size = CGSize(width: anchoCampo, height: altoCampo * 2)
        self.txtDetalleServicio = UITextView(frame: xframe)
        self.txtDetalleServicio.layer.borderWidth = anchoBorder
        self.txtDetalleServicio.layer.borderColor = colorBorde
        self.txtDetalleServicio.layer.cornerRadius = 5
        self.txtDetalleServicio.font = Constantes.currentFonts.NunitoRegular.base
        
        self.txtDetalleServicio.center.x = Constantes.tamaños.centroHorizontal
        self.addToolBar(textView:  self.txtDetalleServicio)
        self.scrollView.addSubview(self.txtDetalleServicio)
        
        //titulo presupuesto
        xframe.origin = CGPoint(x: 10, y: self.txtDetalleServicio.frame.maxY + 10)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: 30)
        let tituloPresupuesto = UILabel(frame: xframe)
        tituloPresupuesto.text = "PRESUPUESTO MÁXIMO"
        tituloPresupuesto.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_2
        tituloPresupuesto.textColor = Constantes.paletaColores.verdePeoples
        tituloPresupuesto.sizeToFit()
        self.scrollView.addSubview(tituloPresupuesto)
        
        //txt presupuesto
        xframe.origin = CGPoint(x: 10, y: tituloPresupuesto.frame.maxY + 10)
        xframe.size = CGSize(width: anchoCampo * 0.3, height: altoCampo)
        self.txtCantPresupuesto = UITextField(frame: xframe)
        self.txtCantPresupuesto.layer.borderWidth = anchoBorder
        self.txtCantPresupuesto.layer.borderColor = colorBorde
        self.txtCantPresupuesto.layer.cornerRadius = 5
        self.txtCantPresupuesto.keyboardType = .decimalPad
        self.scrollView.addSubview(self.txtCantPresupuesto)
        self.txtCantPresupuesto.frame.origin.x = self.txtDetalleServicio.frame.minX
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: altoCampo))
        self.txtCantPresupuesto.rightView = paddingView
        self.txtCantPresupuesto.rightViewMode = .always
        self.txtCantPresupuesto.textAlignment = .right
        self.txtCantPresupuesto.placeholder = "$"
        self.txtCantPresupuesto.font = Constantes.currentFonts.NunitoRegular.base
        self.addToolBar(textField:  self.txtCantPresupuesto)
        
        //txt para picker tipo moneda
        xframe.origin = CGPoint(x: self.txtCantPresupuesto.frame.maxX + 10, y: tituloPresupuesto.frame.maxY + 10)
        lblTipoMoneda = UILabel(frame: xframe)
        lblTipoMoneda.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0)
        lblTipoMoneda.text = divisa.abreviacion
//        self.txtTipoMoneda.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0)
//        self.txtTipoMoneda.isEnabled = false
        self.scrollView.addSubview(lblTipoMoneda)
        
        //titulo fecha
        xframe.origin = CGPoint(x: 10, y: self.txtCantPresupuesto.frame.maxY + 10)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: 30)
        let tituloFecha = UILabel(frame: xframe)
        tituloFecha.text = "FECHA Y HORA"
        tituloFecha.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_2
        tituloFecha.textColor = Constantes.paletaColores.verdePeoples
        tituloFecha.sizeToFit()
        self.scrollView.addSubview(tituloFecha)
        
        //text fecha
        xframe.origin = CGPoint(x: 10, y: tituloFecha.frame.maxY + 10)
        xframe.size = CGSize(width: anchoCampo, height: altoCampo)
        self.txtFechaServicio = UITextField(frame: xframe)
        self.txtFechaServicio.layer.borderWidth = anchoBorder
        self.txtFechaServicio.layer.borderColor = colorBorde
        self.txtFechaServicio.layer.cornerRadius = 5
        self.txtFechaServicio.center.x = Constantes.tamaños.centroHorizontal
        self.scrollView.addSubview(self.txtFechaServicio)
        self.txtFechaServicio.textAlignment = .center
        self.txtFechaServicio.font = Constantes.currentFonts.NunitoRegular.base
        
        //picker
        self.pickerFecha.datePickerMode = .dateAndTime
        self.pickerFecha.minimumDate = Date()
        let toolbarPickerFecha = self.createToolbarFecha()
        
        self.txtFechaServicio.inputView = self.pickerFecha
        self.txtFechaServicio.inputAccessoryView = toolbarPickerFecha
        self.txtFechaServicio.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0)
        
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MMMM/yyyy HH:mm"
        let selectedDate: String = dateFormatter.string(from: Date())
        self.txtFechaServicio.text = selectedDate
        
        //titulo direccion
        xframe.origin = CGPoint(x: 10, y: self.txtFechaServicio.frame.maxY + 10)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: 30)
        let tituloDireccion = UILabel(frame: xframe)
        tituloDireccion.text = "DIRECCIÓN DEL SERVICIO"
        tituloDireccion.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_2
        tituloDireccion.textColor = Constantes.paletaColores.verdePeoples
        tituloDireccion.sizeToFit()
        self.scrollView.addSubview(tituloDireccion)
        
        //text direccion
        xframe.origin = CGPoint(x: 10, y: tituloDireccion.frame.maxY + 10)
        xframe.size = CGSize(width: anchoCampo, height: altoCampo)
        self.txtDireccion = UITextField(frame: xframe)
        self.txtDireccion.layer.borderWidth = anchoBorder
        self.txtDireccion.layer.borderColor = colorBorde
        self.txtDireccion.layer.cornerRadius = 5
        self.txtDireccion.center.x = Constantes.tamaños.centroHorizontal
        self.txtDireccion.delegate = self
        self.txtDireccion.font = Constantes.currentFonts.NunitoRegular.base
        self.txtDireccion.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0)
        self.scrollView.addSubview(self.txtDireccion)
        
        self.view.addSubview(self.scrollView)
        
        let padding = (Constantes.tamaños.altoBotonTop + (self.navigationController?.navigationBar.frame.size.height)!) + Constantes.tamaños.altoStatusBar
        
        //btn
        xframe.origin = CGPoint(x: 0, y: Constantes.tamaños.altoPantalla - padding)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: Constantes.tamaños.altoBotonTop)
        self.btnSolicitar = UIView(frame: xframe)
        self.btnSolicitar.backgroundColor = Constantes.paletaColores.verdePeoples
        
        
        let lblBtnsolicitar = UILabel(frame: xframe)
        lblBtnsolicitar.text = "SOLICITAR"
        lblBtnsolicitar.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_1
        lblBtnsolicitar.sizeToFit()
        lblBtnsolicitar.textColor = UIColor.white
        self.btnSolicitar.addSubview(lblBtnsolicitar)
        lblBtnsolicitar.center.x = Constantes.tamaños.anchoPantalla / 2
        lblBtnsolicitar.center.y = self.btnSolicitar.frame.size.height / 2
        let tapSolicitar = UITapGestureRecognizer(target: self, action: #selector(self.confEnvioDatos))
        self.btnSolicitar.addGestureRecognizer(tapSolicitar)
        self.btnSolicitar.isUserInteractionEnabled = true
        self.view.addSubview(self.btnSolicitar)
        
        self.scrollView.contentSize.height = self.txtDireccion.frame.maxY + padding + 10
    }
    
    func isFormValid()->Bool {
        var error = false
        var xmensaje = ""
        
        let tipoServicio = self.txtTipoServicio.text?.trimmed()
        let tituloSolicitud = self.txtTituloSolicitud.text?.trimmed()
        let detalles = self.txtDetalleServicio.text.trimmed()
        let presupuesto = self.txtCantPresupuesto.text?.trimmed()
        let direccion = self.txtDireccion.text?.trimmed()
        
        if tipoServicio == "" {
            xmensaje = "El tipo de servicio es obligatorio"
            error = true
        }
        
        if tituloSolicitud == "" {
            xmensaje = "El título de la solicitud es obligatorio"
            error = true
        }
        
        if !error && detalles == "" {
            xmensaje = "El detalle de servicio es obligatorio"
            error = true
        }
        
        if !error && presupuesto == "" {
            xmensaje = "El presupuesto de servicio es obligatorio"
            error = true
        }
        
        if !error && direccion == "" {
            xmensaje = "la direccion de servicio es obligatoria"
            error = true
        }
        
        if error {
            self.mensajeSimple(mensaje: xmensaje, dismiss: true)
        }
        
        return !error
    }
    
    @objc func confEnvioDatos(){
        if Functions().isUserLoggedIn() {
            let alert = UIAlertController(title: "People's App", message: "¿Deseas realizar la solicitud con estos datos?", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: {(
                alert: UIAlertAction!) in
                self.solicitarServicio()
            })
            )
            
            alert.addAction(UIAlertAction(title: "CANCEL", style: UIAlertActionStyle.cancel, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func solicitarServicio(){
        
        if (!self.isFormValid()){
            return
        }
        
        self.showLoader()
        
        let tituloSolicitud = self.txtTituloSolicitud.text?.trimmed()
        let detalles = self.txtDetalleServicio.text.trimmed()
        let presupuesto = self.txtCantPresupuesto.text?.trimmed()
        let divisaSend = divisa.id
        
        if self.fechaHoraSend == "" {
            let dateFormatter: DateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:00"
            self.fechaHoraSend = dateFormatter.string(from: self.pickerFecha.date)
        }
                
        //Validación para obtener idCategoria.
        var idCategoria = String()
        if(currentPro != nil) {
            idCategoria = self.currentPro.categoria.id
        }
        else {
            idCategoria = idCategoriaSelected
        }
        
        var idProfesional = String()
        if(self.currentPro != nil) {
            idProfesional = self.currentPro.id
        }
        else {
            idProfesional = ""
        }
        
        Functions().solicitarServicio(titulo: tituloSolicitud!,
                                      descripcion: detalles,
                                      categoria: idCategoria,
                                      presupuesto: presupuesto!,
                                      idDivisa: divisaSend!,
                                      fecha: self.fechaHoraSend,
                                      direccion: self.servDireccionCompleta,
                                      latitude: self.servLatitud,
                                      longitude: self.servLongitud,
                                      idProfesional: idProfesional){ exito, mensaje, error in
            
            self.hideLoader()
            
            if let xerror = error {
                print("solicitarServicio - Ocurrio un error \(xerror)")
                self.mensajeSimple(mensaje: "Error de red", dismiss: false)
            } else {
                if exito {
                    let alert = UIAlertController(title: NSLocalizedString("AppName", comment: ""),
                                                  message: mensaje,
                                                  preferredStyle: UIAlertControllerStyle.alert)
                    
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: {(
                        alert: UIAlertAction!) in
                        Functions().backtoMain()
                    })
                    )
                    
                    self.present(alert, animated: true, completion: nil)
                    
                } else {
                    self.mensajeSimple(mensaje: mensaje, dismiss: false)
                }
            }
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func createToolbarTipoServicio() ->UIToolbar {
        let toolbarPickerServ = UIToolbar()
        toolbarPickerServ.barStyle = UIBarStyle.default
        toolbarPickerServ.isTranslucent = true
        toolbarPickerServ.sizeToFit()
        
        let okBtnPick = UIBarButtonItem(title: "OK",
                                        style: UIBarButtonItemStyle.plain,
                                        target: self,
                                        action: #selector(donePickerServ(sender:)))
        
        let spaceBtnPick = UIBarButtonItem(barButtonSystemItem:UIBarButtonSystemItem.flexibleSpace,
                                           target: nil,
                                           action: nil)
        
        let cancelBtnPick = UIBarButtonItem(title: "Cancel",
                                            style: UIBarButtonItemStyle.plain,
                                            target: self,
                                            action: #selector(seleccionaCancelar))
        
        toolbarPickerServ.setItems([cancelBtnPick, spaceBtnPick, okBtnPick], animated: false)
        toolbarPickerServ.isUserInteractionEnabled = true
        return toolbarPickerServ
    }
    
    func createToolbarFecha() ->UIToolbar {
        let toolbarPickerFecha = UIToolbar()
        toolbarPickerFecha.barStyle = UIBarStyle.default
        toolbarPickerFecha.isTranslucent = true
        toolbarPickerFecha.sizeToFit()
        
        let okBtnPickF = UIBarButtonItem(title: "OK",
                                         style: UIBarButtonItemStyle.plain,
                                         target: self,
                                         action: #selector(self.donePickerFecha(sender:)))
        
        let spaceBtnPickF = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace,
                                            target: nil,
                                            action: nil)
        
        let cancelBtnPickF = UIBarButtonItem(title: "Cancel",
                                             style: UIBarButtonItemStyle.plain,
                                             target: self,
                                             action: #selector(seleccionaCancelar))
        
        toolbarPickerFecha.setItems([cancelBtnPickF, spaceBtnPickF, okBtnPickF], animated: false)
        toolbarPickerFecha.isUserInteractionEnabled = true
        return toolbarPickerFecha
    }
    
    
    
}
