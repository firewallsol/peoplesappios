//
//  DetallePropuestaVC.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 19/10/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit

class DetallePropuestaVC: UIViewController {
    
    enum TipoEvento: Int {
        case rechazar
        case aceptar
    }
    
    //MARK: - Components
    var imgProfesional : UIImageView!
    var lblNombreProfesional : UILabel!
    var lblEspecialidad : UILabel!
    var lblEstatus : UILabel!
    var lblFechaPropuesta : UILabel!
    var lblPrecio : UILabel!
    var lblCashBack : UILabel!
    var btnRechazar : UIButton!
    var btnAceptar : UIButton!
    var btnChat : UIButton!
    var scrollView : UIScrollView!
    
    //MARK: - Vars
    var currentPropuesta : Propuesta!
    var fechaServicio : Date!
    var tipoEventoSelected: TipoEvento!

    //MARK: - View
    override func viewDidLoad() {
        super.viewDidLoad()

        self.automaticallyAdjustsScrollViewInsets = false
        
        self.view.backgroundColor = UIColor.white
        
        self.buildInterface(){ finished in
            //
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationItem.title = NSLocalizedString("TitleProposal", comment: "")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationItem.title = ""
    }
    
    //MARK: - Custom
    func buildInterface(completionHandler: @escaping (Bool) -> ()){
        
        var xframe = CGRect.zero
        
        self.scrollView = UIScrollView(frame: self.view.frame)
        
        //img usuario
        let anchoAltoImagen = Constantes.tamaños.anchoPantalla * 0.3
        xframe.origin = CGPoint(x: 10, y: 20)
        xframe.size = CGSize(width: anchoAltoImagen, height: anchoAltoImagen)
        self.imgProfesional = UIImageView(frame: xframe)
        self.imgProfesional.layer.cornerRadius = self.imgProfesional.frame.size.width / 2
        self.imgProfesional.clipsToBounds = true
        self.imgProfesional.kf.indicatorType = .activity
        self.imgProfesional.kf.setImage(with: self.currentPropuesta.datosProfesional.imagen)
        self.imgProfesional.contentMode = .scaleAspectFill
        self.scrollView.addSubview(self.imgProfesional)
        let tapImg = UITapGestureRecognizer(target: self, action: #selector(self.gotoDetalleProfesional))
        self.imgProfesional.isUserInteractionEnabled = true
        self.imgProfesional.addGestureRecognizer(tapImg)
        
        //especialidad
        xframe.origin = CGPoint(x: self.imgProfesional.frame.maxX + 10, y: 20)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla * 0.6, height: 20)
        self.lblEspecialidad = UILabel(frame: xframe)
        self.lblEspecialidad.adjustsFontSizeToFitWidth = true
        self.lblEspecialidad.font = Constantes.currentFonts.NunitoRegular.minus.LabelFontSize_2
        self.lblEspecialidad.text = self.currentPropuesta.datosProfesional.categoria.nombre
        self.scrollView.addSubview(self.lblEspecialidad)
        self.lblEspecialidad.textAlignment = .center
        self.lblEspecialidad.center.y = self.imgProfesional.frame.minY + (self.imgProfesional.frame.size.height / 2) + 20
        self.lblEspecialidad.textColor = Constantes.paletaColores.verdePeoples
        let tapEspecialidadPro = UITapGestureRecognizer(target: self, action: #selector(self.gotoDetalleProfesional))
        self.lblEspecialidad.isUserInteractionEnabled = true
        self.lblEspecialidad.addGestureRecognizer(tapEspecialidadPro)
        
        //nombre profesional
        let posynombre = self.lblEspecialidad.frame.minY - (5 + 20)
        xframe.origin = CGPoint(x: self.imgProfesional.frame.maxX + 10, y: posynombre)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla * 0.6, height: 25)
        self.lblNombreProfesional = UILabel(frame: xframe)
        self.lblNombreProfesional.adjustsFontSizeToFitWidth = true
        self.lblNombreProfesional.font = Constantes.currentFonts.NunitoBold.base
        self.lblNombreProfesional.text = self.currentPropuesta.datosProfesional.razonSocial
        self.scrollView.addSubview(self.lblNombreProfesional)
        self.lblNombreProfesional.textAlignment = .center
        let tapNombrePro = UITapGestureRecognizer(target: self, action: #selector(self.gotoDetalleProfesional))
        self.lblNombreProfesional.isUserInteractionEnabled = true
        self.lblNombreProfesional.addGestureRecognizer(tapNombrePro)
        
        
        let separacionCampos = Constantes.tamaños.anchoPantalla * 0.07
        
        
        //status
        xframe.origin = CGPoint(x: 10, y: self.imgProfesional.frame.maxY + separacionCampos)
        let tituloEstatus = UILabel(frame: xframe)
        tituloEstatus.textColor = Constantes.paletaColores.verdePeoples
        tituloEstatus.text = NSLocalizedString("Status", comment: "").uppercased()
        tituloEstatus.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_1
        tituloEstatus.sizeToFit()
        self.scrollView.addSubview(tituloEstatus)
        
        //value fecha servicio
        xframe.origin = CGPoint(x: 40, y: tituloEstatus.frame.minY)
        self.lblEstatus = UILabel(frame: xframe)
        self.scrollView.addSubview(self.lblEstatus)
        self.lblEstatus.textColor = UIColor.gray
        self.lblEstatus.text = self.currentPropuesta.estatus.descripcion
        self.lblEstatus.font = Constantes.currentFonts.NunitoRegular.minus.LabelFontSize_1
        self.lblEstatus.sizeToFit()
        self.lblEstatus.frame.origin.x = Constantes.tamaños.anchoPantalla - (self.lblEstatus.frame.size.width + 5)
        
        
        
        //fecha de servicio
        //titulo
        xframe.origin = CGPoint(x: 10, y: self.lblEstatus.frame.maxY + separacionCampos)
        let tituloFechaServicio = UILabel(frame: xframe)
        tituloFechaServicio.textColor = Constantes.paletaColores.verdePeoples
        tituloFechaServicio.text = NSLocalizedString("DateOfService", comment: "").uppercased()
        tituloFechaServicio.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_1
        tituloFechaServicio.sizeToFit()
        self.scrollView.addSubview(tituloFechaServicio)
        
        //value fecha servicio
        xframe.origin = CGPoint(x: 40, y: tituloFechaServicio.frame.minY)
        self.lblFechaPropuesta = UILabel(frame: xframe)
        self.scrollView.addSubview(self.lblFechaPropuesta)
        self.lblFechaPropuesta.textColor = UIColor.gray
        
        self.lblFechaPropuesta.text = Functions().dateToString(laFecha: self.fechaServicio)
        self.lblFechaPropuesta.font = Constantes.currentFonts.NunitoRegular.minus.LabelFontSize_3
        self.lblFechaPropuesta.sizeToFit()
        self.lblFechaPropuesta.frame.origin.x = Constantes.tamaños.anchoPantalla - (self.lblFechaPropuesta.frame.size.width + 5)
        
        
        //precio
        //titulo
        xframe.origin = CGPoint(x: 10, y: tituloFechaServicio.frame.maxY + separacionCampos)
        let tituloPrecio = UILabel(frame: xframe)
        tituloPrecio.textColor = Constantes.paletaColores.verdePeoples
        tituloPrecio.text = NSLocalizedString("Price", comment: "").uppercased()
        tituloPrecio.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_1
        tituloPrecio.sizeToFit()
        self.scrollView.addSubview(tituloPrecio)
        
        //value precio
        xframe.origin = CGPoint(x: 40, y: tituloPrecio.frame.minY)
        self.lblPrecio = UILabel(frame: xframe)
        self.scrollView.addSubview(self.lblPrecio)
        self.lblPrecio.textColor = UIColor.red
        let precio = self.currentPropuesta.presupuesto!
        self.lblPrecio.text = precio.formatoPrecio()
        self.lblPrecio.font = Constantes.currentFonts.NunitoRegular.base
        self.lblPrecio.sizeToFit()
        self.lblPrecio.center.y = tituloPrecio.center.y
        self.lblPrecio.frame.origin.x = Constantes.tamaños.anchoPantalla - (self.lblPrecio.frame.size.width + 60)
        
        //Divisa
        xframe.origin = CGPoint(x: lblPrecio.frame.maxX + 3, y: lblPrecio.frame.minY)
        let lblDivisa = UILabel(frame: xframe)
        lblDivisa.text = currentPropuesta.divisa.abreviacion
        lblDivisa.textColor = UIColor.red
        lblDivisa.sizeToFit()
        lblDivisa.center.y = lblPrecio.center.y
        self.scrollView.addSubview(lblDivisa)
        
        //cashback
        //titulo
        xframe.origin = CGPoint(x: 10, y: tituloPrecio.frame.maxY + separacionCampos)
        let tituloCashback = UILabel(frame: xframe)
        tituloCashback.textColor = Constantes.paletaColores.verdeCompletado
        tituloCashback.text = NSLocalizedString("YouWillGetCashback", comment: "").uppercased()
        tituloCashback.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_1
        tituloCashback.sizeToFit()
        self.scrollView.addSubview(tituloCashback)
        tituloCashback.isHidden = true
        
        //value cashback
        xframe.origin = CGPoint(x: 40, y: tituloCashback.frame.minY)
        self.lblCashBack = UILabel(frame: xframe)
        self.scrollView.addSubview(self.lblCashBack)
        self.lblCashBack.textColor = Constantes.paletaColores.verdeCompletado
        self.lblCashBack.text = NSLocalizedString("YouWillGetCashback", comment: "").uppercased()
        self.lblCashBack.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_1
        self.lblCashBack.sizeToFit()
        self.lblCashBack.center.x = Constantes.tamaños.centroHorizontal
        
        //botones
        let anchoBoton = Constantes.tamaños.anchoPantalla * 0.3
        
        //Rechazar
        
        xframe.origin = CGPoint(x: 30, y: self.lblCashBack.frame.maxY + separacionCampos)
        xframe.size = CGSize(width: anchoBoton, height: Constantes.tamaños.altoBotonTop)
        self.btnRechazar = UIButton(frame: xframe)
        self.btnRechazar.setTitle(NSLocalizedString("ButtonReject", comment: ""), for: .normal)
        self.btnRechazar.backgroundColor = Constantes.paletaColores.verdePeoples
        self.btnRechazar.setTitleColor(UIColor.white, for: .normal)
        self.btnRechazar.titleLabel?.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_1
        self.btnRechazar.layer.cornerRadius = 10
        self.scrollView.addSubview(self.btnRechazar)
        self.btnRechazar.center.x = Constantes.tamaños.centroHorizontal
        self.btnRechazar.addTarget(self, action: #selector(self.confirmaResponderPropuesta(sender:)), for: .touchUpInside)
        self.btnRechazar.tag = TipoEvento.rechazar.hashValue
        
        
        
        //Aceptar
        xframe.origin = CGPoint(x: 30, y: self.lblCashBack.frame.maxY + separacionCampos)
        xframe.size = CGSize(width: anchoBoton, height: Constantes.tamaños.altoBotonTop)
        self.btnAceptar = UIButton(frame: xframe)
        self.btnAceptar.setTitle(NSLocalizedString("ButtonAccept", comment: ""), for: .normal)
        self.btnAceptar.backgroundColor = Constantes.paletaColores.verdePeoples
        self.btnAceptar.setTitleColor(UIColor.white, for: .normal)
        self.btnAceptar.titleLabel?.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_1
        self.btnAceptar.layer.cornerRadius = 10
        self.scrollView.addSubview(self.btnAceptar)
        self.btnAceptar.center.x = self.btnRechazar.frame.minX / 2
        self.btnAceptar.addTarget(self, action: #selector(self.confirmaResponderPropuesta(sender:)), for: .touchUpInside)
        self.btnAceptar.tag = TipoEvento.aceptar.hashValue
        
        //chat
        xframe.origin = CGPoint(x: 30, y: self.lblCashBack.frame.maxY + separacionCampos)
        xframe.size = CGSize(width: anchoBoton, height: Constantes.tamaños.altoBotonTop)
        self.btnChat = UIButton(frame: xframe)
        self.btnChat.setTitle(NSLocalizedString("ButtonChat", comment: ""), for: .normal)
        self.btnChat.backgroundColor = Constantes.paletaColores.verdeCompletado
        self.btnChat.setTitleColor(UIColor.white, for: .normal)
        self.btnChat.titleLabel?.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_1
        self.btnChat.layer.cornerRadius = 10
        self.scrollView.addSubview(self.btnChat)
        let centerXBtnChat = self.btnRechazar.frame.maxX + ((Constantes.tamaños.anchoPantalla - self.btnRechazar.frame.maxX) / 2)
        self.btnChat.center.x = centerXBtnChat
        self.btnChat.addTarget(self, action: #selector(self.gotoChat), for: .touchUpInside)
        
        
        //padding
        let padding = (self.navigationController?.navigationBar.frame.size.height)! + Constantes.tamaños.altoStatusBar + 10
        self.scrollView.contentSize.height = self.btnRechazar.frame.maxY + padding
        
        self.view.addSubview(self.scrollView)
        
        if self.currentPropuesta.estatus.id == "5" { //Cancelada
            self.btnAceptar.isHidden = true
            self.btnRechazar.isHidden = true
            self.btnChat.isHidden = true
        }
        else if self.currentPropuesta.estatus.id != "1" {
            self.btnAceptar.isHidden = true
            self.btnRechazar.isHidden = true
        }
        
        completionHandler(true)
    }
    
    @objc func gotoChat(){
        let elVC = ChatVC()
        elVC.currentPropuesta = self.currentPropuesta
        self.navigationController?.pushViewController(elVC, animated: true)
    }
    
    @objc func confirmaResponderPropuesta(sender: UIButton){
        let tipo = sender.currentTitle
        tipoEventoSelected = TipoEvento(rawValue: sender.tag)
        
        let alert = UIAlertController(title: NSLocalizedString("AppName", comment: ""),
                                      message: "¿Esta seguro que desea \(tipo!) esta propuesta?",
            preferredStyle: UIAlertControllerStyle.alert)
        
        let okButton = UIAlertAction(title: NSLocalizedString("ButtonOK", comment: "").uppercased(),
                                     style: UIAlertActionStyle.default, handler: {(
            alert: UIAlertAction!) in
            self.showLoader()
            self.responderPropuestaActual(tipo: tipo!)
        })
        
        let cancelButton = UIAlertAction(title: NSLocalizedString("ButtonCancel", comment: "").uppercased(),
                                         style: UIAlertActionStyle.cancel, handler: nil)
        
        alert.addAction(cancelButton)
        alert.addAction(okButton)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func responderPropuestaActual(tipo: String){
        Functions().responderPropuesta(idServicio: self.currentPropuesta.idServicio,
                                       idPropuesta: self.currentPropuesta.id,
                                       accion: tipo){ exito, mensaje, error in
            
            self.hideLoader()
            if let _ = error {
                self.mensajeSimple(mensaje: "Error de red", dismiss: false)
            } else {
                if exito {
                    
                    //Notificacion push
                    let user = Functions().getUserName()
                    let deviceId = self.currentPropuesta.datosProfesional.deviceId
                    let subtitle = NSLocalizedString("SubtitleProposal", comment: "")
                    var message = ""
                    
                    if self.tipoEventoSelected == TipoEvento.aceptar {
                        message = String.localizedStringWithFormat(NSLocalizedString("NotificationProposalAccepted", comment: ""), user)
                    }else {
                        message = String.localizedStringWithFormat(NSLocalizedString("NotificationProposalRejected", comment: ""), user)
                    }
                    
                    PushNotifications.enviaNotificacion(userId: deviceId,
                                                        subtitulo: subtitle,
                                                        mensaje: message,
                                                        appId: Constantes.OneSignalAppId.pplesProfesional)
                    
                    let alert = UIAlertController(title: NSLocalizedString("AppName", comment: ""),
                                                  message: mensaje, preferredStyle: UIAlertControllerStyle.alert)
                    
                    let okButton = UIAlertAction(title: NSLocalizedString("ButtonOK", comment: ""),
                                                 style: UIAlertActionStyle.default, handler: {(
                        alert: UIAlertAction!) in
                        _ = self.navigationController?.popViewController(animated: true)
                        NotificationCenter.default.post(name: Constantes.notificationNames.backToListadoPropuesta, object: nil)
                        
                    })
                    
                    alert.addAction(okButton)
                    
                    self.present(alert, animated: true, completion: nil)
                } else {
                    self.mensajeSimple(mensaje: mensaje, dismiss: false)
                }
            }
            
        }
    }
    
    @objc func gotoDetalleProfesional(){
        let elVC = DetalleProfesionalVC()
        elVC.currentPro = self.currentPropuesta.datosProfesional
        self.navigationController?.pushViewController(elVC, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
