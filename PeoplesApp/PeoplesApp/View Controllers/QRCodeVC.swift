//
//  QRCodeVC.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 10/11/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import QRCode

class QRCodeVC: UIViewController {
    
    var btnCerrar : UIImageView!
    var lblNombreUsuario : UILabel!
    var lblEmail : UILabel!
    
    var lblFecha : UILabel!
    var lblMonto : UILabel!
    
    var codigoQR : QR!
    var qrcodeImg : UIImageView!
    
    var userData : Usuario!
    
    var numTicket = ""
    var usrMonto = ""
    var idNegocio = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.userData = Functions().getDataUsuario()

        self.automaticallyAdjustsScrollViewInsets = false
        self.view.backgroundColor = UIColor.white
        
        self.buildInterface()
    }
    
    func buildInterface(){
        var xframe = CGRect.zero
        
        //img cerrar
        let anchoAltoCerrar = Constantes.tamaños.anchoPantalla * 0.1
        xframe.origin = CGPoint(x: 10, y: 40)
        xframe.size = CGSize(width: anchoAltoCerrar, height: anchoAltoCerrar)
        self.btnCerrar = UIImageView(frame: xframe)
        self.btnCerrar.image = Asset.Icons.btncerrar64.image
        self.btnCerrar.contentMode = .scaleAspectFill
        self.view.addSubview(self.btnCerrar)
        let tapCerrar = UITapGestureRecognizer(target: self, action: #selector(self.closeWindow))
        self.btnCerrar.isUserInteractionEnabled = true
        self.btnCerrar.addGestureRecognizer(tapCerrar)
        
        
        //label titulo
        let labelTitulo = UILabel(frame: xframe)
        labelTitulo.text = "Obtener CASHBACK"
        labelTitulo.font = Constantes.currentFonts.NunitoBold.base
        self.view.addSubview(labelTitulo)
        labelTitulo.sizeToFit()
        labelTitulo.center.x = Constantes.tamaños.centroHorizontal
        labelTitulo.center.y = self.btnCerrar.frame.minY + (self.btnCerrar.frame.size.height / 2)
        
        //nombre
        xframe.origin = CGPoint(x: 20, y: self.btnCerrar.frame.maxY + 25)
        self.lblNombreUsuario = UILabel(frame: xframe)
        self.lblNombreUsuario.text = "\(self.userData.nombre!) \(self.userData.apellidos!)"
        self.lblNombreUsuario.font = Constantes.currentFonts.NunitoRegular.base
        self.lblNombreUsuario.sizeToFit()
        self.view.addSubview(self.lblNombreUsuario)
        
        //correo
        xframe.origin = CGPoint(x: 20, y: self.lblNombreUsuario.frame.maxY + 5)
        self.lblEmail = UILabel(frame: xframe)
        self.lblEmail.text = self.userData.email
        self.lblEmail.textColor = Constantes.paletaColores.verdePeoples
        self.lblEmail.font = Constantes.currentFonts.NunitoRegular.minus.LabelFontSize_2
        self.lblEmail.sizeToFit()
        self.view.addSubview(self.lblEmail)
        
        //titulo fecha
        xframe.origin = CGPoint(x: 20, y: self.lblEmail.frame.maxY + 20)
        let tituloFecha = UILabel(frame: xframe)
        tituloFecha.text = "Fecha:"
        tituloFecha.textColor = Constantes.paletaColores.verdePeoples
        tituloFecha.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_1
        tituloFecha.sizeToFit()
        self.view.addSubview(tituloFecha)
        
        //fecha
        xframe.origin = CGPoint(x: tituloFecha.frame.maxX + 10, y: tituloFecha.frame.minY)
        self.lblFecha = UILabel(frame: xframe)
        self.lblFecha.text = Functions().dateToString(formato: "dd 'de' MMMM 'de' yyyy", laFecha: Date())
        self.lblFecha.font = Constantes.currentFonts.NunitoRegular.minus.LabelFontSize_1
        self.lblFecha.sizeToFit()
        self.view.addSubview(self.lblFecha)
        
        //titulo monto
        xframe.origin = CGPoint(x: 15, y: tituloFecha.frame.maxY + 5)
        let tituloMonto = UILabel(frame: xframe)
        tituloMonto.text = "Monto:"
        tituloMonto.textColor = Constantes.paletaColores.verdePeoples
        tituloMonto.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_1
        tituloMonto.sizeToFit()
        self.view.addSubview(tituloMonto)
        
        //fecha
        xframe.origin = CGPoint(x: self.lblFecha.frame.minX, y: tituloMonto.frame.minY)
        self.lblMonto = UILabel(frame: xframe)
        self.lblMonto.text = "$ \(self.usrMonto)"
        self.lblMonto.font = Constantes.currentFonts.NunitoRegular.minus.LabelFontSize_1
        self.lblMonto.sizeToFit()
        self.view.addSubview(self.lblMonto)
        
        tituloFecha.frame.origin.x = self.lblNombreUsuario.frame.minX
        tituloMonto.frame.origin.x = self.lblNombreUsuario.frame.minX
        
        //codigo
        let espacioLibre = Constantes.tamaños.altoPantalla - (self.lblMonto.frame.maxY + Constantes.tamaños.altoStatusBar)
        let anchoAltoCodigo = Constantes.tamaños.anchoPantalla * 0.7
        
        xframe.origin = CGPoint(x: 10, y: self.lblMonto.frame.maxY + 10)
        xframe.size = CGSize(width: anchoAltoCodigo, height: anchoAltoCodigo)
        
        let stringCodigo = "\(self.codigoQR.idRegistro as String)||\(self.codigoQR.nombreEmpresa as String)||\(self.codigoQR.Monto as String)||\(self.codigoQR.idTicket as String)||\(self.codigoQR.correoCliente as String)||\(self.codigoQR.fechaHora as String)||\(self.codigoQR.nombreCliente as String)"
        print("stringCodigo->\(stringCodigo)")
        var qrCode = QRCode(stringCodigo)
        qrCode?.size = xframe.size
        self.qrcodeImg = UIImageView(frame: xframe)
        self.qrcodeImg.image = qrCode?.image
        self.view.addSubview(self.qrcodeImg)
        self.qrcodeImg.center.y = self.lblMonto.frame.maxY + (espacioLibre / 2)
        self.qrcodeImg.center.x = Constantes.tamaños.centroHorizontal
        
    }
    
    @objc func closeWindow(){
        self.dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
