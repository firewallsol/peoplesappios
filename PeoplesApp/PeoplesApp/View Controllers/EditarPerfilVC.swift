//
//  EditarPerfilVC.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 08/09/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import Kingfisher
import GooglePlacePicker
import CocoaLumberjack

class EditarPerfilVC: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate,
UIImagePickerControllerDelegate, UINavigationControllerDelegate, GMSPlacePickerViewControllerDelegate, UITextFieldDelegate {
    
    //MARK: - Components
    var imgUsuario : UIImageView!
    var lblNombreUsr : UILabel!
    var lblCiudad : UILabel!
    var lblIDUsr : CopyableLabel!
    var txtNombre : UITextField!
    var txtApellidos : UITextField!
    var txtDireccion : UITextField!
    var txtCelular : UITextField!
    var txtEmail : UITextField!
    var btnActualizarPago : UIButton!
    var btnModificarContrasenia: UIButton!
    var btnAgregarAfiliador : UIButton!
    var btnEditar : UIView!
    var scrollView : UIScrollView!
    var txtPais: UITextField!
    var picker: UIPickerView!
    
    //MARK: - Vars
    var datosUsuario : Usuario!
    var arrayPaises = [Pais]()
    var paisSelected = Pais()
    var imagePath: String?
    var servLatitud = ""
    var servLongitud = ""
    var servDireccionCompleta = ""
    
    
    //MARK: - View
    override func viewDidLoad() {
        super.viewDidLoad()
        
        arrayPaises = Constantes.arrayPaises
        
        picker = UIPickerView()
        picker.dataSource = self
        picker.delegate = self
        
        let tapGesture = UITapGestureRecognizer()
        tapGesture.addTarget(self, action: #selector(tapToDismiss))
        self.view.isUserInteractionEnabled = true
        self.view.addGestureRecognizer(tapGesture)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        self.automaticallyAdjustsScrollViewInsets = false
        self.view.backgroundColor = UIColor.white
        
        if datosUsuario == nil {
            //Carga inicialmente los datos del usuario.
            self.datosUsuario = Functions().getDataUsuario()
        }
        
        self.buildInterface(){ finished in
            //Configura Avatar
            if let avatar = self.datosUsuario.avatar {
                self.imgUsuario.kf.setImage(with: avatar)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = NSLocalizedString("EditProfile", comment: "")
        self.datosUsuario = Functions().getDataUsuario()
        self.setUserData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationItem.title = ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Keyboard
    @objc func keyboardWillShow(sender: NSNotification) {
        var userInfo = sender.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height
        self.scrollView.contentInset = contentInset
        
        var visibleRect = self.view.frame
        visibleRect.size.height -= keyboardFrame.size.height
        
        var elOrigin = CGPoint.zero
        
        var scroll = false
        
        if self.txtNombre.isFirstResponder {
            visibleRect.size.height -= ((self.txtNombre.inputAccessoryView?.frame.size.height)! * 2)
            elOrigin = self.txtNombre.frame.origin
            scroll = true
        }
        
        if self.txtApellidos.isFirstResponder {
            visibleRect.size.height -= ((self.txtApellidos.inputAccessoryView?.frame.size.height)! * 2)
            elOrigin = self.txtApellidos.frame.origin
            scroll = true
        }
        
        //        if self.txtDireccion.isFirstResponder {
        //            visibleRect.size.height -= ((self.txtDireccion.inputAccessoryView?.frame.size.height)! * 2)
        //            elOrigin = self.txtDireccion.frame.origin
        //            scroll = true
        //        }
        
        if self.txtEmail.isFirstResponder {
            visibleRect.size.height -= ((self.txtEmail.inputAccessoryView?.frame.size.height)! * 2)
            elOrigin = self.txtEmail.frame.origin
            scroll = true
        }
        
        if self.txtCelular.isFirstResponder {
            visibleRect.size.height -= ((self.txtCelular.inputAccessoryView?.frame.size.height)! * 2)
            elOrigin = self.txtCelular.frame.origin
            scroll = true
        }
        
        if ((scroll) && !(visibleRect.contains(elOrigin))){
            let scrollPoint = CGPoint(x: 0, y: elOrigin.y - visibleRect.size.height)
            self.scrollView.setContentOffset(scrollPoint, animated: true)
        }
    }
    
    @objc func keyboardWillHide(sender: NSNotification) {
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        self.scrollView.contentInset = contentInset
    }
    
    //MARK: - Picker
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayPaises.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let pais = arrayPaises[row]
        return pais.descripcion
    }
    
//    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
//        let pais = arrayPaises[row]
//        paisSelected = pais
//        txtPais.text = pais.descripcion
//    }
    
    //MARK: - Image Picker
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        DDLogDebug("Ingresando...")
        self.showLoader()
        
        let imageSelected = info[UIImagePickerControllerEditedImage] as! UIImage
        
        self.guardaImagenLocal(imagen: imageSelected)
        imgUsuario.image = imageSelected
        
        picker.dismiss(animated: true) {
            self.hideLoader()
        }
    }
    
    //MARK: - Text field
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.txtDireccion {
            textField.resignFirstResponder()
            let config = GMSPlacePickerConfig(viewport: nil)
            let placePickerVC = GMSPlacePickerViewController(config: config)
            placePickerVC.delegate = self
            present(placePickerVC, animated: true, completion: nil)
            return false
        }
        
        return true
    }
    
    //MARK: - Google place picker delegate
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true) {
            DDLogInfo("Place name \(place.name)")
            DDLogInfo("Place address \(String(describing: place.formattedAddress!))")
            DDLogInfo("Place coordenates \(place.coordinate)")
            
            self.txtDireccion.text = place.name
            self.servDireccionCompleta = place.formattedAddress!
            self.servLatitud = "\(place.coordinate.latitude)"
            self.servLongitud = "\(place.coordinate.longitude)"
        }
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        DDLogInfo("No place selected")
    }
    
    //MARK: - Custom
    func setUserData() {
        
        //nombre
        self.lblNombreUsr.text = "\(self.datosUsuario.nombre!) \(self.datosUsuario.apellidos!)"
        self.lblNombreUsr.sizeToFit()
        self.lblNombreUsr.center.x = Constantes.tamaños.centroHorizontal
        
        //ciudad
        self.lblCiudad.text = self.datosUsuario.direcciones.first?.descripcion
        self.lblCiudad.sizeToFit()
        self.lblCiudad.center.x = Constantes.tamaños.centroHorizontal
        
        //ID
        //Codigo afiliador
        let stringCodigoAfiliador = NSLocalizedString("Código de afiliador:", comment: "")
        var codigoAfiliador = NSLocalizedString("Sin código", comment: "")
        if let codigo = self.datosUsuario.codigoAfiliador {
            codigoAfiliador = codigo
        }
        
        self.lblIDUsr.text = "\(stringCodigoAfiliador) \(codigoAfiliador)"
        self.lblIDUsr.sizeToFit()
        self.lblIDUsr.center.x = Constantes.tamaños.centroHorizontal
        
        //nombre txt
        self.txtNombre.text = self.datosUsuario.nombre
        
        //apellidos
        self.txtApellidos.text = self.datosUsuario.apellidos
        
        //direccion
        self.txtDireccion.text = self.datosUsuario.direcciones.first?.descripcion
        
        //email
        self.txtEmail.text = self.datosUsuario.email
        
        //telefono
        self.txtCelular.text = self.datosUsuario.telefono
        
        //pais
        if let pais = self.datosUsuario.pais {
            if pais.descripcion != "" {
                self.txtPais.text = NSLocalizedString(pais.descripcion, comment: "")
                self.txtPais.isEnabled = false
                self.txtPais.textColor = UIColor.gray
                paisSelected = pais
            }
        }
    }
    
    func buildInterface(completionHandler: @escaping (Bool) -> ()){
        
        var xframe = CGRect.zero
        let separacionCampos : CGFloat = 10
        let cornerCampos : CGFloat = 3
        
        self.scrollView = UIScrollView(frame: self.view.frame)
        
        //imagen
        let anchoAltoImagen = Constantes.tamaños.anchoPantalla * 0.3
        xframe.origin = CGPoint(x: 10, y: 20)
        xframe.size = CGSize(width: anchoAltoImagen, height: anchoAltoImagen)
        self.imgUsuario = UIImageView(frame: xframe)
        self.imgUsuario.layer.cornerRadius = self.imgUsuario.frame.size.width / 2
        self.imgUsuario.clipsToBounds = true
        self.imgUsuario.kf.indicatorType = .activity
        self.imgUsuario.contentMode = .scaleAspectFill
        self.scrollView.addSubview(self.imgUsuario)
        self.imgUsuario.center.x = Constantes.tamaños.centroHorizontal
        
        let tapGesture = UITapGestureRecognizer()
        tapGesture.numberOfTapsRequired = 1
        tapGesture.addTarget(self, action: #selector(cambiarAvatar))
        self.imgUsuario.isUserInteractionEnabled = true
        self.imgUsuario.addGestureRecognizer(tapGesture)
        
        let buttonCamera = UIButton(frame: CGRect(x: self.imgUsuario.frame.maxX - 30,
                                                  y: self.imgUsuario.frame.maxY - 30,
                                                  width: 30, height: 30))
        
        let image = UIImage(named: "camera")
        buttonCamera.setImage(image, for:.normal)
        buttonCamera.addTarget(self, action: #selector(cambiarAvatar), for:.touchUpInside)
        buttonCamera.isUserInteractionEnabled = true
        buttonCamera.layer.cornerRadius = buttonCamera.frame.height / 2
        buttonCamera.contentMode = .scaleAspectFit
        self.scrollView.addSubview(buttonCamera)
        
        //nombre
        xframe.origin = CGPoint(x: 10, y: self.imgUsuario.frame.maxY + 3)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla * 0.8, height: 30)
        self.lblNombreUsr = UILabel(frame: xframe)
        self.lblNombreUsr.font = Constantes.currentFonts.NunitoBold.base
        self.lblNombreUsr.textColor = Constantes.paletaColores.verdePeoples
        self.scrollView.addSubview(self.lblNombreUsr)
        
        
        self.lblNombreUsr.text = Functions().getUserName()
        self.lblNombreUsr.sizeToFit()
        self.lblNombreUsr.center.x = Constantes.tamaños.centroHorizontal
        self.lblNombreUsr.textAlignment = .center
        
        //ciudad
        xframe.origin = CGPoint(x: 10, y: self.lblNombreUsr.frame.maxY + 3)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla * 0.5, height: 30)
        self.lblCiudad = UILabel(frame: xframe)
        self.lblCiudad.font = Constantes.currentFonts.NunitoRegular.minus.LabelFontSize_4
        self.lblCiudad.textColor = UIColor.gray
        self.lblCiudad.lineBreakMode = .byWordWrapping
        self.lblCiudad.numberOfLines = 2
        self.scrollView.addSubview(self.lblCiudad)
        self.lblCiudad.center.x = Constantes.tamaños.centroHorizontal
        
        //ID
        xframe.origin = CGPoint(x: 10, y: self.lblCiudad.frame.maxY + 3)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla * 0.8, height: 30)
        self.lblIDUsr = CopyableLabel(frame: xframe)
        self.lblIDUsr.font = Constantes.currentFonts.NunitoBold.base
        self.lblIDUsr.textColor = Constantes.paletaColores.verdePeoples
        self.scrollView.addSubview(self.lblIDUsr)
        self.lblIDUsr.text = NSLocalizedString("Código de afiliador:", comment: "")
        self.lblIDUsr.textAlignment = .center
        self.lblIDUsr.sizeToFit()
        self.lblIDUsr.center.x = Constantes.tamaños.centroHorizontal
        
        //nombre
        xframe.size = CGSize(width: Constantes.tamaños.anchoTextfield, height: Constantes.tamaños.altoTextfield)
        xframe.origin = CGPoint(x: 10, y: self.lblIDUsr.frame.maxY + 20)
        self.txtNombre = UITextField(frame: xframe)
        self.txtNombre.placeholder = NSLocalizedString("FirstName", comment: "")
        self.txtNombre.layer.borderWidth = Constantes.tamaños.sizeBordeTextfield
        self.txtNombre.layer.borderColor = Constantes.paletaColores.bordeTextfield.cgColor
        self.txtNombre.layer.cornerRadius = cornerCampos
        self.txtNombre.center.x = Constantes.tamaños.centroHorizontal
        self.txtNombre.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0)
        self.addToolBar(textField: self.txtNombre)
        self.txtNombre.font = Constantes.currentFonts.NunitoRegular.base
        self.scrollView.addSubview(self.txtNombre)
        
        //apellidos
        xframe.origin = CGPoint(x: 10, y: self.txtNombre.frame.maxY + separacionCampos)
        self.txtApellidos = UITextField(frame: xframe)
        self.txtApellidos.placeholder = NSLocalizedString("LastName", comment: "")
        self.txtApellidos.layer.borderWidth = Constantes.tamaños.sizeBordeTextfield
        self.txtApellidos.layer.borderColor = Constantes.paletaColores.bordeTextfield.cgColor
        self.txtApellidos.layer.cornerRadius = cornerCampos
        self.txtApellidos.center.x = Constantes.tamaños.centroHorizontal
        self.txtApellidos.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0)
        self.addToolBar(textField: self.txtApellidos)
        self.txtApellidos.font = Constantes.currentFonts.NunitoRegular.base
        self.scrollView.addSubview(self.txtApellidos)
        
        //direccion
        xframe.origin = CGPoint(x: 10, y: self.txtApellidos.frame.maxY + separacionCampos)
        self.txtDireccion = UITextField(frame: xframe)
        self.txtDireccion.placeholder = NSLocalizedString("Address", comment: "")
        self.txtDireccion.layer.borderWidth = Constantes.tamaños.sizeBordeTextfield
        self.txtDireccion.layer.borderColor = Constantes.paletaColores.bordeTextfield.cgColor
        self.txtDireccion.layer.cornerRadius = cornerCampos
        self.txtDireccion.center.x = Constantes.tamaños.centroHorizontal
        self.txtDireccion.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0)
        self.txtDireccion.delegate = self
        self.addToolBar(textField: self.txtDireccion)
        self.txtDireccion.font = Constantes.currentFonts.NunitoRegular.base
        self.scrollView.addSubview(self.txtDireccion)
        
        //Pais
        xframe.origin = CGPoint(x: 10, y: self.txtDireccion.frame.maxY + separacionCampos)
        self.txtPais = UITextField(frame: xframe)
        self.txtPais.placeholder = NSLocalizedString("Country", comment: "")
        self.txtPais.font = Constantes.currentFonts.NunitoRegular.base
        self.txtPais.layer.borderWidth = Constantes.tamaños.sizeBordeTextfield
        self.txtPais.layer.borderColor = Constantes.paletaColores.bordeTextfield.cgColor
        self.txtPais.layer.cornerRadius = cornerCampos
        self.txtPais.center.x = Constantes.tamaños.centroHorizontal
        self.txtPais.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0)
        self.txtPais.inputView = picker
        self.txtPais.inputAccessoryView = createToolbarPais()
        self.scrollView.addSubview(self.txtPais)
        
        //email
        xframe.origin = CGPoint(x: 10, y: self.txtPais.frame.maxY + separacionCampos)
        self.txtEmail = UITextField(frame: xframe)
        self.txtEmail.placeholder = NSLocalizedString("Email", comment: "")
        self.txtEmail.layer.borderWidth = Constantes.tamaños.sizeBordeTextfield
        self.txtEmail.layer.borderColor = Constantes.paletaColores.bordeTextfield.cgColor
        self.txtEmail.layer.cornerRadius = cornerCampos
        self.txtEmail.center.x = Constantes.tamaños.centroHorizontal
        self.txtEmail.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0)
        self.addToolBar(textField: self.txtEmail)
        self.txtEmail.font = Constantes.currentFonts.NunitoRegular.base
        self.txtEmail.textColor = UIColor.gray
        self.scrollView.addSubview(self.txtEmail)
        self.txtEmail.isUserInteractionEnabled = false
        
        //telefono
        xframe.origin = CGPoint(x: 10, y: self.txtEmail.frame.maxY + separacionCampos)
        self.txtCelular = UITextField(frame: xframe)
        self.txtCelular.placeholder = NSLocalizedString("Cellphone", comment: "")
        self.txtCelular.layer.borderWidth = Constantes.tamaños.sizeBordeTextfield
        self.txtCelular.layer.borderColor = Constantes.paletaColores.bordeTextfield.cgColor
        self.txtCelular.layer.cornerRadius = cornerCampos
        self.txtCelular.center.x = Constantes.tamaños.centroHorizontal
        self.txtCelular.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0)
        self.addToolBar(textField: self.txtCelular)
        self.txtCelular.font = Constantes.currentFonts.NunitoRegular.base
        self.txtCelular.keyboardType = .phonePad
        var frame = txtCelular.frame
        self.scrollView.addSubview(self.txtCelular)
        
        //Modificar contrasenia, solo disponible para usuarios con Registro-Email
        if Int(datosUsuario.tipoLogin.id) == tipoRegistroLogin.email.rawValue {
            xframe.origin = CGPoint(x: 10, y: self.txtCelular.frame.maxY + separacionCampos)
            btnModificarContrasenia = UIButton(frame: xframe)
            frame = xframe
            btnModificarContrasenia.setTitleColor(Constantes.paletaColores.verdePeoples, for: .normal)
            btnModificarContrasenia.setTitle(NSLocalizedString("ChangePassword", comment: "").uppercased(), for: .normal)
            btnModificarContrasenia.titleLabel?.font =  Constantes.currentFonts.NunitoRegular.base
            btnModificarContrasenia.addTarget(self, action: #selector(seleccionaModificarContrasenia), for: .touchUpInside)
            self.scrollView.addSubview(btnModificarContrasenia)
        }
        self.view.addSubview(self.scrollView)
        
        var ypos = frame.maxY
        
        if ((self.datosUsuario.afiliacion.statusEnum == .completada) &&
            (self.datosUsuario.afiliador == nil)) {
            
            xframe.origin = CGPoint(x: 10, y: ypos + 20)
            xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla * 0.6, height: Constantes.tamaños.altoBotonTop)
            self.btnAgregarAfiliador = UIButton(frame: xframe)
            self.btnAgregarAfiliador.setTitle("Agregar Afiliador", for: .normal)
            self.btnAgregarAfiliador.setTitleColor(UIColor.white, for: .normal)
            self.btnAgregarAfiliador.titleLabel?.font = Constantes.currentFonts.NunitoRegular.base
            self.btnAgregarAfiliador.center.x = Constantes.tamaños.centroHorizontal
            self.btnAgregarAfiliador.backgroundColor = Constantes.paletaColores.verdePeoples
            self.btnAgregarAfiliador.layer.cornerRadius = 5
            self.btnAgregarAfiliador.clipsToBounds = true
            self.scrollView.addSubview(self.btnAgregarAfiliador)
            ypos = self.btnAgregarAfiliador.frame.maxY
            self.btnAgregarAfiliador.addTarget(self, action: #selector(self.addAfiliador), for: .touchUpInside)
            
        }
        
        xframe.origin = CGPoint(x: 10, y: ypos + 20)
        xframe.size = CGSize(width: Constantes.tamaños.anchoTextfield, height: Constantes.tamaños.altoTextfield)
        let lblRecibirCashBack = UILabel(frame: xframe)
        lblRecibirCashBack.numberOfLines = 0
        lblRecibirCashBack.textAlignment = .justified
        lblRecibirCashBack.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_2
        lblRecibirCashBack.text = "¿Deseas recibir cashback en tus compras y servicios? Añade o actualiza tu información de pago"
        self.scrollView.addSubview(lblRecibirCashBack)
        lblRecibirCashBack.sizeToFit()
        lblRecibirCashBack.center.x = Constantes.tamaños.centroHorizontal
        
        //actualizar
        //xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla * 0.9, height: Constantes.tamaños.altoTextfield)
        xframe.origin = CGPoint(x: 10, y: lblRecibirCashBack.frame.maxY + 20)
        self.btnActualizarPago = UIButton(frame: xframe)
        self.btnActualizarPago.setTitle("ACTUALIZAR MÉTODO DE PAGO", for: .normal)
        self.btnActualizarPago.setTitleColor(Constantes.paletaColores.verdePeoples, for: .normal)
        self.btnActualizarPago.titleLabel?.font = Constantes.currentFonts.NunitoBold.base
        self.scrollView.addSubview(self.btnActualizarPago)
        self.btnActualizarPago.center.x = Constantes.tamaños.centroHorizontal
        self.btnActualizarPago.addTarget(self, action: #selector(self.gotoMetodoPago), for: .touchUpInside)
        
        //editar
        let xpadding =  (Constantes.tamaños.altoBotonTop) + (self.navigationController?.navigationBar.frame.size.height)! + Constantes.tamaños.altoStatusBar
        xframe.origin = CGPoint(x: 0, y: Constantes.tamaños.altoPantalla - xpadding)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: Constantes.tamaños.altoBotonTop)
        self.btnEditar = UIView(frame: xframe)
        self.btnEditar.backgroundColor = Constantes.paletaColores.verdePeoples
        
        let lblBtnEditar = UILabel(frame: xframe)
        lblBtnEditar.text = NSLocalizedString("ButtonEdit", comment: "").uppercased()
        lblBtnEditar.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_1
        lblBtnEditar.textColor = UIColor.white
        self.btnEditar.addSubview(lblBtnEditar)
        lblBtnEditar.sizeToFit()
        lblBtnEditar.center.x = Constantes.tamaños.anchoPantalla / 2
        lblBtnEditar.center.y = self.btnEditar.frame.size.height / 2
        let tapEditar = UITapGestureRecognizer(target: self, action: #selector(self.confirmaEdicion))
        self.btnEditar.addGestureRecognizer(tapEditar)
        self.btnEditar.isUserInteractionEnabled = true
        self.view.addSubview(self.btnEditar)
        
        
        let padding = (self.navigationController?.navigationBar.frame.size.height)! + Constantes.tamaños.altoStatusBar + 10 + (Constantes.tamaños.altoBotonTop)
        
        self.scrollView.contentSize.height = self.btnActualizarPago.frame.maxY + padding
        
        completionHandler(true)
    }
    
    @objc func gotoMetodoPago(){
        let elVC = MetodosPagoVC()
        elVC.usuarioActual = self.datosUsuario
        self.navigationController?.pushViewController(elVC, animated: true)
    }
    
    @objc func addAfiliador(){
        var inputTextField: UITextField?
        let passwordPrompt = UIAlertController(title: "People's App", message: "Por favor ingrese el código de su afiliador.", preferredStyle: UIAlertControllerStyle.alert)
        passwordPrompt.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
        passwordPrompt.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) -> Void in
            if let value = (inputTextField?.text) {
                if value != "" {
                    //TODO: FALTA IMPLEMENTACIÓN SERVICIO WEB
                    print("WARNING: NO HAY SERVICIO WEB HABILITADO PARA GUARDAR ESTA INFO.")
//                    print("yep llama al servicio y actualiza el afiliador")
                    
                }
            }
            
        }))
        passwordPrompt.addTextField(configurationHandler: {(textField: UITextField!) in
            textField.placeholder = "Código afiliador"
            textField.keyboardType = .emailAddress
            inputTextField = textField
        })
        
        present(passwordPrompt, animated: true, completion: nil)
    }
    
    func isFormValid()->Bool{
        var mensaje = ""
        
        //nombre
        if (self.txtNombre.text?.trimmed() == ""){
            mensaje = "El campo \"Nombre\", es obligatorio"
            
        }
        //nombre
        if (mensaje == "" && self.txtApellidos.text?.trimmed() == ""){
            mensaje = "El campo \"Apellidos\", es obligatorio"
            
        }
        
        //pais
        if (mensaje == "" && self.txtPais.text?.trimmed() == ""){
            mensaje = "El campo \"Pais\", es obligatorio"
            
        }
        
        //celular
        if (mensaje == "" && (self.txtCelular.text?.trimmed() == "")){
            mensaje = "El campo \"Celular\", es obligatorio"
            
        }
        
        //direccion
        if (mensaje == "" && (self.txtDireccion.text?.trimmed() == "")){
            mensaje = "El campo \"Dirección\", es obligatorio"
        }
        
        if mensaje != "" {
            
            self.mensajeSimple(mensaje: mensaje)
            
            return false
        }
        
        
        return true
    }
    
    @objc func confirmaEdicion(){
        let alert = UIAlertController(title: "People's App", message: "¿Esta seguro que desea actualizar su perfil con esta información?", preferredStyle: UIAlertControllerStyle.alert)
        
        let okButton = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: {(
            alert: UIAlertAction!) in
            self.sendData()
        })
        
        let cancelButton = UIAlertAction(title: "CANCEL", style: UIAlertActionStyle.cancel, handler: nil)
        
        alert.addAction(cancelButton)
        alert.addAction(okButton)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func sendData(){
        if !self.isFormValid() {
            return
        }
        
        self.showLoader()
        
        let xnombre = (self.txtNombre.text?.trimmed())!
        let xapellidos = (self.txtApellidos.text?.trimmed())!
        let xtelefono = (self.txtCelular.text?.trimmed())!
        var xdireccion = ""
        var xlatitud = ""
        var xlongitud = ""
        var xIdDireccion = ""
        
        if let direccion = self.datosUsuario.direcciones.first {
            xIdDireccion = direccion.id
        }
        
        if servDireccionCompleta != "" && servLatitud != "" && servLongitud != "" {
            //Actualiza direccion
            xdireccion = servDireccionCompleta
            xlatitud = servLatitud
            xlongitud = servLongitud
        }
        else {
            if let direccion = self.datosUsuario.direcciones.first {
                //Retiene direccion
                xdireccion = direccion.descripcion
                xlatitud = direccion.latitud
                xlongitud = direccion.longitud
            }
        }
        
        var xImagePath = ""
        if let imagePathTemp = imagePath {
            xImagePath = imagePathTemp
        }
        
        Functions().updateUsuarioPerfil(imagePath: xImagePath,
                                        nombre: xnombre,
                                        apellidos: xapellidos,
                                        telefono: xtelefono,
                                        pais: paisSelected.id,
                                        idDireccion: xIdDireccion,
                                        direccion: xdireccion,
                                        latitud: xlatitud,
                                        longitud: xlongitud) { (exito, mensaje, error) in
                                            self.hideLoader()
                                            if let xerror = error {
                                                print("GET datos cashback - Ocurrio un error \(xerror)")
                                                self.mensajeSimple(mensaje: "Error de red, revise su conexion", dismiss: false)
                                            } else {
                                                if exito { //se deja separado por si despues quieren cambiar algo o sacarlo de esta pantalla
                                                    self.alertaPopViewWithMessage(message: mensaje)
                                                } else {
                                                    self.mensajeSimple(mensaje: mensaje, dismiss: false)
                                                }
                                            }
                                            
        }
    }
    
    @objc func tapToDismiss() {
        self.view.endEditing(true)
    }
    
    //MARK: - Events
    @objc func cambiarAvatar() {
        let message = NSLocalizedString("AlertChooseOption", comment: "")
        let alertController = UIAlertController(title: NSLocalizedString("AppName", comment: ""),
                                                message: message,
                                                preferredStyle: UIAlertControllerStyle.actionSheet)
        
        //ImagePicker.
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        
        let cameraMessage = NSLocalizedString("TakePicture", comment: "")
        let cameraAction = UIAlertAction(title: cameraMessage,
                                         style: UIAlertActionStyle.default)
        {(alertAction) in
            
            if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
            {
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera
                imagePicker.cameraCaptureMode = .photo
                imagePicker.modalPresentationStyle = .fullScreen
                self.present(imagePicker, animated: true, completion: nil)
            }
            else
            {
                let cameraAccessMessage = NSLocalizedString("CameraAccessMessage", comment: "")
                self.mensajeSimple(mensaje: cameraAccessMessage)
            }
        }
        
        let imageMessage = NSLocalizedString("ChoosePicture", comment: "")
        let imageAction = UIAlertAction(title: imageMessage,
                                        style: UIAlertActionStyle.default)
        { (alertAction) in
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        }
        
        let cancelText = NSLocalizedString("ButtonCancel", comment: "")
        let cancelAction = UIAlertAction(title: cancelText,
                                         style: UIAlertActionStyle.cancel,
                                         handler: nil)
        
        alertController.addAction(cameraAction)
        alertController.addAction(imageAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func guardaImagenLocal(imagen: UIImage) {
        DispatchQueue.global(qos:.default).async {
            let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory,
                                                                        .userDomainMask,
                                                                        true).first!
            
            let imageName = "profileImage.jpg"
            let imagePathTemp = "\(documentDirectory)/\(imageName)"
            self.imagePath = imagePathTemp
            DDLogInfo("ImageLocalPath: \(imagePathTemp)")
            
            if let dataImage = UIImageJPEGRepresentation(imagen, 9.0) {
                do {
                    try dataImage.write(to: URL(fileURLWithPath: imagePathTemp))
                    DDLogInfo("Se guardó imagen con éxito.")
                }
                catch {
                    DDLogError("Ocurrio un error al guardar la imagen localmente.")
                }
            }
        }
    }
    
    @objc func seleccionaModificarContrasenia() {
        let viewController = ModificarContraseniaVC()
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func alertaPopViewWithMessage(message: String) {
        let alert = UIAlertController(title: NSLocalizedString("AppName", comment: ""),
                                      message: message,
                                      preferredStyle: .actionSheet)
        
        let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""),
                                     style: .default) { (action) in
                                        self.navigationController?.popViewController(animated: true)
        }
        
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func createToolbarPais() ->UIToolbar {
        let toolbarPickerFecha = UIToolbar()
        toolbarPickerFecha.barStyle = UIBarStyle.default
        toolbarPickerFecha.isTranslucent = true
        toolbarPickerFecha.sizeToFit()
        
        let okBtnPickF = UIBarButtonItem(title: NSLocalizedString("OK", comment:""),
                                         style: UIBarButtonItemStyle.plain,
                                         target: self,
                                         action: #selector(self.selectKeyboardButton(sender:)))
        
        let spaceBtnPickF = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace,
                                            target: nil,
                                            action: nil)
        
        let cancelBtnPickF = UIBarButtonItem(title: NSLocalizedString("Cancel", comment: ""),
                                             style: UIBarButtonItemStyle.plain,
                                             target: self,
                                             action: #selector(self.selectKeyboardButton(sender:)))
        
        toolbarPickerFecha.setItems([cancelBtnPickF, spaceBtnPickF, okBtnPickF], animated: false)
        toolbarPickerFecha.isUserInteractionEnabled = true
        return toolbarPickerFecha
    }
    
    @objc func selectKeyboardButton(sender: UIBarButtonItem) {
        if sender.title == NSLocalizedString("OK", comment: "") {
            
            let idSelected = picker.selectedRow(inComponent: 0)
            let pais = self.arrayPaises[idSelected]
            self.txtPais.text = pais.descripcion
            paisSelected = pais
        }
        self.view.endEditing(true)
    }
    
}
