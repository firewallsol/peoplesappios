//
//  ConfiguracionVC.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 11/09/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit

class ConfiguracionVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var tableConfiguracion : UITableView!
    
    //var notificationsCell = UITableViewCell()
    var historialPagosCell = UITableViewCell()
    var comoFuncionaCell = UITableViewCell()
    var HelpCell = UITableViewCell()
    var ajustesCell = UITableViewCell()
    //var LogOutCell = UITableViewCell()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor.white
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        self.setBarMenu()
        
        self.buildInterface()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationItem.title = ""
    }
    
    func buildInterface(){
        var xframe = CGRect.zero
        //tableview
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: 300)
        self.tableConfiguracion = UITableView(frame: xframe)
        self.tableConfiguracion.backgroundColor = UIColor.white
        self.tableConfiguracion.delegate = self
        self.tableConfiguracion.dataSource = self
        self.tableConfiguracion.isScrollEnabled = false
        self.tableConfiguracion.tableFooterView = UIView(frame: CGRect.zero)
        
        /*
        //notifications
        self.notificationsCell.textLabel?.text = "Notificaciones"
        self.notificationsCell.textLabel?.textColor = Constantes.paletaColores.verdePeoples
        self.notificationsCell.imageView?.image = Functions().scaleImageToSize(sourceImage: Asset.payPal.image, scaledToSize: CGSize(width: 20, height: 20))
        
        self.notificationsCell.imageView?.contentMode = .scaleAspectFill
        self.notificationsCell.imageView?.clipsToBounds = true
        self.notificationsCell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        self.notificationsCell.selectionStyle = .none
        */
        
        //historial
        self.historialPagosCell.textLabel?.text = "Historial de pagos"
        self.historialPagosCell.textLabel?.textColor = Constantes.paletaColores.verdePeoples
        self.historialPagosCell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        self.historialPagosCell.selectionStyle = .none
        //self.historialPagosCell.imageView?.image = Functions().scaleImageToSize(sourceImage: Asset.icClock48.image, scaledToSize: CGSize(width: 20, height: 20))
        self.historialPagosCell.textLabel?.font = Constantes.currentFonts.NunitoRegular.base
        
        //como funciona
        self.comoFuncionaCell.textLabel?.text = "¿Como funciona?"
        self.comoFuncionaCell.textLabel?.textColor = Constantes.paletaColores.verdePeoples
        self.comoFuncionaCell.imageView?.backgroundColor = UIColor.black
        self.comoFuncionaCell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        self.comoFuncionaCell.selectionStyle = .none
        self.comoFuncionaCell.textLabel?.font = Constantes.currentFonts.NunitoRegular.base
        
        //help
        self.HelpCell.textLabel?.text = "Ayuda"
        self.HelpCell.textLabel?.textColor = Constantes.paletaColores.verdePeoples
        self.HelpCell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        self.HelpCell.selectionStyle = .none
        self.HelpCell.textLabel?.font = Constantes.currentFonts.NunitoRegular.base
        
        //ajustes
        self.ajustesCell.textLabel?.text = "Ajustes"
        self.ajustesCell.textLabel?.textColor = Constantes.paletaColores.verdePeoples
        self.ajustesCell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        self.ajustesCell.selectionStyle = .none
        self.ajustesCell.textLabel?.font = Constantes.currentFonts.NunitoRegular.base
        
        //logout
        /*
        self.LogOutCell.textLabel?.text = "Cerrar sesión"
        self.LogOutCell.textLabel?.textColor = Constantes.paletaColores.verdePeoples
        self.LogOutCell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        self.LogOutCell.selectionStyle = .none
        self.LogOutCell.textLabel?.font = Constantes.currentFonts.NunitoRegular.base
        */
        self.view.addSubview(self.tableConfiguracion)
        
        self.tableConfiguracion.reloadData()
        self.tableConfiguracion.layoutIfNeeded()
    }
    
    //table view delegate datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0: return self.historialPagosCell
        case 1: return self.comoFuncionaCell
        case 2: return self.HelpCell
        case 3: return self.ajustesCell
        //case 4: return self.LogOutCell
        default: fatalError("La celda no corresponde")
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0: break; //historial
        case 1: break; //como funciona
        case 2: self.gotoAyuda(); break; //ayuda
        case 3: self.gotoAjustes(); break; //ajustes
        //case 4: self.cierraSesion(); break; //cerrar sesion
        default:
            break
        }
    }
    
    //table view delegate datasource - fin
    
    func gotoAyuda(){
        let elVC = AyudaVC()
        self.navigationController?.pushViewController(elVC, animated: true)
    }
    
    func gotoAjustes(){
        let elVC = AjustesVC()
        self.navigationController?.pushViewController(elVC, animated: true)
    }
    
    /*
    func cierraSesion(){
        if Functions().isUserLoggedIn() {
            Functions().borraTodoCierraSesion()
            Functions().backtoMain()
        }
        
    }
     */
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
