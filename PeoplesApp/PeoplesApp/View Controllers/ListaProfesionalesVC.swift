//
//  ListaProfesionalesVC.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 05/07/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import Kingfisher
import CoreLocation
import CocoaLumberjack

class ListaProfesionalesVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var tableListaProfesionales : UITableView!
    var btnSolicitar : UIView!
    
    let nombreCeldaPro = "celdaListaProfesionales"
    
    var arrayProfesionales = [Profesional]()
    var currentCategoria = Categoria()
    var ubicacionActual: CLLocationCoordinate2D!
    
    var padding : CGFloat = 0
    
    var showLoaderBool = true

    //MARK: - View
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        self.padding = (Constantes.tamaños.altoBotonTop + (self.navigationController?.navigationBar.frame.size.height)!) + Constantes.tamaños.altoStatusBar
        
        self.buildInterface(){ finished in
            Functions().getProfesionalesCategoria(latitud: self.ubicacionActual.latitude.description,
                                                  longitud: self.ubicacionActual.longitude.description,
                                                  categoria: self.currentCategoria.id) {exito, mensaje, profesionales, error in
                
                self.showLoaderBool = false
                
                if self.isLoaderAnimating() {
                    self.hideLoader()
                }
                
                if let xerror = error {
                    DDLogError(xerror.localizedDescription)
                    self.mensajeSimple(mensaje: "Error de red", dismiss: false)
                } else {
                    if exito {
                        self.arrayProfesionales = profesionales
                        
                        if self.arrayProfesionales.count > 0 {
                            self.tableListaProfesionales.reloadData()
                            self.tableListaProfesionales.contentSize.height += self.padding
                        }
                        else {
                            let message = "No existen profesionales disponibles en esta categoría."
                            self.alertaConMensaje(mensaje: message)
                        }
                        
                    } else {
                        DDLogError(mensaje)
                        self.mensajeSimple(mensaje: "Error desde servicio", dismiss: false)
                    }
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if self.showLoaderBool {
            self.showLoader()
        }
        
        self.navigationController?.navigationBar.isTranslucent = false
        
        self.navigationController?.navigationBar.tintColor = Constantes.paletaColores.verdePeoples
        
        self.title = self.currentCategoria.nombre
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = ""
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       
    }
    
    func buildInterface(completionHandler: @escaping (Bool) -> ()){
        self.tableListaProfesionales = UITableView(frame: self.view.frame)
        self.view.addSubview(self.tableListaProfesionales)
        self.tableListaProfesionales.delegate = self
        self.tableListaProfesionales.dataSource = self
        self.tableListaProfesionales.register(CeldaTVListaProfesionales.self, forCellReuseIdentifier: self.nombreCeldaPro)
        self.tableListaProfesionales.tableFooterView = UIView(frame: CGRect.zero)
        
        //btn
        var xframe = CGRect.zero
        
        xframe.origin = CGPoint(x: 0, y: Constantes.tamaños.altoPantalla - self.padding)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: Constantes.tamaños.altoBotonTop)
        self.btnSolicitar = UIView(frame: xframe)
        self.btnSolicitar.backgroundColor = Constantes.paletaColores.verdePeoples
        
        let lblBtnsolicitar = UILabel(frame: xframe)
        lblBtnsolicitar.text = NSLocalizedString("RequestService", comment: "").uppercased()
        lblBtnsolicitar.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_1
        lblBtnsolicitar.sizeToFit()
        lblBtnsolicitar.textColor = UIColor.white
        self.btnSolicitar.addSubview(lblBtnsolicitar)
        lblBtnsolicitar.center.x = Constantes.tamaños.anchoPantalla / 2
        lblBtnsolicitar.center.y = self.btnSolicitar.frame.size.height / 2
        let tapSolicitar = UITapGestureRecognizer(target: self, action: #selector(self.gotoSolicitarServicio(tap:)))
        self.btnSolicitar.addGestureRecognizer(tapSolicitar)
        self.btnSolicitar.isUserInteractionEnabled = true
        
        self.view.addSubview(self.btnSolicitar)
        
        completionHandler(true)
    }
    
    @objc func gotoSolicitarServicio(tap:UITapGestureRecognizer) {
        if Functions().isUserLoggedIn() {
            //Valida que tenga profesionales
            if arrayProfesionales.count > 0 {
                
                //Valida que el usuario tenga un país de origen.
                let pais = Functions().getUserPais()
                if pais.descripcion != "" {
                    let elVC = SolicitarServicioVC()
                    //        elVC.categoriaDefault = self.currentCategoria.nombre
                    elVC.categoriaSelected = self.currentCategoria
                    elVC.sender = .listaPro
                    self.navigationController?.pushViewController(elVC, animated: true)
                }
                else {
                    let mensaje = NSLocalizedString("AlertShowEditProfileOnProfileIncomplete", comment: "")
                    let titulo = NSLocalizedString("AppName", comment: "")
                    let viewController = EditarPerfilVC()
                    
                    self.alertaRedirige(viewController: viewController, mensaje: mensaje, titulo: titulo)
                }
            }
            else {
                let message = NSLocalizedString("NoAvailableProfessionals", comment: "")
                alertaConMensaje(mensaje: message)
            }
        }
        else {
            let mensaje = NSLocalizedString("AlertShowLoginOnRequestService", comment: "")
            let titulo = NSLocalizedString("AppName", comment: "")
            let viewController = LoginVC()
            
            self.alertaRedirige(viewController: viewController, mensaje: mensaje, titulo: titulo)
        }
    }
    
    //MARK: - TableView datasource y delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableView.validaExistenciaDatos(datos: arrayProfesionales as AnyObject)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constantes.tamaños.altoCeldaNegociosMain
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayProfesionales.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.nombreCeldaPro, for: indexPath) as! CeldaTVListaProfesionales
        
        let elProfesional = self.arrayProfesionales[indexPath.row]
        
        cell.imgProfesional.kf.indicatorType = .activity
        cell.imgProfesional.kf.setImage(with: elProfesional.imagen)
        
        cell.lblNombreProfesional.text = elProfesional.razonSocial
        let altolabelNombre = cell.lblNombreProfesional.heightForView()
        cell.lblNombreProfesional.frame.size.height = altolabelNombre
        
        
        cell.lblProfesion.text = elProfesional.categoria.nombre
        let altolabelProfesion = cell.lblProfesion.heightForView()
        cell.lblProfesion.frame.size.height = altolabelProfesion
        
        cell.lblProfesion.center.y = cell.frame.size.height / 2
        cell.lblNombreProfesional.frame.origin.y = cell.lblProfesion.frame.minY - (1 + cell.lblNombreProfesional.frame.size.height)
        
        cell.lblDistancia.text = "\(elProfesional.distancia!) km"
        cell.lblDistancia.sizeToFit()
        cell.lblDistancia.frame.origin.x = cell.frame.size.width - (cell.lblDistancia.frame.size.width + 10)
        
        cell.ratingStars.rating = Double(elProfesional.calificacion)!
        cell.ratingStars.frame.origin.x = Constantes.tamaños.anchoPantalla - (cell.ratingStars.frame.size.width + 5)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let elVC = DetalleProfesionalVC()
        elVC.currentPro = self.arrayProfesionales[indexPath.row]
        self.navigationController?.pushViewController(elVC, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func alertaConMensaje(mensaje: String) {
        let alerta = UIAlertController(title: "People's App",
                                       message: mensaje,
                                       preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK",
                                     style: .default) { (action) in
                                        self.navigationController?.popViewController(animated: true)
        }
        
        alerta.addAction(okAction)
        self.present(alerta, animated: true, completion: nil)
    }
    
}
