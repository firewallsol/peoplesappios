//
//  DetalleComercioVC.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 04/07/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import GoogleMaps
import Kingfisher

class DetalleComercioVC: UIViewController {
    
    //MARK: - Components
    var imgComercio : UIImageView!
    var imgLike : UIImageView!
    var imgShare : UIImageView!
    var nombreComercio : UILabel!
    var descComercio: UILabel!
    var ic_direccion : UIImageView!
    var direcComercio : UILabel!
    var btnCashBack : UIView!
    var lblBtnCashBack : UILabel!
    var scrollView : UIScrollView!
    
    //MARK: - Vars
    var navTintColor = UIColor()
    var currentEmpresa : Empresa!

    //MARK: - View
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        self.scrollView = UIScrollView(frame: self.view.frame)
        self.scrollView.backgroundColor = UIColor.white
        self.view.addSubview(self.scrollView)
        
        self.buildInterface()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navTintColor = (self.navigationController?.navigationBar.tintColor)!
        self.navigationController?.navigationBar.tintColor = Constantes.paletaColores.verdePeoples
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isTranslucent = false
        
        self.navigationController?.navigationBar.tintColor = Constantes.paletaColores.verdePeoples
        /*
        self.navigationController?.navigationBar.tintColor = self.navTintColor
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        self.navigationController?.navigationBar.shadowImage = nil
        */
    }
    
    //MARK: - Events
    func buildInterface(){
        var xframe = CGRect.zero
        
        //imgComercio
        xframe.origin = CGPoint(x: 0, y: 0)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: Constantes.tamaños.altoPantalla * 0.35)
        self.imgComercio = UIImageView(frame: xframe)
        self.imgComercio.kf.indicatorType = .activity
        self.imgComercio.kf.setImage(with: self.currentEmpresa.imagen)
        self.imgComercio.contentMode = .scaleAspectFill
        self.imgComercio.clipsToBounds = true
        self.scrollView.addSubview(self.imgComercio)
        
        let anchoAltoIcons = Constantes.tamaños.anchoPantalla * 0.07
        //img share
        xframe.origin = CGPoint(x: Constantes.tamaños.anchoPantalla - (anchoAltoIcons + 10), y: self.imgComercio.frame.maxY - (anchoAltoIcons + 10))
        xframe.size = CGSize(width: anchoAltoIcons, height: anchoAltoIcons)
        self.imgShare = UIImageView(frame: xframe)
        self.imgShare.contentMode = .scaleAspectFill
        self.imgShare.image = Asset.Icons.icShare48.image
        self.imgShare.tintColor = Constantes.paletaColores.verdePeoples
        self.imgShare.isUserInteractionEnabled = true
        let shareTap = UITapGestureRecognizer(target: self, action: #selector(seleccionaBotonCompartir(sender:)))
        self.imgShare.addGestureRecognizer(shareTap)
        self.scrollView.addSubview(self.imgShare)
        
        //img like
        xframe.origin = CGPoint(x: self.imgShare.frame.minX - (15 + anchoAltoIcons), y: self.imgComercio.frame.maxY - (anchoAltoIcons + 10))
        xframe.size = CGSize(width: anchoAltoIcons, height: anchoAltoIcons)
        self.imgLike = UIImageView(frame: xframe)
        self.imgLike.contentMode = .scaleAspectFill
        if Functions().existeIdEnFavoritos(id: self.currentEmpresa.id, donde: .empresa){
            self.imgLike.image = Asset.Icons.icFav.image
            self.imgLike.tintColor = .red
        } else {
            self.imgLike.image = Asset.Icons.icFav.image
            self.imgLike.tintColor = .lightGray
        }
        self.scrollView.addSubview(self.imgLike)
        let likeTap = UITapGestureRecognizer(target: self, action: #selector(self.likePressed(sender:)))
        self.imgLike.isUserInteractionEnabled = true
        self.imgLike.addGestureRecognizer(likeTap)
        
        //nombre
        xframe.origin = CGPoint(x: 10, y: self.imgComercio.frame.maxY + 5)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla * 0.9, height: 30)
        self.nombreComercio = UILabel(frame: xframe)
        self.nombreComercio.text = self.currentEmpresa.razonSocial
        self.nombreComercio.font = Constantes.currentFonts.NunitoBold.base
        self.scrollView.addSubview(self.nombreComercio)
        
        //desc
        xframe.origin = CGPoint(x: 10, y: self.nombreComercio.frame.maxY + 5)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla * 0.9, height: 30)
        self.descComercio = UILabel(frame: xframe)
        self.descComercio.text = self.currentEmpresa.descripcion
        self.descComercio.textColor = UIColor.gray
        self.descComercio.numberOfLines = 0
        self.descComercio.font = Constantes.currentFonts.NunitoRegular.minus.LabelFontSize_3
        self.descComercio.textAlignment = .justified
        self.descComercio.sizeToFit()
        self.scrollView.addSubview(self.descComercio)
        
        //img location
        xframe.origin = CGPoint(x: 10, y: self.descComercio.frame.maxY + 10)
        let anchoAltoImgLoc = Constantes.tamaños.anchoPantalla * 0.07
        xframe.size = CGSize(width: anchoAltoImgLoc, height: anchoAltoImgLoc)
        self.ic_direccion = UIImageView(frame: xframe)
        self.ic_direccion.image = Asset.Icons.icLocation.image
        self.ic_direccion.contentMode = .scaleAspectFill
        self.scrollView.addSubview(self.ic_direccion)
        
        //direccion
        xframe.origin = CGPoint(x: self.ic_direccion.frame.maxX + 10, y: self.descComercio.frame.maxY + 10)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla * 0.8, height: 30)
        self.direcComercio = UILabel(frame: xframe)
        self.direcComercio.numberOfLines = 0
        self.direcComercio.font = Constantes.currentFonts.NunitoRegular.minus.LabelFontSize_3
        self.direcComercio.text = self.currentEmpresa.direcciones.first?.descripcion
        self.direcComercio.sizeToFit()
        self.scrollView.addSubview(self.direcComercio)
        
        //center img ubicacion con label
        let centerImgLocation = self.direcComercio.frame.minY + (self.direcComercio.frame.size.height / 2)
        self.ic_direccion.center.y = centerImgLocation
        
        //maps
        
        let latitud = CLLocationDegrees(Double((self.currentEmpresa.direcciones.first?.latitud)!)!)
        let longitud = CLLocationDegrees(Double((self.currentEmpresa.direcciones.first?.longitud)!)!)
        xframe.origin = CGPoint(x: 0, y: self.direcComercio.frame.maxY + 15)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: Constantes.tamaños.altoPantalla * 0.35)
        let camera = GMSCameraPosition.camera(withLatitude: latitud, longitude: longitud, zoom: 17.0)
        let mapView = GMSMapView.map(withFrame: xframe, camera: camera)
        mapView.isUserInteractionEnabled = false
        self.scrollView.addSubview(mapView)
        
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        
        marker.position = CLLocationCoordinate2D(latitude: latitud, longitude: longitud)
        marker.title = self.nombreComercio.text
        marker.map = mapView
        marker.appearAnimation = .pop
        mapView.selectedMarker = marker
        
        let altoBoton = Constantes.tamaños.anchoPantalla * 0.13
        //boton cash
        xframe.origin = CGPoint(x: 0, y: Constantes.tamaños.altoPantalla - altoBoton)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: altoBoton)
        self.btnCashBack = UIView(frame: xframe)
        self.btnCashBack.backgroundColor = Constantes.paletaColores.verdePeoples
        self.lblBtnCashBack = UILabel(frame: xframe)
        self.lblBtnCashBack.text = "OBTENER CASHBACK"
        self.lblBtnCashBack.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_1
        self.lblBtnCashBack.sizeToFit()
        self.lblBtnCashBack.textColor = UIColor.white
        self.btnCashBack.addSubview(self.lblBtnCashBack)
        self.lblBtnCashBack.center.x = Constantes.tamaños.anchoPantalla / 2
        self.lblBtnCashBack.center.y = self.btnCashBack.frame.size.height / 2
        
        let tapBtnCash = UITapGestureRecognizer(target: self, action: #selector(self.obtenerCashBack))
        self.btnCashBack.isUserInteractionEnabled = true
        self.btnCashBack.addGestureRecognizer(tapBtnCash)
        
        self.view.addSubview(self.btnCashBack)
        
        self.scrollView.contentSize.height = mapView.frame.maxY + altoBoton + 5
    }
    
    @objc func likePressed(sender: UITapGestureRecognizer){
        
        if Functions().isUserLoggedIn() {
            let mensaje = Functions().agregarQuitarFavorito(id: self.currentEmpresa.id,
                                                            elJson: self.currentEmpresa.jsonData,
                                                            donde: .empresa)
            self.mensajeSimple(mensaje: mensaje)
            if Functions().existeIdEnFavoritos(id: self.currentEmpresa.id, donde: .empresa){
                self.imgLike.image = Asset.Icons.icFav.image
                self.imgLike.tintColor = .red
            } else {
                self.imgLike.image = Asset.Icons.icFav.image
                self.imgLike.tintColor = .lightGray
            }
        }
    }
    
    @objc func obtenerCashBack(){
        //Si no está logueado el usuario.
        if !Functions().isUserLoggedIn() {
            let mensaje = NSLocalizedString("AlertShowLoginOnClaimCashback", comment: "")
            let titulo = NSLocalizedString("AppName", comment: "")
            let viewController = LoginVC()
            
            self.alertaRedirige(viewController: viewController, mensaje: mensaje, titulo: titulo)
            return
        }
        
        //Valida que tenga pais.
        let pais = Functions().getUserPais()
        if pais.descripcion == "" {
            let titulo = NSLocalizedString("AppName", comment: "")
            let mensaje = NSLocalizedString("AlertShowEditProfileOnClaimCashback", comment: "")
            let viewController = EditarPerfilVC()
            self.alertaRedirige(viewController: viewController, mensaje: mensaje, titulo: titulo)
            return
        }
        
        //Valida que tenga cuenta Paypal.
        if Functions().getUserPayPal() == "" {
            let titulo = NSLocalizedString("AppName", comment: "")
            let mensaje = NSLocalizedString("AlertShowPaymentMethodsOnClaimCashback", comment: "")
            let viewController = MetodosPagoVC()
            
            self.alertaRedirige(viewController: viewController, mensaje: mensaje, titulo: titulo)
            return
        }
        
        
        var montoTextField: UITextField?
        var ticketTextField: UITextField?
        let cashBackPrompt = UIAlertController(title: "People's App", message: "Por favor ingrese el monto de su consumo y su No. de Ticket.", preferredStyle: UIAlertControllerStyle.alert)
        cashBackPrompt.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
        cashBackPrompt.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) -> Void in
            
            var monto = ""
            var ticket = ""
            
            if let value = (montoTextField?.text) {
                monto = value
            }
            
            if let value = (ticketTextField?.text) {
                ticket = value
            }
            
            self.validaEnviaSolicitudCashBack(monto: monto, ticket: ticket)
            
        }))
        cashBackPrompt.addTextField(configurationHandler: {(textField: UITextField!) in
            textField.placeholder = "Monto de consumo"
            textField.keyboardType = .decimalPad
            montoTextField = textField
        })
        cashBackPrompt.addTextField(configurationHandler: {(textField: UITextField!) in
            textField.placeholder = "No. de Ticket"
            ticketTextField = textField
        })
        
        present(cashBackPrompt, animated: true, completion: nil)
    }
    
    func validaEnviaSolicitudCashBack(monto: String, ticket: String){
        if monto == "" || ticket == "" {
            self.mensajeSimple(mensaje: "El monto de consumo y el No. de ticket es requerido para reclamar cashback")
        } else {
            
            print("el monto-> \(monto)     el ticket->\(ticket)")
            self.showLoader()
            
            Functions().registrarConsumo(idEmpresa: self.currentEmpresa.id, monto: monto, ticket: ticket){ exito, mensaje, codigoQR, error in
                
                self.hideLoader()
                
                if let xerror = error {
                    
                    print("GET registrarConsumo - Ocurrio un error \(xerror)")
                    self.mensajeSimple(mensaje: "Error de red, revise su conexion", dismiss: false)
                } else {
                    if exito {
                        self.gotoQRVC(codigo: codigoQR, monto: monto)
                    } else {
                        self.mensajeSimple(mensaje: mensaje, dismiss: false)
                    }
                }
            
            }
        }
    }
    
    func gotoQRVC(codigo: QR, monto: String){
        
        let alert = UIAlertController(title: "People's App", message: "¿Deseas generar el código QR de este cashback?", preferredStyle: UIAlertControllerStyle.alert)
        
        let okButton = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: {(
            alert: UIAlertAction!) in
            
            let elVC = QRCodeVC()
            elVC.codigoQR = codigo
            elVC.usrMonto = monto
           // elVC.numTicket = ticket
           // elVC.idNegocio = self.currentEmpresa.id
            self.present(elVC, animated: true, completion: nil)
            
        })
        
        let cancelButton = UIAlertAction(title: "CANCEL", style: UIAlertActionStyle.cancel, handler: nil)
        
        alert.addAction(cancelButton)
        alert.addAction(okButton)
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func seleccionaBotonCompartir(sender: UITapGestureRecognizer) {
        //        self.showLoader()
        //        DispatchQueue.global(qos:.userInteractive).async {
        var imagen = UIImage()
        var finalString = String()
        
        if let image = imgComercio.image {
            imagen = image
        }
        
        if let xnombre = nombreComercio {
            finalString += "\(xnombre)\n"
        }
        
        if let xdescripcion = descComercio {
            finalString += "\(xdescripcion)\n"
        }
        
        if let xdireccion = direcComercio {
            finalString += "\(xdireccion)"
        }
        
        let activityView = UIActivityViewController(activityItems: [imagen, finalString],
                                                    applicationActivities: nil)
        
        activityView.excludedActivityTypes = [.airDrop, .openInIBooks,
                                              .print, .addToReadingList,
                                              .postToFlickr, .postToVimeo,
                                              .postToWeibo, .postToTencentWeibo]
        
        //            DispatchQueue.main.async {
        self.present(activityView, animated: true)
        //                {
        //                    self.hideLoader()
        //                }
        //            }
        //        }
    }

}
