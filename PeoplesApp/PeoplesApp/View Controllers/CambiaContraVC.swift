//
//  CambiaContraVC.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 12/09/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit

class CambiaContraVC: UIViewController {
    
    var txtContraActual : UITextField!
    var txtContra : UITextField!
    var txtContraConf : UITextField!
    
    var btnModificar : UIView!
    
    var scrollView : UIScrollView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        self.view.backgroundColor = UIColor.white
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        self.buildInterface()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "Modificar contraseña"
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationItem.title = ""
    }
    
    @objc func keyboardWillShow(sender: NSNotification) {
        var userInfo = sender.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height
        self.scrollView.contentInset = contentInset
        
        var visibleRect = self.view.frame
        visibleRect.size.height -= keyboardFrame.size.height
        
        var elOrigin = CGPoint.zero
        
        var scroll = false
        
        if self.txtContraActual.isFirstResponder {
            visibleRect.size.height -= ((self.txtContraActual.inputAccessoryView?.frame.size.height)! * 2)
            elOrigin = self.txtContraActual.frame.origin
            scroll = true
        }
        
        if self.txtContra.isFirstResponder {
            visibleRect.size.height -= ((self.txtContra.inputAccessoryView?.frame.size.height)! * 2)
            elOrigin = self.txtContra.frame.origin
            scroll = true
        }
        
        if self.txtContraConf.isFirstResponder {
            visibleRect.size.height -= ((self.txtContraConf.inputAccessoryView?.frame.size.height)! * 2)
            elOrigin = self.txtContraConf.frame.origin
            scroll = true
        }
        
        if ((scroll) && !(visibleRect.contains(elOrigin))){
            let scrollPoint = CGPoint(x: 0, y: elOrigin.y - visibleRect.size.height)
            self.scrollView.setContentOffset(scrollPoint, animated: true)
        }
    }
    
    @objc func keyboardWillHide(sender: NSNotification) {
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        self.scrollView.contentInset = contentInset
    }
    
    func buildInterface(){
        
        self.scrollView = UIScrollView(frame: self.view.frame)
        self.scrollView.backgroundColor = UIColor.white
        var xframe = CGRect.zero
        let separacionCampos : CGFloat = 10
        let cornerCampos : CGFloat = 3
        
        //contra actual
        xframe.size = CGSize(width: Constantes.tamaños.anchoTextfield, height: Constantes.tamaños.altoTextfield)
        xframe.origin = CGPoint(x: 10, y: 10)
        self.txtContraActual = UITextField(frame: xframe)
        self.txtContraActual.placeholder = "Contraseña actual"
        self.txtContraActual.layer.borderWidth = Constantes.tamaños.sizeBordeTextfield
        self.txtContraActual.layer.borderColor = Constantes.paletaColores.bordeTextfield.cgColor
        self.txtContraActual.layer.cornerRadius = cornerCampos
        self.txtContraActual.center.x = Constantes.tamaños.centroHorizontal
        self.txtContraActual.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0)
        self.scrollView.addSubview(self.txtContraActual)
        self.txtContraActual.font = Constantes.currentFonts.NunitoRegular.base
        
        //nueva
        xframe.origin = CGPoint(x: 10, y: self.txtContraActual.frame.maxY + separacionCampos)
        self.txtContra = UITextField(frame: xframe)
        self.txtContra.placeholder = "Nueva contraseña"
        self.txtContra.layer.borderWidth = Constantes.tamaños.sizeBordeTextfield
        self.txtContra.layer.borderColor = Constantes.paletaColores.bordeTextfield.cgColor
        self.txtContra.layer.cornerRadius = cornerCampos
        self.txtContra.center.x = Constantes.tamaños.centroHorizontal
        self.txtContra.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0)
        self.scrollView.addSubview(self.txtContra)
        self.txtContra.font = Constantes.currentFonts.NunitoRegular.base
        
        //repetir
        xframe.origin = CGPoint(x: 10, y: self.txtContra.frame.maxY + separacionCampos)
        self.txtContraConf = UITextField(frame: xframe)
        self.txtContraConf.placeholder = "Repetir nueva constraseña"
        self.txtContraConf.layer.borderWidth = Constantes.tamaños.sizeBordeTextfield
        self.txtContraConf.layer.borderColor = Constantes.paletaColores.bordeTextfield.cgColor
        self.txtContraConf.layer.cornerRadius = cornerCampos
        self.txtContraConf.center.x = Constantes.tamaños.centroHorizontal
        self.txtContraConf.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0)
        self.scrollView.addSubview(self.txtContraConf)
        self.txtContraConf.font = Constantes.currentFonts.NunitoRegular.base
        
        //editar
        let xpadding =  (Constantes.tamaños.altoBotonTop) + (self.navigationController?.navigationBar.frame.size.height)! + Constantes.tamaños.altoStatusBar
        xframe.origin = CGPoint(x: 0, y: Constantes.tamaños.altoPantalla - xpadding)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: Constantes.tamaños.altoBotonTop)
        self.btnModificar = UIView(frame: xframe)
        self.btnModificar.backgroundColor = Constantes.paletaColores.verdePeoples
        
        let lblBtnModificar = UILabel(frame: xframe)
        lblBtnModificar.text = "MODIFICAR CONTRASEÑA"
        lblBtnModificar.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_1
        lblBtnModificar.textColor = UIColor.white
        self.btnModificar.addSubview(lblBtnModificar)
        lblBtnModificar.sizeToFit()
        lblBtnModificar.center.x = Constantes.tamaños.anchoPantalla / 2
        lblBtnModificar.center.y = self.btnModificar.frame.size.height / 2
        //let tapMetodoPago = UITapGestureRecognizer(target: self, action: #selector(self.gotoSolicitarServicio(tap:)))
        //self.btnSolicitarServicio.addGestureRecognizer(tapSolicitar)
        //self.btnSolicitarServicio.isUserInteractionEnabled = true
        self.view.addSubview(self.scrollView)
        
        self.view.addSubview(self.btnModificar)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
