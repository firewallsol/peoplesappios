//
//  ChatVC.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 05/09/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import OneSignal
import CocoaLumberjack
import JSQMessagesViewController

struct Conversation {
    let firstName: String?
    let lastName: String?
    let preferredName: String?
    let smsNumber: String
    let id: String?
    let latestMessage: String?
    let isRead: Bool
}

class ChatVC: JSQMessagesViewController {
    
    var messages = [JSQMessage]()
    let defaults = UserDefaults.standard
    var conversation: Conversation?
    var incomingBubble: JSQMessagesBubbleImage!
    var outgoingBubble: JSQMessagesBubbleImage!
    
    var lastTimeStamp = "0"
    var refreshTimer = Timer()
    var isTimerActive = false
    var currentPropuesta: Propuesta!
    fileprivate var displayName: String!
    var idChat = "0"
    var isLoadingServer = false
    
    //MARK: - View
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.senderId = self.getsenderId()
        self.senderDisplayName = self.getsenderDisplayName()
        
        // Bubbles with tails
        incomingBubble = JSQMessagesBubbleImageFactory().incomingMessagesBubbleImage(with: Constantes.paletaColores.verdePeoples)
        outgoingBubble = JSQMessagesBubbleImageFactory().outgoingMessagesBubbleImage(with: UIColor.lightGray)
        
        //avatars
        collectionView?.collectionViewLayout.incomingAvatarViewSize = CGSize(width: kJSQMessagesCollectionViewAvatarSizeDefault, height:kJSQMessagesCollectionViewAvatarSizeDefault )
        collectionView?.collectionViewLayout.outgoingAvatarViewSize = CGSize(width: kJSQMessagesCollectionViewAvatarSizeDefault, height:kJSQMessagesCollectionViewAvatarSizeDefault )
        
        //hide accesory button
        self.inputToolbar.contentView.leftBarButtonItem = nil
        
        // This is a beta feature that mostly works but to make things more stable it is diabled.
        collectionView?.collectionViewLayout.springinessEnabled = false
        
        automaticallyScrollsToMostRecentMessage = false 
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        listenerMessages()
        self.refreshTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.listenerMessages), userInfo: nil, repeats: true)
        
        self.navigationItem.title = self.currentPropuesta.datosProfesional.razonSocial!
        
        //Disable notifications
        OneSignal.inFocusDisplayType = .none
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.refreshTimer.invalidate()
        
        //Enable notifications.
        OneSignal.inFocusDisplayType = .notification
    }
    
    @objc func listenerMessages() {
        if !isLoadingServer {
            isLoadingServer = true
            repeatReceiveMessages()
        }
    }
    
    func repeatReceiveMessages() {
        DDLogVerbose("Ingresando...")
        
        DispatchQueue.global(qos: .userInteractive).async {
            Functions().getMensajesChat(idServicio: self.currentPropuesta.idServicio,
                                        fecha: self.lastTimeStamp,
                                        idProfesional: self.currentPropuesta.datosProfesional.id){
                                            listaMensajes, exito, descError, error in
                                            //Return from server.
                                            self.isLoadingServer = false
                                            
                                            DDLogVerbose("lastTimeStamp: \(self.lastTimeStamp)")
                                            
                                            if self.isLoaderAnimating() {
                                                self.hideLoader()
                                            }
                                            
                                            if let xerror = error {
                                                DDLogError(xerror.localizedDescription)
                                                self.mensajeSimple(mensaje: "Error de recepción de mensajes")
                                            }
                                            else {
                                                if exito {
                                                    //Si contiene mensajes, regresa la conversación al mensaje más reciente.
                                                    if listaMensajes.count > 0 {
                                                        DDLogVerbose("Messages append: \(listaMensajes)")
                                                        
                                                        self.messages.append(contentsOf: listaMensajes)
                                                        self.setTimeStamp()
                                                        
                                                        DispatchQueue.main.async {
                                                            self.collectionView?.reloadData()
                                                            self.collectionView?.layoutIfNeeded()
                                                            self.scrollToBottom(animated: true)
                                                            self.finishReceivingMessage(animated: true)
                                                        }
                                                    }
                                                }
//                                                else {
                                                    //self.mensajeSimple(mensaje: descError)
//                                                }
                                            }
                                            
            }
        }
    }
    
    func setTimeStamp(){
        if let value = self.messages.last?.date {
            self.lastTimeStamp = Functions().dateToString(formato: "dd-MM-yyyy HH:mm:ss.SSS", laFecha: value)
        }
    }
    
    override func didPressSend(_ button: UIButton, withMessageText text: String, senderId: String, senderDisplayName: String, date: Date) {
        
        if self.inputToolbar.contentView.textView.hasText() {
            Functions().sendMensajeChat(idProfesional: self.currentPropuesta.datosProfesional.id,
                                        idServicio: self.currentPropuesta.idServicio, mensaje: text){ exito, descError, error in
                                            
                                            if let xerror = error {
                                                DDLogError(xerror.localizedDescription)
                                                self.mensajeSimple(mensaje: "Ocurrio un error al enviar")
                                            } else {
                                                if exito {
                                                    let deviceId = self.currentPropuesta.datosProfesional.deviceId
                                                    print("OBTIENE DEVICEID PROFESIONAL: \(String(describing: deviceId)) ")
                                                    
                                                    PushNotifications.enviaNotificacion(userId: deviceId,
                                                                                        subtitulo: "Chat",
                                                                                        mensaje: text,
                                                                                        appId: Constantes.OneSignalAppId.pplesProfesional) //PeoplesAppPro ID "8cd3fb7e-217f-49c0-90f0-ddf3a5f4ce51"
                                                    
                                                    self.finishSendingMessage(animated: true)
                                                }
                                                else {
                                                    self.mensajeSimple(mensaje: descError)
                                                }
                                            }
                                            
            }
        }
    }
    
    //MARK: - JSQMessages CollectionView DataSource
    func getsenderId() -> String {
        return Functions().getUserId()
    }
    
    func getsenderDisplayName() -> String {
        return Functions().getUserName()
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, messageDataForItemAt indexPath: IndexPath) -> JSQMessageData {
        return messages[indexPath.item]
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, messageBubbleImageDataForItemAt indexPath: IndexPath) -> JSQMessageBubbleImageDataSource {
        
        return messages[indexPath.item].senderId == self.senderId ? outgoingBubble : incomingBubble
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, avatarImageDataForItemAt indexPath: IndexPath) -> JSQMessageAvatarImageDataSource? {
        let ximage = JSQMessagesAvatarImage(avatarImage: UIImage(),
                                            highlightedImage: UIImage(),
                                            placeholderImage: UIImage())
        
        return ximage
        
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, attributedTextForCellTopLabelAt indexPath: IndexPath) -> NSAttributedString? {
        if (indexPath.item % 3 == 0) {
            let message = self.messages[indexPath.item]
            
            return JSQMessagesTimestampFormatter.shared().attributedTimestamp(for: message.date)
        }
        
        return nil
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath) -> NSAttributedString? {
        let message = messages[indexPath.item]
        
        return NSAttributedString(string: message.senderDisplayName)
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout, heightForCellTopLabelAt indexPath: IndexPath) -> CGFloat {
        if indexPath.item % 3 == 0 {
            return kJSQMessagesCollectionViewCellLabelHeightDefault
        }
        
        return 0.0
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout, heightForMessageBubbleTopLabelAt indexPath: IndexPath) -> CGFloat {
        let currentMessage = self.messages[indexPath.item]
        
        if currentMessage.senderId == self.senderId {
            return 0.0
        }
        
        if indexPath.item - 1 > 0 {
            let previousMessage = self.messages[indexPath.item - 1]
            if previousMessage.senderId == currentMessage.senderId {
                return 0.0
            }
        }
        
        return kJSQMessagesCollectionViewCellLabelHeightDefault;
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
