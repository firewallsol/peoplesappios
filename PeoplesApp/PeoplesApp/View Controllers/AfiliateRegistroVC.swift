//
//  AfiliateRegistroVC.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 14/09/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import BEMCheckBox

class AfiliateRegistroVC: UIViewController, UITextViewDelegate {
    
    //MARK: - Components
    var scrollView : UIScrollView!
    var txtNombre : UITextField!
    var txtApellidos : UITextField!
    var txtRFC : UITextField!
    var txtCelular : UITextField!
    var txtEmail : UITextField!
    var txtContra : UITextField!
    var txtDirFacturacion : UITextView!
    var txtCodigoAfiliador: UITextField!
    var chkTerminos : BEMCheckBox!
    var btnContinuar : UIView!
    
    //MARK: - View
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        self.automaticallyAdjustsScrollViewInsets = false
        self.view.backgroundColor = UIColor.white
        
        self.buildInterface()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationItem.title = "Afiliate - Paso 1"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationItem.title = ""
    }
    
    //MARK: - Keyboard
    @objc func keyboardWillShow(sender: NSNotification) {
        var userInfo = sender.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height
        self.scrollView.contentInset = contentInset
        
        var visibleRect = self.view.frame
        visibleRect.size.height -= keyboardFrame.size.height
        
        var elOrigin = CGPoint.zero
        
        var scroll = false
        
        if self.txtCelular.isFirstResponder {
            visibleRect.size.height -= ((self.txtCelular.inputAccessoryView?.frame.size.height)! * 2)
            elOrigin = self.txtCelular.frame.origin
            scroll = true
        }
        
        /*
         if self.txtEmail.isFirstResponder {
         visibleRect.size.height -= ((self.txtEmail.inputAccessoryView?.frame.size.height)! * 2)
         elOrigin = self.txtEmail.frame.origin
         scroll = true
         }
         
         if self.txtContra.isFirstResponder {
         visibleRect.size.height -= ((self.txtContra.inputAccessoryView?.frame.size.height)! * 2)
         elOrigin = self.txtContra.frame.origin
         scroll = true
         }
         */
        
        if self.txtDirFacturacion.isFirstResponder {
            visibleRect.size.height -= ((self.txtDirFacturacion.inputAccessoryView?.frame.size.height)! * 2)
            elOrigin = self.txtDirFacturacion.frame.origin
            scroll = true
        }
        
        if ((scroll) && !(visibleRect.contains(elOrigin))){
            let scrollPoint = CGPoint(x: 0, y: elOrigin.y - visibleRect.size.height)
            self.scrollView.setContentOffset(scrollPoint, animated: true)
        }
    }
    
    @objc func keyboardWillHide(sender: NSNotification) {
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        self.scrollView.contentInset = contentInset
    }
    
    //MARK: - Events
    func buildInterface(){
        self.scrollView = UIScrollView(frame: self.view.frame)
        self.scrollView.backgroundColor = UIColor.white
        var xframe = CGRect.zero
        let separacionCampos : CGFloat = 10
        let cornerCampos : CGFloat = 3
        
        //nombre
        xframe.size = CGSize(width: Constantes.tamaños.anchoTextfield, height: Constantes.tamaños.altoTextfield)
        xframe.origin = CGPoint(x: 10, y: 10)
        self.txtNombre = UITextField(frame: xframe)
        self.txtNombre.placeholder = FWS.firstName
        self.txtNombre.layer.borderWidth = Constantes.tamaños.sizeBordeTextfield
        self.txtNombre.layer.borderColor = Constantes.paletaColores.bordeTextfield.cgColor
        self.txtNombre.layer.cornerRadius = cornerCampos
        self.txtNombre.center.x = Constantes.tamaños.centroHorizontal
        self.txtNombre.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0)
        self.addToolBar(textField: self.txtNombre)
        self.scrollView.addSubview(self.txtNombre)
        self.txtNombre.font = Constantes.currentFonts.NunitoRegular.base
        
        //apellidos
        xframe.origin = CGPoint(x: 10, y: self.txtNombre.frame.maxY + separacionCampos)
        self.txtApellidos = UITextField(frame: xframe)
        self.txtApellidos.placeholder = FWS.lastName
        self.txtApellidos.layer.borderWidth = Constantes.tamaños.sizeBordeTextfield
        self.txtApellidos.layer.borderColor = Constantes.paletaColores.bordeTextfield.cgColor
        self.txtApellidos.layer.cornerRadius = cornerCampos
        self.txtApellidos.center.x = Constantes.tamaños.centroHorizontal
        self.txtApellidos.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0)
        self.addToolBar(textField: self.txtApellidos)
        self.scrollView.addSubview(self.txtApellidos)
        self.txtApellidos.font = Constantes.currentFonts.NunitoRegular.base
        
        //rfc
        xframe.origin = CGPoint(x: 10, y: self.txtApellidos.frame.maxY + separacionCampos)
        self.txtRFC = UITextField(frame: xframe)
        self.txtRFC.placeholder = FWS.lblJoinRFC
        self.txtRFC.layer.borderWidth = Constantes.tamaños.sizeBordeTextfield
        self.txtRFC.layer.borderColor = Constantes.paletaColores.bordeTextfield.cgColor
        self.txtRFC.layer.cornerRadius = cornerCampos
        self.txtRFC.center.x = Constantes.tamaños.centroHorizontal
        self.txtRFC.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0)
        self.txtRFC.autocapitalizationType = .allCharacters
        self.addToolBar(textField: self.txtRFC)
        self.scrollView.addSubview(self.txtRFC)
        self.txtRFC.font = Constantes.currentFonts.NunitoRegular.base
        
        //celular
        xframe.origin = CGPoint(x: 10, y: self.txtRFC.frame.maxY + separacionCampos)
        self.txtCelular = UITextField(frame: xframe)
        self.txtCelular.placeholder = FWS.cellphone
        self.txtCelular.layer.borderWidth = Constantes.tamaños.sizeBordeTextfield
        self.txtCelular.layer.borderColor = Constantes.paletaColores.bordeTextfield.cgColor
        self.txtCelular.layer.cornerRadius = cornerCampos
        self.txtCelular.center.x = Constantes.tamaños.centroHorizontal
        self.txtCelular.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0)
        self.txtCelular.keyboardType = .phonePad
        self.addToolBar(textField: self.txtCelular)
        self.scrollView.addSubview(self.txtCelular)
        self.txtCelular.font = Constantes.currentFonts.NunitoRegular.base
        
        //Codigo afiliador
        xframe.origin = CGPoint(x: 10, y: self.txtCelular.frame.maxY + separacionCampos)
        txtCodigoAfiliador = UITextField(frame: xframe)
        txtCodigoAfiliador.layer.borderWidth = Constantes.tamaños.sizeBordeTextfield
        txtCodigoAfiliador.layer.borderColor = Constantes.paletaColores.bordeTextfield.cgColor
        txtCodigoAfiliador.layer.cornerRadius = cornerCampos
        txtCodigoAfiliador.center.x = Constantes.tamaños.centroHorizontal
        txtCodigoAfiliador.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0)
        txtCodigoAfiliador.placeholder = "Código de afiliador"
        txtCodigoAfiliador.font = Constantes.currentFonts.NunitoRegular.base
        self.addToolBar(textField: self.txtCodigoAfiliador)
        self.scrollView.addSubview(txtCodigoAfiliador)
        /*
         //email --- fuera temporalmente
         xframe.origin = CGPoint(x: 10, y: self.txtCelular.frame.maxY + separacionCampos)
         self.txtEmail = UITextField(frame: xframe)
         self.txtEmail.placeholder = "E-mail"
         self.txtEmail.layer.borderWidth = Constantes.tamaños.sizeBordeTextfield
         self.txtEmail.layer.borderColor = Constantes.paletaColores.bordeTextfield.cgColor
         self.txtEmail.layer.cornerRadius = cornerCampos
         self.txtEmail.center.x = Constantes.tamaños.centroHorizontal
         self.txtEmail.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0)
         self.addToolBar(textField: self.txtEmail)
         self.scrollView.addSubview(self.txtEmail)
         self.txtEmail.isHidden = true
         
         //contra --- fuera temporalmente
         xframe.origin = CGPoint(x: 10, y: self.txtEmail.frame.maxY + separacionCampos)
         self.txtContra = UITextField(frame: xframe)
         self.txtContra.placeholder = "Constraseña"
         self.txtContra.layer.borderWidth = Constantes.tamaños.sizeBordeTextfield
         self.txtContra.layer.borderColor = Constantes.paletaColores.bordeTextfield.cgColor
         self.txtContra.layer.cornerRadius = cornerCampos
         self.txtContra.isSecureTextEntry = true
         self.txtContra.center.x = Constantes.tamaños.centroHorizontal
         self.txtContra.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0)
         self.addToolBar(textField: self.txtContra)
         self.scrollView.addSubview(self.txtContra)
         self.txtContra.isHidden = true
         
         */
        
        //direccion
        xframe.origin = CGPoint(x: 10, y: self.txtCodigoAfiliador.frame.maxY + separacionCampos)
        xframe.size = CGSize(width: Constantes.tamaños.anchoTextfield, height: Constantes.tamaños.altoTextfield * 2)
        self.txtDirFacturacion = UITextView(frame: xframe)
        self.txtDirFacturacion.layer.borderWidth = Constantes.tamaños.sizeBordeTextfield
        self.txtDirFacturacion.layer.borderColor = Constantes.paletaColores.bordeTextfield.cgColor
        self.txtDirFacturacion.layer.cornerRadius = cornerCampos
        self.txtDirFacturacion.isSecureTextEntry = true
        self.txtDirFacturacion.center.x = Constantes.tamaños.centroHorizontal
        self.txtDirFacturacion.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0)
        self.txtDirFacturacion.text = "Dirección de facturación"
        self.txtDirFacturacion.textColor = UIColor.lightGray
        self.txtDirFacturacion.delegate = self
        self.txtDirFacturacion.font = Constantes.currentFonts.NunitoRegular.base
        self.addToolBar(textView: self.txtDirFacturacion)
        self.scrollView.addSubview(self.txtDirFacturacion)
        
        //check terminos
        let anchoAltoCheck = Constantes.tamaños.anchoPantalla * 0.06
        xframe.size = CGSize(width: anchoAltoCheck, height: anchoAltoCheck)
        xframe.origin = CGPoint(x: self.txtDirFacturacion.frame.minX, y: self.txtDirFacturacion.frame.maxY + separacionCampos)
        self.chkTerminos = BEMCheckBox(frame: xframe)
        self.chkTerminos.tintColor = Constantes.paletaColores.verdePeoples
        self.chkTerminos.onCheckColor = UIColor.white
        self.chkTerminos.onFillColor = Constantes.paletaColores.verdePeoples
        self.chkTerminos.onTintColor = Constantes.paletaColores.verdePeoples
        self.chkTerminos.onAnimationType = .fill
        self.chkTerminos.offAnimationType = .fill
        self.scrollView.addSubview(self.chkTerminos)
        
        //label terminos
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla * 0.75, height: 30)
        xframe.origin = CGPoint(x: self.chkTerminos.frame.maxX + separacionCampos, y: self.chkTerminos.frame.minY)
        let lblTerminos = UILabel(frame: xframe)
        lblTerminos.text = "Acepto términos y condiciones de uso"
        lblTerminos.adjustsFontSizeToFitWidth = true
        lblTerminos.textColor = UIColor.gray
        lblTerminos.font = Constantes.currentFonts.NunitoRegular.minus.LabelFontSize_1
        lblTerminos.center.y = self.chkTerminos.frame.minY + (self.chkTerminos.frame.size.height / 2)
        self.scrollView.addSubview(lblTerminos)
        
        self.view.addSubview(self.scrollView)
        
        //btn continuar
        let padding = (Constantes.tamaños.altoBotonTop + (self.navigationController?.navigationBar.frame.size.height)!) + Constantes.tamaños.altoStatusBar
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla, height: Constantes.tamaños.altoBotonTop)
        xframe.origin = CGPoint(x: 0, y: Constantes.tamaños.altoPantalla - padding)
        self.btnContinuar = UIView(frame: xframe)
        self.btnContinuar.backgroundColor = Constantes.paletaColores.verdePeoples
        self.view.addSubview(self.btnContinuar)
        
        let lblBtnCont = UILabel(frame: xframe)
        lblBtnCont.text = "CONTINUAR"
        lblBtnCont.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_1
        lblBtnCont.textColor = UIColor.white
        btnContinuar.addSubview(lblBtnCont)
        lblBtnCont.sizeToFit()
        lblBtnCont.center.x = Constantes.tamaños.anchoPantalla / 2
        lblBtnCont.center.y = btnContinuar.frame.size.height / 2
        let tapBtnCont = UITapGestureRecognizer(target: self, action: #selector(self.gotoAfiliateDoctos))
        self.btnContinuar.addGestureRecognizer(tapBtnCont)
        self.btnContinuar.isUserInteractionEnabled = true
        
        self.scrollView.contentSize.height = self.chkTerminos.frame.maxY + padding + (Constantes.tamaños.anchoPantalla * 0.1)
    }
    
    func isFormValid()->Bool {
        
        var mensaje = ""
        
        //nombre
        if (self.txtNombre.text?.trimmed() == ""){
            mensaje = "El campo \"Nombre\", es obligatorio"
            
        }
        //apellidos
        if (mensaje == "" && self.txtApellidos.text?.trimmed() == ""){
            mensaje = "El campo \"Apellidos\", es obligatorio"
            
        }
        
        //rfc
        if (mensaje == "" && self.txtRFC.text?.trimmed() == ""){
            mensaje = "El campo \"RFC\", es obligatorio"
            
        }
        
        //direccion
        if (mensaje == "" && self.txtDirFacturacion.text?.trimmed() == ""){
            mensaje = "El campo \"Direccion\", es obligatorio"
            
        }
        
        //codigo afiliador
//        if (mensaje == "" && self.txtCodigoAfiliador.text?.trimmed() == ""){
//            mensaje = "El campo \"Código de afiliador\", es obligatorio"
//        }
        
        //terminos y condiciones
        if (mensaje == "" && !self.chkTerminos.on){
            mensaje = "Debe aceptar los términos y condiciones"
        }
        
        if mensaje != "" {
            
            self.mensajeSimple(mensaje: mensaje)
            
            return false
        }
        return true
    }
    
    @objc func gotoAfiliateDoctos(){
        if !self.isFormValid() {
            return
        }
        
        let elVC = AfiliateDocumentosVC()
        let codigoAfiliador = txtCodigoAfiliador.text?.trimmed()
        
        elVC.datosUsuario = self.getDatosUsuario()
        elVC.codigoAfiliador = codigoAfiliador
        self.navigationController?.pushViewController(elVC, animated: true)
        
//        let codigoAfiliador = txtCodigoAfiliador.text?.trimmed()
//        validaCodigoAfiliador(codigoAfiliador: codigoAfiliador!) { (success) in
//            if !success {
//                return
//            }
//            else {
//                let elVC = AfiliateDocumentosVC()
//                elVC.datosUsuario = self.getDatosUsuario()
//                elVC.codigoAfiliador = codigoAfiliador
//                self.navigationController?.pushViewController(elVC, animated: true)
//            }
//        }
    }
    
    func getDatosUsuario()-> Usuario {
        let unUsuario = Usuario()
        
        unUsuario.nombre = self.txtNombre.text?.trimmed()
        unUsuario.apellidos = self.txtApellidos.text?.trimmed()
        unUsuario.tinRfc = self.txtRFC.text?.trimmed()
        unUsuario.telefono = self.txtCelular.text?.trimmed()
        
        let unaDireccion = Direccion()
        unaDireccion.descripcion = self.txtDirFacturacion.text.trimmed()
        unUsuario.direcciones = [Direccion]()
        unUsuario.direcciones.append(unaDireccion)
        
        return unUsuario
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Dirección de facturación"
            textView.textColor = UIColor.lightGray
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func validaCodigoAfiliador(codigoAfiliador: String, completion:@escaping(Bool)->()) {
        Functions().validaCodigoAfiliador(codigoAfiliador: codigoAfiliador) { (success, mensaje, error) in
            if error != nil {
                self.mensajeSimple(mensaje: "Ocurrió un error al validar el código de afiliador")
                return
            }
            
            if success {
                completion(success)
            } else {
                self.mensajeSimple(mensaje: mensaje)
                completion(success)
            }
        }
    }
    
    
    
    
}
