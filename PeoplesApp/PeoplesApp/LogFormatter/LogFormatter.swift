//
//  LogFormatter.swift
//  Ejemplo-Lumberjack
//
//  Created by Jesus G. on 15/11/17.
//  Copyright © 2017 Jesus G. All rights reserved.
//

import UIKit
import CocoaLumberjack

class LogFormatter: NSObject, DDLogFormatter {
    var loggerCount: Int
    let threadUnsafeDateFormatter: DateFormatter!

    override init() {
        loggerCount = 0
        threadUnsafeDateFormatter = DateFormatter()
        threadUnsafeDateFormatter.dateFormat = "yyyy/MM/dd HH:mm:ss:SSS"
    }
    
    class func configureLogger() {
        DDLog.add(DDTTYLogger.sharedInstance) //XCode Console.
        DDLog.add(DDASLLogger.sharedInstance) //Apple System Logs
        DDTTYLogger.sharedInstance.logFormatter = LogFormatter()
        DDASLLogger.sharedInstance.logFormatter = LogFormatter()
        
        //Crear archivo para almacenar Logs.
        let fileLogger:DDFileLogger = DDFileLogger()
        fileLogger.rollingFrequency = TimeInterval(60 * 60 * 24) //24 horas.
        fileLogger.logFileManager.maximumNumberOfLogFiles = 7 //Numero máximo de archivos a crear.
        DDLog.add(fileLogger)
        
        //Especifica nivel de Logs.
        defaultDebugLevel = .verbose
    }
    
    func format(message logMessage: DDLogMessage) -> String? {
        var logLevel = String()
        
        switch logMessage.flag {
        case DDLogFlag.error : logLevel = "Erro"
        case DDLogFlag.warning : logLevel = "Warn"
        case DDLogFlag.info : logLevel = "Info"
        case DDLogFlag.debug :  logLevel = "Debu"
        case DDLogFlag.verbose : logLevel = "Verb"
        default: logLevel = "Verb"
        }
        
//        let queue = logMessage.queueLabel
        let dateAndTime = threadUnsafeDateFormatter.string(from: logMessage.timestamp)
        let method = logMessage.function!
        let logMsg = logMessage.message
        
//        return "[\(logLevel)][\(queue)] \(dateAndTime) | \(method)) | \(logMsg)"
        return "[\(logLevel)] \(dateAndTime) | \(method)) | \(logMsg)"
    }
    
    func didAdd(to logger: DDLogger, in queue: DispatchQueue) {
        loggerCount += 1
    }
    
    func willRemove(from logger: DDLogger) {
        loggerCount -= 1
    }
    
}
