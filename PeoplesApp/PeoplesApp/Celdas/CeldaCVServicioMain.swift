//
//  CeldaCVServicioMain.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 04/07/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit

class CeldaCVServicioMain: UICollectionViewCell {
    var imgServicio : UIImageView = UIImageView()
    var lblServicio : UILabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        var xframe = CGRect.zero
        xframe.origin = CGPoint(x: 10, y: 10)
        xframe.size = CGSize(width: frame.size.width / 2 , height: frame.size.width / 2)
        self.imgServicio.frame = xframe
        
        self.contentView.addSubview(self.imgServicio)
        self.lblServicio.numberOfLines = 2
        self.lblServicio.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_3
        self.lblServicio.textAlignment = .center
        self.lblServicio.lineBreakMode = .byTruncatingTail
        self.contentView.addSubview(self.lblServicio)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
