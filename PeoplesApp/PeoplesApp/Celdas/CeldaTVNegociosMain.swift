//
//  CeldaTVNegociosMain.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 30/06/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit

class CeldaTVNegociosMain: UITableViewCell {
    
    var imgNegocio : UIImageView!
    var lblNombreNegocio : UILabel!
    var lblDirecNegocio : UILabel!
    var lblCashBack : UILabel!
    var lblDistancia : UILabel!
    var imgLike : UIImageView!
    var imgShare : UIImageView!
    
    var anchoCelda : CGFloat = 0
    var altoCelda : CGFloat = 0
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.frame.size.height = Constantes.tamaños.altoCeldaNegociosMain
        self.contentView.frame.size.height = Constantes.tamaños.altoCeldaNegociosMain
        self.anchoCelda = Constantes.tamaños.anchoPantalla
        self.altoCelda = Constantes.tamaños.altoCeldaNegociosMain
        self.selectionStyle = .none
        self.accessoryType = .none
        self.buildInterface()
        
        
    }
    
    func buildInterface(){
        var xframe = CGRect.zero
        
        //imagen
        xframe.origin = CGPoint(x: 10, y: 10)
        let altoImg = self.altoCelda * 0.8
        xframe.size = CGSize(width: altoImg * 1.3, height: altoImg)
        self.imgNegocio = UIImageView(frame: xframe)
        self.contentView.addSubview(self.imgNegocio)
        self.imgNegocio.center.y = self.altoCelda / 2
        self.imgNegocio.layer.cornerRadius = 3
        self.imgNegocio.clipsToBounds = true
        
        //label nombre
        let anchoLblNombre = self.anchoCelda * 0.6
        xframe.origin = CGPoint(x: self.imgNegocio.frame.maxX + 15, y: self.imgNegocio.frame.minY + (altoImg * 0.09))
        xframe.size = CGSize(width: anchoLblNombre, height: 20)
        self.lblNombreNegocio = UILabel(frame: xframe)
        self.lblNombreNegocio.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_2
        self.lblNombreNegocio.adjustsFontSizeToFitWidth = true
        self.contentView.addSubview(self.lblNombreNegocio)
        
        //direccion
        xframe.origin = CGPoint(x: self.lblNombreNegocio.frame.minX, y: self.lblNombreNegocio.frame.maxY + 5)
        xframe.size.width = anchoLblNombre * 0.9
        self.lblDirecNegocio = UILabel(frame: xframe)
        self.lblDirecNegocio.font = Constantes.currentFonts.NunitoRegular.minus.LabelFontSize_4
        self.lblDirecNegocio.textColor = UIColor.lightGray
        self.lblDirecNegocio.numberOfLines = 2
        self.contentView.addSubview(self.lblDirecNegocio)
        
        //cash back
        let posycashback = self.altoCelda - (self.lblDirecNegocio.frame.maxY + 10)
        xframe.origin = CGPoint(x: self.lblNombreNegocio.frame.minX, y: posycashback)
        xframe.size = CGSize(width: self.anchoCelda * 0.3, height: 15)
        self.lblCashBack = UILabel(frame: xframe)
        self.lblCashBack.font = Constantes.currentFonts.NunitoRegular.minus.LabelFontSize_3
        self.lblCashBack.textColor = Constantes.paletaColores.verdeCompletado
        
        self.contentView.addSubview(self.lblCashBack)
        
        //distancia
        xframe.origin = CGPoint(x: self.anchoCelda, y: 3)
        xframe.size = CGSize(width: self.anchoCelda * 0.3, height: 30)
        self.lblDistancia = UILabel(frame: xframe)
        self.lblDistancia.font = Constantes.currentFonts.NunitoRegular.minus.LabelFontSize_4
        self.contentView.addSubview(self.lblDistancia)
        
        let anchoAltoIcShare = self.anchoCelda * 0.06
        //img share
        xframe.origin = CGPoint(x: self.anchoCelda + anchoAltoIcShare, y: self.altoCelda - (anchoAltoIcShare + 5))
        xframe.size = CGSize(width: anchoAltoIcShare, height: anchoAltoIcShare)
        self.imgShare = UIImageView(frame: xframe)
        self.imgShare.image = Asset.Icons.icShare48.image
        self.imgShare.tintColor = Constantes.paletaColores.verdePeoples
        self.imgShare.contentMode = .scaleAspectFit
        self.contentView.addSubview(self.imgShare)
        
        let anchoAltoIcLike = self.anchoCelda * 0.06
        //img like
        xframe.origin = CGPoint(x: self.imgShare.frame.minX - (anchoAltoIcLike + 10), y: self.altoCelda - (anchoAltoIcLike + 5))
        xframe.size = CGSize(width: anchoAltoIcLike, height: anchoAltoIcLike)
        self.imgLike = UIImageView(frame: xframe)
        self.imgLike.image = Asset.Icons.icFav.image
        self.imgLike.tintColor = .lightGray
        
        self.imgLike.contentMode = .scaleAspectFit
        self.contentView.addSubview(self.imgLike)
        
        //print("desde celda main anchocelda->\(self.frame.size.width)")
        //print("desde celda main altocelda->\(self.altoCelda)")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
