//
//  CeldaTVReembolsoPerfil.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 06/09/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit

class CeldaTVReembolsoPerfil: UITableViewCell {
    
    var imgIcono : UIImageView!
    var lblReembolso : UILabel!
    var lblVerHistorial : UILabel!
    
    var anchoCelda : CGFloat = 0
    var altoCelda : CGFloat = 0
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.frame.size.height = Constantes.tamaños.altoCeldaPerfilDefault
        self.contentView.frame.size.height = Constantes.tamaños.altoCeldaPerfilDefault
        self.anchoCelda = self.contentView.frame.size.width
        self.altoCelda = self.contentView.frame.size.height
        
        self.buildInterface()
        
        self.selectionStyle = .none
    }
    
    func buildInterface(){
        var xframe = CGRect.zero
        
        //imagen
        xframe.origin = CGPoint(x: 10, y: 10)
        let altoImg = Constantes.tamaños.altoCeldaPerfilDefault * 0.7
        xframe.size = CGSize(width: altoImg, height: altoImg)
        self.imgIcono = UIImageView(frame: xframe)
        self.imgIcono.center.y = self.altoCelda / 2
        self.imgIcono.contentMode = .scaleAspectFill
        self.contentView.addSubview(self.imgIcono)
        
        //reembolso
        xframe.origin = CGPoint(x: self.imgIcono.frame.maxX + 10, y: 10)
        xframe.size = CGSize(width: self.anchoCelda * 0.5, height: 30)
        self.lblReembolso = UILabel(frame: xframe)
        self.lblReembolso.textColor = Constantes.paletaColores.verdePeoples
        self.lblReembolso.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_1
        self.contentView.addSubview(self.lblReembolso)
        self.lblReembolso.text = "REEMBOLSOS"
        self.lblReembolso.sizeToFit()
        self.lblReembolso.center.y = self.altoCelda / 2
        
        self.lblVerHistorial = UILabel(frame: xframe)
        self.lblVerHistorial.text = "Ver historial"
        self.lblVerHistorial.textColor = UIColor.gray
        self.lblVerHistorial.font = Constantes.currentFonts.NunitoRegular.minus.LabelFontSize_3
        self.lblVerHistorial.sizeToFit()
        self.contentView.addSubview(self.lblVerHistorial)
        self.lblVerHistorial.center.y = self.altoCelda / 2
        self.lblVerHistorial.frame.origin.x = Constantes.tamaños.anchoPantalla - (self.lblVerHistorial.frame.size.width + 5)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
