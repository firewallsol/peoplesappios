//
//  CeldaTVPedidos.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 02/08/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit

class CeldaTVPedidos: UITableViewCell {
    
    var imgProfesional : UIImageView!
    var lblNombreServicio : UILabel!
    var lblNombreProfesional : UILabel!
    var estatusView : UIView!
    var lblEstatus : UILabel!
    var lblFecha : UILabel!
    
    var anchoCelda : CGFloat = 0
    var altoCelda : CGFloat = 0
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.frame.size.height = Constantes.tamaños.altoCeldaPedidos
        self.contentView.frame.size.height = Constantes.tamaños.altoCeldaPedidos
        self.anchoCelda = self.contentView.frame.size.width
        self.altoCelda = self.contentView.frame.size.height
        
        self.buildInterface()
        
        self.selectionStyle = .none
    }
    
    func buildInterface(){
        var xframe = CGRect.zero
        
        //imagen
        xframe.origin = CGPoint(x: 10, y: 10)
        let altoImg = Constantes.tamaños.altoCeldaPedidos * 0.8
        xframe.size = CGSize(width: altoImg, height: altoImg)
        self.imgProfesional = UIImageView(frame: xframe)
        self.imgProfesional.layer.cornerRadius = self.imgProfesional.frame.size.width / 2
        self.imgProfesional.clipsToBounds = true
        self.imgProfesional.center.y = self.altoCelda / 2
        self.contentView.addSubview(self.imgProfesional)
        
        //nombre servicio
        xframe.origin = CGPoint(x: self.imgProfesional.frame.maxX + 10, y: 10)
        xframe.size = CGSize(width: self.anchoCelda * 0.6, height: 30)
        self.lblNombreServicio = UILabel(frame: xframe)
        self.lblNombreServicio.numberOfLines = 2
        self.lblNombreServicio.textColor = Constantes.paletaColores.verdePeoples
        self.lblNombreServicio.font = Constantes.currentFonts.NunitoRegular.minus.LabelFontSize_2
        self.lblNombreServicio.lineBreakMode = .byTruncatingTail
        self.lblNombreServicio.adjustsFontSizeToFitWidth = true
        self.contentView.addSubview(self.lblNombreServicio)
        
        //nombre profesional
        xframe.origin = CGPoint(x: self.imgProfesional.frame.maxX, y: self.lblNombreServicio.frame.maxY)
        xframe.size = CGSize(width: self.anchoCelda * 0.4, height: 30)
        self.lblNombreProfesional = UILabel(frame: xframe)
        self.lblNombreProfesional.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_4
        self.lblNombreProfesional.numberOfLines = 2
        self.lblNombreProfesional.lineBreakMode = .byTruncatingTail
        self.lblNombreProfesional.adjustsFontSizeToFitWidth = true
        self.contentView.addSubview(self.lblNombreProfesional)
        
        //view estatus
        xframe.origin = CGPoint(x: self.anchoCelda, y: 10)
        xframe.size = CGSize(width: self.anchoCelda * 0.3, height: self.altoCelda * 0.4)
        self.estatusView = UIView(frame: xframe)
        self.estatusView.backgroundColor = Constantes.paletaColores.verdePeoples
        self.estatusView.layer.cornerRadius = 5
        self.contentView.addSubview(self.estatusView)
        
        //label estatus
        self.lblEstatus = UILabel(frame: xframe)
        self.lblEstatus.textColor = UIColor.white
        self.lblEstatus.font = Constantes.currentFonts.NunitoRegular.minus.LabelFontSize_3
        self.estatusView.addSubview(self.lblEstatus)
        
        
        //fecha
        xframe.origin = CGPoint(x: self.anchoCelda, y: self.estatusView.frame.maxY)
        xframe.size.width = self.anchoCelda * 0.3
        self.lblFecha = UILabel(frame: xframe)
        self.lblFecha.font = Constantes.currentFonts.NunitoRegular.minus.LabelFontSize_4
        self.lblFecha.textColor = UIColor.gray
        self.contentView.addSubview(self.lblFecha)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
