//
//  CeldaTVDetailStylePerfil.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 07/09/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit

class CeldaTVDetailStylePerfil: UITableViewCell {
    
    var lblMonto : UILabel!
    
    var anchoCelda : CGFloat = 0
    var altoCelda : CGFloat = 0
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.frame.size.height = Constantes.tamaños.altoCeldaPerfilDefault
        self.contentView.frame.size.height = Constantes.tamaños.altoCeldaPerfilDefault
        self.anchoCelda = self.contentView.frame.size.width
        self.altoCelda = self.contentView.frame.size.height
        
        self.buildInterface()
        
        self.selectionStyle = .none
    }
    
    func buildInterface(){
        var xframe = CGRect.zero
        xframe.origin = CGPoint(x: 10, y: 10)
        xframe.size = CGSize(width: self.anchoCelda * 0.3, height: 30)
        
        //monto
        self.lblMonto = UILabel(frame: xframe)
        self.contentView.addSubview(self.lblMonto)
        self.lblMonto.font = Constantes.currentFonts.NunitoBold.base
        self.lblMonto.textColor = Constantes.paletaColores.verdeCompletado
        self.lblMonto.center.y = self.altoCelda / 2
        self.lblMonto.frame.origin.x = Constantes.tamaños.anchoPantalla - (self.lblMonto.frame.size.width)
        
        self.textLabel?.font = Constantes.currentFonts.NunitoRegular.base
        self.detailTextLabel?.font = Constantes.currentFonts.NunitoRegular.minus.LabelFontSize_3
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
