//
//  CeldaTVNotificacion.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 12/09/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit

class CeldaTVNotificacion: UITableViewCell {
    
    var imgNotif : UIImageView!
    var lblTitulo : UILabel!
    var lblFecha : UILabel!
    
    var anchoCelda : CGFloat = 0
    var altoCelda : CGFloat = 0
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.frame.size.height = Constantes.tamaños.altoCeldaOpinion
        self.contentView.frame.size.height = Constantes.tamaños.altoCeldaPedidos
        self.anchoCelda = self.contentView.frame.size.width
        self.altoCelda = self.contentView.frame.size.height
        
        self.buildInterface()
        
        self.selectionStyle = .none
    }
    
    func buildInterface(){
        var xframe = CGRect.zero
        
        //imagen
        xframe.origin = CGPoint(x: 5, y: 10)
        let altoImg = Constantes.tamaños.altoCeldaPedidos * 0.8
        xframe.size = CGSize(width: altoImg, height: altoImg)
        self.imgNotif = UIImageView(frame: xframe)
        self.imgNotif.layer.cornerRadius = self.imgNotif.frame.size.width / 2
        self.imgNotif.clipsToBounds = true
        self.contentView.addSubview(self.imgNotif)
        
        //titulo
        xframe.origin = CGPoint(x: self.imgNotif.frame.maxX + 10, y: 10)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla * 0.7, height: 30)
        self.lblTitulo = UILabel(frame: xframe)
        self.lblTitulo.font = Constantes.currentFonts.NunitoRegular.minus.LabelFontSize_2
        self.lblTitulo.textColor = Constantes.paletaColores.verdePeoples
        self.lblTitulo.numberOfLines = 3
        self.lblTitulo.textAlignment = .justified
        self.contentView.addSubview(self.lblTitulo)
        
        //fecha
        xframe.origin = CGPoint(x: 10, y: self.lblTitulo.frame.maxY + 5)
        xframe.size = CGSize(width: Constantes.tamaños.anchoPantalla * 0.7, height: 30)
        self.lblFecha = UILabel(frame: xframe)
        self.lblFecha.font = Constantes.currentFonts.NunitoRegular.minus.LabelFontSize_4
        self.lblFecha.textColor = UIColor.gray
        self.contentView.addSubview(self.lblFecha)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
