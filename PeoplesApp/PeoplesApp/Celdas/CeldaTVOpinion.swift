//
//  CeldaTVOpinion.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 14/07/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import Cosmos

class CeldaTVOpinion: UITableViewCell {
    
    var imgOpinion : UIImageView!
    var lblNombrePersona : UILabel!
    var lblOpinion : UILabel!
    var starsView: CosmosView!
    
    var anchoCelda : CGFloat = 0
    var altoCelda : CGFloat = 0
    
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.frame.size.height = Constantes.tamaños.altoCeldaOpinion
        self.contentView.frame.size.height = Constantes.tamaños.altoCeldaOpinion
        self.anchoCelda = self.contentView.frame.size.width
        self.altoCelda = self.contentView.frame.size.height
        
        self.buildInterface()
        
        self.selectionStyle = .none
    }
    
    func buildInterface(){
        var xframe = CGRect.zero
        
        //imagen
        xframe.origin = CGPoint(x: 10, y: 10)
        let altoImg = Constantes.tamaños.altoCeldaOpinion * 0.6
        xframe.size = CGSize(width: altoImg, height: altoImg)
        self.imgOpinion = UIImageView(frame: xframe)
        self.imgOpinion.layer.cornerRadius = self.imgOpinion.frame.size.width / 2
        self.imgOpinion.clipsToBounds = true
        self.contentView.addSubview(self.imgOpinion)
        
        //nombre
        xframe.origin = CGPoint(x: self.imgOpinion.frame.maxX + 10, y: 10)
        xframe.size = CGSize(width: self.anchoCelda * 0.7, height: 30)
        self.lblNombrePersona = UILabel(frame: xframe)
        self.lblNombrePersona.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_1
        self.lblNombrePersona.adjustsFontSizeToFitWidth = true
        self.lblNombrePersona.lineBreakMode = .byTruncatingTail
        self.contentView.addSubview(self.lblNombrePersona)
        
        //Stars
        xframe.origin = CGPoint(x:self.lblNombrePersona.frame.minX, y: self.lblNombrePersona.frame.minY + 25)
        xframe.size = CGSize(width: self.anchoCelda * 0.7, height: 10)
        self.starsView = CosmosView(frame: xframe)
        self.starsView.isUserInteractionEnabled = false
        self.contentView.addSubview(self.starsView)

        //opinion
        xframe.origin = CGPoint(x: 10, y: self.imgOpinion.frame.maxY + 15)
        xframe.size = CGSize(width: self.anchoCelda * 0.8, height: 30)
        self.lblOpinion = UILabel(frame: xframe)
        self.lblOpinion.font = Constantes.currentFonts.NunitoRegular.minus.LabelFontSize_4
        self.lblOpinion.numberOfLines = 0
        
        self.contentView.addSubview(self.lblOpinion)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
