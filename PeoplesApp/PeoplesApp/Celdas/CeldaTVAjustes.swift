//
//  CeldaTVAjustes.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 11/09/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit

class CeldaTVAjustes: UITableViewCell {
    
    var btnSwitch : UISwitch!
    
    var anchoCelda : CGFloat = 0
    var altoCelda : CGFloat = 0
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.frame.size.height = Constantes.tamaños.altoCeldaPerfilDefault
        self.contentView.frame.size.height = Constantes.tamaños.altoCeldaPerfilDefault
        self.anchoCelda = self.contentView.frame.size.width
        self.altoCelda = self.contentView.frame.size.height
        
        self.buildInterface()
        
        self.selectionStyle = .none
    }
    
    func buildInterface(){
        var xframe = CGRect.zero
        xframe.origin = CGPoint(x: 10, y: 10)
        xframe.size = CGSize(width: self.anchoCelda * 0.3, height: self.anchoCelda * 0.2)
        
        //monto
        self.btnSwitch = UISwitch(frame: xframe)
        self.contentView.addSubview(self.btnSwitch)
        self.btnSwitch.frame.origin.x = Constantes.tamaños.anchoPantalla - ((self.anchoCelda * 0.2) + 5)
        
        self.textLabel?.font = Constantes.currentFonts.NunitoRegular.base
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
