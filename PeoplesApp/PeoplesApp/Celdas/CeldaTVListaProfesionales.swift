//
//  CeldaListaProfesionales.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 05/07/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import Cosmos

class CeldaTVListaProfesionales: UITableViewCell {
    
    var imgProfesional : UIImageView!
    var lblNombreProfesional : UILabel!
    var lblProfesion : UILabel!
    var lblDistancia : UILabel!
    
    var ratingStars : CosmosView!
    
    var anchoCelda : CGFloat = 0
    var altoCelda : CGFloat = 0
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.frame.size.height = Constantes.tamaños.altoCeldaNegociosMain
        self.contentView.frame.size.height = Constantes.tamaños.altoCeldaNegociosMain
        self.anchoCelda = self.contentView.frame.size.width
        self.altoCelda = self.contentView.frame.size.height
        
        self.buildInterface()
        
        self.selectionStyle = .none
    }
    
    func buildInterface(){
        var xframe = CGRect.zero
        
        //imagen
        xframe.origin = CGPoint(x: 5, y: 10)
        let altoImg = self.altoCelda * 0.7
        xframe.size = CGSize(width: altoImg, height: altoImg)
        self.imgProfesional = UIImageView(frame: xframe)
        self.imgProfesional.layer.cornerRadius = self.imgProfesional.frame.size.width / 2
        self.imgProfesional.clipsToBounds = true
        self.contentView.addSubview(self.imgProfesional)
        self.imgProfesional.center.y = self.altoCelda / 2
        
        
        let anchoLabel = self.anchoCelda * 0.68
        
            
        //lblprofesion
        xframe.origin = CGPoint(x: self.imgProfesional.frame.maxX + 15, y: self.altoCelda * 0.1)
        
        xframe.size = CGSize(width: anchoLabel, height: 30)
        self.lblProfesion = UILabel(frame: xframe)
        self.lblProfesion.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_4
        self.lblProfesion.textColor = Constantes.paletaColores.verdePeoples
        self.contentView.addSubview(self.lblProfesion)
        self.lblProfesion.center.y = self.altoCelda / 2
        
        self.lblProfesion.adjustsFontSizeToFitWidth = true
        
        //distancia
        xframe.origin = CGPoint(x: self.anchoCelda - 20, y: 5)
        xframe.size = CGSize(width: 50, height: 30)
        self.lblDistancia = UILabel(frame: xframe)
        self.lblDistancia.font = Constantes.currentFonts.NunitoRegular.minus.LabelFontSize_4
        self.contentView.addSubview(self.lblDistancia)
        
        //lbl nombre
        let yposNombre = self.lblProfesion.frame.minY - 35
        xframe.origin = CGPoint(x: self.imgProfesional.frame.maxX + 15, y: yposNombre)
        xframe.size = CGSize(width: anchoLabel, height: 30)
        self.lblNombreProfesional = UILabel(frame: xframe)
        self.lblNombreProfesional.font = Constantes.currentFonts.NunitoRegular.minus.LabelFontSize_1
        self.lblNombreProfesional.adjustsFontSizeToFitWidth = true
        self.contentView.addSubview(self.lblNombreProfesional)
        
        //rating
        let anchoRating = Constantes.tamaños.anchoPantalla * 0.4
        let altoRating = Constantes.tamaños.anchoPantalla * 0.15
        let ratingxpos = Constantes.tamaños.anchoPantalla - (anchoRating + 5)
        xframe.origin = CGPoint(x: ratingxpos, y: self.lblProfesion.frame.maxY + 5)
        xframe.size = CGSize(width: anchoRating, height: altoRating)
        self.ratingStars = CosmosView(frame: xframe)
        self.ratingStars.settings.updateOnTouch = false
        self.ratingStars.settings.totalStars = 5
        self.ratingStars.settings.fillMode = .precise
        self.ratingStars.settings.starSize = Double(Constantes.tamaños.anchoPantalla * 0.08)
        self.ratingStars.rating = 0
        self.contentView.addSubview(self.ratingStars)
        
        
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
