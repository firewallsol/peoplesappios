//
//  DetalleConsumoCashbackVC.swift
//  PeoplesApp
//
//  Created by Jesús García on 16/02/18.
//  Copyright © 2018 firewallsoluciones. All rights reserved.
//

import UIKit

class DetalleConsumoCashbackVC: UIViewController {
    //MARK: - Components
    var viewStatus = UIView()
    var lblStatus = UILabel()
    var lblNegocio = UILabel()
    var lblConsumo = UILabel()
    var lblFechaConsumo = UILabel()
    var lblCashbackString = UILabel()
    var viewStatusCashback = UIView()
    var lblStatusCashback = UILabel()
    var lblMontoCashback = UILabel()
    var lblFechaPagoCashback = UILabel()
    
    
    //MARK: - Vars
    var cashback = Cashback()
    
    //MARK: - Init
    init() {
        super.init(nibName: nil, bundle: nil)
        title = NSLocalizedString("Detalle cashback", comment: "")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - View
    override func viewDidLoad() {
        super.viewDidLoad()
        lblFechaPagoCashback.isHidden = true
        viewStatusCashback.isHidden = true
        
        buildInterface()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        setData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Custom
    func buildInterface() {
        let marginGuide = view.layoutMarginsGuide
        
        //View
        view.backgroundColor = UIColor.white
        
        //Negocio
        view.addSubview(lblNegocio)
        lblNegocio.translatesAutoresizingMaskIntoConstraints = false
        lblNegocio.textColor = Constantes.paletaColores.verdePeoples
        lblNegocio.font = Constantes.currentFonts.NunitoBold.base
        lblNegocio.textAlignment = .center
        
        lblNegocio.topAnchor.constraint(equalTo: marginGuide.topAnchor, constant: 10).isActive = true
        lblNegocio.leadingAnchor.constraint(equalTo: marginGuide.leadingAnchor).isActive = true
        lblNegocio.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor).isActive = true
        
        //View status
        view.addSubview(viewStatus)
        viewStatus.translatesAutoresizingMaskIntoConstraints = false
        viewStatus.backgroundColor = Constantes.paletaColores.verdeCompletado
        viewStatus.layer.cornerRadius = 5
        viewStatus.clipsToBounds = true
        viewStatus.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor).isActive = true
        viewStatus.topAnchor.constraint(equalTo: lblNegocio.bottomAnchor, constant: 20).isActive = true
        viewStatus.widthAnchor.constraint(equalToConstant: 130).isActive = true
        viewStatus.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        //Label status
        view.addSubview(lblStatus)
        lblStatus.translatesAutoresizingMaskIntoConstraints = false
        lblStatus.font = Constantes.currentFonts.NunitoRegular.base
        lblStatus.textColor = UIColor.white
        
        lblStatus.centerXAnchor.constraint(equalTo: viewStatus.centerXAnchor).isActive = true
        lblStatus.centerYAnchor.constraint(equalTo: viewStatus.centerYAnchor).isActive = true
        
        //Consumo
        view.addSubview(lblConsumo)
        lblConsumo.translatesAutoresizingMaskIntoConstraints = false
        lblConsumo.font = Constantes.currentFonts.NunitoRegular.base
        lblConsumo.numberOfLines = 0
        
        lblConsumo.topAnchor.constraint(equalTo: viewStatus.topAnchor).isActive = true
        lblConsumo.leadingAnchor.constraint(equalTo: marginGuide.leadingAnchor).isActive = true
        lblConsumo.trailingAnchor.constraint(equalTo: viewStatus.leadingAnchor, constant: -5).isActive = true
        
        //Fecha consumo
        view.addSubview(lblFechaConsumo)
        lblFechaConsumo.translatesAutoresizingMaskIntoConstraints = false
        lblFechaConsumo.font = Constantes.currentFonts.NunitoRegular.base
        lblFechaConsumo.numberOfLines = 0
        
        lblFechaConsumo.topAnchor.constraint(equalTo: lblConsumo.bottomAnchor, constant: 10).isActive = true
        lblFechaConsumo.leadingAnchor.constraint(equalTo: marginGuide.leadingAnchor).isActive = true
        lblFechaConsumo.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor).isActive = true
        
        //Cashback static
        view.addSubview(lblCashbackString)
        lblCashbackString.translatesAutoresizingMaskIntoConstraints = false
        lblCashbackString.textColor = Constantes.paletaColores.verdePeoples
        lblCashbackString.font = Constantes.currentFonts.NunitoBold.base
        lblCashbackString.text = "Cashback"
        
        lblCashbackString.topAnchor.constraint(equalTo: lblFechaConsumo.bottomAnchor, constant: 20).isActive = true
        lblCashbackString.leadingAnchor.constraint(equalTo: marginGuide.leadingAnchor).isActive = true
        lblCashbackString.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor).isActive = true
        
        //ViewStatusCashback
        view.addSubview(viewStatusCashback)
        viewStatusCashback.translatesAutoresizingMaskIntoConstraints = false
        viewStatusCashback.backgroundColor = Constantes.paletaColores.verdeCompletado
        viewStatusCashback.layer.cornerRadius = 5
        viewStatusCashback.clipsToBounds = true
        
        viewStatusCashback.topAnchor.constraint(equalTo: lblCashbackString.bottomAnchor, constant: 5).isActive = true
        viewStatusCashback.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor).isActive = true
        viewStatusCashback.widthAnchor.constraint(equalToConstant: 130).isActive = true
        viewStatusCashback.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        //LabelStatusCashback
        viewStatusCashback.addSubview(lblStatusCashback)
        lblStatusCashback.translatesAutoresizingMaskIntoConstraints = false
        lblStatusCashback.font = Constantes.currentFonts.NunitoRegular.base
        lblStatusCashback.textColor = UIColor.white
        
        lblStatusCashback.centerXAnchor.constraint(equalTo: viewStatusCashback.centerXAnchor).isActive = true
        lblStatusCashback.centerYAnchor.constraint(equalTo: viewStatusCashback.centerYAnchor).isActive = true
        
        //Monto cashback
        view.addSubview(lblMontoCashback)
        lblMontoCashback.translatesAutoresizingMaskIntoConstraints = false
        lblMontoCashback.font = Constantes.currentFonts.NunitoRegular.base
        lblMontoCashback.numberOfLines = 0
        
        lblMontoCashback.topAnchor.constraint(equalTo: viewStatusCashback.topAnchor).isActive = true
        lblMontoCashback.leadingAnchor.constraint(equalTo: marginGuide.leadingAnchor).isActive = true
        lblMontoCashback.trailingAnchor.constraint(equalTo: viewStatusCashback.leadingAnchor, constant: -5).isActive = true
        
        //Fecha de pago cashback
        view.addSubview(lblFechaPagoCashback)
        lblFechaPagoCashback.translatesAutoresizingMaskIntoConstraints = false
        lblFechaPagoCashback.font = Constantes.currentFonts.NunitoRegular.base
        lblFechaPagoCashback.numberOfLines = 0
        
        lblFechaPagoCashback.topAnchor.constraint(equalTo: lblMontoCashback.bottomAnchor, constant: 10).isActive = true
        lblFechaPagoCashback.leadingAnchor.constraint(equalTo: marginGuide.leadingAnchor).isActive = true
        lblFechaPagoCashback.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor).isActive = true
    }
    
    func setData() {
        lblStatus.text = cashback.estatus.descripcion
        lblNegocio.text = cashback.deudor.nombre
        
        //Consumo
        let consumo = NSLocalizedString(cashback.tipo.rawValue, comment: "").capitalized
        let monto = cashback.monto.formatoPrecio()
        let divisa = cashback.divisa.abreviacion!
        
        lblConsumo.text = "\(consumo)\n\(monto) \(divisa)"
        
        //Fecha consumo
        let fechaString = NSLocalizedString("Fecha de consumo", comment: "")
        let fecha = cashback.fecha.formatoFecha(usarFormato: "dd/MM/yyyy")
        lblFechaConsumo.text = "\(fechaString)\n\(fecha)"
        
        //Estatus cashback
        if let unCargo = cashback.cargo {
            if unCargo.estatus.descripcion != nil && unCargo.estatus.descripcion != "" {
                viewStatusCashback.isHidden = false
                lblStatusCashback.text = unCargo.estatus.descripcion
            }
            
            if unCargo.fecha != "" {
                let fechaPagoString = NSLocalizedString("Fecha de cargo", comment: "")
                let fechaPago = unCargo.fecha.formatoFecha(usarFormato: "dd/MM/yyyy")
                lblFechaPagoCashback.text = "\(fechaPagoString)\n\(fechaPago)"
                lblFechaPagoCashback.isHidden = false
            }
        }
        
        //Monto cashback
        let montoCashbackString = NSLocalizedString("Monto de cashback", comment: "")
        let montoCashback = cashback.cashbackCliente.formatoPrecio()
        lblMontoCashback.text = "\(montoCashbackString)\n\(montoCashback) \(divisa)"
        
//        if cashback.cargo.fecha != "" {
//            let fechaPagoString = NSLocalizedString("Fecha de cargo", comment: "")
//            let fechaPago = cashback.cargo.fecha.formatoFecha(usarFormato: "dd/MM/yyyy")
//            lblFechaPagoCashback.text = "\(fechaPagoString)\n\(fechaPago)"
//            lblFechaPagoCashback.isHidden = false
//        }
    }
    
}
