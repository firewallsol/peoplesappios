//
//  RecuperarContrasenia.swift
//  PeoplesApp
//
//  Created by Jesús García on 02/02/18.
//  Copyright © 2018 firewallsoluciones. All rights reserved.
//

import UIKit

class RecuperarContraseniaVC: UIViewController {

    //MARK: - Components
    var txtEmail : UITextField!
    var btnIngresar : UIView!

    var scrollView : UIScrollView!
    var showSideMenu = false
    
    //MARK: - View
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        self.buildInterface()
    }

    
    //MARK:  - Keyboard
    @objc func keyboardWillShow(sender: NSNotification) {
        var userInfo = sender.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height
        self.scrollView.contentInset = contentInset
        
        var visibleRect = self.view.frame
        visibleRect.size.height -= keyboardFrame.size.height
        
        var elOrigin = CGPoint.zero
        
        let scroll = false
        
        if self.txtEmail.isFirstResponder {
            visibleRect.size.height -= ((self.txtEmail.inputAccessoryView?.frame.size.height)! * 2)
            elOrigin = self.txtEmail.frame.origin
        }
        
        if ((scroll) && !(visibleRect.contains(elOrigin))){
            let scrollPoint = CGPoint(x: 0, y: elOrigin.y - visibleRect.size.height)
            self.scrollView.setContentOffset(scrollPoint, animated: true)
        }
    }
    
    @objc func keyboardWillHide(sender: NSNotification) {
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        self.scrollView.contentInset = contentInset
    }
    
    //MARK: - Events
    func buildInterface(){
        
        //Navigation bar
        let navBar: UINavigationBar = UINavigationBar(frame: CGRect(x: 0, y: 20,
                                                                    width: self.view.frame.width,
                                                                    height:40))
        
        let navItem = UINavigationItem(title: NSLocalizedString("RecoverPassword", comment: ""))
        
        let btnCancel = UIBarButtonItem(title: NSLocalizedString("ButtonCancel", comment: ""),
                                        style:.done,
                                        target: self,
                                        action: #selector(seleccionaBotonCancelar))
        
        navItem.rightBarButtonItem = btnCancel;
        navBar.setItems([navItem], animated: false);
        navBar.backgroundColor = UIColor.white
        navBar.tintColor = Constantes.paletaColores.verdePeoples
        navBar.isTranslucent = false
        self.view.addSubview(navBar);
        
        //Scroll
        self.scrollView = UIScrollView(frame:CGRect(x: navBar.frame.minX,
                                                    y: navBar.frame.maxY + 2,
                                                    width: self.view.frame.width,
                                                    height: self.view.frame.height))
        
        self.scrollView.backgroundColor = UIColor.white

        var xframe = CGRect()
        xframe.origin = CGPoint(x: 10, y: 10)
        xframe.size = CGSize(width: 10, height: 30)
        
        let lblingresarconemail = UILabel(frame: xframe)
        lblingresarconemail.font = UIFont.boldSystemFont(ofSize: lblingresarconemail.font.pointSize)
        lblingresarconemail.textColor = Constantes.paletaColores.verdePeoples
        lblingresarconemail.text = NSLocalizedString("EnterYourMail", comment: "").uppercased()
        lblingresarconemail.font = Constantes.currentFonts.NunitoRegular.base
        lblingresarconemail.sizeToFit()
        self.scrollView.addSubview(lblingresarconemail)
        
        lblingresarconemail.center.x = Constantes.tamaños.centroHorizontal
        
        //txt email
        xframe.origin = CGPoint(x: 10, y: lblingresarconemail.frame.maxY + 20)
        xframe.size = CGSize(width: Constantes.tamaños.anchoTextfield, height: Constantes.tamaños.altoTextfield)
        self.txtEmail = UITextField(frame: xframe)
        self.txtEmail.layer.borderColor = Constantes.paletaColores.bordeTextfield.cgColor
        self.txtEmail.layer.borderWidth = Constantes.tamaños.sizeBordeTextfield
        self.txtEmail.layer.cornerRadius = 3
        self.txtEmail.center.x = Constantes.tamaños.centroHorizontal
        self.txtEmail.placeholder = NSLocalizedString("Email", comment: "")
        self.txtEmail.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0)
        self.txtEmail.keyboardType = .emailAddress
        self.txtEmail.autocapitalizationType = .none
        self.txtEmail.autocorrectionType = .no
        self.txtEmail.font = Constantes.currentFonts.NunitoRegular.base
        self.addToolBar(textField: self.txtEmail)
        self.scrollView.addSubview(self.txtEmail)
        
        //btn ingresar
        xframe.origin = CGPoint(x: 10, y: self.txtEmail.frame.maxY + 20)
        xframe.size = CGSize(width: Constantes.tamaños.anchoTextfield / 1.5, height: Constantes.tamaños.altoTextfield)
        self.btnIngresar = UIView(frame: xframe)
        self.btnIngresar.backgroundColor = Constantes.paletaColores.verdePeoples
        self.btnIngresar.layer.cornerRadius = 10
        self.btnIngresar.center.x = Constantes.tamaños.centroHorizontal
        self.scrollView.addSubview(self.btnIngresar)
        //label
        let lblBtnIngresar = UILabel(frame: xframe)
        lblBtnIngresar.text = NSLocalizedString("Recover", comment: "").uppercased()
        lblBtnIngresar.textColor = UIColor.white
        lblBtnIngresar.font = Constantes.currentFonts.NunitoBold.minus.LabelFontSize_1
        lblBtnIngresar.sizeToFit()
        self.btnIngresar.addSubview(lblBtnIngresar)
        lblBtnIngresar.center.x = self.btnIngresar.frame.size.width / 2
        lblBtnIngresar.center.y = self.btnIngresar.frame.size.height / 2
        let tapBtnIngresar = UITapGestureRecognizer(target: self, action: #selector(seleccionaBotonRecuperar))
        self.btnIngresar.isUserInteractionEnabled = true
        self.btnIngresar.addGestureRecognizer(tapBtnIngresar)
        
        self.view.addSubview(self.scrollView)
    }

    @objc func seleccionaBotonCancelar() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func seleccionaBotonRecuperar() {
        if !validaForma() {
            return
        }
        
        let email = txtEmail.text?.trimmed()
        self.recuperarContrasenia(email: email!)
    }
    
    func validaForma()->Bool{
        var mensaje = ""
        
        //nombre
        if (self.txtEmail.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == ""){
            mensaje = "El campo \"Email\", es obligatorio"
        }
        
        if !String.isValidEmailAddress(emailAddressString: txtEmail.text!) {
            mensaje = "El formato de correo electrónico es incorrecto"
        }
        
        if mensaje != "" {
            self.mensajeSimple(mensaje: mensaje)
            return false
        }
        return true
    }
    
    func recuperarContrasenia(email: String) {
        Functions().recuperarContrasenia(email: email) { (success, message, error) in
            //
            if error != nil {
                self.mensajeSimple(mensaje: "Ocurrió un error al recuperar contraseña")
                return
            }
            
            if success {
                self.alertAndDismissWith(message: message,
                                         buttonTitle: NSLocalizedString("OK", comment: ""))
            }
            else {
                self.mensajeSimple(mensaje: message)
            }
        }
    }
    

    


}
