//
//  AvisoPrivacidadVC.swift
//  PeoplesApp
//
//  Created by Jesús García on 28/12/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit

class AvisoPrivacidadVC: UIViewController {
    
    static let urlString = "http://firewallsoluciones.com.mx/"

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("TermsAndConditions", comment: "")
        
        super.viewDidLoad()
        let webView = UIWebView(frame: self.view.frame)
        webView.scalesPageToFit = true
        
        let url = URL(string: AvisoPrivacidadVC.urlString)
        let request = URLRequest(url: url!)
        webView.loadRequest(request)
        
        self.view.addSubview(webView)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
