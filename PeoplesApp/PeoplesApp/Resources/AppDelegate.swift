//
//  AppDelegate.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 28/06/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import FacebookCore
import GooglePlaces
import GoogleMaps
import Fabric
import Crashlytics
import OneSignal
import CocoaLumberjack

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    enum UrlScheme: String {
        //Url scheme on info.plist
        case facebook =  "1454017611350780"
        case google =  "264760724840-2b38ol7e7bfrjqku0gicsk7issoc0tkq"
    }
    
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
        LogFormatter.configureLogger()
        return true
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        //Fabric
        Fabric.with([Crashlytics.self])
        
        //Google Services, Places
        GMSServices.provideAPIKey(Constantes.strings.GoogleMapsApiKey)
        GMSPlacesClient.provideAPIKey(Constantes.strings.GooglePlacesApiKey)
        configuraGoogleSignIn()
        
        //facebook
        SDKApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        //Configurar OneSignal
        PushNotifications.configureOneSignalWith(launchOptions: launchOptions)

        
        self.gotoMainScreen()
        
        return true
    }
    
    func gotoMainScreen(){
        // Initialize the window
        self.window = UIWindow.init(frame: UIScreen.main.bounds)
        
        let navPrincipal = PrincipalNavControllerNC()
        
        let xmainScreenVC = MainVC()
        //        Functions().saveSideMenuIndex(index: 0)
        
        navPrincipal.view.backgroundColor = UIColor.white
        navPrincipal.pushViewController(xmainScreenVC, animated: true)
        
        self.window?.rootViewController = navPrincipal
        self.window?.makeKeyAndVisible()
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        AppEventsLogger.activate(application)
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        let stringScheme = url.absoluteString
        
        if stringScheme.contains(UrlScheme.facebook.rawValue) {
            return SDKApplicationDelegate.shared.application(app, open: url, options: options)
        }
            
        else if stringScheme.contains(UrlScheme.google.rawValue) {
            return GIDSignIn.sharedInstance().handle(url,
                                                     sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String,
                                                     annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        }
        else {
            DDLogError("No se localizó el URLScheme: \(stringScheme)")
        }
        
        return false
    }
    
    //MARK: -  Custom
    func configuraGoogleSignIn() {
        DDLogInfo("Ingresando..")
        
        //Configura GoogleContext Shared Instance.
        var configureError: NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        
        if configureError != nil {
            DDLogError("Error configuring Google services: \(String(describing: configureError))")
        }
        else {
            DDLogInfo("Se configuró correctamente el SharedInstance de Google.")
        }
    }
    


    
    
}

