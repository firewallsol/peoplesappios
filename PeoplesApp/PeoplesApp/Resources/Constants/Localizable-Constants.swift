// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// swiftlint:disable explicit_type_interface identifier_name line_length nesting type_body_length type_name
internal enum FWS {
  /// I accept terms and conditions of use
  internal static let acceptTerms = FWS.tr("Localizable", "AcceptTerms")
  /// Address
  internal static let address = FWS.tr("Localizable", "Address")
  /// Select an option
  internal static let alertChooseOption = FWS.tr("Localizable", "AlertChooseOption")
  /// Enter your current password
  internal static let alertCurrentPassword = FWS.tr("Localizable", "AlertCurrentPassword")
  /// The format of email is incorrect
  internal static let alertEmailFormatIncorrect = FWS.tr("Localizable", "AlertEmailFormatIncorrect")
  /// The field '%@' can't be empty
  internal static func alertFieldEmpty(_ p1: String) -> String {
    return FWS.tr("Localizable", "AlertFieldEmpty", p1)
  }
  /// The field '%@' is required
  internal static func alertFieldRequired(_ p1: String) -> String {
    return FWS.tr("Localizable", "AlertFieldRequired", p1)
  }
  /// Enter a new password.
  internal static let alertNewPassword = FWS.tr("Localizable", "AlertNewPassword")
  /// The new password and its confirmation do not match.
  internal static let alertPasswordNotMatch = FWS.tr("Localizable", "AlertPasswordNotMatch")
  /// In order to claim cashback you must complete the information of your profile. Do you want to edit your profile now?
  internal static let alertShowEditProfileOnClaimCashback = FWS.tr("Localizable", "AlertShowEditProfileOnClaimCashback")
  /// To request a service, you must complete the information of your profile. Do you want to edit your profile now?
  internal static let alertShowEditProfileOnProfileIncomplete = FWS.tr("Localizable", "AlertShowEditProfileOnProfileIncomplete")
  /// In order to claim cashback you must login to People's App. Do you want to login now?
  internal static let alertShowLoginOnClaimCashback = FWS.tr("Localizable", "AlertShowLoginOnClaimCashback")
  /// In order to request a service you must login to People's App. Do you want to login now?
  internal static let alertShowLoginOnRequestService = FWS.tr("Localizable", "AlertShowLoginOnRequestService")
  /// In order to claim cashback you must have a registered PayPal account. Do you want to add it now?
  internal static let alertShowPaymentMethodsOnClaimCashback = FWS.tr("Localizable", "AlertShowPaymentMethodsOnClaimCashback")
  /// The snapshot of '%@' is required
  internal static func alertSnapshotRequired(_ p1: String) -> String {
    return FWS.tr("Localizable", "AlertSnapshotRequired", p1)
  }
  /// People's App
  internal static let appName = FWS.tr("Localizable", "AppName")
  /// Business
  internal static let business = FWS.tr("Localizable", "Business")
  /// Accept
  internal static let buttonAccept = FWS.tr("Localizable", "ButtonAccept")
  /// Cancel
  internal static let buttonCancel = FWS.tr("Localizable", "ButtonCancel")
  /// Chat
  internal static let buttonChat = FWS.tr("Localizable", "ButtonChat")
  /// Continue
  internal static let buttonContinue = FWS.tr("Localizable", "ButtonContinue")
  /// Edit
  internal static let buttonEdit = FWS.tr("Localizable", "ButtonEdit")
  /// OK
  internal static let buttonOK = FWS.tr("Localizable", "ButtonOK")
  /// Reject
  internal static let buttonReject = FWS.tr("Localizable", "ButtonReject")
  /// There is no access to the camera.
  internal static let cameraAccessMessage = FWS.tr("Localizable", "CameraAccessMessage")
  /// Cellphone
  internal static let cellphone = FWS.tr("Localizable", "Cellphone")
  /// Change password
  internal static let changePassword = FWS.tr("Localizable", "ChangePassword")
  /// Choose an image
  internal static let choosePicture = FWS.tr("Localizable", "ChoosePicture")
  /// Configurations
  internal static let configurations = FWS.tr("Localizable", "Configurations")
  /// Country
  internal static let country = FWS.tr("Localizable", "Country")
  /// Current password
  internal static let currentPassword = FWS.tr("Localizable", "CurrentPassword")
  /// Date of service
  internal static let dateOfService = FWS.tr("Localizable", "DateOfService")
  /// Edit profile
  internal static let editProfile = FWS.tr("Localizable", "EditProfile")
  /// Email
  internal static let email = FWS.tr("Localizable", "Email")
  /// Enter your E-mail
  internal static let enterYourMail = FWS.tr("Localizable", "EnterYourMail")
  /// An error occurred when obtaining professional evaluations.
  internal static let errorOnRequestingOpinions = FWS.tr("Localizable", "ErrorOnRequestingOpinions")
  /// First name
  internal static let firstName = FWS.tr("Localizable", "FirstName")
  /// Home
  internal static let home = FWS.tr("Localizable", "Home")
  /// Join
  internal static let join = FWS.tr("Localizable", "Join")
  /// Last name
  internal static let lastName = FWS.tr("Localizable", "LastName")
  /// Add Paypal account
  internal static let lblJoinAddPaypalAccount = FWS.tr("Localizable", "lblJoinAddPaypalAccount")
  /// Signed W-9
  internal static let lblJoinIdDoc = FWS.tr("Localizable", "lblJoinIdDoc")
  /// It is very easy to just follow the following steps:
  internal static let lblJoinItsEasy = FWS.tr("Localizable", "lblJoinItsEasy")
  /// Affiliate and for every business you affiliate and visit one of our affiliates you receive reimbursements in your account.
  internal static let lblJoinParraph1 = FWS.tr("Localizable", "lblJoinParraph1")
  /// Proof of address
  internal static let lblJoinProofOfAddres = FWS.tr("Localizable", "lblJoinProofOfAddres")
  /// SS#/EI#
  internal static let lblJoinRFC = FWS.tr("Localizable", "lblJoinRFC")
  /// Complete your basic registration
  internal static let lblJoinStep1 = FWS.tr("Localizable", "lblJoinStep1")
  /// Use the file scan to attach your documents
  internal static let lblJoinStep2 = FWS.tr("Localizable", "lblJoinStep2")
  /// Make the registration payment and start earning with People's App
  internal static let lblJoinStep3 = FWS.tr("Localizable", "lblJoinStep3")
  /// Use the file scan to add the following documents:
  internal static let lblUploadDocs = FWS.tr("Localizable", "lblUploadDocs")
  /// Login
  internal static let login = FWS.tr("Localizable", "Login")
  /// Login\nor Sign up
  internal static let loginOrSignup = FWS.tr("Localizable", "LoginOrSignup")
  /// Logout
  internal static let logout = FWS.tr("Localizable", "Logout")
  /// México
  internal static let mexico = FWS.tr("Localizable", "Mexico")
  /// In order to request a service, you must login to People's App.
  internal static let needLoginToRequestService = FWS.tr("Localizable", "NeedLoginToRequestService")
  /// New password
  internal static let newPassword = FWS.tr("Localizable", "NewPassword")
  /// There are no professionals available in this category.
  internal static let noAvailableProfessionals = FWS.tr("Localizable", "NoAvailableProfessionals")
  /// No information available.
  internal static let noInformationAvailable = FWS.tr("Localizable", "noInformationAvailable")
  /// %@ has accepted your proposal.
  internal static func notificationProposalAccepted(_ p1: String) -> String {
    return FWS.tr("Localizable", "NotificationProposalAccepted", p1)
  }
  /// %@ has rejected your proposal.
  internal static func notificationProposalRejected(_ p1: String) -> String {
    return FWS.tr("Localizable", "NotificationProposalRejected", p1)
  }
  /// Notifications
  internal static let notifications = FWS.tr("Localizable", "Notifications")
  /// Orders
  internal static let orders = FWS.tr("Localizable", "Orders")
  /// Password
  internal static let password = FWS.tr("Localizable", "Password")
  /// Price
  internal static let price = FWS.tr("Localizable", "Price")
  /// Profile
  internal static let profile = FWS.tr("Localizable", "Profile")
  /// Recover
  internal static let recover = FWS.tr("Localizable", "Recover")
  /// Recover password
  internal static let recoverPassword = FWS.tr("Localizable", "RecoverPassword")
  /// Registry
  internal static let registry = FWS.tr("Localizable", "Registry")
  /// Repeat your new password
  internal static let repeatPassword = FWS.tr("Localizable", "RepeatPassword")
  /// Request service
  internal static let requestService = FWS.tr("Localizable", "RequestService")
  /// Services
  internal static let services = FWS.tr("Localizable", "Services")
  /// Status
  internal static let status = FWS.tr("Localizable", "Status")
  /// Take a picture
  internal static let takePicture = FWS.tr("Localizable", "TakePicture")
  /// Terms and conditions
  internal static let termsAndConditions = FWS.tr("Localizable", "TermsAndConditions")
  /// Proposal
  internal static let titleProposal = FWS.tr("Localizable", "TitleProposal")
  /// United States Of America
  internal static let unitedStatesOfAmerica = FWS.tr("Localizable", "United States of America")
  /// You will get cashback
  internal static let youWillGetCashback = FWS.tr("Localizable", "YouWillGetCashback")
}
// swiftlint:enable explicit_type_interface identifier_name line_length nesting type_body_length type_name

extension FWS {
  fileprivate static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    let format = NSLocalizedString(key, tableName: table, bundle: Bundle(for: BundleToken.self), comment: "")
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

private final class BundleToken {}
