// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
  import AppKit.NSImage
  internal typealias AssetColorTypeAlias = NSColor
  internal typealias Image = NSImage
#elseif os(iOS) || os(tvOS) || os(watchOS)
  import UIKit.UIImage
  internal typealias AssetColorTypeAlias = UIColor
  internal typealias Image = UIImage
#endif

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

@available(*, deprecated, renamed: "ImageAsset")
internal typealias AssetType = ImageAsset

internal struct ImageAsset {
  internal fileprivate(set) var name: String

  internal var image: Image {
    let bundle = Bundle(for: BundleToken.self)
    #if os(iOS) || os(tvOS)
    let image = Image(named: name, in: bundle, compatibleWith: nil)
    #elseif os(OSX)
    let image = bundle.image(forResource: name)
    #elseif os(watchOS)
    let image = Image(named: name)
    #endif
    guard let result = image else { fatalError("Unable to load image named \(name).") }
    return result
  }
}

internal struct ColorAsset {
  internal fileprivate(set) var name: String

  #if swift(>=3.2)
  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
  internal var color: AssetColorTypeAlias {
    return AssetColorTypeAlias(asset: self)
  }
  #endif
}

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
internal enum Asset {
  internal static let avatarDefault = ImageAsset(name: "avatarDefault")
  internal enum CarruselDummys {
    internal static let carrousel1 = ImageAsset(name: "carrousel1")
    internal static let carrousel2 = ImageAsset(name: "carrousel2")
    internal static let carrousel3 = ImageAsset(name: "carrousel3")
    internal static let carrousel4 = ImageAsset(name: "carrousel4")
  }
  internal enum CarruselServicios {
    internal static let carruselServ1 = ImageAsset(name: "carruselServ1")
    internal static let carruselServ2 = ImageAsset(name: "carruselServ2")
    internal static let carruselServ3 = ImageAsset(name: "carruselServ3")
    internal static let carruselServ4 = ImageAsset(name: "carruselServ4")
    internal static let carruselServ5 = ImageAsset(name: "carruselServ5")
  }
  internal enum FacesDummys {
    internal static let face1 = ImageAsset(name: "face1")
    internal static let face2 = ImageAsset(name: "face2")
    internal static let face3 = ImageAsset(name: "face3")
    internal static let face4 = ImageAsset(name: "face4")
    internal static let face5 = ImageAsset(name: "face5")
  }
  internal enum Icons {
    internal static let payPal = ImageAsset(name: "PayPal")
    internal static let btncerrar64 = ImageAsset(name: "btncerrar_64")
    internal static let camera = ImageAsset(name: "camera")
    internal static let icCameraPlus64 = ImageAsset(name: "ic_cameraPlus_64")
    internal static let icCheck64 = ImageAsset(name: "ic_check_64")
    internal static let icClock48 = ImageAsset(name: "ic_clock_48")
    internal static let icDelete48 = ImageAsset(name: "ic_delete_48")
    internal static let icFacebookBtn64 = ImageAsset(name: "ic_facebookBtn_64")
    internal static let icFav = ImageAsset(name: "ic_fav")
    internal static let icGoogleplusBtn64 = ImageAsset(name: "ic_googleplusBtn_64")
    internal static let icLocation = ImageAsset(name: "ic_location")
    internal static let icMenuAlt = ImageAsset(name: "ic_menu-alt")
    internal static let icPaymentMethod64 = ImageAsset(name: "ic_paymentMethod_64")
    internal static let icQuestion64 = ImageAsset(name: "ic_question_64")
    internal static let icShare48 = ImageAsset(name: "ic_share_48")
    internal static let icStarGray48 = ImageAsset(name: "ic_star_gray_48")
    internal static let tempStar = ImageAsset(name: "temp_star")
  }
  internal static let pplsAppLogo = ImageAsset(name: "pplsAppLogo")
  internal static let pplsAppLogo120 = ImageAsset(name: "pplsAppLogo120")

  // swiftlint:disable trailing_comma
  internal static let allColors: [ColorAsset] = [
  ]
  internal static let allImages: [ImageAsset] = [
    avatarDefault,
    CarruselDummys.carrousel1,
    CarruselDummys.carrousel2,
    CarruselDummys.carrousel3,
    CarruselDummys.carrousel4,
    CarruselServicios.carruselServ1,
    CarruselServicios.carruselServ2,
    CarruselServicios.carruselServ3,
    CarruselServicios.carruselServ4,
    CarruselServicios.carruselServ5,
    FacesDummys.face1,
    FacesDummys.face2,
    FacesDummys.face3,
    FacesDummys.face4,
    FacesDummys.face5,
    Icons.payPal,
    Icons.btncerrar64,
    Icons.camera,
    Icons.icCameraPlus64,
    Icons.icCheck64,
    Icons.icClock48,
    Icons.icDelete48,
    Icons.icFacebookBtn64,
    Icons.icFav,
    Icons.icGoogleplusBtn64,
    Icons.icLocation,
    Icons.icMenuAlt,
    Icons.icPaymentMethod64,
    Icons.icQuestion64,
    Icons.icShare48,
    Icons.icStarGray48,
    Icons.tempStar,
    pplsAppLogo,
    pplsAppLogo120,
  ]
  // swiftlint:enable trailing_comma
  @available(*, deprecated, renamed: "allImages")
  internal static let allValues: [AssetType] = allImages
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

internal extension Image {
  @available(iOS 1.0, tvOS 1.0, watchOS 1.0, *)
  @available(OSX, deprecated,
    message: "This initializer is unsafe on macOS, please use the ImageAsset.image property")
  convenience init!(asset: ImageAsset) {
    #if os(iOS) || os(tvOS)
    let bundle = Bundle(for: BundleToken.self)
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(OSX) || os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

internal extension AssetColorTypeAlias {
  #if swift(>=3.2)
  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
  convenience init!(asset: ColorAsset) {
    let bundle = Bundle(for: BundleToken.self)
    #if os(iOS) || os(tvOS)
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(OSX)
    self.init(named: asset.name, bundle: bundle)
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
  #endif
}

private final class BundleToken {}
