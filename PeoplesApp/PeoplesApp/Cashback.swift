//
//  Cashback.swift
//  PeoplesApp
//
//  Created by Jesús García on 16/02/18.
//  Copyright © 2018 firewallsoluciones. All rights reserved.
//

import Foundation
import CocoaLumberjack

struct Cashback {
    var tipo: TipoCashback
    var id: String
    var usuarioId: String
    var deudor: Deudor
    var monto: String
    var fecha: String
    var cashbackCliente: String
    var estatus: EstatusCashback
    var divisa: Divisa
    var cargo: Cargo?
    
    init() {
        tipo = TipoCashback.consumo
        id = ""
        usuarioId = ""
        deudor = Deudor()
        monto = ""
        fecha = ""
        cashbackCliente = ""
        estatus = EstatusCashback()
        divisa = Divisa()
    }
    
    func tipoCashbackFromString(string:String) ->TipoCashback {
        var unTipo = TipoCashback.consumo
        
        switch string {
        case TipoCashback.consumo.rawValue:
            unTipo = TipoCashback.consumo
            break
        case TipoCashback.servicio.rawValue:
            unTipo = TipoCashback.servicio
            break
        case TipoCashback.afiliacion.rawValue:
            unTipo = TipoCashback.afiliacion
        default:
            DDLogWarn("El tipo de cashback, es inexistente: \(string)")
        }
        return unTipo
    }
}

enum TipoCashback: String {
    case consumo =  "consumo"
    case servicio =  "servicio"
    case afiliacion = "afiliacion"
}

struct Deudor {
    var id: String
    var nombre: String
    
    init() {
        id = ""
        nombre = ""
    }
}

struct Cargo {
    var fecha: String
    var estatus: EstatusCargo
    var pago: String
    
    init() {
        fecha = ""
        estatus = EstatusCargo()
        pago = ""
    }
}
