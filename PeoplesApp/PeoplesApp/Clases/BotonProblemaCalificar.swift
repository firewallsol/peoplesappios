//
//  BotonProblemaCalificar.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 04/09/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit

class BotonProblemaCalificar: UIView {
    
    var icono : UIImageView!
    var titulo : UILabel!
    var isSelected : Bool = false
    
    override init(frame: CGRect){
        super.init(frame: frame)
        
        self.layer.cornerRadius = 10
        self.layer.borderColor = UIColor.gray.cgColor
        self.layer.borderWidth = Constantes.tamaños.bordeBotonesCalificar
        
        var xframe = CGRect.zero
        xframe.size.width = frame.size.height * 0.6
        xframe.size.height = frame.size.height * 0.6
        xframe.origin.x = 5
        
        self.icono = UIImageView(frame: xframe)
        self.icono.center.y = frame.size.height / 2
        self.icono.contentMode = .scaleAspectFit
        self.addSubview(self.icono)
        
        xframe.origin.x = self.icono.frame.maxX + 3
        xframe.size.width = frame.size.width * 0.65
        xframe.size.height = frame.size.height * 0.9
        self.titulo = UILabel(frame: xframe)
        self.titulo.center.y = frame.size.height / 2
        self.titulo.numberOfLines = 0
        self.titulo.adjustsFontSizeToFitWidth = true
        self.titulo.textAlignment = .center
        self.titulo.lineBreakMode = .byClipping
        self.titulo.minimumScaleFactor = 0.1
        self.titulo.textColor = UIColor.gray
        self.titulo.font = Constantes.currentFonts.NunitoRegular.base
        self.addSubview(self.titulo)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setSelected(){
        self.layer.borderColor = Constantes.paletaColores.verdePeoples.cgColor
        self.titulo.textColor = Constantes.paletaColores.verdePeoples
        self.icono.tintColor = Constantes.paletaColores.verdePeoples
        self.isSelected = true
    }
    
    func setUnSelected(){
        self.layer.borderColor = UIColor.gray.cgColor
        self.titulo.textColor = UIColor.gray
        self.icono.tintColor = UIColor.gray
        self.isSelected = false
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
