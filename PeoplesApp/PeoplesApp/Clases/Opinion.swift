//
//  Opinion.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 14/07/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit

class Opinion {
    var nombre : String!
    var urlImg : String!
    var opinion : String!
    var calificacion: String!
}
