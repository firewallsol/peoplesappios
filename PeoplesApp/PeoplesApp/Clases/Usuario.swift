//
//  Usuario.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 06/10/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit

class Usuario : Empresa_Profesional {
    var id : String!
    var nombre : String!
    var apellidos : String!
    var telefono : String!
    var email : String!
    var tinRfc : String!
    var afiliador : Usuario?
    var tipoLogin : tipoLoginUsuario!
    var tipo : tipoUsuario!
    var estatus : estatusUsuario!
    var wantCashback : Bool!
    var amountCashback : Float!
    var avatar : URL!
    var cuentaPaypal : String!
    var direcciones : [Direccion]!
    var afiliacion : Afiliacion!
    var pais: Pais!
    var divisa: Divisa!
    var deviceId : String!
    var codigoAfiliador: String?

}
