//
//  Pais.swift
//  PeoplesApp
//
//  Created by Jesús García on 19/12/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import Foundation

class Pais:NSObject, NSCoding {
    var id: String!
    var descripcion: String!
    var abreviacion: String!
    
    override init() {
        self.id = ""
        self.descripcion = ""
        self.abreviacion = ""
    }
    
    init(id: String, descripcion: String) {
        self.id = id
        self.descripcion = descripcion
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.id, forKey: "id")
        aCoder.encode(self.descripcion, forKey: "descripcion")
        aCoder.encode(self.abreviacion, forKey: "abreviacion")
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.id = aDecoder.decodeObject(forKey: "id") as? String
        self.descripcion = aDecoder.decodeObject(forKey: "descripcion") as? String
        self.abreviacion = aDecoder.decodeObject(forKey: "abreviacion") as? String
    }
}
