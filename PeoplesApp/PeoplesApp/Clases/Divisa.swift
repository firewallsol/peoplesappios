//
//  Divisa.swift
//  PeoplesApp
//
//  Created by Jesús García on 19/12/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit

class Divisa: NSObject, NSCoding {
    var id: String!
    var pais: Pais!
    var moneda: String!
    var abreviacion: String!
    
    override init() {}
    
    init(id: String, pais: Pais, moneda: String, abreviacion: String) {
        self.id = id
        self.pais = pais
        self.moneda = moneda
        self.abreviacion = abreviacion
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.id, forKey: "id")
        aCoder.encode(self.pais, forKey: "pais")
        aCoder.encode(self.moneda, forKey: "moneda")
        aCoder.encode(self.abreviacion, forKey: "abreviacion")
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.id = aDecoder.decodeObject(forKey: "id") as? String
        self.pais = aDecoder.decodeObject(forKey: "pais") as? Pais
        self.moneda = aDecoder.decodeObject(forKey: "moneda") as? String
        self.abreviacion = aDecoder.decodeObject(forKey: "abreviacion") as? String
    }
}
