//
//  Profesional.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 05/07/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//
import UIKit

class Profesional: Usuario {
    var imagen : URL!
    var distancia : String!
    var razonSocial : String!
    var descripcion : String!
    var calificacion : String!
    var esPrincipal : Bool!
    var categoria : Categoria!
    var jsonData : String!
}
