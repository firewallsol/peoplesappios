//
//  estructuraBase.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 06/10/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

class EstructuraBase {
    var id : String!
    var descripcion : String!
}

class tipoLoginUsuario: EstructuraBase {}

class tipoUsuario : EstructuraBase {}

class estatusUsuario : EstructuraBase{}

class EstatusServicio : EstructuraBase {
    enum EstatusDelServicio: String {
        case abierta = "ABIERTA"
        case asignada = "ASIGNADA"
        case completada = "COMPLETADA"
        case cancelada = "CANCELADA"
    }
}

class EstatusPropuesta : EstructuraBase {
    enum EstatusDePropuesta: String {
        case espera = "ESPERA"
        case aceptada = "ACEPTADA"
        case rechazada = "RECHAZADA"
        case solicitada = "SOLICITADA"
        case cancelada = "CANCELADA"
    }
}

class EstatusCashback: EstructuraBase {
    enum Keys: String {
        case solicitado = "1"
        case aprobado = "2"
        case rechazado = "3"
    }
}

class EstatusCargo: EstructuraBase {
    enum Keys: String {
        case pendiente = "1"
        case pagado = "2"
        case cancelado = "3"
        case transferido = "4"
        case enviado = "6"
        case porValidar = "7"
    }
}

