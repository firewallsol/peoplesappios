//
//  Propuesta.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 18/10/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit

class Propuesta {
    var id : String!
    var idServicio : String!
    var datosProfesional : Profesional!
    var descripcion : String!
    var presupuesto : String!
    var estatus : EstatusPropuesta!
    var divisa: Divisa!
}
