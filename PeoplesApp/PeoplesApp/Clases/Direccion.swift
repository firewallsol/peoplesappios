//
//  Direccion.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 12/10/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import Foundation

class Direccion: NSObject, NSCoding {
    var id : String!
    var descripcion : String!
    var alias : String!
    var latitud : String!
    var longitud : String!
    var esPredeterminado : Bool = false
    
    required override init() {}
    
    required init?(coder aDecoder: NSCoder) {
        //Usado para guardar y leer direcciones desde UserDefaults.
        self.id = aDecoder.decodeObject(forKey: "id") as? String
        self.descripcion = aDecoder.decodeObject(forKey: "descripcion") as? String
        self.alias = aDecoder.decodeObject(forKey: "alias") as? String
        self.latitud = aDecoder.decodeObject(forKey: "latitud") as? String
        self.longitud = aDecoder.decodeObject(forKey: "longitud") as? String
        self.esPredeterminado = aDecoder.decodeBool(forKey: "esPredeterminado")
    }
    
    func encode(with aCoder: NSCoder) {
        //Usado para guardar y leer direcciones desde UserDefaults.
        aCoder.encode(self.id, forKey: "id")
        aCoder.encode(self.descripcion, forKey: "descripcion")
        aCoder.encode(self.alias, forKey: "alias")
        aCoder.encode(self.latitud, forKey: "latitud")
        aCoder.encode(self.longitud, forKey: "longitud")
        aCoder.encode(self.esPredeterminado, forKey: "esPredeterminado")
    }
}
