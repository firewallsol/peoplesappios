//
//  QR.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 14/11/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit

class QR {
    
    var id : String!
    var idRegistro : String!
    var idTicket : String!
    var Monto : String!
    var nombreCliente : String!
    var correoCliente : String!
    var nombreEmpresa : String!
    var fechaHora : String!
    var jsonData : String!

}
