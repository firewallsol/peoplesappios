//
//  DatosCashback.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 08/11/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import Foundation

class DatosCashback {
    var id : String!
    var idUsuario : String!
    var cashbackPendiente : String!
    var cashbackAfiliados : String!
    var cashbackCobrado : String!
    var fechaUltimoCobro : String!
    var afiliados : [Empresa_Profesional]!
}
