//
//  Categoria.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 21/09/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//
import UIKit

class Categoria {
    var id : String!
    var nombre : String!
    var imagen : URL!
    
}
