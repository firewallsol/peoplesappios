//
//  Servicio.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 02/08/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit

class Servicio {
    var id : String!
    var usuario : Usuario!
    var tituloSolicitud: String!
    var descripcion : String!
    var categoria : Categoria!
    var estatus : EstatusServicio!
    var presupuesto : String!
    var divisa: Divisa!
    var fechaServicio : Date!
    var direccion : String!
    var latitude : String!
    var longitute : String!
    var fechaSolicitud : Date!
    var propuestas : [Propuesta]!
    var profesionalCalificado: Bool!
}
