//
//  Empresa.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 11/10/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//
import UIKit

class Empresa: Empresa_Profesional  {
    var id : String!
    var imagen : URL!
    var cash_back : String!
    var distancia : String!
    var razonSocial : String!
    var tin_rfc : String!
    var descripcion : String!
    var calificacion : Float!
    var categoria : Categoria!
    var esPrincipal : Bool = false
    var direcciones : [Direccion]!
    var jsonData : String!
}
