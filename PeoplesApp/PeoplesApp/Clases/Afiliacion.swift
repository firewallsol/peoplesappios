//
//  Afiliacion.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 03/11/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit

enum EstatusAfiliacion : String {
    case noAfiliado = "1"
    case solicitada = "2"
    case preaprobada = "3"
    case rechazada = "4"
    case completada = "5"
    case denegada = "6"
}

class Afiliacion {
    var id : String!
    var intentos : Int!
    var statusEnum : EstatusAfiliacion!
    var estatus : EstructuraBase!
}
