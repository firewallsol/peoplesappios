//
//  estructuraBase.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 06/10/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

class EstructuraBase {
    var id : String!
    var descripcion : String!
}

class tipoLoginUsuario: EstructuraBase {}

class tipoUsuario : EstructuraBase {}

class estatusUsuario : EstructuraBase{}

class EstatusServicio : EstructuraBase {
    
    ///TEST
    enum EstatusDelServicio: String {
        case Abierta = "ABIERTA"
        case Asignada = "ASIGNADA"
        case Completada = "COMPLETADA"
        case Cancelada = "CANCELADA"
    }
}

class EstatusPropuesta : EstructuraBase {
    
    ///TEST
    enum EstatusDePropuesta: String {
        case Espera = "ESPERA"
        case Aceptada = "ACEPTADA"
        case Rechazada = "RECHAZADA"
        case Solicitada = "SOLICITADA"
        case Cancelada = "CANCELADA"
    }
}
