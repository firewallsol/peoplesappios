//
//  ViewControllerExtension.swift
//  PeoplesApp
//
//  Created by Leonel Sanchez on 30/06/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import SideMenu
import NVActivityIndicatorView

var xactivityIndicator : NVActivityIndicatorView!

extension UIViewController {
    func setBarMenu(setLogo: Bool = false){
        
        self.navigationController?.navigationBar.isTranslucent = false
        
        let leftbbitem = UIBarButtonItem(image: Asset.Icons.icMenuAlt.image, style: .plain, target: self, action: #selector(self.showMenu))
        self.navigationItem.leftBarButtonItem = leftbbitem
        self.navigationController?.navigationBar.tintColor = Constantes.paletaColores.verdePeoples
        
        let sidem = SideMenuVC()
        sidem.navBarHeight = (self.navigationController?.navigationBar.frame.size.height)!
        let menuLeftNavigationController = UISideMenuNavigationController(rootViewController: sidem)
        menuLeftNavigationController.leftSide = true
        SideMenuManager.default.menuLeftNavigationController = menuLeftNavigationController
        SideMenuManager.default.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        
        if setLogo {
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 120, height: 40))
            imageView.image = Asset.pplsAppLogo.image
            imageView.contentMode = .scaleAspectFit
            self.navigationItem.titleView = imageView
        }
    }
    
    @objc func showMenu(){
        present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    func addToolBar(textField: UITextField? = nil, textView: UITextView? = nil){
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = Constantes.paletaColores.verdePeoples
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.donePressed))
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.cancelPressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        
        if let _ = textField {
            textField?.inputAccessoryView = toolBar
        } else {
            textView?.inputAccessoryView = toolBar
        }
        
    }
    
    @objc func donePressed(){
        view.endEditing(true)
    }
    
    @objc func cancelPressed(){
        view.endEditing(true) // or do something
    }
    
    func showLoader(optSize: CGSize? = nil, optType: NVActivityIndicatorType? = nil, optColor: UIColor? = nil){
        
        if !self.isLoaderAnimating() {
            xactivityIndicator = NVActivityIndicatorView(frame: CGRect.zero)
            xactivityIndicator.frame.size = optSize ?? Constantes.tamaños.loaderSize
            xactivityIndicator.type = optType ?? .ballRotateChase
            xactivityIndicator.color = optColor ?? Constantes.paletaColores.verdePeoples
            xactivityIndicator.center.x = Constantes.tamaños.anchoPantalla / 2
            xactivityIndicator.center.y = Constantes.tamaños.altoPantalla / 2
            self.view.addSubview(xactivityIndicator)
            
            self.view.isUserInteractionEnabled = false
            
            xactivityIndicator.startAnimating()
        }
    }
    
    func hideLoader(){
        if xactivityIndicator != nil {
            if xactivityIndicator.isAnimating {
                xactivityIndicator.stopAnimating()
                self.view.isUserInteractionEnabled = true
            }
        }
    }

    
    
//    func showLoader(optSize: CGSize? = nil, optType: NVActivityIndicatorType? = nil, optColor: UIColor? = nil){
//        
//        if !self.isLoaderAnimating() {
//            xactivityIndicator = NVActivityIndicatorView(frame: CGRect.zero)
//            xactivityIndicator.frame.size = optSize ?? Constantes.tamaños.loaderSize
//            xactivityIndicator.type = optType ?? .ballRotateChase
//            xactivityIndicator.color = optColor ?? Constantes.paletaColores.verdePeoples
//            xactivityIndicator.center.x = Constantes.tamaños.anchoPantalla / 2
//            xactivityIndicator.center.y = (Constantes.tamaños.altoPantalla / 2) - (Constantes.tamaños.altoStatusBar + 44)
//            self.view.addSubview(xactivityIndicator)
//            
//            xactivityIndicator.startAnimating()
//        }
//        
//    }
//    
//    func hideLoader(){
//        if xactivityIndicator.isAnimating {
//            xactivityIndicator.stopAnimating()
//        }
//    }
    
    func isLoaderAnimating()->Bool {
        if let _ = xactivityIndicator {
            return xactivityIndicator.isAnimating
        }
        return false
    }
    
    func mensajeSimple(mensaje: String, dismiss: Bool = true){
        let alert = UIAlertController(title: "People's App", message: mensaje, preferredStyle: UIAlertControllerStyle.actionSheet)
        if !dismiss {
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
        }
        self.present(alert, animated: true, completion: nil)
        
        if dismiss {
            let when = DispatchTime.now() + 2.5
            DispatchQueue.main.asyncAfter(deadline: when){
                alert.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    func alertaRedirige(viewController: UIViewController, mensaje: String, titulo: String ) {
        let alerta = UIAlertController(title: titulo,
                                       message: mensaje,
                                       preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("ButtonCancel", comment: ""),
                                         style: .cancel,
                                         handler: nil)
        
        let okAction = UIAlertAction(title: "OK",
                                     style: .default) { (alertAction) in
                                        //Show view controller.
                                        self.navigationController?.pushViewController(viewController, animated: true)
        }
        
        alerta.addAction(cancelAction)
        alerta.addAction(okAction)
        
        self.present(alerta, animated: true, completion: nil)
    }
    
    func alertAndDismissWith(message: String, buttonTitle:String) {
        let alert = UIAlertController(title: NSLocalizedString("AppName", comment: ""),
                                      message: message,
                                      preferredStyle: .actionSheet)
        
        let okAction = UIAlertAction(title: buttonTitle,
                                     style: .default) { (action) in
                                        if (self.navigationController != nil) {
                                            self.navigationController?.popViewController(animated: true)
                                        } else {
                                            self.dismiss(animated: true, completion: nil)
                                        }
        }
        
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    

}

extension UILabel{
    func heightForView(numberoflines : Int = 1) -> CGFloat{
        
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.frame.size.width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = numberoflines
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = self.font
        label.text = self.text!
        label.sizeToFit()
        
        return label.frame.height
    }
}


