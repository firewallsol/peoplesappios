//
//  TableViewExtension.swift
//  PeoplesAppPro
//
//  Created by Jesús García on 05/12/17.
//  Copyright © 2017 Firewall Soluciones. All rights reserved.
//

import UIKit

extension UITableView {
    
    func validaExistenciaDatos(datos: AnyObject) -> Int {
        var numOfSections = 0
        
        numOfSections = datos.count > 0 ? 1:0
        
        if numOfSections > 0 {
            self.separatorStyle = .singleLine
            self.backgroundView = nil
            numOfSections = 1
        }
        else {
            let noDataLabel = UILabel(frame: CGRect(x: 0,
                                                    y: 0,
                                                    width: self.bounds.size.width,
                                                    height: self.bounds.size.height))
            noDataLabel.text = FWS.noInformationAvailable
            noDataLabel.textColor = UIColor.black
            noDataLabel.textAlignment = NSTextAlignment.center
            
            self.backgroundView = noDataLabel
            self.separatorStyle = .none
        }
        return numOfSections
    }
    
    // Reload data with a completion handler.
    public func reloadData(_ completion: @escaping () -> Void) {
        UIView.animate(withDuration: 0, animations: {
            self.reloadData()
        }, completion: { _ in
            completion()
        })
    }
    
}
