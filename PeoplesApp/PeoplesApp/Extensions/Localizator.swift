//
//  Localizator.swift
//  PeoplesApp
//
//  Created by Jesús García on 09/02/18.
//  Copyright © 2018 firewallsoluciones. All rights reserved.
//

import Foundation
import CocoaLumberjack

private class Localizator {
    
    static let sharedInstance = Localizator()
    
    lazy var localizableDictionary: NSDictionary! = {
        if let path = Bundle.main.path(forResource: "Localizable", ofType: "plist") {
            return NSDictionary(contentsOfFile: path)
        }
        fatalError("Localizable file NOT found")
    }()
    
    func localize(string: String) -> String {
        
        //        //Filtrer Version
        //        var localizedString = ""
        //        let arrayDict = localizableDictionary.filter { ($0.key as! String) == string}
        //
        //        print("DICTIONARY: \(arrayDict)")
        //
        //        if arrayDict.count > 0 {
        //            //Get value string, from KeyWords in Plist
        //            let firstItem = arrayDict.first?.value
        //            let dictionary = NSDictionary(object: firstItem?.value, forKey: firstItem?.key as! NSCopying)
        //            localizedString = dictionary.value(forKey: "value") as! String
        //        }
        //        else {
        //            DDLogError("Missing translation for: \(string)")
        //            localizedString = ""
        //        }
        
        guard let localizedString = (localizableDictionary.value(forKey: string) as AnyObject).value(forKey: "value") as? String else {
            DDLogError("Missing translation for: \(string)")
            //            assertionFailure("Missing translation for: \(string)")
            return ""
        }
        return localizedString
    }
}

extension String {
    var localized: String {
        return Localizator.sharedInstance.localize(string: self)
    }
}
