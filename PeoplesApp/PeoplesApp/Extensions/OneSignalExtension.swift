//
//  OneSignalExtension.swift
//  PeoplesAppPro
//
//  Created by Jesús García on 04/12/17.
//  Copyright © 2017 Firewall Soluciones. All rights reserved.
//

import UIKit
import CocoaLumberjack
import OneSignal


class PushNotifications: OneSignal {
    enum NotificationKeys:String {
        case includePlayerIds = "include_player_ids"
        case contents =  "contents"
        case headings = "headings"
        case subtitle = "subtitle"
        case iosBadgeType = "ios_badgeType"
        case iosBadgeCount = "ios_badgeCount"
        case appId = "app_id"
    }

    
    class func configureOneSignalWith(launchOptions: [UIApplicationLaunchOptionsKey: Any]?) {
//        OneSignal.setLogLevel(ONE_S_LOG_LEVEL.LL_DEBUG, visualLevel: ONE_S_LOG_LEVEL.LL_DEBUG) //logger
        
        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false]
        
        OneSignal.initWithLaunchOptions(launchOptions,
                                        appId: Constantes.OneSignalAppId.pplesCliente,
                                        handleNotificationAction: nil,
                                        settings: onesignalInitSettings)
        
        
        // Recommend moving the below line to prompt for push after informing the user about
        //   how your app will use them.
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
        })
        
        // Sync hashed email if you have a login system or collect it.
        //   Will be used to reach the user at the most optimal time of day.
        // OneSignal.syncHashedEmail(userEmail)
    }
    
    class func obtienePlayerID() -> String {
        //Obtiene estatus y ID del dispositivo actual.
        let  status = OneSignal.getPermissionSubscriptionState()
        
        var xplayerID = ""
        if let playerID = status?.subscriptionStatus.userId {
            xplayerID = playerID
        }
        
        DDLogDebug("PlayerID: " + xplayerID)
        return xplayerID
    }

    class func enviaNotificacion(userId: String?, subtitulo: String, mensaje: String, appId:String? = nil) {
        //        ///TESTING>
        //        //Obtiene estatus y ID del dispositivo actual.
        //        let  status = OneSignal.getPermissionSubscriptionState()
        //        let userId = status?.subscriptionStatus.userId
        //        ///>
        
        if userId != nil {
            DDLogInfo("DeviceID: \(userId!) mensaje: \(mensaje)")
            
            var notificationContent = [
                NotificationKeys.includePlayerIds.rawValue: [userId]
                ,NotificationKeys.contents.rawValue: ["en": mensaje] // Required unless "content_available": true or "template_id" is set
                ,NotificationKeys.headings.rawValue: ["en": NSLocalizedString("AppName", comment: "")]
                ,NotificationKeys.subtitle.rawValue: ["en": subtitulo]
                //                ,NotificationKeys.iosBadgeType.rawValue: ["en": "Increase"]
                //                ,NotificationKeys.iosBadgeCount.rawValue: ["en": 1]
                ] as [String : Any]
            
            if let xappId = appId {
                //Send to a destination App.
                notificationContent[NotificationKeys.headings.rawValue] = ["en": NSLocalizedString("AppNameDestination", comment: "")]
                notificationContent[NotificationKeys.appId.rawValue] = xappId
            }
            
            
            OneSignal.postNotification(notificationContent, onSuccess: { (dictionary) in
                DDLogInfo("Se envió notificación: " + mensaje)
            }, onFailure: { (error) in
                DDLogError("Ocurrio un error: " + (error?.localizedDescription)!)
            })
        }
    }

    
    
}
