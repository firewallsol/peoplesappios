//
//  StringExtension.swift
//  PeoplesApp
//
//  Created by Jesús García on 02/02/18.
//  Copyright © 2018 firewallsoluciones. All rights reserved.
//

extension String {
    //MARK: - Methods
    func trimmed()->String{
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    func URLAllowed()->String{
        return self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
    
    var encodeEmoji: String{
        if let encodeStr = NSString(cString: self.cString(using: .nonLossyASCII)!, encoding: String.Encoding.utf8.rawValue){
            return encodeStr as String
        }
        return self
    }
    
    var decodeEmoji: String{
        let data = self.data(using: String.Encoding.utf8);
        let decodedStr = NSString(data: data!, encoding: String.Encoding.nonLossyASCII.rawValue)
        if let str = decodedStr{
            return str as String
        }
        return self
    }
    
    func formatoPrecio() ->String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.roundingMode = .halfUp
        formatter.maximumFractionDigits = 2
        
        let number = NSNumber(value: Double(self)!)
        return formatter.string(from: number)!
    }
    
    func formatoFecha(usarFormato: String) -> String {
        let date = Functions().stringToDate(formato:"yyyy-MM-dd HH:mm:ss", elString: self)
        return Functions().dateToString(formato: usarFormato, laFecha: date).capitalized
    }
    
    static func isValidEmailAddress(emailAddressString: String) -> Bool {
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
    
    
}
